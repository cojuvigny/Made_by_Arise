#ifndef _SOLVEUR_
#define _SOLVEUR_

#include "parcours.h"
#include "file.h"
#include "test.h"
#include "comptage.h"
#include "optimisation.h"


/**
 * @file solveur.h
 * 
 * Ce fichier décrit un ensemble de fonctions permettant de trouver une des meilleures solutions rapidement
 * Il contient 3 fonctions et 1 type.
 * 
 * - le type \a data_solveur définit les données contenues dans le tas
 * 
 * - void solveur(int nombre_tuile, int taille_grille, ptuile* tab_tuile,ptuile * premiere_tuile);			renvoie la meilleure opti trouvée
 * - best_of_the_best(resultat * tableau_resultat)															retourne la meilleure des solutions au solveur
 * - nettoyer_tableau_tuile(ptuile * tab_tuile,int nombre_tuile)   											nettoye un tableau de tuile
 */




/**
 * \struct data_solveur 
 * \brief structure des données contenues dans le tas
 *
 * Permet de garder en mémoire le détail des données de chaque partie contenue dans le tas
 *
 */
typedef struct data_solveur
{
	int nombre_tuile_restante;
	int nombre_tuile_totale;
	int taille_grille;
	file_p tete;
	file_p queue;
	resultat * res;
	parcours_p partie;
	int regle;

} data_solveur;

/**
 * @brief  					renvoie la meilleure opti trouvée
 * @param 	nombre_tuile  	le nombre de tuile de la partie
 * @param 	taille_grille 	la taille de la grille
 * @param 	tab_tuile     	les tuiles de la partie
 * @param 	premiere_tuile 	la tuile initialement posée sur la grille
 * @param   regle			la regle du jeu choisie
 * @return 					le resultat
 */
resultat solveur(int nombre_tuile_restante, int nombre_tuile_totale, int taille_grille, file_p main_jeu_tete, parcours_p jeu, int regle);

/**
 * @brief  						retourne la meilleure des solutions au solveur
 * @param  tableau_resultat 	le tableau des différentes solutions fournies par le solveur
 * @return                 		l'indice dans le tableau des résultats du meilleures résultats
 */
int best_of_the_best(resultat * tableau_resultat);

/**
 * @brief  						nettoye un tableau de tuile
 * @param  tab_tuile 			le tableau des différentes solutions fournies par le solveur
 * @param  nombre_tuile 		le nombre de tuiles du tableau
 * @return                 		rien
 */
void nettoyer_tableau_tuile(ptuile * tab_tuile,int nombre_tuile);


#endif
