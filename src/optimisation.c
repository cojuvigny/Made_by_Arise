#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "tuile.h"
#include "grille.h"
#include "test.h"
#include "parcours.h"
#include "file.h"
#include "comptage.h"
#include "affichage.h"
#include "optimisation.h"


int max2 (int a, int b)
{
	if (a>b) return a;
	return b;
}


int valeur_tuile(ptuile *p, int regle)
{
	int i;
	int count=0;
	for (i=0;i<6;i++)
	{
		switch ((*p)->tab[i])
		{
			case 'L': 
			count= regle != 0 ? count+2 : count+3;
			break;
		case 'F':
			count= regle != 4 ? count+1 : count+3;
			break;
		case 'V':
			count= regle != 3 ? count+1 : count+2;
			break;
		case 'P':
			count= regle != 1 ? count+0 : count+1;
			break;
		case 'U':
			count= regle != 2 ? count+3 : count+4;
			break;
		case 'R':
			count= regle != 2 ? count+3 : count+4;
			break;
			default:
			count=count+0;
			break;
		}

	}
	return count;
}

int valeur2_tuile(ptuile *p)
{
	int i;
	int count=0;
	for (i=0;i<6;i++)
	{
		switch ((*p)->tab[i])
		{
			case 'L': 
			if (i==2 || i==3)
			{
				count=count+2;
			}
			else {
				count=count-5;
			}
			break;
		case 'F':
			count=count+2;
			break;
		case 'V':
			count=count+3;
			break;
		case 'P':
			if (i==2 || i==3)
			{
				count=count+4;
			}
			else {
				count=count+6;
			}
			break;
		case 'U':
			count=count+1;
			break;
		case 'R':
			count=count+1;
			break;
			default:
			count=count+0;
			break;
		}

	}
	return count;
}

points proba_tuiles(file_p t)
{
	points p;
	p.somme=0;
	p.points_r_and_u=0;
	p.points_forets=0;	
	p.points_villes=0;
	p.points_lacs=0;
	file_p current=t;
	ptuile tu;
	int i;
	while ( current!=NULL)
	{
		tu=current->data;
		for (i=0;i<6;i++)
		{
			switch (tu->tab[i])
			{
				case 'L': 
				p.points_lacs++;
				break;
			case 'F':
				p.points_forets++;
				break;
			case 'V':
				p.points_villes++;
				break;
			case 'U':
				p.points_r_and_u++;
				break;
			case 'R':
				p.points_r_and_u++;
				break;
				default:
				break;
			}

		}
		current=current->suite;
		p.somme=p.somme+6;
	}
	return p;
}


int valeur_min( file_p tuiles, int regle)
{
   ptuile *t;
	int count=100;
	int j=0;
	int id=-1;
	while(tuiles!=NULL) {
		t = tuiles->data;
		j=valeur_tuile(t,regle);/*optimisation l 38*/
		if (count>j) {
			count=j;
			id=(*t)->id;
		}

		tuiles=tuiles->suite;
	}

	return id;
}

int valeur_max( file_p tuiles, int regle)
{
	ptuile *t;
	int count=0;
	int j=0;
	int id=-1;
	while(tuiles!=NULL) {
		t = tuiles->data;
		j=valeur_tuile (t,regle);
		if (count<=j) {
			count=j;
			id=(*t)->id;
		}

		tuiles=tuiles->suite;
	}

	return id;
}

int valeur2_max ( file_p tuiles){
	int count=0;
	int j=0;
	ptuile *t = (ptuile *)calloc(1,sizeof(ptuile));
	int id=-1;
	while(tuiles!=NULL)
	{
		t = tuiles->data;
		j=valeur2_tuile (t);
		if (count<j)
		{
			count=j;
			id=(*t)->id;
		}

		tuiles=tuiles->suite;
	}

	return id;
}


couple max_position(parcours_p * p, ptuile tuile,int * temp, int regle)
{
	int size=(*p)->tour->g->size;
	int count=0;
	couple c = {0,0,0};
	int i=0;
	int j=0;
	int r=0;
	for (i=0;i<size;i++)
	{
		for (j=0;j<size;j++)
		{	
			for (r=0;r<4;r++)
			{
				if (add_tour(p,tuile,i,j,r,(*p)->tour->g->size))
				{
					count=comptage((*p)->tour->g,regle,0).somme;/*comptage.c l 303*/
					if (*temp<=count)/*Pas < . Sinon quand il n'y a que des village on ne change jamais c et on renvoie (0;0;0) ce qui est pas terrible*/
					{
						*temp=count;
						c.x=i;
						c.y=j;
						c.r=r;
					}
					remove_tour(p);					
				}
			}		
		}
	}
	return c;/*On renvoie c qui maximise les points*/
}


couple max_village_position (parcours_p * p, ptuile tuile,int * temp)
{
	int size=(*p)->tour->g->size;
	int count=0;
	couple c = {0,0,0};
	int i=0;
	int j=0;
	int r=0;
	int presence_carre=0;
	for (i=0;i<size;i++)
	{
		for (j=0;j<size;j++)
		{	
			for (r=0;r<4;r++)
			{
				if (add_tour(p,tuile,i,j,r,(*p)->tour->g->size))/*Si la tuile est valide, on la place*/
				{
					int x,y;
					for (x=0;x<size;x++)	
					{
						for (y=0;y<size;y++)
						{
							count=max(test_village2((*p)->tour->g,x,y,size,&presence_carre),count);/*On compte les points de village qu'elle donne. comptage.c l 251 et l 15*/
						}
					}
					if (*temp<count)
					{
						*temp=count;
						c.x=i;
						c.y=j;
						c.r=r;
					}
					remove_tour(p);

					
				}
			}		
		}
	}
	return c;
}


couple max_lac_position(parcours_p * p, ptuile tuile,int * temp)
{
	int size=(*p)->tour->g->size;
	int count=0;
	couple c = {0,0,0};
	int i=0;
	int j=0;
	int r=0;
	for (i=0;i<size;i++)
	{
		for (j=0;j<size;j++)
		{	
			for (r=0;r<4;r++)
			{
				if (add_tour(p,tuile,i,j,r,size))
				{
					count=test_presence_totale((*p)->tour->g,'L');
					if (*temp<count)
					{
						*temp=count;
						c.x=i;
						c.y=j;
						c.r=r;
					}
					remove_tour(p);

					
				}
			}		
		}
	}
	return c;
}

couple max_proba_position (parcours_p * p, ptuile tuile,int * temp, int regle)
{
	int size=(*p)->tour->g->size;
	int count=0;
	couple c = {0,0,0};
	int i=0;
	int j=0;
	int r=0;
	int pb_lac=0;
	int pb_plaine=0;
	int pb_ressource=0;
	int pb_usine=0;
	int pb_foret=0;
	int pb_village=0;
	for (i=0;i<size;i++)
	{
		for (j=0;j<size;j++)
		{	
			switch ((*p)->tour->g->cells[i][j].type)
			{
				case 'L': 
				pb_lac++;
				break;
			case 'F':
				pb_foret++;
				break;
			case 'V':
				pb_village++;
				break;
			case 'P':
				pb_plaine++;
				break;
			case 'U':
				pb_usine++;
				break;
			case 'R':
				pb_ressource++;
				break;
				default:
				break;
			}
		}
	}
	for (i=0;i<size;i++)
	{
		for (j=0;j<size;j++)
		{	
			for (r=0;r<4;r++)
			{
				if (add_tour(p,tuile,i,j,r,size))
				{
					count=(test_pb((*p)->tour->g,pb_lac, pb_ressource, pb_usine, pb_foret,pb_village,pb_plaine,regle,0)).somme;
					if (*temp<count)
					{
						*temp=count;
						c.x=i;
						c.y=j;
						c.r=r;
					}
					remove_tour(p);					
				}
			}		
		}
	}
	return c;
}

couple max_proba2_position(parcours_p * p, ptuile tuile,points t,int * temp, int regle)
{
	int size=(*p)->tour->g->size;
	int count=0;
	couple c = {0,0,0};
	int i=0;
	int j=0;
	int r=0;
	for (i=0;i<size;i++)
	{
		for (j=0;j<size;j++)
		{	
			for (r=0;r<4;r++)
			{
				if (add_tour(p,tuile,i,j,r,size))
				{

					count=(test_pb2 ((*p)->tour->g,t.points_lacs,t.points_villes,t.somme,regle));
					if (*temp<count)
					{
						*temp=count;
						c.x=i;
						c.y=j;
						c.r=r;
					}
					remove_tour(p);


					
				}
			}		
		}
	}
	return c;
}


void inclusion_rec ( parcours_p * p, file_p * tete, file_p * queue,ptuile * sortie,int compt,int type, resultat * res, int regle)
{
	if ((*tete)==NULL)
	{
		
		if (type == 1)
		{
			res->description = "technique du min";
		}
		else
		{
			res->description = "technique du max";
		}
		
		res->points_partie = comptage((*p)->tour->g,regle,0).somme;
		res->partie_opti = (*p);
		return;
	}
	else
	{
	/*On doit poser les tuiles dans le bon ordre*/
		int size=(*p)->tour->g->size;
		couple c = {0,0,0};
		int pts = 0;
		int id=(type==0)?valeur_max(*tete,regle):valeur_min(*tete,regle);/*On retourne la tuile qui à la plus grande valeur : optimisation.c l 186*/
		/*printf("id choisi : %d\n",id);*/
		defiler_tri(tete,queue,sortie,sizeof(ptuile),id);
		/*affichage_tuile(*sortie,0);*/
		/*affichage_grille((*p)->tour->g,0,0);*/
		c=max_position(p,*sortie,&pts,regle);/*La position qui maximise les points : optimisation.c l 228*/
		/*printf("(%d;%d;%d)\n",c.x, c.y, c.r);*/
		add_tour(p,*sortie, c.x, c.y, c.r,size);
		/*affichage_grille((*p)->tour->g,0,0);On vérifie si la tuile à bien été posée */
		inclusion_rec(p,tete,queue,sortie,compt++,type,res,regle);
	}
}

void inclusion2_rec ( parcours_p * p, file_p * tete, file_p * queue,ptuile * sortie,int compt,int type, resultat * res, int regle)
{
	if ((*tete)==NULL)
	{
		
		if (type == 1)
		{
			res->description = "technique du min avec 2 tuiles";
		}
		else
		{
			res->description = "technique du max avec 2 tuiles";
		}
		
		res->points_partie = comptage((*p)->tour->g,regle,0).somme;
		res->partie_opti = (*p);
		return;
	}
	else
	{
	/*On doit poser les tuiles dans le bon ordre*/
		int size=(*p)->tour->g->size;
		couple c1 = {0,0,0};
		couple c3 = {0,0,0};
		int pts = 0;
		int pts2 = 0;
		int id=(type==0)?valeur_max(*tete,regle):valeur_min(*tete,regle);/*On retourne la tuile qui à la plus grande valeur : optimisation.c l 186*/
		/*printf("id choisi : %d\n",id);*/
		defiler_tri(tete,queue,sortie,sizeof(ptuile),id);
		if ((*tete)==NULL )
		{
			c1=max_position(p,*sortie,&pts,regle);
			add_tour(p,*sortie, c1.x, c1.y, c1.r,size);
			inclusion2_rec(p,tete,queue,sortie,compt++,type,res,regle);
		}
		else
		{
			ptuile t=*sortie;
			int id2=(type==0)?valeur_max(*tete,regle):valeur_min(*tete,regle);
			/*printf("id2 choisi : %d\n",id2);*/
			defiler_tri(tete,queue,sortie,sizeof(ptuile),id2);
			ptuile t2=*sortie;
			/*affichage_tuile(*sortie,0);*/
			/*affichage_grille((*p)->tour->g,0,0);*/
			c1=max_position(p,t2,&pts,regle);/*La position qui maximise les points : optimisation.c l 228*/
			add_tour(p,t2, c1.x, c1.y, c1.r,size);
			max_position(p,t,&pts2,regle);/*La position qui maximise les points : optimisation.c l 228*/
			remove_tour(p);	
			int count=pts2;
			pts=0;
			pts2=0;
			c3=max_position(p,t,&pts,regle);/*La position qui maximise les points : optimisation.c l 228*/
			add_tour(p,t, c3.x, c3.y, c3.r,size);
			max_position(p,t2,&pts2,regle);/*La position qui maximise les points : 	optimisation.c l 228*/

			/*printf("(%d;%d;%d)\n",c.x, c.y, c.r);*/
			if ( pts2!=count) {
				add_tour(p,t, c3.x, c3.y, c3.r,size);
				enfiler_tri(&t2,tete,queue,sizeof(ptuile));
			}
			else
			{
				add_tour(p,t2, c1.x, c1.y, c1.r,size);
				enfiler_tri(&t,tete,queue,sizeof(ptuile));
			}
			/*affichage_grille((*p)->tour->g,0,0);On vérifie si la tuile à 	bien été posée */
			inclusion2_rec(p,tete,queue,sortie,compt++,type,res,regle);
		}
	}
}

void inclusion3_rec ( parcours_p * p, file_p * tete, file_p * queue,ptuile * sortie,int compt,int type, resultat * res, int regle)
{
	if ((*tete)==NULL)
	{
		
		if (type == 1)
		{
			res->description = "technique du min avec 3 tuiles";
		}
		else
		{
			res->description = "technique du max avec 3 tuiles";
		}
		
		res->points_partie = comptage((*p)->tour->g,regle,0).somme;
		res->partie_opti = (*p);
		return;
	}
	else
	{
	/*On doit poser les tuiles dans le bon ordre*/
		int size=(*p)->tour->g->size;
		couple c = {0,0,0};
		couple c1 = {0,0,0};
		couple c2 = {0,0,0};
		couple c3 = {0,0,0};
		int pts1 = 0;
		int pts2 = 0;
		int pts3 = 0;
		int id=(type==0)?valeur_max(*tete,regle):valeur_min(*tete,regle);/*On retourne la tuile qui à la plus grande valeur : optimisation.c l 186*/
		/*printf("id choisi : %d\n",id);*/
		defiler_tri(tete,queue,sortie,sizeof(ptuile),id);
		if ((*tete)==NULL)
		{
			c1=max_position(p,*sortie,&pts1,regle);
			add_tour(p,*sortie, c1.x, c1.y, c1.r,size);
			inclusion3_rec(p,tete,queue,sortie,compt++,type,res,regle);
		}
		else 
		{
			if(((*tete)->suite)==NULL)
			{
				c1=max_position(p,*sortie,&pts1,regle);
				add_tour(p,*sortie, c1.x, c1.y, c1.r,size);
				inclusion3_rec(p,tete,queue,sortie,compt++,type,res,regle);
			}
			else
			{
				int re=0;
				int count;
				ptuile t=*sortie;
				int id2=(type==0)?valeur_max(*tete,regle):valeur_min(*tete,regle);
			/*printf("id2 choisi : %d\n",id2);*/
				defiler_tri(tete,queue,sortie,sizeof(ptuile),id2);
				ptuile t2=*sortie;
				int id3=(type==0)?valeur_max(*tete,regle):valeur_min(*tete,regle);
				defiler_tri(tete,queue,sortie,sizeof(ptuile),id3);
				ptuile t3=*sortie;
			/*affichage_tuile(*sortie,0);*/
			/*affichage_grille((*p)->tour->g,0,0);*/
			c1=max_position(p,t,&pts1,regle);/*La position qui maximise les points : optimisation.c l 228*/
				add_tour(p,t, c.x, c.y, c.r,size);
			c=max_position(p,t2,&pts2,regle);/*La position qui maximise les points : optimisation.c l 228*/
				add_tour(p,t2, c.x, c.y, c.r,size);
				max_position(p,t3,&pts3,regle);
				count=pts3;
				remove_tour(p);	
				pts2=0;
				pts3=0;
				c=max_position(p,t3,&pts2,regle);
				add_tour(p,t3, c.x, c.y, c.r,size);
				c=max_position(p,t2,&pts3,regle);
				if (count<pts3) { count=pts3; re=1;}
				pts1=0;
				pts2=0;
				pts3=0;
				remove_tour(p);	
				remove_tour(p);	
				c2=max_position(p,t2,&pts1,regle);
				add_tour(p,t2, c.x, c.y, c.r,size);
				max_position(p,t,&pts2,regle);
				add_tour(p,t, c.x, c.y, c.r,size);
				max_position(p,t3,&pts3,regle);
				if (count<pts3) { count=pts3; re=2;}
				pts2=0;
				pts3=0;
				remove_tour(p);
				max_position(p,t3,&pts2,regle);
				add_tour(p,t3, c.x, c.y, c.r,size);
				max_position(p,t,&pts3,regle);
				if (count<pts3) { count=pts3; re=3;}
				pts1=0;
				pts2=0;
				pts3=0;
				remove_tour(p);	
				remove_tour(p);	
				c3=max_position(p,t3,&pts1,regle);
				add_tour(p,t3, c.x, c.y, c.r,size);
				max_position(p,t,&pts2,regle);
				add_tour(p,t, c.x, c.y, c.r,size);
				max_position(p,t2,&pts3,regle);
				if (count<pts3) { count=pts3; re=4;}
				pts2=0;
				pts3=0;
				remove_tour(p);
				max_position(p,t2,&pts2,regle);
				add_tour(p,t2, c.x, c.y, c.r,size);
				max_position(p,t,&pts3,regle);
				if (count<pts3) { count=pts3; re=5;}
				pts1=0;
				pts2=0;
				pts3=0;
				remove_tour(p);	
				remove_tour(p);	


			/*printf("(%d;%d;%d)\n",c.x, c.y, c.r);*/
				if ( re==0 || re==1) {
					add_tour(p,t, c1.x, c1.y, c1.r,size);
					enfiler_tri(&t2,tete,queue,sizeof(ptuile));
					enfiler_tri(&t3,tete,queue,sizeof(ptuile));
				}
				if ( re==2 || re==3) {
					add_tour(p,t2, c2.x, c2.y, c2.r,size);
					enfiler_tri(&t,tete,queue,sizeof(ptuile));
					enfiler_tri(&t3,tete,queue,sizeof(ptuile));
				}
				if ( re==4 || re==5) {
					add_tour(p,t3, c3.x, c3.y, c3.r,size);
					enfiler_tri(&t,tete,queue,sizeof(ptuile));
					enfiler_tri(&t2,tete,queue,sizeof(ptuile));
				}
			/*affichage_grille((*p)->tour->g,0,0);On vérifie si la tuile à 	bien été posée */
				inclusion3_rec(p,tete,queue,sortie,compt++,type,res,regle);
			}
		}
	}
}

void max_modifgrille_rec ( parcours_p * p, file_p * tete, file_p * queue,ptuile * sortie,int compt,resultat * res, int regle)
{
	if ((*tete)==NULL)
	{
		
		res->description = "technique de la modif de grille";
		
		res->points_partie = comptage((*p)->tour->g,regle,0).somme;
		res->partie_opti = (*p);
		return;
	}
	else
	{
	/*On doit poser les tuiles dans le bon ordre*/
		int size=(*p)->tour->g->size;
		couple c = {0,0,0};
		int pts = 0;
		defiler(tete,queue,sortie,sizeof(ptuile));
		//affichage_tuile(*sortie,0);
		c=max_position(p,*sortie,&pts,regle);
		add_tour(p,*sortie, c.x, c.y, c.r,size);
		max_modifgrille_rec(p,tete,queue,sortie,compt++,res,regle);
	}
}

void max_modiftuile_rec ( parcours_p * p, file_p * tete, file_p * queue,ptuile * sortie,int compt,points t, resultat * res, int regle)
{
	if ((*tete)==NULL)
	{
		
		res->description = "technique de la modif de tuile";
		
		res->points_partie = comptage((*p)->tour->g,regle,0).somme;
		res->partie_opti = (*p);
		return;
	}
	else
	{
	/*On doit poser les tuiles dans le bon ordre*/
		int size=(*p)->tour->g->size;
		couple c = {0,0,0};
		int pts = 0;
		int id=valeur_max(*tete,regle);
		defiler_tri(tete,queue,sortie,sizeof(ptuile),id);
		//affichage_tuile(*sortie,0);
		c=max_proba2_position(p,*sortie,t,&pts,regle);
		add_tour(p,*sortie, c.x, c.y, c.r,size);
		max_modiftuile_rec(p,tete,queue,sortie,compt++,t,res, regle);
	}
}

void max_proba_rec( parcours_p * p, file_p * tete, file_p * queue,ptuile * sortie,int compt,points t,resultat * res,  int regle)
{
	if ((*tete)==NULL)
	{
		
		res->description = "technique de la modif de la proba";
		
		res->points_partie = comptage((*p)->tour->g,regle,0).somme;
		res->partie_opti = (*p);
		return;
	}
	else
	{
	/*On doit poser les tuiles dans le bon ordre*/
		int size=(*p)->tour->g->size;
		couple c = {0,0,0};
		int pts = 0;
		int id=valeur2_max(*tete);
		defiler_tri(tete,queue,sortie,sizeof(ptuile),id);
		//affichage_tuile(*sortie,0);
		c=max_proba2_position(p,*sortie,t,&pts,regle);
		add_tour(p,*sortie, c.x, c.y, c.r,size);
		max_proba_rec(p,tete,queue,sortie,compt++,t,res, regle);
	}
}


void village_inclusion_rec ( parcours_p * p, file_p * tete, file_p * queue,ptuile * sortie,int compt,resultat * res,  int regle)
{
	if ((*tete)==NULL)
	{
		
		res->description = "technique des villages";
		
		res->points_partie = comptage((*p)->tour->g,regle,0).somme;
		res->partie_opti = (*p);
		return;
	}
	else
	{
	/*On doit poser les tuiles dans le bon ordre*/
		int size=(*p)->tour->g->size;
		couple c = {0,0,0};
		int pts = 0;
		int id=valeur_max(*tete,regle);/*On retourne la tuile qui à la plus grande valeur : optimisation.c l 186*/
		defiler_tri(tete,queue,sortie,sizeof(ptuile),id);
		//affichage_tuile(*sortie,0);
		c=max_village_position(p,*sortie,&pts);/*La position qui maximise les points des villages : optimisation.c l 261*/
		add_tour(p,*sortie, c.x, c.y, c.r,size);
		village_inclusion_rec(p,tete,queue,sortie,compt++,res, regle);
	}
}


void lac_inclusion_rec ( parcours_p * p, file_p * tete, file_p * queue,ptuile * sortie,int compt,resultat * res,  int regle)
{
	if ((*tete)==NULL)
	{
		
		res->description = "technique des lacs";
		
		res->points_partie = comptage((*p)->tour->g,regle,0).somme;
		res->partie_opti = (*p);
		return;
	}
	else
	{
	/*On doit poser les tuiles dans le bon ordre*/
		int size=(*p)->tour->g->size;
		couple c = {0,0,0};
		int pts = 0;
		int id=valeur_max(*tete,regle);
		defiler_tri(tete,queue,sortie,sizeof(ptuile),id);
		//affichage_tuile(*sortie,0);
		c=max_lac_position(p,*sortie,&pts);/*La position qui maximise les points des villages : optimisation.c l 288*/
		add_tour(p,*sortie, c.x, c.y, c.r,size);
		lac_inclusion_rec(p,tete,queue,sortie,compt++,res,regle);
	}
}


int best( parcours_p * p, file_p tuiles, int regle)
{
	if (tuiles==NULL)
	{
		return comptage((*p)->tour->g,regle,1).somme;
	}
	else
	{
		/*On doit poser les tuiles dans le bon ordre*/
		int n=(-1);
		int size=(*p)->tour->g->size;
		file_p point=tuiles;
		int i=0;
		int j=0;
		int r=0;
		while (point!=NULL)
		{
			for (i=0;i<size;i++)
			{
				for (j=0;j<size;j++)
				{
					ptuile *a = (ptuile *)calloc(1,sizeof(ptuile));

					a = point->data;

					for (r=0;r<4;r++)
					{
						if (add_tour(p,*a,i,j,r,(*p)->tour->g->size))
						{							
							n=max2(comptage((*p)->tour->g,regle,0).somme,n);
							remove_tour(p);
						}
					}		

				}

			}
			point=point->suite;
		}
		return n;
	}
}


couple place_opt_lacs ( parcours_p * p, file_p tuiles )
{
	couple solution;
	if (tuiles==NULL)
	{
		solution.x=-1;
		solution.y=-1;
		return solution;
	}
	else
	{
		/*On doit poser les tuiles dans le bon ordre*/
		int best=-1;
		int size=(*p)->tour->g->size;
		file_p point=tuiles;
		int i=0;
		int j=0;
		int r=0;
		int temp=0;
		while (point!=NULL)
		{
			for (i=0;i<size;i++)
			{
				for (j=0;j<size;j++)
				{
					for (r=0;r<4;r++)
					{

						ptuile *a = (ptuile *)calloc(1,sizeof(ptuile));

						a = point->data;

						if (add_tour(p,*a,i,j,r,(*p)->tour->g->size))
						{
							temp=test_presence_totale((*p)->tour->g,'L');
							if (best< temp){
								solution.x=i;
								solution.y=j;
								solution.r=r;
								best=temp;	
							}
							remove_tour(p);
						}
					}		

				}

			}
			point=point->suite;
		}
	}
	return solution;
}


couple place_opt_village ( parcours_p * p, file_p tuiles )
{
	couple solution;
	if (tuiles==NULL)
	{
		solution.x=-1;
		solution.y=-1;
		solution.r=-1;
		return solution;
	}
	else
	{
		/*On doit poser les tuiles dans le bon ordre*/
		int best=-1;
		int size=(*p)->tour->g->size;
		file_p point=tuiles;
		int i=0;
		int j=0;
		int r=0;
		int temp=0;
		int presence_carre=0;
		while (point!=NULL)
		{
			for (i=0;i<size;i++)
			{
				for (j=0;j<size;j++)
				{
					for (r=0;r<4;r++)
					{
						ptuile *a = (ptuile *)calloc(1,sizeof(ptuile));

						a = point->data;

						if (add_tour(p,*a,i,j,r,(*p)->tour->g->size))
						{
							temp=test_village2((*p)->tour->g,i,j,(*p)->tour->g->size,&presence_carre);
							if (best< temp){
								solution.x=i;
								solution.y=j;
								solution.r=r;
								best=temp;	
							}
							remove_tour(p);
						}
					}		

				}

			}
			point=point->suite;
		}
	}
	return solution;
}

/*Les informations sur les maximum de points que l'on peut attendre */


int max_village_rec ( parcours_p * p, file_p tuiles )
{
	int size=(*p)->tour->g->size;
	int temp=-1;
	int nbr_villes=0;
	int presence_carre=0;
	if (tuiles==NULL)
	{
		int i;
		int j;
		for (i=0;i<size;i++)
		{
			for (j=0;j<size;j++)
			{
				nbr_villes=max2 (nbr_villes,test_village2((*p)->tour->g,i,j,size,&presence_carre));
			}
		}
		return nbr_villes;
	}
	else
	{
	/*On doit poser les tuiles dans le bon ordre*/
		file_p point=tuiles;
		int i=0;
		int j=0;
		int r=0;
		while (point!=NULL)
		{
			for (i=0;i<size;i++)
			{
				for (j=0;j<size;j++)
				{	
					for (r=0;r<4;r++)
					{

						ptuile *a = (ptuile *)calloc(1,sizeof(ptuile));

						a = point->data;

						if (add_tour(p,*a,i,j,r,(*p)->tour->g->size))
						{
							temp=max2 (temp, max_village_rec(p,point->suite));
							remove_tour(p);
						}
					}		
				}

			}
			point=point->suite;
		}
	}
	return temp;
}		


int max_lacs_rec ( parcours_p * p, file_p tuiles )
{
	int size=(*p)->tour->g->size;
	int temp=-1;
	if (tuiles==NULL)
	{
		return test_presence_totale((*p)->tour->g,'L');
	}
	else
	{
	/*On doit poser les tuiles dans le bon ordre*/
		file_p point=tuiles;
		int i=0;
		int j=0;
		int r=0;
		while (point!=NULL)
		{
			for (i=0;i<size;i++)
			{
				for (j=0;j<size;j++)
				{	
					for (r=0;r<4;r++)
					{

						ptuile *a = (ptuile *)calloc(1,sizeof(ptuile));

						a = point->data;

						if (add_tour(p,*a,i,j,r,(*p)->tour->g->size))
						{
							temp=max2 (temp, max_lacs_rec(p,point->suite));
							remove_tour(p);
						}
					}		
				}

			}
			point=point->suite;
		}
	}
	return temp;
}	


int max_retu_rec ( parcours_p * p, file_p tuiles )
{
	int size=(*p)->tour->g->size;
	int temp=-1;
	int nbr_u=0;
	int nbr_r=0;
	if (tuiles==NULL)
	{
		int i;
		int j;
		for (i=0;i<size;i++)
		{
			for (j=0;j<size;j++)
			{
				if ((*p)->tour->g->cells[i][j].type=='R')
				{
					nbr_r+=1;
				}	
				if ((*p)->tour->g->cells[i][j].type=='U')
				{
					nbr_u+=1;
				}
			}
		}
		return min(nbr_u,nbr_r);
	}
	else
	{
	/*On doit poser les tuiles dans le bon ordre*/
		file_p point=tuiles;
		int i=0;
		int j=0;
		int r=0;
		while (point!=NULL)
		{
			for (i=0;i<size;i++)
			{
				for (j=0;j<size;j++)
				{	
					for (r=0;r<4;r++)
					{

						ptuile *a = (ptuile *)calloc(1,sizeof(ptuile));

						a = point->data;

						if (add_tour(p,*a,i,j,r,(*p)->tour->g->size))
						{
							temp=max2 (temp, max_retu_rec(p,point->suite));
							remove_tour(p);
						}
					}		
				}

			}
			point=point->suite;
		}
	}
	return temp;
}


int max_forets_rec ( parcours_p * p, file_p tuiles )
{
	int size=(*p)->tour->g->size;
	int temp=-1;
	int nbr_forets=0;
	if (tuiles==NULL)
	{
		int i;
		int j;
		for (i=0;i<size;i++)
		{
			for (j=0;j<size;j++)
			{
				if ((*p)->tour->g->cells[i][j].type=='F')
				{
					nbr_forets+=1;
				}
			}
		}
		return nbr_forets;
	}
	else
	{
	/*On doit poser les tuiles dans le bon ordre*/
		file_p point=tuiles;
		int i=0;
		int j=0;
		int r=0;
		while (point!=NULL)
		{
			for (i=0;i<size;i++)
			{
				for (j=0;j<size;j++)
				{	
					for (r=0;r<4;r++)
					{

						ptuile *a = (ptuile *)calloc(1,sizeof(ptuile));

						a = point->data;

						if (add_tour(p,*a,i,j,r,(*p)->tour->g->size))
						{
							temp=max2 (temp, max_forets_rec(p,point->suite));
							remove_tour(p);
						}
					}		
				}

			}
			point=point->suite;
		}
	}
	return temp;
}	


/*Fonction Honshu_Opt
- prend en entrée un parcours et une liste L de tuiles à poser
- Si L est vide (toutes les tuiles sont posées), renvoyer le score
actuel de la grille.
- Sinon
== Best <-- (-1)
== Pour toute tuile t de L, toute rotation r de t, et toute position (x,
y) de la grille:
=== Si (x, y) et r sont une manière valide de poser la tuile t sur la
grille
==== Poser la tuile sur la grille
==== Calculer récursivement le score Tmp_Score avec Honshu_Opt(n, T U t, L)
==== Best <-- max(Best, Tmp_Score)
==== Retirer la tuile de la grille
== Renvoyer Best*/
int max_rec ( parcours_p * p, file_p tuiles, int temp,int regle)
{
	if (tuiles==NULL)
	{
		return comptage((*p)->tour->g,regle,0).somme;
	}
	else
	{
	/*On doit poser les tuiles dans le bon ordre*/
		int size=(*p)->tour->g->size;
		file_p point=tuiles;
		int i=0;
		int j=0;
		int r=0;
		while (point!=NULL)
		{
			for (i=0;i<size;i++)
			{
				for (j=0;j<size;j++)
				{	
					for (r=0;r<4;r++)
					{
						ptuile *a = (ptuile *)calloc(1,sizeof(ptuile));

						a = point->data;

						if (add_tour(p, *a,i,j,r,(*p)->tour->g->size))
						{
							temp=max2 (temp, max_rec(p,point->suite,-1,regle));
							remove_tour(p);
						}
					}		
				}

			}
			point=point->suite;
		}
	}
	return temp;
}	
