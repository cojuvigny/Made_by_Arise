#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

#include "tuile.h"
#include "rwfile.h"
#include "affichage.h"
#include "grille.h"
#include "test.h"
#include "file.h"
#include "parcours.h"
#include "comptage.h"
#include "test_cunit_lot_C.h"
#define M 5

int setup(void)  { return 0; }
int teardown(void) { return 0; }

int initialisation_catalogue_C(void){

	if (CUE_SUCCESS != CU_initialize_registry())
		return CU_get_error();

	CU_pSuite pSuite = CU_add_suite("echantillon_test",setup,teardown);

	if( ( NULL == CU_add_test(pSuite, "Test si on renvoie effectivement un village ", test_test_village) ) ||
		( NULL == CU_add_test(pSuite, "Test décompte des points", test_comptage) ))
	{
		CU_cleanup_registry();
		return CU_get_error();
	}

	CU_basic_run_tests();
	CU_basic_show_failures(CU_get_failure_list());
	CU_cleanup_registry();

	return 0;
}

void test_comptage(void){
	int regle = -1;
	grille_p g=create_grille_vide(4);
	int i,j;
	char t[16] = {'V','V','V','L','F','F','U','L','F','V','U','L','F','U','R','L'};
	
	for (i=0;i<4;i++)
	{
		for (j=0;j<4;j++)
		{
			g->cells[i][j].type=t[4*i+j];
			g->cells[i][j].id=0;
		}
	}

	affichage_grille(g,0,0,regle);

	int a = comptage(g, regle,1).somme;
	printf("Le résultat est %d",a);
	CU_ASSERT_EQUAL(a,24);
	free_grille(g);
}

void test_test_village(void){
	printf("Le test est visuel pour l'instant\n");
	int x=14,y=0,regle=0;
	grille_p g2 = create_grille_full_random(15);

	printf("\n La dalle choisie est (ligne/colonne) %d/%d est : %c \n\n",x,y,lire_dalle(g2,x,y));


	test_village(g2,x,y,15,regle);

	free_grille(g2);
}



