#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

#include "tuile.h"
#include "rwfile.h"
#include "affichage.h"
#include "grille.h"
#include "test.h"
#include "tour.h"
#include "parcours.h"
#include "file.h"
#include "comptage.h"
#include "optimisation.h"
#include "test_cunit_lot_C.h"
#include "simulation.h"
#include "aetoile.h"
#include "couleur.h"


int main()
{

	printf("\n------------------------------------\n");
	printf("\n----------Tests simulation----------\n");
	printf("\n------------------------------------\n");

	simulation_point();
	
	
	return 0;
}