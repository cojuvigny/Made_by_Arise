#ifndef _OPTI_
#define _OPTI_

#include "parcours.h"
#include "file.h"
#include "test.h"
#include "comptage.h"

/**
 * @file optimisation.h
 * 
 * Ce fichier décrit un ensemble de fonctions concernant l'optimisation du score du jeu.
 * Il contient 20 fonctions et 2 types.
 * 
 * - le type \a couple définit un couple d'entiers: la position spatiale sur la grille, ainsi que sa rotation.
 * - le type \a resultat définit les résultats d'une optimisation.
 * 
 * - valeur_tuile (ptuile * p, int regle) 																							calcul de la pondération de la tuile
 * - valeur2_tuile (ptuile * p) 																								calcul de la pondération de la tuile selon d'autres critères
 * - proba_tuiles (file_p t) 																									trouve les occurences des types dans une file de tuile
 * - valeur_min ( file_p tuiles, int regle) 																						trouve la tuile ayant la valeur la plus basse
 * - valeur_max ( file_p tuiles, int regle) 																						trouve la tuile ayant la valeur la plus haute
 * - valeur2_max ( file_p tuiles) 																								trouve la tuile ayant la valeur la plus haute avec valeur2_tuile 
 * - max_position(parcours_p * p, ptuile tuile,int * temp, int regle) 																trouve la position où on a le max de points avec ajout
 * - max_village_position (parcours_p * p, ptuile tuile,int * temp)																trouve la position où on a le max de points avec ajout avec les villages 
 * - max_lac_position (parcours_p * p, ptuile tuile,int * temp) 																trouve la position où on a le max de points avec ajout avec les lacs
 * - max_proba_position (parcours_p * p, ptuile tuile,int * temp, int regle) 														trouve la position où on a le max de points avec ajout avec les place_opt_lacs
 * - max_proba2_position (parcours_p * p, ptuile tuile, points t,int * temp, int regle) 											trouve la position où on a le max de points avec ajout avec les lacs
 * - inclusion_rec ( parcours_p * p, file_p * tete, file_p * queue, ptuile * sortie, int compt,resultat * res, int regle) 			trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille avec la valeur la plus haute ( recursivement)
 * - inclusion2_rec ( parcours_p * p, file_p * tete, file_p * queue,ptuile * sortie,int compt,int type, resultat * res, int regle)
 trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille avec la valeur la plus haute ( recursivement) et en ajoutant une tuile selon la tuile probable insérer ensuite
 * - inclusion3_rec ( parcours_p * p, file_p * tete, file_p * queue,ptuile * sortie,int compt,int type, resultat * res, int regle) 
 trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille avec la valeur la plus haute ou la plus basse(recursivement) et en ajoutant une tuile selon les 2 tuiles que l'on insérera probablement à la suite
 * - max_modifgrille_rec ( parcours_p * p, file_p * tete, file_p * queue, ptuile * sortie, int compt,resultat * res, int regle) 			trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille avec un max pour les grilles qui dépend du nombre de lacs/...
 * - max_modiftuile_rec ( parcours_p * p, file_p * tete, file_p * queue, ptuile * sortie, int compt, points t,resultat * res, int regle) 	trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille avec un max pour les grilles qui dépend du nombre de lacs/...
 * - village_inclusion_rec ( parcours_p * p, file_p * tete, file_p * queue, ptuile * sortie, int compt,resultat * res, int regle) 			trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille avec la valeur la plus haute pour les villages( recursivement)
 * - lac_inclusion_rec ( parcours_p * p, file_p * tete, file_p * queue, ptuile * sortie, int compt,resultat * res, int regle) 				trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille avec la valeur la plus haute pour les lacs( recursivement)
 * - best ( parcours_p * p, file_p tuiles, int regle) 																			trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille
 * - place_opt_lacs ( parcours_p * p, file_p tuiles) 																			trouve la position pour laquelle le maximum de points pour les lacs seulement est atteint
 * - place_opt_village ( parcours_p * p, file_p tuiles) 																		trouve la position pour laquelle le maximum de points pour les villes seulement est atteint
 * - max_village_rec ( parcours_p * p, file_p tuiles ) 																			trouve le maximum de points obtenu grâce au village que l'on peut obtenir en ajoutant les tuiles de la liste à la grille
 * - max_rec ( parcours_p * p, file_p tuiles, int regle) 																					trouve le maximum de points que l'on peut obtenir en ajoutant les tuiles de la liste à la grille
 */


/**
 * \struct 	couple 
 * \brief 	structure de données de position d'une tuile
 *
 * \a cases définit un couple d'entiers: la position spatiale sur la grille, ainsi que sa rotation.
 */
typedef struct couple
{
	int x;
	int y;
	int r;
}couple,*couple_p;


/**
 * \struct 	resultat 
 * \brief 	structure qui contient les resultats d'un optimisation
 *
 * L'ensemble des résultats d'une optimisation
 */
typedef struct resultat
{
	char * description;
	int points_partie;
	parcours_p partie_opti;
}resultat,*resultat_p;



/** 
 * @brief 			calcul de la pondération de la tuile
 * @param  	p 		un pointeur de tuile 
 * @param   regle	la regle du jeu choisie
 * @return 			valeur de la tuile (pondération)
 */
int valeur_tuile (ptuile * p, int regle);

/** 
 * @brief 		calcul numéro 2 de la pondération de la tuile
 * @param  	p 	un pointeur de tuile 
 * @return 		valeur de la tuile (pondération)
 */
int valeur2_tuile (ptuile *p);

/** 
 * @brief 		???????????
 * @param 	t   une liste de tuiles 
 * @return 		???????????
 */
points proba_tuiles (file_p t);

/** 
 * @brief 		trouve la tuile ayant la valeur la plus basse
 * @param 	t   une liste de tuiles 
 * regle		la regle du jeu choisie
 * @return 		l'id de la tuile ayant la valeur la plus basse
 */
int valeur_min ( file_p tuiles, int regle);

/** 
 * @brief 			trouve la tuile ayant la valeur la plus haute
 * @param 	tuile 	une liste de tuiles 
 * regle			la regle du jeu choisie
 * @return 			l'id de la tuile ayant la valeur la plus haute
 */
int valeur_max ( file_p tuiles, int regle);

/** 
 * @brief 			trouve la tuile ayant la valeur la plus haute pour une autre fonction: valeur2_tuile
 * @param 	tuile 	une liste de tuiles 
 * @return 			l'id de la tuile ayant la valeur la plus haute
 */
int valeur2_max ( file_p tuiles);

/** 
 * @brief 			trouve la position où on a le max de points avec ajout
 * @param 	p		un parcours
 * @param 	tuile 	une liste de tuiles 
 * @param	tmp 	un pointeur sur le nombre de points courant
 * @param   regle	la regle du jeu choisie
 * @return 		    la position et de la rotation
 */
couple max_position(parcours_p * p, ptuile tuile,int * temp, int regle);

/** 
 * @brief 			trouve la position où on a le max de points avec ajout avec les villages
 * @param 	p		un parcours
 * @param 	tuile 	une liste de tuiles 
 * @param	tmp 	un pointeur sur le nombre de points courant
 * @return 		    la position et de la rotation
 */
couple max_village_position (parcours_p * p, ptuile tuile,int * temp);

/** 
 * @brief 			trouve la position où on a le max de points avec ajout avec les lacs
 * @param 	p		un parcours
 * @param 	tuile 	une liste de tuiles 
 * @param	tmp 	un pointeur sur le nombre de points courant
 * @return 		    la position et de la rotation
 */
couple max_lac_position (parcours_p * p, ptuile tuile,int * temp);

/** 
 * @brief 			trouve la position où on a le max de points avec ajout avec les lacs
 * @param 	p		un parcours
 * @param 	tuile 	une liste de tuiles 
 * @param	tmp 	un pointeur sur le nombre de points courant
 * @param   regle	la regle du jeu choisie
 * @return 		    la position et de la rotation
 */
couple max_proba_position (parcours_p * p, ptuile tuile,int * temp, int regle);

/** 
 * @brief 			trouve la position où on a le max de points avec ajout avec les lacs
 * @param 	p		un parcours
 * @param 	tuile 	une liste de tuiles 
 * @param   t 		le nombre de points marqué
 * @param	tmp 	un pointeur sur le nombre de points courant
 * @param   regle	la regle du jeu choisie
 * @return 		    la position et de la rotation
 */
couple max_proba2_position (parcours_p * p, ptuile tuile, points t,int * temp, int regle);

/** 
 * @brief 			trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille avec la valeur la plus haute ou la plus basse(recursivement)
 * @param 	p		un parcours
 * @param 	tete 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param 	queue 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param   sortie 	un pointeur pour recupérer la data défilée
 * @param   compt 	un compteur du nombre de tuiles défilées
 * @param 	type 	si on choisit la tuile à la valeur la plus haute (0) ou la plus basse (1)
 * @param   res     le resultat
 * @param   regle	la regle du jeu choisie
 * @return			rien 
 */
void inclusion_rec ( parcours_p * p, file_p * tete, file_p * queue,ptuile * sortie,int compt,int type, resultat * res, int regle);

/** 
 * @brief 			trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille avec la valeur la plus haute ou la plus basse(recursivement) et en ajoutant une tuile selon la tuile que l'on insérera probablement à la suite
 * @param 	p		un parcours
 * @param 	tete 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param 	queue 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param   sortie 	un pointeur pour recupérer la data défilée
 * @param   compt 	un compteur du nombre de tuiles défilées
 * @param 	type 	si on choisit la tuile à la valeur la plus haute (0) ou la plus basse (1)
 * @param   res     le resultat
 * @param   regle		la regle du jeu choisie
 * @return			rien 
 */
void inclusion2_rec ( parcours_p * p, file_p * tete, file_p * queue,ptuile * sortie,int compt,int type, resultat * res, int regle);

/** 
 * @brief 			trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille avec la valeur la plus haute ou la plus basse(recursivement) et en ajoutant une tuile selon les 2 tuiles que l'on insérera probablement à la suite
 * @param 	p		un parcours
 * @param 	tete 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param 	queue 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param   sortie 	un pointeur pour recupérer la data défilée
 * @param   compt 	un compteur du nombre de tuiles défilées
 * @param 	type 	si on choisit la tuile à la valeur la plus haute (0) ou la plus basse (1)
 * @param   res     le resultat
 * @return			rien 
 */
void inclusion3_rec ( parcours_p * p, file_p * tete, file_p * queue,ptuile * sortie,int compt,int type, resultat * res, int regle);

/** 
 * @brief 			trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille avec un max pour les grilles qui dépend du nombre de lacs/...
 * @param 	p		un parcours
 * @param 	tete 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param 	queue 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param   sortie 	un pointeur pour recupérer la data défilée
 * @param   compt 	un compteur du nombre de tuiles défilées
 * @param   res     le resultat
 * @param   regle	la regle du jeu choisie
 * @return			rien 
 */
void max_modifgrille_rec ( parcours_p * p, file_p * tete, file_p * queue, ptuile * sortie, int compt, resultat * res, int regle);

/** 
 * @brief 			trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille avec un max pour les grilles qui dépend du nombre de lacs/...
 * @param 	p		un parcours
 * @param 	tete 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param 	queue 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param   sortie 	un pointeur pour recupérer la data défilée
 * @param   compt 	un compteur du nombre de tuiles défilées
 * @param   t 		le nombre de points marqué
 * @param   res     le resultat
 * @param   regle	la regle du jeu choisie
 * @return 			rien 
 */void max_modiftuile_rec ( parcours_p * p, file_p * tete, file_p * queue, ptuile * sortie, int compt, points t, resultat * res, int regle);

/** 
 * @brief 			trouve le maximum de points que l'on peut obtenir en tentant d'optimiser suivant l'occurence des types
 * @param 	p		un parcours
 * @param 	tete 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param 	queue 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param   sortie 	un pointeur pour recupérer la data défilée
 * @param   compt 	un compteur du nombre de tuiles défilées
 * @param   t 		le nombre de points marqué
 * @param   res     le resultat
 * @param   regle	la regle du jeu choisie
 * @return 			rien 
 */
void max_proba_rec ( parcours_p * p, file_p * tete, file_p * queue, ptuile * sortie, int compt, points t, resultat * res, int regle);

/** 
 * @brief 			trouve le maximum de points que l'on peut obtenir en tentant d'optimiser une seconde fois suivant l'occurence des types
 * @param 	p		un parcours
 * @param 	tete 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param 	queue 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param   sortie 	un pointeur pour recupérer la data défilée
 * @param   compt 	un compteur du nombre de tuiles défilées
 * @param   t 		le nombre de points marqué
 * @param   res     le resultat
 * @param   regle	la regle du jeu choisie
 * @return 			rien 
 */
void max_proba2_rec ( parcours_p * p, file_p * tete, file_p * queue, ptuile * sortie, int compt, points t, resultat * res, int regle);


/** 
 * @brief 			trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille avec la valeur la plus haute pour les villages( recursivement)
 * @param 	p		un parcours
 * @param 	tete 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param 	queue 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param   sortie 	un pointeur pour recupérer la data défilée
 * @param   compt 	un compteur du nombre de tuiles défilées
 * @param   res     le resultat
 * @param   regle	la regle du jeu choisie
 * @return			rien 
 */
void village_inclusion_rec ( parcours_p * p, file_p * tete, file_p * queue, ptuile * sortie, int compt, resultat * res, int regle);

/** 
 * @brief 			trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille avec la valeur la plus haute pour les lacs( recursivement)
 * @param 	p		un parcours
 * @param 	tete 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param 	queue 	un pointeur sur la tete d'une liste de tuiles à poser
 * @param   sortie 	un pointeur pour recupérer la data défilée
 * @param   compt 	un compteur du nombre de tuiles défilées
 * @param   res     le resultat
 * @param   regle	la regle du jeu choisie
 * @return 			rien 
 */
void lac_inclusion_rec ( parcours_p * p, file_p * tete, file_p * queue, ptuile * sortie, int compt, resultat * res, int regle);

/** 
 * @brief 			trouve le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille
 * @param 	p 		un parcours
 * @param 	tuiles 	une liste de tuiles à poser
 * @param   regle		la regle du jeu choisie
 * @return 			le maximum de points que l'on peut obtenir en ajoutant une tuile de la liste à la grille
 */
int best ( parcours_p * p, file_p tuiles, int regle);

/** 
 * @brief 			trouve la position pour laquelle le maximum de points pour les lacs seulement est atteint
 * @param 	P 		un parcours
 * @param 	tuiles 	une liste de tuiles à poser
 * @return 			la position et la rotation du maximum
 */
couple place_opt_lacs ( parcours_p * p, file_p tuiles);

/** 
 * @brief 			trouve la position pour laquelle le maximum de points pour les villes seulement est atteint
 * @param 	P 		un parcours
 * @param 	tuiles 	une liste de tuiles à poser
 * @return 			la position et la rotation  du maximum
 */
couple place_opt_village ( parcours_p * p, file_p tuiles);

/** 
 * @brief 			trouve le maximum de points obtenu grâce au village que l'on peut obtenir en ajoutant les tuiles de la liste à la grille
 * @param 	P		un parcours
 * @param 	tuiles 	une liste de tuiles à poser
 * @return 			le maximum de points obtenu grâce au village que l'on peut obtenir en ajoutant les tuiles de la liste à la grille
 */
int max_village_rec ( parcours_p * p, file_p tuiles );

/** 
 * @brief 			trouve le maximum de points obtenu grâce aux lacs que l'on peut obtenir en ajoutant les tuiles de la liste à la grille
 * @param 	P		un parcours
 * @param 	tuiles 	une liste de tuiles à poser
 * @return 			le maximum de points obtenu grâce aux lacs que l'on peut obtenir en ajoutant les tuiles de la liste à la grille
 */
int max_lacs_rec ( parcours_p * p, file_p tuiles );

/** 
 * @brief 			trouve le maximum de points obtenu grâce aux ressources et usines que l'on peut obtenir en ajoutant les tuiles de la liste à la grille
 * @param 	P		un parcours
 * @param 	tuiles 	une liste de tuiles à poser
 * @return 			le maximum de points obtenu grâce aux ressources et usines que l'on peut obtenir en ajoutant les tuiles de la liste à la grille
 */
int max_retu_rec ( parcours_p * p, file_p tuiles );

/** 
 * @brief 			trouve le maximum de points obtenu grâce aux forets que l'on peut obtenir en ajoutant les tuiles de la liste à la grille
 * @param 	P		un parcours
 * @param 	tuiles 	une liste de tuiles à poser
 * @return 			le maximum de points obtenu grâce aux forets que l'on peut obtenir en ajoutant les tuiles de la liste à la grille
 */
int max_forets_rec ( parcours_p * p, file_p tuiles );

/** 
 * @brief 			trouve le maximum de points que l'on peut obtenir en ajoutant les tuiles de la liste à la grille
 * @param 	P 		un parcours
 * @param 	tuiles 	une liste de tuiles à poser
 * @param   regle		la regle du jeu choisie
 * @return 			le maximum de points que l'on peut obtenir en ajoutant les tuiles de la liste à la grille
 */
int max_rec ( parcours_p * p, file_p tuiles, int temp,  int regle);

 #endif