#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <ncurses.h>
#include <string.h>

#include "loop_nc.h"
#include "file.h"
#include "comptage.h"
#include "loop.h"
#include "couleur.h"



void nc_print_tuile(WINDOW* main, ptuile tuile, int x, int y){
	mvwprintw(main, x, y+1, "___");
	mvwprintw(main, x+1, y, "|%c %c|",tuile->tab[0],tuile->tab[1]);
	mvwprintw(main, x+2, y, "|   |");
	mvwprintw(main, x+3, y, "|%c %c|",tuile->tab[2],tuile->tab[3]);
	mvwprintw(main, x+4, y, "|   |");
	mvwprintw(main, x+5, y, "|%c %c|",tuile->tab[4],tuile->tab[5]);
	mvwprintw(main, x+6, y, "|___|");
	mvwprintw(main, x+7, y+2, "%d",tuile->id);
}

void nc_print_tour(ptour tour, int sz_grille){
	int i,j;
	for (i=0;i<sz_grille+5;i++){
		for (j=0;j<sz_grille+5;j++){
			move(i,j);
			printw(" ");
		}
	}
	int sz=tour->g->size;
  /* Affichage du cadre de la grille */
	for (i=0;i<sz_grille;i+=2){
		if ((i/2+1)/10>=1){
			move(0,i+3);
			printw("%d",(i/2+1)/10);
		}
	}
	move(0,0);
	printw("\\ y");
	move(1,1);
	printw("\\ ");

	for (i=0;i<sz_grille;i+=2){
		printw("%d ",(i/2+1)%10);
	}

	move(2,1);
	printw("x\\");
	for (i=0;i<sz_grille-1;i++){
		printw("_");
	}
	move(2+sz_grille,3);
	for (i=0;i<sz_grille-1;i++){
		printw("_");
	}

	for (i=0;i<sz_grille;i+=2){
		move(i+3,1);
		printw("%d",(i/2+1)%10);
		if ((i/2+1)/10>=1){
			move(i+3,0);
			printw("%d",(i/2+1)/10);
		}
	}

	for (i=0;i<sz_grille;i++){
		move(i+3,2);
		printw("|");
		move(i+3,2+sz_grille);
		printw("|"); 
	}
  /* Affichage du contenu de la grille */
  /*   par->tour->g->cells[][]   */
	for (i=0;i<sz;i++){
		for (j=0;j<sz;j++){
			move(3+2*i,3+2*j);
			printw("%c",tour->g->cells[i][j].type);
			if ((i!=sz-1) && (tour->g->cells[i][j].id!=tour->g->cells[i+1][j].id)){
				move(3+2*i+1,3+2*j);
				printw("_");
			} 
			if ((j!=sz-1) && (tour->g->cells[i][j].id!=tour->g->cells[i][j+1].id)){
				move(3+2*i,3+2*j+1);
				printw("|");
				move(3+2*i+1,3+2*j+1);
				printw("|");
			}
			if ((i!=sz-1) && (j!=sz-1) && (tour->g->cells[i][j].id!=tour->g->cells[i+1][j+1].id)){
				move(3+2*i+1,3+2*j+1);
				if (tour->g->cells[i][j].id==tour->g->cells[i][j+1].id) {
					if (tour->g->cells[i][j].id==tour->g->cells[i+1][j].id) printw(" ");
					else printw("_");
				}
				else printw("|");
			}
		}
	}
	
  //refresh();
}


int nc_get_nb(){
	int n=100;
	int k=0;
	char* str = malloc(n*sizeof(char));
	while (k < n) {
		str[k] = getch();
		if (str[k] == '\n') {
			break;
		} else if (str[k] == 127) {
			str[k] = 0;
			k--;
			str[k] = 0;
			k--; 
		}
		k++;
	}
	n=atoi(str);
	free(str);
	return n;
}


void nc_loop_init(parcours_p* parc, file_p* main_jeu_tete,file_p* main_jeu_queue, int* sz_main, int* sz_grille) {

	int scan=0;
	int choix=0, id_depart;
	ptuile tuile_initiale;
	int c1=0;
	int type_charge=0;
	file_p tete_fic_t=NULL,queue_fic_t=NULL;
	file_p tete_fic_g=NULL,queue_fic_g=NULL;


	int nombre_fic_t = 0;
	nombre_fic_t = compt_fic("bin/tuile",&tete_fic_t,&queue_fic_t);
	int nombre_fic_g = 0;
	nombre_fic_g = compt_fic("bin/grille",&tete_fic_g,&queue_fic_g); 

	srand(time(NULL));

	while(!(c1==13))
	{   

		if( c1==67 || c1==68)
		{
			type_charge+=1;  
		}

		printf(BLEU "##################################################\n" RESET);
		printf(BLEU "##################Jeu de Honshu###################\n" RESET);
		printf(BLEU "######Tout le jeu se joue avec les flèches########\n" RESET);
		printf(BLEU "####Pour faire tourner une tuile, appuyer sur r###\n" RESET);
		printf(BLEU "###Si vous vous retrouvez bloqué, appuyer sur x###\n" RESET);
		printf(BLEU "##Pour plus d'informations en jeu, appuyer sur i##\n" RESET);
		printf(BLEU "##################################################\n\n" RESET);
		printf(ROUGE "Comment voulez-vous choisir vos tuiles ?\n\n" RESET);
		if (type_charge%2==0){
			printf(INV "aléatoirement" RESET " fichier\n\n");
			choix=1;
		}
		else{
			printf( "aléatoirement" INV " fichier\n\n" RESET);
			choix=0;
		}

		if(system("/bin/stty raw")) {exit(EXIT_FAILURE);}
		c1 = getchar();
		if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);}

		clrscr();
	}

	if(system("/bin/stty raw")) {exit(EXIT_FAILURE);}
	if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);}

	c1=0;

	
	if (choix == 1) {

		printf(ROUGE  "Quelle est la taille de votre grille ? (Entre 5 et 30) \n\n" RESET);
		scan=0;
		while (scan != 1 || (*sz_grille<5 || *sz_grille>30)){
			printf(">  ");
			scan = scanf("%d",sz_grille);
			vider_buffer();
		}

		printf(ROUGE  "Combien de tuiles voulez-vous dans votre main ? (Entre 0 et 50) \n\n" RESET);
		scan=0;
		while (scan != 1 || (*sz_main<0 || *sz_main>50)){
			printf(">  ");
			scan = scanf("%d",sz_main);
			vider_buffer();
		}
		clrscr();
		printf(ROUGE "... Chargement\n" RESET);

		id_depart = *sz_main;
		ptuile * tab_tuile = init_tuile_random_tab(*sz_main);
		init_main_jeu(tab_tuile,main_jeu_tete,main_jeu_queue,*sz_main);
        sleep(1); /*Pour que les tuiles soient différentes*/

		tuile_initiale = init_tuile_random(id_depart); 
		free(tab_tuile);
	}

	else 
	{
		char* tuile_f_name = calloc(30,sizeof(char));
		char* grille_f_name = calloc(30,sizeof(char));
		int tuile_t_sz;


		scan=0;
		c1=0;

        while(!(c1==13)) /*Choix de la tuile*/
		{   
			if( c1==66){
				scan+=1;
			} 

			if( c1==65){
				scan-=1;
				if (scan <0){
					scan = nombre_fic_t-1;
				}
			}   
			
			printf(ROUGE "Choisissez le fichier de tuile : \n\n" RESET);
			affiche_fic(tete_fic_t,scan%nombre_fic_t,&tuile_f_name);
			
			if(system("/bin/stty raw")) {exit(EXIT_FAILURE);} 
			c1 = getchar();
			if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);} 
			clrscr();
		}

		
		c1=0;
		scan=0;
		
		while(!(c1==13))
		{   
			if( c1==66){
				scan+=1;
			} 

			if( c1==65){
				scan-=1;
				if (scan <0){
					scan = nombre_fic_g-1;
				}
			}   
			
			printf(ROUGE "Choisissez le fichier de grille : \n\n" RESET);
			affiche_fic(tete_fic_g,scan%nombre_fic_g,&grille_f_name);
			
			if(system("/bin/stty raw")) {exit(EXIT_FAILURE);} 
			c1 = getchar();
			if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);}
			clrscr(); 
		}

		clrscr();
		printf(ROUGE "... Chargement\n" RESET);
		

		char dos_t[80];
		char dos_g[80];
		strcpy(dos_t, "bin/tuile/");
		strcpy(dos_g, "bin/grille/");

		ptuile * pre_tab_tuile = read_file_tuile(strcat(dos_t,tuile_f_name), &tuile_t_sz);
		
		struct info_grille info;
		read_file_grille(strcat(dos_g,grille_f_name), &info);
		*sz_grille = info.sz_grille;
		*sz_main = info.sz_main;

		ptuile * tab_tuile = calloc(*sz_main,sizeof(ptuile));

		
		tuile_initiale = pre_tab_tuile[info.id_depart];
		tuile_initiale->id = *sz_main;

		int i = 0;
		while (i < *sz_main)
		{
			ptuile tuile_courante = copie_tuile(pre_tab_tuile[info.ids[i]]);
			tuile_courante->id = i;
			tab_tuile[i] = tuile_courante;;          
			i++;
		}

		init_main_jeu(tab_tuile,main_jeu_tete,main_jeu_queue,*sz_main);

		free(tuile_f_name);
		free(grille_f_name);

		for (int i = 0; i < tuile_t_sz; ++i){
			if (i != info.id_depart)
				free_tuile(pre_tab_tuile[i]);
		}
		free(info.ids);
		free(pre_tab_tuile);
		free(tab_tuile);
	}
	*parc = create_tour(tuile_initiale, (int)floor(*sz_grille / 2), (int)floor(*sz_grille / 2), 1, *sz_grille);

	free(tuile_initiale);
	vider_liste_fic(&tete_fic_t,&queue_fic_t,nombre_fic_t);
	vider_liste_fic(&tete_fic_g,&queue_fic_g,nombre_fic_g);
	nettoyer(&tete_fic_t,&queue_fic_t);
	nettoyer(&tete_fic_g,&queue_fic_g);

}


void nc_init_main_jeu(ptuile * tab_tuile,file_p* main_jeu_tete,file_p* main_jeu_queue,int sz_main)
{
	int i=0;
	ptuile tuile_courante = NULL;
	while(i != sz_main)
	{
		tuile_courante = tab_tuile[i];
		enfiler_tri(&tuile_courante,main_jeu_tete,main_jeu_queue,sizeof(ptuile));
		i++;
	}
}


void nc_loop(parcours_p par,file_p* main_jeu_tete,file_p* main_jeu_queue, int sz_main, int sz_grille, int *regle) {
    /*affichage des tuiles restantes*/
	int i, x, y, o, choix;
	int nb_tour = 0;
	int retrait;
	ptuile* inventaire_tuile = calloc(sz_main,sizeof(ptuile));

	initscr();
	WINDOW* main_joueur;

    /* Vérification de la taille du terminal */ 
	if ((COLS<sz_grille+6*4+10)||(LINES<sz_grille+5)){
		fprintf(stderr, "Terminal trop petit\n");
		endwin();
	}

	main_joueur = subwin(stdscr, 9*3+1, 6*4, 2, sz_grille*2+5);
	mvwprintw(main_joueur, 1, 1, "Vos tuiles :");


	
	loop:

	nc_print_tour(par->tour,sz_grille*2);
	move(LINES-3, COLS-10);    
	printw("(tour %d)",nb_tour);
	wclear(main_joueur);
	if (*main_jeu_tete == NULL)
		goto coda;

    /* Affichage des tuiles */
	
	file_p maillon_courant = *main_jeu_tete;
	ptuile * courant = malloc(sizeof(tuile));
	memcpy(courant, maillon_courant->data, sizeof(tuile));
	nc_print_tuile(main_joueur, *courant, 2, 1);
	i=0;
	while (maillon_courant->suite!= NULL){
		maillon_courant = maillon_courant->suite;
		i++;
		x = 2+(i/4)*9;
		y = 1+(i%4)*6;
		memcpy(courant, maillon_courant->data, sizeof(tuile));
		nc_print_tuile(main_joueur, *courant, x, y);
	} 
	wrefresh(main_joueur);
	free(courant);

	choix:
    /* Commandes utilisateur */
	move(LINES-2,3);
	printw("                                                                            ");
	move(LINES-2,3);
	printw("Choisissez votre tuile : ");
	choix=nc_get_nb();
	if  (defiler_tri(main_jeu_tete,main_jeu_queue,&inventaire_tuile[nb_tour],sizeof(inventaire_tuile[nb_tour]),choix)) {
		move(LINES-3,3);
		printw("%d est un chiffre invalide, merci de choisir un des id affiché !",choix);
		goto choix;
	}
	move(LINES-3,3);
	printw("                                                                                  ");
	move(LINES-2,3);
	printw("Où voulez-vous la placer ?");
	move(LINES-1,3);
	printw("                                             ");
	move(LINES-1,3);
	printw("x : ");
	x=nc_get_nb()-1;
	move(LINES-1,3);
	printw("                                             ");
	move(LINES-1,3);
	printw("y : ");
	y=nc_get_nb()-1;
	move(LINES-1,3);
	printw("                                             ");
	move(LINES-1,3);
	printw("orientation : ");
	o=nc_get_nb()%4;
	move(LINES-3,3);

	
	if (!add_tour(&par, inventaire_tuile[nb_tour], x, y, o, sz_grille)) {
		retour:
		enfiler_tri(&inventaire_tuile[nb_tour],main_jeu_tete,main_jeu_queue,sizeof(ptuile));
		sleep(1);
		getch();
		move(LINES-3,3);
		printw("                                                         ");
		goto loop;
	}

	nc_print_tour(par->tour,sz_grille*2);
	move(LINES-1,3);
	printw("                                             ");
	move(LINES-2,3);
	printw("Voulez-vous enlever la tuile (oui=1|non=0) ? ");
	retrait = nc_get_nb();
	if (retrait){
		remove_tour(&par);
		goto retour;
	}

	nb_tour++;
	goto loop;

	coda:
	delwin(main_joueur);
	endwin();
	printf("\n\n*********Le résultat est %d points. Bravo !*********\n\n",comptage(par->tour->g,*regle,1).somme);

	printf("Au nom de toute l'équipe de Made_by_Arise, nous vous remercions d'avoir participé à cette partie de Honshu.\nA bientôt !\n\n\n\n");

	for (int i = 0; i < nb_tour; ++i)
	{
		free_tuile(inventaire_tuile[i]);
	}
	free(inventaire_tuile);
	remove_all_tour(&par);
}

void nc_vider_buffer(){
	int c = 0;
	while (c != '\n' && c != EOF)
	{
		c = getchar();
	}
}
