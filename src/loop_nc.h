#ifndef _LOOPNC_
#define _LOOPNC_

#include <ncurses.h>
#include "rwfile.h"
#include "grille.h"
#include "tuile.h"
#include "file.h"
#include "tour.h"
#include "parcours.h"
#include "comptage.h"
#include "affichage.h"


/**
 * @file loop_nc.h
 * 
 * Ce fichier décrit l'ensemble des fonctions permettant de réaliser une boucle de jeu.
 * Il contient 7 fonctions.
 *  
 * - nc_print_tuile(WINDOW* main, ptuile tuile, int x, int y)														affiche une tuile en position (x,y)
 * - nc_print_tour(ptour tour, int sz_grille) 																		affiche la grille et son contenu
 * - nc_get_nb()												 													permet de récupérer un nombre rentré par l'utilisateur
 * - nc_loop_init(parcours_p* parc, file_p* main_jeu_tete,file_p* main_jeu_queue, int* sz_main, int* sz_grille, int *regle) 	réalise la boucle de jeu
 * - nc_loop(parcours_p par,file_p* main_jeu_tete,file_p* main_jeu_queue, int sz_main, int sz_grille) 				initialise le jeu
 * - nc_init_main_jeu(ptuile * tab_tuile,file_p* main_jeu_tete,file_p* main_jeu_queue,int sz_main, int *regle) 					initialise la main de jeu
 * - nc_vider_buffer()																								vide le buffer
 */

/**
 * @brief 			affiche une tuile en position (x,y)
 * @param 	main 	une fenêtre 
 * @param  	tuile 	une liste de tuile
 * @param	x		la coordonnée x de la tuile
 * @param	y		la coordonnée y de la tuile
 * @return 			void
 */
void nc_print_tuile(WINDOW* main, ptuile tuile, int x, int y);

/**
 * @brief 				affiche la grille et son contenu
 * @param	tour 		un tour à afficher et la taille de la grille
 * @param   sz_grille	la taille de la grille
 * @return 				void
 */
void nc_print_tour(ptour tour, int sz_grille);

/**
 * @brief 	permet de récupérer un nombre rentré par l'utilisateur
 * @param 	rien
 * @return 	l'entier rentré par l'utilisateur
 */
int nc_get_nb();

/**
 * @brief 					réalise la boucle de jeu
 * @param 	parc           	un parcours de jeu
 * @param 	main_jeu_tete  	un pointeur sur la tete de file de tuiles
 * @param 	main_jeu_queue 	un pointeur sur la queue de file de tuiles
 * @param 	sz_main     	le nombre de tuiles dans la main
 * @param 	sz_grille     	la taille de la grille
 * @return 					rien
 */
void nc_loop_init(parcours_p* parc, file_p* main_jeu_tete,file_p* main_jeu_queue, int* sz_main, int* sz_grille);

/**
 * @brief 					initialise la boucle de jeu
 * @param 	par           	un parcours de jeu
 * @param 	main_jeu_tete  	un pointeur sur la tete de file de tuiles
 * @param 	main_jeu_queue 	un pointeur sur la queue de file de tuiles
 * @param 	sz_main        	le nombre de tuiles dans la main
 * @param 	sz_grille      	la taille de la grille
 * @param   regle		la regle du jeu choisie
 * @return 					rien
 */
void nc_loop(parcours_p par,file_p* main_jeu_tete,file_p* main_jeu_queue, int sz_main, int sz_grille, int *regle);

/**
 * @brief 					initialise la main de jeu
 * @param 	tab_tuile       le tableau des tuiles selectionnées
 * @param 	main_jeu_tete  	un pointeur sur la tete de file de tuiles
 * @param 	main_jeu_queue 	un pointeur sur la queue de file de tuiles
 * @param 	sz_main        	le nombre de tuiles dans la main
 * @return 					rien
 */
void nc_init_main_jeu(ptuile * tab_tuile,file_p* main_jeu_tete,file_p* main_jeu_queue,int sz_main);

/**
 * @brief vide le buffer
 * @param rien
 * @return rien
 */
void nc_vider_buffer();

#endif
