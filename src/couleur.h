#ifndef _COULEUR_
#define _COULEUR_ 

#include <stdio.h>
#include "tour.h"
#include "optimisation.h"



/**
 * @file couleur.h
 * 
 * Ce fichier décrit un ensemble de fonctions concernant l'affichage interactif et amélioré du jeu.
 * Il contient 2 fonctions et 13 directives de préprocesseur.
 * 
 * - grille_evolution(grille_p g) 													permet de copier la grille courante
 * - int bouger_tuile(ptour tour,ptuile t,int *x,int *y, int *o, int regle)  		bouger la tuile sur le plateau de jeu avec des tests dynamiques
 */


/**
 * @brief nettoyer l'écran
 */
#define clrscr() printf("\033[H\033[2J") 

/**
 * @brief couleur noire du texte
 */
#define NOIR   "\x1B[48;2;0;0;0m"

/**
 * @brief couleur brune du texte
 */
#define USINE   "\x1B[48;2;132;46;27m"

/**
 * @brief couleur verte du texte
 */
#define FORET   "\x1B[48;2;50;205;50m"

/**
 * @brief couleur bleue du texte
 */
#define LAC   "\x1B[48;2;0;0;255m"

/**
 * @brief couleur violette du texte
 */
#define VILLAGE   "\x1B[48;2;128;0;128m"

/**
 * @brief couleur orange du texte
 */
#define RESSOURCE   "\x1B[48;2;223;109;20m"

/**
 * @brief couleur jaune du texte
 */
#define PLAINE   "\x1B[48;2;255;222;117m"

/**
 * @brief inverser les couleurs des écritures (utilisé pour les selections)
 */
#define INV "\x1B[0m\x1B[7m"

/**
 * @brief couleur rouge du texte 
 */
#define DANGER  "\x1B[31;1m"

/**
 * @brief couleur rouge du texte et du fond de texte
 */
#define ROUGE "\x1B[1m\x1B[31m"

/**
 * @brief couleur bleue du texte et du fond de texte
 */
#define BLEU "\x1B[3m\x1B[48;2;135;200;239m" 

/**
 * @brief revenir à la couleur par defaut
 */
#define RESET "\x1B[0m"

/**
 * @brief cacher le curseur
 */
#define HIDE "\e["


/**
 * @brief 		permet de copier la grille courante
 * @param 	g 	la grille à copier
 * @return 		la copie de la grille courante
 */
grille_p grille_evolution(grille_p g);



/**
 * @brief 			bouger la tuile sur le plateau de jeu avec des tests dynamiques
 * @param  tour 	le tour de jeu courant
 * @param  t    	la tuile à poser
 * @param  x    	la coordonnée x de la tuile
 * @param  y    	la coordonnée y de la tuile
 * @param  o    	la rotation de la tuile
 * @param  regle	la regle du jeu choisie
 * @return     		un entier pour savoir si on a ou non quitter de force le jeu
 */	
int bouger_tuile(ptour tour,ptuile t,int *x,int *y, int *o, int regle);




#endif
