#ifndef _AFFICHAGE_
#define _AFFICHAGE_

#include "tuile.h"
#include "grille.h"
#include "tour.h"
#include "optimisation.h"

/**
 * @file affichage.h
 * 
 * Ce fichier décrit l'ensemble des fonctions d'affichage des différents éléments du jeu sur la console. 
 * Il contient 7 fonctions.
 * 
 * - affichage_tour(ptour t, int regle) 											affiche la grille et l'index d'un tour
 * - affichage_tuile(ptuile t, int o) 												affiche une tuile selon l'orientation souhaitée
 * - affichage_grille(grille_p g, int regle) 										affiche la grille 
 * - affichage_solution(resultat * save);											affiche la solution
 * - affichage_grille_no_limit(grille_p g,int id_courant,int info_sup,int regle)	affiche la grille même si la tuile est mal posée 
 * - affichage_index_tour(ptour tour) 												affiche l'index
 * - affichage_couleur(char terrain) 												affiche le terrain en couleur
 * 
 */

/**
 * @brief 			affiche les grilles et l'index d'un tour
 * @param 	t 		un tour
 * @param   regle	la regle du jeu choisie
 * @return 			rien
 */
void affichage_tour(ptour t, int regle);

/**
 * @brief 		affiche une tuile selon l'orientation souhaitée : 1 et 3 sont des orientation couchées et 0 et 2 sont des orientations debout
 * @param 	t 	une tuile
 * @param 	o 	un entier pour l'orientation choisie
 * @return 		rien
 */
void affichage_tuile(ptuile t, int o);

/**
 * @brief 				affiche la grille
 * @param 	g       	une grille
 * @param 	info_sup 	un entier qui permet d'affcher des infos en plus
 * @param 	help    	un entier qui permet d'affcher la manière de marquer des points
 * @param   regle		la regle du jeu choisie
 * @return 				rien
 */
void affichage_grille(grille_p g, int info_sup, int help, int regle);


/**
 * @brief 				affiche la solution
 * @param 	save		la solution
 * @return 				rien
 */
void affichage_solution(resultat * save);


/**
 * @brief 			  	affiche la grille même si la tuile est mal posée 
 * @param 	g 		  	une grille
 * @param 	id_courant  id de la tuile que l'on veut poser sur la tuile
 * @param 	info_sup    un entier qui permet d'affcher des infos en plus 
 * @param 	help 		un entier qui permet d'affcher la manière de marquer des points
 * @param   regle		la regle du jeu choisie
 * @return 			  	rien
 */
void affichage_grille_no_limit(grille_p g,int id_courant,int info_sup,int help, int regle);



/**
 * @brief 			affiche l'index 
 * @param 	tour 	le tour contenant l'index 
 * @return 			rien
 */
void affichage_index_tour(ptour tour);



/**
 * @brief 			 affiche le terrain en couleur
 * @param	terrain  le terrain à afficher (char)
 * @param 	danger 	 affiche en rouge les terrains invalides
 * @return 			 rien
 */
void affichage_couleur(char terrain, int danger);

#endif
