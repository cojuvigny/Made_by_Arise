#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <math.h>

#include "tuile.h"
#include "couleur.h"
#define N 6

char type[N] = {'P','F','V','R','U','L'};


ptuile init_tuile_random(int id) {
    /*Attention à ne pas utiliser cette fonction à moins de une seconde d'intervalle car sinon on obtient plusieurs
    fois le même résultat à cause de la valeur du time() qui ne change qu'à chaque seconde*/
	srand(time(NULL)); 
	ptuile t = (ptuile)malloc(sizeof(tuile));

	if (t == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}

	int i;

	for (i = 0; i < N; i++) {
		t->tab[i] = type[rand()%(N)];        
	}
	t->id = id;
	return t; 
}

int choix_aleatoire_biaise(float prctP,float prctF,float prctV,float prctR,float prctU,float prctL){

	float choix = (float) (rand()%(1000000)) / 10000;
	float ecart = pow(10,-5);
	if ((choix-prctP) <= ecart)
	{
		return 0;
	}

	else if ((choix-prctP-prctF)<= ecart)
	{
		return 1;
	}

	else if ((choix-prctP-prctF-prctV)<= ecart)
	{
		return 2;
	}

	else if ((choix-prctP-prctF-prctV-prctR)<= ecart)
	{
		return 3;
	}

	else if ((choix-prctP-prctF-prctV-prctR-prctU)<= ecart)
	{
		return 4;
	}

	else if ((choix-prctP-prctF-prctV-prctR-prctU-prctL)<= ecart)
	{
		return 5;
	}

	else
	{
		return 5;
	}

}


ptuile* init_tuile_random_tab(int n) {
	int i, j;
	srand(time(NULL));
	ptuile* tab = (ptuile*)malloc(n * sizeof(ptuile));

	if (tab == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}


	for (i = 0; i < n; i++){
		tab[i] = (ptuile)malloc(sizeof(tuile));
		
		if (tab[i] == NULL){
			fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
			exit(EXIT_FAILURE);
		}
	} 



	for (i = 0; i < n; i++){
		for (j = 0; j < N; j++) {
			tab[i]->tab[j] = type[rand()%N]; 
		}
		tab[i]->id = i;
	} 
	
	return tab;
}

ptuile* init_tuile_random_tab_biaise(int n,float prctP,float prctF,float prctV,float prctR,float prctU,float prctL) {
	int i, j;
	srand(time(NULL));
	ptuile* tab = (ptuile*)calloc(n,sizeof(ptuile));
	if (tab == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}


	for (i = 0; i < n; i++){
		tab[i] = (ptuile)calloc(1,sizeof(tuile));

		if (tab[i] == NULL){
			fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
			exit(EXIT_FAILURE);
		}
	} 



	for (i = 0; i < n; i++){
		for (j = 0; j < N; j++) {
			tab[i]->tab[j] = type[choix_aleatoire_biaise(prctP,prctF,prctV,prctR,prctU,prctL)]; 
		}
		tab[i]->id = i;
	} 
	
	return tab;
}



ptuile init_tuile_tab(char* tab,int id) {

	ptuile t = (ptuile)malloc(sizeof(tuile));

	if (t == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}

	int i;
	for (i = 0; i < N; i++){
		t->tab[i] = tab[i]; 
	} 
	t->id = id;
	return t;
}


void free_tuile(ptuile t) {
	free(t);
}


char* lire_tuile(ptuile t) {
	char* tc = (char*)calloc(N+1,sizeof(char));

	if (tc == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}

	int i;

	for (i = 0; i < N; i++) tc[i] = t->tab[i];
	return tc;
}


ptuile copie_tuile(ptuile t){
	int i;
	ptuile tuile_copie = (ptuile)malloc(sizeof(tuile));

	if (tuile_copie == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}
	
	for (i = 0; i < N; ++i){
		tuile_copie->tab[i]=t->tab[i];
	}

	tuile_copie->id = t->id;

	return tuile_copie;
}


void lever_coucher_tuile(ptuile t,ptuile *tuile_copie, int orientation) {
	int tab_lever[3][6] = {{2,4,-1,1,-4,-2},{5,3,1,-1,-3,-5},{3,-1,2,-2,1,-3}};
	int i;
	for (i = 0; i < N; ++i){
		(*tuile_copie)->tab[i+tab_lever[orientation-1][i]] = t->tab[i]; 
	}

	return;

}


void rotation_tuile(ptuile t,ptuile *tuile_copie, int orientation) {

	if (orientation == 0) return;

	lever_coucher_tuile(t,tuile_copie,orientation);
	
	return; 
}
