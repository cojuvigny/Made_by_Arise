#ifndef _TEST_CUNIT_LOT_B_
#define _TEST_CUNIT_LOT_B_

#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

/**
 * @file test_cunit_lot_B.h
 * 
 * Ce fichier décrit un ensemble de fonctions concernant les test unitaires du lot B.
 * Il contient 7 fonctions.
 * 
 * - setup(void) 							fonction de mise à jour du catalogue
 * - teardown(void)							fonction de mise à jour du catalogue
 * - initialisation_catalogue_C(void) 		fonction d'initialisation du catalogue de tests du lot B
 * - test_func(void) 						teste la fonction func sur son résultat attendu
 */



/**
 * @brief	fonction de mise à jour du catalogue
 * @return  un entier
 */
int setup(void) ;

/**
 * @brief	fonction de mise à jour du catalogue
 * @return  un entier
 */
int teardown(void);


/**
 * @brief	fonction d'initialisation du catalogue de tests du lot B
 * @return  un entier
 */
int initialisation_catalogue_B(void);


/**
 * @brief	Fonction de test de test_limitation
 * @return  rien
 */
void test_test_limitation(void);

/**
 * @brief	Fonction de test de test_recouvrement
 * @return  rien
 */
void test_test_recouvrement(void);

/**
 * @brief	Fonction de test de enfiler
 * @return  rien
 */
void test_enfiler(void);

/**
 * @brief	Fonction de test de defiler
 * @return  rien
 */
void test_defiler(void);

#endif
