#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

#include "tuile.h"
#include "rwfile.h"
#include "affichage.h"
#include "grille.h"
#include "test.h"
#include "parcours.h"
#include "test_cunit_lot_A.h"

int main() {
	printf("\n---------------------------\n");
	printf("\n--------Tests CUnit--------\n");
	printf("\n---------------------------\n");

	initialisation_catalogue_A();

	return EXIT_SUCCESS;
}
