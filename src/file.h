#include <unistd.h>

#ifndef _FILE_
#define _FILE_



/**
 * @file file.h
 * 
 * Ce fichier décrit un ensemble de fonctions concernant la file utilisée pour le test de village.
 * Il contient 7 fonctions et 1 type.
 * 
 * - le type \a file définit un maillon de liste chainée générique
 * 
 * - enfiler_tri(const void * data,file_p *tete,file_p *queue,size_t size) 					enfiler un maillon n'importe où en renvoyant ses données
 * - enfiler(int x,int y,file_p *tete,file_p *queue) 										enfile un maillon en queue de liste
 * - defiler(file_p *tete,file_p *queue, int *sortie) 										défiler un maillon en tete de liste en renvoyant ses données
 * - defiler_tri(file_p *tete,file_p *queue, void * sortie,size_t size,int id_a_defiler) 	défiler un maillon de liste n'importe où en renvoyant ses données
 * - print_file_tuile(file_p tete,int choix) 												afficher la file de tuile
 * - plus_petit(int a, int b) 																compare deux entiers
 * - nettoyer(file_p *tete, file_p *queue) 													nettoye la file
 * - nettoyer_tuile(file_p *tete, file_p *queue)											nettoye la file de tuile
 * - allocation_dynamique_matrice(int ** mat,int size) 										alloue de la mémoire pour une matrice
 * - liberer(int ** mat,int size) 															nettoye la matrice
 */



/**
 * \struct 	file 
 * \brief 	structure de maillon d'une file générique
 *
 * Chaque maillon contient des données data ainsi qu'un pointeur sur un autre maillon
 *
 */
typedef struct file
{
	void * data;
	struct file * suite;

}file,*file_p;


/**
 * @brief 		enfiler un maillon en queue de liste
 * @param 	x 	la coordonnée x de la ville
 * @param 	y 	la coordonnée y de la ville
 * @param tete 	un pointeur sur la tete de la file
 * @param queue un pointeur sur la queue de la file
 * @return 		rien
 */
void enfiler(const void * data, file_p *tete,file_p *queue, size_t size);


/**
 * @brief 			enfiler un maillon n'importe où en renvoyant ses données
 * @param 	data	une donnée
 * @param 	tete 	un pointeur sur la tete de la file
 * @param 	queue 	un pointeur sur la queue de la file
 * @param 	size	la taille de la data défilée
 * @return 			rien
 */
void enfiler_tri(const void * data, file_p *tete, file_p *queue, size_t size);

/**
 * @brief 			défiler un maillon en tete de liste en renvoyant ses données
 * @param 	tete 	un pointeur sur la tete de la file
 * @param 	queue 	un pointeur sur la queue de la file
 * @param 	size 	la taille de la data défilée
 * @return 			rien
 */
void defiler(file_p *tete,file_p *queue, void * sortie,size_t size);


/**
 * @brief 			défiler un maillon de liste n'importe où en renvoyant ses données
 * @param 	tete 	un pointeur sur la tete de la file
 * @param 	queue 	un pointeur sur la queue de la file
 * @param 	sortie 	un pointeur pour recupérer la data défilée
 * @param 	taille 	la taille de la data défilée
 * @param 	id 		id de la data à défiler
 * @return 			rien
 */
int defiler_tri(file_p *tete,file_p *queue, void * sortie,size_t size,int id_a_defiler);

/**
 * @brief           supprime un maillon de liste en fonction de son contenue
 * @param   tete    un pointeur sur la tete de la file
 * @param   elem    l'élément à supprimer
 * @return          rien
 */
int supprimer_elem(file_p *tete, void* elem);

/**
 * @brief 			afficher la file de tuile
 * @param  tete     un pointeur sur la tête de la file
 * @param  choix    la tuile selectionnée avec le curseur
 * @param  nb_tuile le nombre de tuiles de la file
 * @return          l'id de la tuile choisie
 */
int print_file_tuile(file_p tete, int choix, int nb_tuile);

/**
 * @brief 			trouver le cardinal de la file
 * @param 	tete 	la tete de la file
 * @return 			le cardinal
 */
int cardinal (file_p tete);

/**
 * @brief 		compare deux entiers
 * @param 	a 	entier
 * @param 	b 	entier
 * @return 		renvoie a-b si a est le plus grand et a sinon
 */
int plus_petit(int a, int b);

/**
 * @brief 			nettoye la file
 * @param 	tete 	un pointeur sur la tete de la file
 * @param 	queue 	un pointeur sur la queue de la file
 * @return 			rien
 */
void nettoyer(file_p *tete, file_p *queue);

/**
 * @brief 			nettoye la file de tuile
 * @param 	tete 	un pointeur sur la tete de la file
 * @param 	queue 	un pointeur sur la queue de la file
 * @return 			rien
 */
void nettoyer_tuile(file_p *tete, file_p *queue);

/**
 * @brief 			alloue de la mémoire pour une matrice
 * @param 	mat 	une matrice
 * @param 	size 	la taille de la matrice
 * @return 			la matrice
 */
int ** allocation_dynamique_matrice(int ** mat,int size);

/**
 * @brief 			nettoye la matrice
 * @param  mat 		une matrice
 * @param  size		la taille de la matrice
 * @return 			NULL si tout c'est bien passé
 */
int ** liberer(int ** mat,int size);

#endif
