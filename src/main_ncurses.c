#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

#include "tuile.h"
#include "rwfile.h"
#include "affichage.h"
#include "grille.h"
#include "test.h"
#include "parcours.h"
#include "comptage.h"
#include "loop.h"
#include "loop_nc.h"
#include "test_cunit_lot_B.h"
#include "couleur.h"



int main(){

	int sz_main; //nombre de tuile
    int sz_grille; //taille de la grille
    file_p main_jeu_tete = NULL,main_jeu_queue = NULL; //file de tuiles

    parcours_p parc = NULL; //Premier maillon de l partie

    clrscr();
    
    nc_loop_init(&parc, &main_jeu_tete,&main_jeu_queue, &sz_main, &sz_grille);
    nc_loop(parc, &main_jeu_tete,&main_jeu_queue, sz_main, sz_grille);

    return EXIT_SUCCESS;
}
