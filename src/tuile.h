#ifndef _TUILE_
#define _TUILE_

#define N 6

/**
 * @file tuile.h
 * 
 * Ce fichier décrit l'ensemble des fonctions permettant d'utiliser les tuiles du jeu.
 * Il contient 8 fonctions et 1 type.
 * 
 * - le type \a tuile définit une tuile du plan
 * 
 * - init_tuile_random()                                                                                         crée une tuile aléatoire
 * - choix_aleatoire_biaise(int prctP,int prctF,int prctV,int prctR,int prctU,int prctL)                         donne un terrain aléatoirement de manière biaisée
 * - init_tuile_random_tab(int n)                                                                                crée un tableau de n tuiles aléatoires
 * - init_tuile_random_tab_biaise(int n,float prctP,float prctF,float prctV,float prctR,float prctU,float prctL) crée un tableau de n tuiles aléatoires de manière biaisée
 * - init_tuile_tab(char* tab)                                                                                   crée une tuile à partir d'un tableau de char
 * - free_tuile(ptuile t)                                                                                        libère l'espace mémoire alloué à la tuile \a t
 * - lire_tuile(ptuile t)                                                                                        lit la tuile \a t par ligne
 * - copie_tuile(ptuile t)                                                                                 		 renvoie une copie de tuile
 * - lever_coucher_tuile(ptuile t,ptuile *tuile_copie, int orientation)                                          fait pivoter la tuile de 90°,180° ou 270° (obsolète)
 * - rotation_tuile(ptuile t,ptuile *tuile_copie, int orientation)                                             	 oriente une tuile dans le sens voulu par l'utilisateur (obsolète)
 */



/**
 * @struct 	tuile
 * @brief 	structure définissant une tuile à poser sur le plateau de jeu
 *
 * Une tuile est caractérisée par son id et par 6 terrains contenus dans un tableau de char
 */
typedef struct tuile{
	char tab[N];
	int id;
}tuile, *ptuile;

/**
 * @brief 		crée une tuile aléatoire
 * @param 	id 	l'identifiant de la tuile
 * @return 		Une tuile random
 */
ptuile init_tuile_random(int id);


/**
 * @brief 			donne un terrain aléatoirement de manière biaisée
 * @param 	prctP 	le pourcentage de chance d'avoir une plaine
 * @param 	prctF 	le pourcentage de chance d'avoir une foret
 * @param 	prctV 	le pourcentage de chance d'avoir un village
 * @param 	prctR 	le pourcentage de chance d'avoir une ressource
 * @param 	prctU 	le pourcentage de chance d'avoir une usine
 * @param 	prctL 	le pourcentage de chance d'avoir un lac
 * @return 			le terrain choisi (son indice)
 */
int choix_aleatoire_biaise(float prctP,float prctF,float prctV,float prctR,float prctU,float prctL);

/**
 * @brief 		crée un tableau de n tuiles aléatoires
 * @param 	n 	un entier positif le nombre de tuiles
 * @return 		un tableau de tuiles aléatoires
 */
ptuile* init_tuile_random_tab(int n);


/**
 * @brief 			crée un tableau de n tuiles aléatoires de manière biaisée
 * @param 	n 		un entier positif le nombre de tuiles
 * @param 	prctP 	le pourcentage de chance d'avoir une plaine
 * @param 	prctF 	le pourcentage de chance d'avoir une foret
 * @param 	prctV 	le pourcentage de chance d'avoir un village
 * @param 	prctR 	le pourcentage de chance d'avoir une ressource
 * @param 	prctU 	le pourcentage de chance d'avoir une usine
 * @param 	prctL 	le pourcentage de chance d'avoir un lac
 * @return 			un tableau de tuiles aléatoires
 */
ptuile* init_tuile_random_tab_biaise(int n,float prctP,float prctF,float prctV,float prctR,float prctU,float prctL);

/**
 * @brief 				crée une tuile à partir d'un tableau de char
 * @param 		tab 	tableau de 6 char
 * @param 		id 		pour identifier la tuile
 * @attention 			ne pas oublier de libérer la mémoire !
 * @return 				la tuile correspondant au tableau en paramètre
 */
ptuile init_tuile_tab(char* tab, int id);

/**
 * @brief 		libère l'espace mémoire alloué à la tuile \a t
 * @param 	t 	une tuile
 * @return 		rien
 */
void free_tuile(ptuile t);

/**
 * @brief 		lit la tuile \a t par ligne, la tuile se lit ligne par ligne
 * @param 	t 	une tuile
 * @return 		un tableau contenant les éléments de la tuile
 */
char* lire_tuile(ptuile t);


/**
 * @brief 		renvoie une copie de tuile
 * @param 	t 	une tuile
 * @return 		la tuile tuile_copie copie de la tuile t)
 */
ptuile copie_tuile(ptuile t);

/**
 * @brief 					fait pivoter la tuile de 90°,180° ou 270°
 * @param 	t 				une tuile, et son orientation
 * @param 	tuile_copie 	un pointeur sur une copie de t
 * @param 	orientation 	son orientation
 * @return 					rien
 */
void lever_coucher_tuile(ptuile t,ptuile *tuile_copie, int orientation);

/**
 * @brief 						oriente une tuile dans le sens voulu par l'utilisateur
 * @attention 					La tuile de base est levée. Le sens de lecture de la tuile est de haut en bas et de gauche à droite
 * @param 		t 				une tuile, et son orientation
 * @param 		tuile_copie 	un pointeur sur une copie de t
 * @param 		orientation 	son orientation
 * @return 						rien
 */
void rotation_tuile(ptuile t,ptuile *tuile_copie, int orientation);

#endif
