#ifndef _TEST_
#define _TEST_

#include "tuile.h"
#include "tour.h"

/**
 * @file test.h
 * 
 * Ce fichier décrit un ensemble de fonctions concernant les tests de recouvrement et de limitation de zone de jeu. 
 * Il contient 4 fonctions et 1 types.
 * 
 * - le type \a Bool définit un enum de booléens
 * 
 * - test_limitation(int x, int y, int size,int o) 						effectue les différents tests de limitation selon les règles
 * - test_recouvrement(ptour s, int x, int y, int o) 					effectue les différents tests de recouvrement selon les règles
 * - recherche_id( ptour s,int id_recherche) recherche 					la ligne de l'index associée à la tuile d'id id_recherche
 * - test_village(ptour s,int x, int y, int size, int o, int regle) 	renvoie la taille du village associe à des villes
 */

/**
 * \enum 	Bool
 * \brief 	enum de booléens
 *
 * false est un int qui vaut 0 et true est un int qui vaut 1.
 */
typedef enum Bool{False,True} Bool;


/**
 * @brief 				effectue les différents tests de limitation selon les règles
 * @attention 			premier test à effectuer dans la fonction de test | 0<=o<=3
 * @param  		x    	la coordonnée x de la tuile
 * @param 		y    	la coordonnée y de la tuile
 * @param  		size 	la taille de la carte
 * @param  		o    	l'orientation de la tuile
 * @return 				retourne false si la tuile dépasse la zone de jeu 
 */

Bool test_limitation(int x, int y, int size,int o);

/**
 * @brief 		effectue les différents tests de recouvrement selon les règles
 * @param  	s 	le tour actuel
 * @param  	x 	la coordonnée x ou l'on veut poser la tuile
 * @param 	y 	la coordonnée y ou l'on veut poser la tuile
 * @param  	o 	l'orientation choisie pour la tuile
 * @return  	un enum Bool
 */
Bool test_recouvrement(ptour s, int x, int y, int o);

/**
 * @brief					recherche la ligne de l'index associée à la tuile d'id id_recherche
 * @param  	s            	le tour actuel
 * @param  	id_recherche 	l'id de la tuile recherché
 * @return              	un entier i
 */
int recherche_id(ptour s, int id_recherche);

/**
 * @brief			renvoie la taille du village associe à des villes
 * @param  	g    	la grille de jeu en cours
 * @param  	x    	la coordonnée x de la tuile
 * @param 	y    	la coordonnée y de la tuile
 * @param  	size 	la taille de la carte
 * @param   regle	la regle du jeu choisie
 * @return      	la taille du village
 */
int test_village(grille_p g,int x, int y, int size, int regle);

#endif
