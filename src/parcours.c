#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "tuile.h"
#include "grille.h"
#include "tour.h"
#include "parcours.h"
#include "test.h"
#include "affichage.h"
#include "couleur.h"


parcours_p create_tour(ptuile t, int x, int y, int o, int size)
{
	parcours_p suivant = (parcours_p) malloc(sizeof(parcours));

	if (suivant == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}

	suivant->precedent=NULL;
	suivant->tour=create_tour_vide(size,1);
	insert_tuile(suivant->tour->g,x,y, t, o);
	ajout_placement(suivant->tour->index[0],t->id,6,o,x,y);
	return suivant;
}


int add_tour(parcours_p *p, ptuile t,int x, int y,int o, int size)
{
	if (test_recouvrement((*p)->tour,x,y,o)){
		parcours_p suivant = (parcours_p) malloc(sizeof(parcours));

		if (suivant == NULL){
			fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
			exit(EXIT_FAILURE);
		}

		suivant->precedent=*p;
		suivant->tour=create_tour_vide(size,(*p)->tour->sz_index+1);
		suivant->tour->sz_index=(*p)->tour->sz_index+1;
		copy_grille((*p)->tour->g,suivant->tour->g);
		insert_tuile(suivant->tour->g,x,y, t, o);
		modif_index((*p)->tour,suivant->tour,o,x,y,t->id);
		*p = suivant;

		return 1;
	}

	return 0;	
}


int remplace_tour(parcours_p *p, ptuile t,int x, int y,int o, int size)
{
	if (test_recouvrement((*p)->tour,x,y,o)){
		parcours_p suivant = (parcours_p) malloc(sizeof(parcours));

		if (suivant == NULL){
			fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
			exit(EXIT_FAILURE);
		}

		suivant->precedent=NULL;
		suivant->tour=create_tour_vide(size,(*p)->tour->sz_index+1);
		suivant->tour->sz_index=(*p)->tour->sz_index+1;
		copy_grille((*p)->tour->g,suivant->tour->g);
		insert_tuile(suivant->tour->g,x,y, t, o);
		modif_index((*p)->tour,suivant->tour,o,x,y,t->id);
		remove_all_tour(p);
		*p = suivant;

		return 1;
	}

	return 0;	
}


void copie_partie(parcours_p *origine,parcours_p *copie)
{
	
	*copie = (parcours_p) malloc(sizeof(parcours));

	if (copie == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}

	(*copie)->precedent=NULL;
	(*copie)->tour=create_tour_vide((*origine)->tour->g->size,(*origine)->tour->sz_index);
	(*copie)->tour->sz_index=(*origine)->tour->sz_index;
	copy_grille((*origine)->tour->g,(*copie)->tour->g);
	copie_index(origine,copie);
}


void copie_index(parcours_p *origine,parcours_p *copie)
{
	
	int i;
	for (i=0;i<(*origine)->tour->sz_index;i++)
	{
		int id2=(*origine)->tour->index[i]->id;
		int nbr2=(*origine)->tour->index[i]->nbrcases;
		int orientation2=(*origine)->tour->index[i]->orientation;
		int x2=(*origine)->tour->index[i]->x;
		int y2=(*origine)->tour->index[i]->y;
		ajout_placement((*copie)->tour->index[i],id2,nbr2,orientation2,x2,y2);
	}
}


void remove_tour(parcours_p *p)
{
	if ((*p)->precedent != NULL){
		parcours_p g = *p;
		(*p)=(*p)->precedent;
		g->precedent=NULL;
		free_tour((g)->tour);
		free(g);
	}

}

void remove_all_tour2(parcours_p *p)
{

	while ((*p)->precedent!=NULL)
	{
		parcours_p g=*p;
		(*p)=(*p)->precedent;
		g->precedent=NULL;
		free_tour(g->tour);
		free(g);
	}
}

void remove_all_tour(parcours_p *p)
{
	while (*p!=NULL)
	{
		parcours_p g=*p;
		(*p)=(*p)->precedent;
		g->precedent=NULL;
		free_tour(g->tour);
		//printf("On a fini de free g->tour\n");
		free(g);
	}

	free (*p);
}
