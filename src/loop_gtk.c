#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <math.h>
#include <gtk-3.0/gtk/gtk.h>
#include <string.h>
#include <pthread.h>

#include "file.h"
#include "tour.h"
#include "parcours.h"
#include "comptage.h"
#include "couleur.h"
#include "rwfile.h"
#include "affichage.h"
#include "solveur.h"
#include "aetoile.h"
#include "optimisation.h"
#include "loop_gtk.h"

pthread_mutex_t  mutex = PTHREAD_MUTEX_INITIALIZER;
#define Lock()   pthread_mutex_lock(&mutex);
#define Unlock() pthread_mutex_unlock(&mutex);    

#define DEPLACEMENT_POSITION(grid, image, r, i, j, k, l) switch (r) {\
                                        case 0 :\
                                            gtk_image_set_pixel_size(GTK_IMAGE(image), 5);\
                                            switch (3 * k + l) {\
                                                case 0 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i, j);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i, j, 1, 1);\
                                                    break;\
                                                case 1 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i + 1, j);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i + 1, j, 1, 1);\
                                                    break;\
                                                case 2 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i, j + 1);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i, j + 1, 1, 1);\
                                                    break;\
                                                case 3 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i + 1, j + 1);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i + 1, j + 1, 1, 1);\
                                                    break;\
                                                case 4 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i, j + 2);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i, j + 2, 1, 1);\
                                                    break;\
                                                case 5 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i + 1, j + 2);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i + 1, j + 2, 1, 1);\
                                                    break;\
                                                default :\
                                                    break;\
                                            }\
                                            break;\
                                        case 1 :\
                                            gtk_image_set_pixel_size(GTK_IMAGE(image), 5);\
                                            switch (3 * k + l) {\
                                                case 0 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i + 2, j);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i + 2, j, 1, 1);\
                                                    break;\
                                                case 1 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i + 2, j + 1);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i + 2, j + 1, 1, 1);\
                                                    break;\
                                                case 2 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i + 1, j);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i + 1, j, 1, 1);\
                                                    break;\
                                                case 3 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i + 1, j + 1);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i + 1, j + 1, 1, 1);\
                                                    break;\
                                                case 4 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i, j);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i, j, 1, 1);\
                                                    break;\
                                                case 5 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i, j + 1);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i, j + 1, 1, 1);\
                                                    break;\
                                                default :\
                                                    break;\
                                            }\
                                            break;\
                                        case 2 :\
                                            gtk_image_set_pixel_size(GTK_IMAGE(image), 5);\
                                            switch (3 * k + l) {\
                                                case 0 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i + 1, j + 2);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i + 1, j + 2, 1, 1);\
                                                    break;\
                                                case 1 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i, j + 2);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i, j + 2, 1, 1);\
                                                    break;\
                                                case 2 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i + 1, j + 1);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i + 1, j + 1, 1, 1);\
                                                    break;\
                                                case 3 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i, j + 1);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i, j + 1, 1, 1);\
                                                    break;\
                                                case 4 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i + 1, j);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i + 1, j, 1, 1);\
                                                    break;\
                                                case 5 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i, j);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i, j, 1, 1);\
                                                    break;\
                                                default :\
                                                    break;\
                                            }\
                                            break;\
                                        case 3 :\
                                            gtk_image_set_pixel_size(GTK_IMAGE(image), 5);\
                                            switch (3 * k + l) {\
                                                case 0 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i, j + 1);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i, j + 1, 1, 1);\
                                                    break;\
                                                case 1 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i, j);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i, j, 1, 1);\
                                                    break;\
                                                case 2 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i + 1, j + 1);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i + 1, j + 1, 1, 1);\
                                                    break;\
                                                case 3 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i + 1, j);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i + 1, j, 1, 1);\
                                                    break;\
                                                case 4 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i + 2, j + 1);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i + 2, j + 1, 1, 1);\
                                                    break;\
                                                case 5 :\
                                                    image_old = gtk_grid_get_child_at(GTK_GRID(grid), i + 2, j);\
                                                    gtk_widget_destroy(image_old);\
                                                    gtk_grid_attach(GTK_GRID(grid), image, i + 2, j, 1, 1);\
                                                    break;\
                                                default :\
                                                    break;\
                                            }\
                                            break;\
                                        default :\
                                            break;\
                                    }                     


void* chargement_attente_fin(void* wind) {
    GtkWidget *window = (GtkWidget*)wind;
    gtk_widget_show_all(window);
    Unlock();
    return NULL;
}

void init_main_jeu(ptuile* tab_tuile, file_p* main_jeu_tete,file_p* main_jeu_queue,int sz_main) {
	int i = 0;
	ptuile tuile_courante = NULL;
	while(i != sz_main) {
		tuile_courante = tab_tuile[i];
        char* t = lire_tuile(tuile_courante);
		enfiler_tri(&tuile_courante, main_jeu_tete, main_jeu_queue, sizeof(ptuile));
        free(t);
		i++;
	}
}

void initialisation_parametres_jeu(GtkWidget *window, gpointer data) {
	init_att _data = (init_att)data;
    GtkApplication *app = _data->fenetre->app;
    window = _data->fenetre->window;
	*(_data->sz_grille) = 5;
	*(_data->sz_main) = 2;

    /*Destruction de l'ancienne fenetre*/
    gtk_widget_destroy(window); 

    /*Creation de la nouvelle*/
    window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "Initialisation Honshu I");
    gtk_window_set_default_size(GTK_WINDOW(window), 200, 75);
    gtk_container_set_border_width(GTK_CONTAINER(window), 25);
    _data->fenetre->window = window;

    GtkWidget* box;
    GtkWidget* button_random;
    GtkWidget* button_from_file;

    /*Création du box*/
    box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
    gtk_box_set_homogeneous(GTK_BOX(box), TRUE);
    gtk_container_add(GTK_CONTAINER(window), box);

    /*Création bouton "aléatoire"*/
    button_random = gtk_button_new_with_label("Random");
	g_signal_connect_swapped(button_random, "clicked", G_CALLBACK(gtk_aleatoire_choix),(gpointer)_data);
    gtk_box_pack_start(GTK_BOX(box), button_random, 0, 0, 0);

    /*Création bouton "depuis fichier"*/
    button_from_file = gtk_button_new_with_label("From File");
	g_signal_connect_swapped(button_from_file, "clicked", G_CALLBACK(gtk_aleatoire_choix), (gpointer)_data);
    gtk_box_pack_start(GTK_BOX(box), button_from_file, 0, 0, 0);

    /*Affichage de toute la grille*/
	gtk_widget_show_all(window);
}

void nbr_cases_modif_select(GtkRange *range, gpointer _data) {

    /*On récupère la valeur du curseur à l'instant t*/
    *(((init_att)_data)->sz_grille) = (int)gtk_range_get_value(range);
}

void nbr_tuiles_modif_select(GtkRange *range, gpointer _data) {
    init_att data = (init_att)_data;

    /*Onrécupère la valeur du curseur à l'instant t*/
    *(data->sz_main) = (int)gtk_range_get_value(range);
}

void gtk_aleatoire_choix(gpointer _data) {
    init_att        data    = (init_att)_data;
    GtkApplication  *app    = data->fenetre->app;
    GtkWidget       *window = data->fenetre->window;
    *(data->sz_grille)      = 5;
    *(data->sz_main)        = 5;
    
    GtkWidget* box;
    GtkWidget* button;
    GtkWidget* scale;
    GtkWidget* label;

    /*Destruction de la fenêtre précedente*/
    gtk_widget_destroy(window);

    /*Creation de la nouvelle fenêtre*/
    window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "Initialisation partie");
    gtk_window_set_default_size(GTK_WINDOW(window), 200, 300);
    gtk_container_set_border_width(GTK_CONTAINER(window), 25);
    data->fenetre->window = window;

    /*Création de box*/
    box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    gtk_box_set_homogeneous(GTK_BOX(box), TRUE);
    gtk_container_add(GTK_CONTAINER(window), box);

    /*Création des labels*/
    label = gtk_label_new("Choisissez les caractéristiques de votre partie");
    gtk_container_add(GTK_CONTAINER(box), label);

    label = gtk_label_new("Sélection du nombre de cases :");
    gtk_container_add(GTK_CONTAINER(box), label);

    label = gtk_label_new("Sélection du nombre de tuiles :");
    gtk_container_add(GTK_CONTAINER(box), label);

    /*Création du sélecteur du nombre de cases*/
    scale = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 5., 30., 1.);
    gtk_container_add(GTK_CONTAINER(box), scale);
    gtk_box_reorder_child(GTK_BOX(box), scale, 2);

    /* Allow it to expand horizontally (if there's space), and
    * set the vertical alignment
    */
    gtk_widget_set_hexpand (scale, TRUE);
    gtk_widget_set_valign (scale, GTK_ALIGN_START);

    g_signal_connect(scale, "value-changed", G_CALLBACK(nbr_cases_modif_select), (gpointer)data);

    /*Création du sélecteur du nombre de tuiles*/
    scale = gtk_scale_new_with_range(GTK_ORIENTATION_HORIZONTAL, 5., 50., 1.);
    gtk_container_add(GTK_CONTAINER(box), scale);
    gtk_widget_set_hexpand(scale, TRUE);
    gtk_widget_set_valign(scale, GTK_ALIGN_START);

    g_signal_connect(scale, "value-changed", G_CALLBACK(nbr_tuiles_modif_select), (gpointer)data);

    /*Création du bouton*/
    button = gtk_button_new_with_label("Lancer");
    g_signal_connect_swapped(button, "clicked", G_CALLBACK(creation_jeu_random), (gpointer)data);
    gtk_container_add(GTK_CONTAINER(box), button);
   //TODO se renseigner sur les GtkAdjustements et sur la fonction : gtk_range_get_value(GtkRange *range);

    /* peut-être pas nécessaire
    GError *error = NULL;
    gchar *filename;

    builder = gtk_builder_new();
    filename = g_build_filename("../xml/choix_test.glade", NULL);
    gtk_builder_add_from_file(builder, "../xml/choix_test.glade", &error);
    g_free(filename);
    window = GTK_WIDGET(gtk_builder_get_object(builder, "ChoixMenu"));

    */
    gtk_widget_show_all(window);
}

void creation_jeu_random(gpointer user_data) {

    init_att        data            = (init_att)user_data;
    int*            sz_grille       = data->sz_grille;
    int*            sz_main         = data->sz_main;
    ptuile          *tabtuile       = (ptuile*)malloc(*sz_main * sizeof(ptuile)); 
    GtkApplication  *app            = data->fenetre->app;
    GtkWidget       *window         = data->fenetre->window;
    GtkWidget       *box;
    GtkWidget       *label;
    pthread_t       thread;
    pthread_attr_t  att;
    ptuile          tuile_initiale;

    pthread_attr_init(&att);

    /*Destruction de la fenêtre précedente*/
    gtk_widget_destroy(window);

    /*Création de la nouvelle fenêtre*/
    window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "Loading...");
    gtk_window_set_default_size(GTK_WINDOW(window), 200, 200);
    data->fenetre->window = window;

    /*Création de la box*/
    box = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
    gtk_container_add(GTK_CONTAINER(window), box);

    /*Création du label*/
    label = gtk_label_new("Loading...");
    gtk_box_set_homogeneous(GTK_BOX(box), TRUE);
    gtk_container_add(GTK_CONTAINER(box), label);


    Lock();
    pthread_create(&thread, &att, chargement_attente_fin, (void*)window);

    Lock();

    /*Création de éléments du jeu*/
    tabtuile = init_tuile_random_tab(*sz_main + 1);
    tuile_initiale = tabtuile[*sz_main];
    
    init_main_jeu(tabtuile, &(data->tete), &(data->queue), *sz_main);

    free(tabtuile);

    data->parc = create_tour(tuile_initiale, (int)floor(*sz_grille / 2), (int)floor(*sz_grille / 2), 1, *sz_grille);

    *(data->nbr_tour) = 0;

    free(tuile_initiale);

    gtk_widget_destroy(window);

    /*Création de la fenêtre principale du jeu*/

    /*Création de la fenêtre*/
    window = gtk_application_window_new(app);
    gtk_window_set_title(GTK_WINDOW(window), "Honshu");
    gtk_container_set_border_width(GTK_CONTAINER(window), 25);
    data->fenetre->window = window;

    loop((gpointer)data);
}

void gtk_affichage_credits(GtkWidget *button, appWindow args) {

	GtkApplication *app = args->app;
	GtkWidget *window = args->window;

    /*Destruction de l'ancienne fenetre*/
    gtk_widget_destroy(window);

    /*Recréation de celle-çi*/
	window = gtk_application_window_new(app);
	gtk_window_set_title (GTK_WINDOW(window), "Crédits");
	gtk_window_set_default_size(GTK_WINDOW(window), 200, 200);
	gtk_container_set_border_width (GTK_CONTAINER (window), 50);

	GtkWidget* box;
	GtkWidget* label;

    /*Création du box*/
    box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    gtk_box_set_homogeneous(GTK_BOX(box), TRUE);
    gtk_container_add(GTK_CONTAINER(window), box);
    
    /*Création du label*/
    
    const char* titre = "                                   Honshu";
    const char* createurs = "\n\n                          Made_by_Arise\n\n";
    const char* remerciement = "Directeur général      : Corentin Juvigny\nDirecteur technique : Adrien Blassiau\nProducteur executif  : Alexandre Brunoud\nMain designer              : Romain Derre\n";
    
    //char* credits = "<span foreground=\"#f60f28\" size=\"xx-large\"><b>Honshu</b></span>\n\n<span foreground=\"#1332e3\" size=\"large\"><b>Made_by_Arise</b></span>\n\n<span><b>Directeur général : Corentin Juvigny</b>\n\n<b>Directeur technique : Adrien Blassiau</b>\n\n<b>Producteur executif : Alexandre Brunoud</b>\n\n<b>Main designer : Romain Derre</b>\n\n</span>";
    
    char* credits = (char*)calloc((strlen(ROUGE) + strlen(titre) + strlen(remerciement) + strlen(createurs) + strlen(RESET)), sizeof(char*));

    //strcpy(credits, ROUGE);
	strcat(credits, titre);
    //strcat(credits, RESET);
    strcat(credits, createurs);
    strcat(credits, remerciement);
    char* credits_converti = g_locale_to_utf8(credits, -1, NULL, NULL, NULL);
    label = gtk_label_new(credits_converti);
    free(credits_converti);
    //gtk_label_set_use_markup(GTK_LABEL(label), TRUE);
    //gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_CENTER); 
    gtk_box_pack_start(GTK_BOX(box), label, 0, 0, 0);

    /*Création bouton*/
    button = gtk_button_new_with_label("Return Menu");
    g_signal_connect(button, "clicked", G_CALLBACK(end_credits), NULL); 
    g_signal_connect_swapped(button, "clicked", G_CALLBACK(gtk_widget_destroy), window);
    gtk_box_pack_start(GTK_BOX(box), button, 0, 0, 0);
    
    /*Affichage box*/
	gtk_widget_show_all(window);
}

void end_credits() {
    execl("./bin/jeu_gtk", "jeu_gtk", NULL);
}

void affichage_solution_gtk(gpointer user_data) {
    
    data_solveur_gtk    data            =   (data_solveur_gtk)user_data;
    GtkApplication      *app            =   data->app;  
    GtkWidget           *window;
    GtkWidget           *box;
    GtkWidget           *label;
    GtkWidget           *grid;
    GtkWidget           *image;
    GtkWidget           *separator;
    GtkAccelGroup       *accel_group;
    GClosure            *closure;
    resultat            result         =   data->result;
    int                 i, j;
    int                 sz_main         =   data->sz_main;
    int                 sz_grille       =   data->sz_grille;
    int                 nbr_tour        =   data->nbr_tour;
    file_p              main_jeu_tete   =   data->main_jeu_tete;
    parcours_p          parc            =   data->jeu;
    grille_p            grille;
    char                *aff_result;
    char                temp_result[100];
    char                *aff_result_converti;

    /*Création de la fenêtre*/
    window = gtk_application_window_new(app);
	gtk_window_set_title (GTK_WINDOW(window), "Résultat solveur");
	gtk_container_set_border_width (GTK_CONTAINER(window), 10);

    /*Création de la fenêtre d'attente*/
    //window_wait = gtk_application_window_new(app);
    //gtk_window_set_title(GTK_WINDOW(window_wait), "Loading");
    
    /*Création des raccourcis claviers*/
    accel_group = gtk_accel_group_new();
    closure = g_cclosure_new_swap(G_CALLBACK(gtk_widget_destroy), window, NULL);
    gtk_accel_group_connect(accel_group, GDK_KEY_X, GDK_CONTROL_MASK, GDK_CONTROL_MASK, closure);
    gtk_window_add_accel_group(GTK_WINDOW(window), accel_group);

    /*Création de la box*/
    box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    gtk_box_set_baseline_position(GTK_BOX(box), GTK_BASELINE_POSITION_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(box), 20);
    gtk_container_add(GTK_CONTAINER(window), box);

    /*Création du label*/
    //label = gtk_label_new("Loading...");
    //gtk_container_add(GTK_CONTAINER(window_wait), label);

    //gtk_widget_show_all(window_wait);
    
    /*Calcule du résultat*/
    if (data->isSet == FALSE) {
        g_print("Calculs en cours...\n");
        result = solveur(sz_main - nbr_tour, sz_main, sz_grille, main_jeu_tete, parc,0);
        data->result = result;
        data->isSet = TRUE;
        g_print("description : %s\npoints_partie : %d\n", result.description, result.points_partie);
    }
    /*Création du grid*/
    grid = gtk_grid_new();
    gtk_grid_set_row_spacing(GTK_GRID(grid), 1);
    gtk_grid_set_column_spacing(GTK_GRID(grid), 1);
    gtk_widget_set_hexpand(grid, TRUE);
    gtk_container_add(GTK_CONTAINER(box), grid);

    /*Création du séparateur horizontal entre le grid et le label*/
    separator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_container_add(GTK_CONTAINER(box), separator);
   
    /*Création du second label*/
    aff_result = (char*)calloc(strlen(result.description) + 100, sizeof(char));
    sprintf(temp_result, "\nPoint : %d", result.points_partie);
    strcat(aff_result, result.description);
    strcat(aff_result, temp_result);
    aff_result_converti = g_locale_to_utf8(aff_result, -1, NULL, NULL, NULL);
    label = gtk_label_new(aff_result_converti);
    gtk_container_add(GTK_CONTAINER(box), label);
    free(aff_result);
    free(aff_result_converti);

    /*Remplisage de la grille*/
    for (i = 0; i < sz_grille; i++) {
        for (j = 0; j < sz_grille; j++) {
            grille = result.partie_opti->tour->g;
            char tuile = lire_dalle(grille, i, j);
            switch (tuile) {
                case 'P' :
                    image = gtk_image_new_from_file("./res/plain25px.png");
                    break;
                case 'F' :
                    image = gtk_image_new_from_file("./res/forest25px.png");
                    break;
                case 'V' :
                    image = gtk_image_new_from_file("./res/city25px.png");
                    break;
                case 'R' :
                    image = gtk_image_new_from_file("./res/ressource25px.png");
                    break;
                case 'U' :
                    image = gtk_image_new_from_file("./res/factory25px.png");
                    break;
                case 'L' :
                    image = gtk_image_new_from_file("./res/lake25px.png");
                    break;
                default :
                    image = gtk_image_new_from_file("./res/void25px.png");
                    break;
            }
            gtk_image_set_pixel_size(GTK_IMAGE(image), 5);
            gtk_grid_attach(GTK_GRID(grid), image, j, i, 1, 1);
        }
    }

    gtk_widget_show_all(window);
}

void init_loop_anim(GtkWidget *event_box, gpointer unused, gpointer user_data) {
    
    data_loop_gtk   data    =   (data_loop_gtk)user_data;
    int             size    =   *(data->attr->sz_main) - *(data->attr->nbr_tour);
    int             i;

    if (unused != NULL) unused = NULL;

    /*Récupère la grille qui a été sélectionnée*/
    for (i = 0; i < size; i++) {
        if (event_box == data->tab_grid_tuiles[i]->event_box) {
            data->tuile = *(data->tab_grid_tuiles[i]->tuile);
            char* test_tuile = lire_tuile(*(data->tab_grid_tuiles[i]->tuile));
            free(test_tuile);
            break;
        }
    }

    /*On débranche les signaux des event_boxs (Inneficace mais pourquoi ?!!)*/
    for (i = 0; i < size; i++) g_signal_handler_disconnect(data->tab_grid_tuiles[i]->event_box, data->tab_grid_tuiles[i]->handlerID);
    free(data->tab_grid_tuiles);

    /*Initialisation de la position de la tuile à placer*/
    data->i = 0;
    data->j = 0;
    data->r = 0;

    loop_anim((gpointer)data);
}

void deplacement_gauche(gpointer user_data) {

    data_loop_gtk data = (data_loop_gtk)user_data;
    GtkWidget       *window         =   data->attr->fenetre->window;
    GtkWidget       *grid           =   data->grid;
    GtkWidget       *image;
    GtkWidget       *image_old;    
    int             sz_grille       =   *(data->attr->sz_grille);
    int             i               =   data->i;
    int             j               =   data->j;
    int             r               =   data->r;
    int             k, l;
    parcours_p      parc            =   data->attr->parc;
    grille_p        grille;

    /*Enregistrement du déplacement*/
    i--;
    data->i--;

    /*Test de sortie du terrain de jeu*/
    if (i < 0) {
        data->i++;
        goto coda;
    }

    /*Destruction des éléments de la grille*/
    for (k = 0; k < sz_grille; k++) {
        for (l = 0; l < sz_grille; l++) {
            image_old = gtk_grid_get_child_at(GTK_GRID(grid), l, k);
            gtk_widget_destroy(image_old);
        }
    }

    /*Reconstruction de la grille*/
    for (k = 0; k < sz_grille; k++) {
        for (l = 0; l < sz_grille; l++) {
            grille = parc->tour->g;
            char tuile = lire_dalle(grille, k, l);
            switch (tuile) {
                case 'P' :
                    image = gtk_image_new_from_file("./res/plain25px.png");
                    break;
                case 'F' :
                    image = gtk_image_new_from_file("./res/forest25px.png");
                    break;
                case 'V' :
                    image = gtk_image_new_from_file("./res/city25px.png");
                    break;
                case 'R' :
                    image = gtk_image_new_from_file("./res/ressource25px.png");
                    break;
                case 'U' :
                    image = gtk_image_new_from_file("./res/factory25px.png");
                    break;
                case 'L' :
                    image = gtk_image_new_from_file("./res/lake25px.png");
                    break;
                default :
                    image = gtk_image_new_from_file("./res/void25px.png");
                    break;
            }
            gtk_image_set_pixel_size(GTK_IMAGE(image), 5);
            gtk_grid_attach(GTK_GRID(grid), image, l, k, 1, 1);
        }
    }

    /*Apparition de la tuile à l'endroit déplacé*/
    Bool isPlacable = test_recouvrement(data->attr->parc->tour, j, i, r);
    if (isPlacable) {
        for (k = 0; k < 2; k++) {
            for (l = 0; l < 3; l++) {
                char* tuile = lire_tuile(data->tuile);
                char carac = tuile[3 * k + l];
                switch (carac) {
                    case 'P' :
                        image = gtk_image_new_from_file("./res/plain25px.png");
                        break;
                    case 'F' :
                        image = gtk_image_new_from_file("./res/forest25px.png");
                        break;
                    case 'V' :
                        image = gtk_image_new_from_file("./res/city25px.png");
                        break;
                    case 'R' :
                        image = gtk_image_new_from_file("./res/ressource25px.png");
                        break;
                    case 'U' :
                        image = gtk_image_new_from_file("./res/factory25px.png");
                            break;
                    case 'L' :
                        image = gtk_image_new_from_file("./res/lake25px.png");
                        break;
                    default :
                        image = gtk_image_new_from_file("./res/void25px.png");
                        break;
                } 
                DEPLACEMENT_POSITION(grid, image, r, i, j, k, l);
                free(tuile);
            }
        }
    }
    else {
        for (k = 0; k < 2; k++) {
            for (l = 0; l < 3; l++) {
                char* tuile = lire_tuile(data->tuile);
                char carac = tuile[3 * k + l];
                switch (carac) {
                    case 'P' :
                        image = gtk_image_new_from_file("./res/plain25pxred.png");
                        break;
                    case 'F' :
                        image = gtk_image_new_from_file("./res/forest25pxred.png");
                        break;
                    case 'V' :
                        image = gtk_image_new_from_file("./res/city25pxred.png");
                        break;
                    case 'R' :
                        image = gtk_image_new_from_file("./res/ressource25pxred.png");
                        break;
                    case 'U' :
                        image = gtk_image_new_from_file("./res/factory25pxred.png");
                            break;
                    case 'L' :
                        image = gtk_image_new_from_file("./res/lake25pxred.png");
                        break;
                    default :
                        image = gtk_image_new_from_file("./res/void25pxred.png");
                        break;
                } 
                DEPLACEMENT_POSITION(grid, image, r, i, j, k, l);
                free(tuile);
            }
        }
    }

coda :
    gtk_widget_show_all(window);
} 

void deplacement_droit(gpointer user_data) {

    data_loop_gtk data = (data_loop_gtk)user_data;
    GtkWidget       *window         =   data->attr->fenetre->window;
    GtkWidget       *grid           =   data->grid;
    GtkWidget       *image;
    GtkWidget       *image_old;    
    int             sz_grille       =   *(data->attr->sz_grille);
    int             i               =   data->i;
    int             j               =   data->j;
    int             r               =   data->r;
    int             k, l;
    parcours_p      parc            =   data->attr->parc;
    grille_p        grille;

    /*Test de sortie du terrain de jeu*/
    switch (r) {
        case 0 : case 2 :
            if (i + 3 > sz_grille) goto coda;
            break;
        case 1 : case 3 :
            if (i + 4 > sz_grille) goto coda;
            break;
        default :
            goto coda;
            break;
    }

    /*Enregistrement du déplacement*/
    i++;
    data->i++;

    /*Destruction des éléments de la grille*/

    for (k = 0; k < sz_grille; k++) {
        for (l = 0; l < sz_grille; l++) {
            image_old = gtk_grid_get_child_at(GTK_GRID(grid), l, k);
            gtk_widget_destroy(image_old);
        }
    }

    /*Reconstruction de la grille*/
    for (k = 0; k < sz_grille; k++) {
        for (l = 0; l < sz_grille; l++) {
            grille = parc->tour->g;
            char tuile = lire_dalle(grille, k, l);
            switch (tuile) {
                case 'P' :
                    image = gtk_image_new_from_file("./res/plain25px.png");
                    break;
                case 'F' :
                    image = gtk_image_new_from_file("./res/forest25px.png");
                    break;
                case 'V' :
                    image = gtk_image_new_from_file("./res/city25px.png");
                    break;
                case 'R' :
                    image = gtk_image_new_from_file("./res/ressource25px.png");
                    break;
                case 'U' :
                    image = gtk_image_new_from_file("./res/factory25px.png");
                    break;
                case 'L' :
                    image = gtk_image_new_from_file("./res/lake25px.png");
                    break;
                default :
                    image = gtk_image_new_from_file("./res/void25px.png");
                    break;
            }
            gtk_image_set_pixel_size(GTK_IMAGE(image), 5);
            gtk_grid_attach(GTK_GRID(grid), image, l, k, 1, 1);
        }
    }
    /*Apparition de la tuile à l'endroit déplacé*/
    Bool isPlacable = test_recouvrement(data->attr->parc->tour, j, i, r);
    if (isPlacable) {
        for (k = 0; k < 2; k++) {
            for (l = 0; l < 3; l++) {
                char* tuile = lire_tuile(data->tuile);
                char carac = tuile[3 * k + l];
                switch (carac) {
                    case 'P' :
                        image = gtk_image_new_from_file("./res/plain25px.png");
                        break;
                    case 'F' :
                        image = gtk_image_new_from_file("./res/forest25px.png");
                        break;
                    case 'V' :
                        image = gtk_image_new_from_file("./res/city25px.png");
                        break;
                    case 'R' :
                        image = gtk_image_new_from_file("./res/ressource25px.png");
                        break;
                    case 'U' :
                        image = gtk_image_new_from_file("./res/factory25px.png");
                            break;
                    case 'L' :
                        image = gtk_image_new_from_file("./res/lake25px.png");
                        break;
                    default :
                        image = gtk_image_new_from_file("./res/void25px.png");
                        break;
                }
                DEPLACEMENT_POSITION(grid, image, r, i, j, k, l);
                free(tuile);
            }
        }
    }
    else {
        for (k = 0; k < 2; k++) {
            for (l = 0; l < 3; l++) {
                char* tuile = lire_tuile(data->tuile);
                char carac = tuile[3 * k + l];
                switch (carac) {
                    case 'P' :
                        image = gtk_image_new_from_file("./res/plain25pxred.png");
                        break;
                    case 'F' :
                        image = gtk_image_new_from_file("./res/forest25pxred.png");
                        break;
                    case 'V' :
                        image = gtk_image_new_from_file("./res/city25pxred.png");
                        break;
                    case 'R' :
                        image = gtk_image_new_from_file("./res/ressource25pxred.png");
                        break;
                    case 'U' :
                        image = gtk_image_new_from_file("./res/factory25pxred.png");
                            break;
                    case 'L' :
                        image = gtk_image_new_from_file("./res/lake25pxred.png");
                        break;
                    default :
                        image = gtk_image_new_from_file("./res/void25pxred.png");
                        break;
                }
                DEPLACEMENT_POSITION(grid, image, r, i, j, k, l);
                free(tuile);
            }
        }
    }

coda :
    gtk_widget_show_all(window);
} 

void deplacement_haut(gpointer user_data) {

    data_loop_gtk data = (data_loop_gtk)user_data;
    GtkWidget       *window         =   data->attr->fenetre->window;
    GtkWidget       *grid           =   data->grid;
    GtkWidget       *image;
    GtkWidget       *image_old;    
    int             sz_grille       =   *(data->attr->sz_grille);
    int             i               =   data->i;
    int             j               =   data->j;
    int             r               =   data->r;
    int             k, l;
    parcours_p      parc            =   data->attr->parc;
    grille_p        grille;

    /*Enregistrement du déplacement*/
    j--;
    data->j--;

    /*Test de sortie du terrain de jeu*/
    if (j < 0) {
        data->j++;
        goto coda;
    }

    /*Destruction des éléments de la grille*/

    for (k = 0; k < sz_grille; k++) {
        for (l = 0; l < sz_grille; l++) {
            image_old = gtk_grid_get_child_at(GTK_GRID(grid), l, k);
            gtk_widget_destroy(image_old);
        }
    }

    /*Reconstruction de la grille*/
    for (k = 0; k < sz_grille; k++) {
        for (l = 0; l < sz_grille; l++) {
            grille = parc->tour->g;
            char tuile = lire_dalle(grille, k, l);
            switch (tuile) {
                case 'P' :
                    image = gtk_image_new_from_file("./res/plain25px.png");
                    break;
                case 'F' :
                    image = gtk_image_new_from_file("./res/forest25px.png");
                    break;
                case 'V' :
                    image = gtk_image_new_from_file("./res/city25px.png");
                    break;
                case 'R' :
                    image = gtk_image_new_from_file("./res/ressource25px.png");
                    break;
                case 'U' :
                    image = gtk_image_new_from_file("./res/factory25px.png");
                    break;
                case 'L' :
                    image = gtk_image_new_from_file("./res/lake25px.png");
                    break;
                default :
                    image = gtk_image_new_from_file("./res/void25px.png");
                    break;
            }
            gtk_image_set_pixel_size(GTK_IMAGE(image), 5);
            gtk_grid_attach(GTK_GRID(grid), image, l, k, 1, 1);
        }
    }
    /*Apparition de la tuile à l'endroit déplacé*/
    Bool isPlacable = test_recouvrement(data->attr->parc->tour, j, i, r);
    if (isPlacable) {
        for (k = 0; k < 2; k++) {
            for (l = 0; l < 3; l++) {
                char* tuile = lire_tuile(data->tuile);
                char carac = tuile[3 * k + l];
                switch (carac) {
                    case 'P' :
                        image = gtk_image_new_from_file("./res/plain25px.png");
                        break;
                    case 'F' :
                        image = gtk_image_new_from_file("./res/forest25px.png");
                        break;
                    case 'V' :
                        image = gtk_image_new_from_file("./res/city25px.png");
                        break;
                    case 'R' :
                        image = gtk_image_new_from_file("./res/ressource25px.png");
                        break;
                    case 'U' :
                        image = gtk_image_new_from_file("./res/factory25px.png");
                        break;
                    case 'L' :
                        image = gtk_image_new_from_file("./res/lake25px.png");
                        break;
                    default :
                        image = gtk_image_new_from_file("./res/void25px.png");
                        break;
                }
                DEPLACEMENT_POSITION(grid, image, r, i, j, k, l);
                free(tuile);
            }
        }
    }
    else {
        for (k = 0; k < 2; k++) {
            for (l = 0; l < 3; l++) {
                char* tuile = lire_tuile(data->tuile);
                char carac = tuile[3 * k + l];
                switch (carac) {
                    case 'P' :
                        image = gtk_image_new_from_file("./res/plain25pxred.png");
                        break;
                    case 'F' :
                        image = gtk_image_new_from_file("./res/forest25pxred.png");
                        break;
                    case 'V' :
                        image = gtk_image_new_from_file("./res/city25pxred.png");
                        break;
                    case 'R' :
                        image = gtk_image_new_from_file("./res/ressource25pxred.png");
                        break;
                    case 'U' :
                        image = gtk_image_new_from_file("./res/factory25pxred.png");
                            break;
                    case 'L' :
                        image = gtk_image_new_from_file("./res/lake25pxred.png");
                        break;
                    default :
                        image = gtk_image_new_from_file("./res/void25pxred.png");
                        break;
                }
                DEPLACEMENT_POSITION(grid, image, r, i, j, k, l);
                free(tuile);
            }
        }
    }

coda :
    gtk_widget_show_all(window);
} 

void deplacement_bas(gpointer user_data) {

    data_loop_gtk data = (data_loop_gtk)user_data;
    GtkWidget       *window         =   data->attr->fenetre->window;
    GtkWidget       *grid           =   data->grid;
    GtkWidget       *image;
    GtkWidget       *image_old;    
    int             sz_grille       =   *(data->attr->sz_grille);
    int             i               =   data->i;
    int             j               =   data->j;
    int             r               =   data->r;
    int             k, l;
    parcours_p      parc            =   data->attr->parc;
    grille_p        grille;

    /*Test de sortie du terrain de jeu*/
    switch (r) {
        case 0 : case 2 :
            if (j + 4 > sz_grille) goto coda;
            break;
        case 1 : case 3 :
            if (j + 3 > sz_grille) goto coda;
            break;
        default :
            goto coda;
            break;
    }

    /*Enregistrement du déplacement*/
    j++;
    data->j++;

    /*Destruction des éléments de la grille*/
    for (k = 0; k < sz_grille; k++) {
        for (l = 0; l < sz_grille; l++) {
            image_old = gtk_grid_get_child_at(GTK_GRID(grid), l, k);
            gtk_widget_destroy(image_old);
        }
    }

    /*Reconstruction de la grille*/
    for (k = 0; k < sz_grille; k++) {
        for (l = 0; l < sz_grille; l++) {
            grille = parc->tour->g;
            char tuile = lire_dalle(grille, k, l);
            switch (tuile) {
                case 'P' :
                    image = gtk_image_new_from_file("./res/plain25px.png");
                    break;
                case 'F' :
                    image = gtk_image_new_from_file("./res/forest25px.png");
                    break;
                case 'V' :
                    image = gtk_image_new_from_file("./res/city25px.png");
                    break;
                case 'R' :
                    image = gtk_image_new_from_file("./res/ressource25px.png");
                    break;
                case 'U' :
                    image = gtk_image_new_from_file("./res/factory25px.png");
                    break;
                case 'L' :
                    image = gtk_image_new_from_file("./res/lake25px.png");
                    break;
                default :
                    image = gtk_image_new_from_file("./res/void25px.png");
                    break;
            }
            gtk_image_set_pixel_size(GTK_IMAGE(image), 5);
            gtk_grid_attach(GTK_GRID(grid), image, l, k, 1, 1);
        }
    }
    /*Apparition de la tuile à l'endroit déplacé*/
    Bool isPlacable = test_recouvrement(data->attr->parc->tour, j, i, r);
    if (isPlacable) {
        for (k = 0; k < 2; k++) {
            for (l = 0; l < 3; l++) {
                char* tuile = lire_tuile(data->tuile);
                char carac = tuile[3 * k + l];
                switch (carac) {
                    case 'P' :
                        image = gtk_image_new_from_file("./res/plain25px.png");
                        break;
                    case 'F' :
                        image = gtk_image_new_from_file("./res/forest25px.png");
                        break;
                    case 'V' :
                        image = gtk_image_new_from_file("./res/city25px.png");
                        break;
                    case 'R' :
                        image = gtk_image_new_from_file("./res/ressource25px.png");
                        break;
                    case 'U' :
                        image = gtk_image_new_from_file("./res/factory25px.png");
                        break;
                    case 'L' :
                        image = gtk_image_new_from_file("./res/lake25px.png");
                        break;
                    default :
                        image = gtk_image_new_from_file("./res/void25px.png");
                        break;
                }
                DEPLACEMENT_POSITION(grid, image, r, i, j, k, l);
                free(tuile);
            }
        }
    }
     else {
        for (k = 0; k < 2; k++) {
            for (l = 0; l < 3; l++) {
                char* tuile = lire_tuile(data->tuile);
                char carac = tuile[3 * k + l];
                switch (carac) {
                    case 'P' :
                        image = gtk_image_new_from_file("./res/plain25pxred.png");
                        break;
                    case 'F' :
                        image = gtk_image_new_from_file("./res/forest25pxred.png");
                        break;
                    case 'V' :
                        image = gtk_image_new_from_file("./res/city25pxred.png");
                        break;
                    case 'R' :
                        image = gtk_image_new_from_file("./res/ressource25pxred.png");
                        break;
                    case 'U' :
                        image = gtk_image_new_from_file("./res/factory25pxred.png");
                            break;
                    case 'L' :
                        image = gtk_image_new_from_file("./res/lake25pxred.png");
                        break;
                    default :
                        image = gtk_image_new_from_file("./res/void25pxred.png");
                        break;
                }
                DEPLACEMENT_POSITION(grid, image, r, i, j, k, l);
                free(tuile);
            }
        }
    }   

coda :
    gtk_widget_show_all(window);
} 

void deplacement_rotation(gpointer user_data) {

    data_loop_gtk data = (data_loop_gtk)user_data;
    GtkWidget       *window         =   data->attr->fenetre->window;
    GtkWidget       *grid           =   data->grid;
    GtkWidget       *image;
    GtkWidget       *image_old;    
    int             sz_grille       =   *(data->attr->sz_grille);
    int             i               =   data->i;
    int             j               =   data->j;
    int             r               =   data->r;
    int             k, l;
    parcours_p      parc            =   data->attr->parc;
    grille_p        grille;

    /*Enregistrement du déplacement*/
    r = r == 3 ? 0 : r + 1;
    data->r = data->r == 3 ? 0 : data->r + 1;

    /*Destruction des éléments de la grille*/

    for (k = 0; k < sz_grille; k++) {
        for (l = 0; l < sz_grille; l++) {
            image_old = gtk_grid_get_child_at(GTK_GRID(grid), l, k);
            gtk_widget_destroy(image_old);
        }
    }

    /*Reconstruction de la grille*/
    for (k = 0; k < sz_grille; k++) {
        for (l = 0; l < sz_grille; l++) {
            grille = parc->tour->g;
            char tuile = lire_dalle(grille, k, l);
            switch (tuile) {
                case 'P' :
                    image = gtk_image_new_from_file("./res/plain25px.png");
                    break;
                case 'F' :
                    image = gtk_image_new_from_file("./res/forest25px.png");
                    break;
                case 'V' :
                    image = gtk_image_new_from_file("./res/city25px.png");
                    break;
                case 'R' :
                    image = gtk_image_new_from_file("./res/ressource25px.png");
                    break;
                case 'U' :
                    image = gtk_image_new_from_file("./res/factory25px.png");
                    break;
                case 'L' :
                    image = gtk_image_new_from_file("./res/lake25px.png");
                    break;
                default :
                    image = gtk_image_new_from_file("./res/void25px.png");
                    break;
            }
            gtk_image_set_pixel_size(GTK_IMAGE(image), 5);
            gtk_grid_attach(GTK_GRID(grid), image, l, k, 1, 1);
        }
    }

    /*Apparition de la tuile à l'endroit déplacé*/
    Bool isPlacable = test_recouvrement(data->attr->parc->tour, j, i, r);
    if (isPlacable) {
        for (k = 0; k < 2; k++) {
            for (l = 0; l < 3; l++) {
                char* tuile = lire_tuile(data->tuile);
                char carac = tuile[3 * k + l];
                switch (carac) {
                    case 'P' :
                        image = gtk_image_new_from_file("./res/plain25px.png");
                        break;
                    case 'F' :
                        image = gtk_image_new_from_file("./res/forest25px.png");
                        break;
                    case 'V' :
                        image = gtk_image_new_from_file("./res/city25px.png");
                        break;
                    case 'R' :
                        image = gtk_image_new_from_file("./res/ressource25px.png");
                        break;
                    case 'U' :
                        image = gtk_image_new_from_file("./res/factory25px.png");
                        break;
                    case 'L' :
                        image = gtk_image_new_from_file("./res/lake25px.png");
                        break;
                    default :
                        image = gtk_image_new_from_file("./res/void25px.png");
                        break;
                }
                DEPLACEMENT_POSITION(grid, image, r, i, j, k, l);
                free(tuile);
            }
        }
    }
    else {
        for (k = 0; k < 2; k++) {
            for (l = 0; l < 3; l++) {
                char* tuile = lire_tuile(data->tuile);
                char carac = tuile[3 * k + l];
                switch (carac) {
                    case 'P' :
                        image = gtk_image_new_from_file("./res/plain25pxred.png");
                        break;
                    case 'F' :
                        image = gtk_image_new_from_file("./res/forest25pxred.png");
                        break;
                    case 'V' :
                        image = gtk_image_new_from_file("./res/city25pxred.png");
                        break;
                    case 'R' :
                        image = gtk_image_new_from_file("./res/ressource25pxred.png");
                        break;
                    case 'U' :
                        image = gtk_image_new_from_file("./res/factory25pxred.png");
                            break;
                    case 'L' :
                        image = gtk_image_new_from_file("./res/lake25pxred.png");
                        break;
                    default :
                        image = gtk_image_new_from_file("./res/void25pxred.png");
                        break;
                }
                DEPLACEMENT_POSITION(grid, image, r, i, j, k, l);
                free(tuile);
            }
        }
    }

    gtk_widget_show_all(window);
}

void end_loop_anim(gpointer user_data) {

    data_loop_gtk data = (data_loop_gtk)user_data;
    int             sz_grille       =   *(data->attr->sz_grille);
    int             i               =   data->i;
    int             j               =   data->j;
    int             r               =   data->r;
    int             recouv;
    int             k;

    recouv = add_tour(&(data->attr->parc), data->tuile, j, i, r, sz_grille);

    for (k = 2; k < 8; k++) gtk_accel_group_disconnect(data->accel_group, data->closure[k]);

    if (recouv == 0) {
        data->i = 0;
        data->j = 0;
        data->r = 0;
        loop_anim((gpointer)data);
    }
    else {
        *(data->attr->nbr_tour) = *(data->attr->nbr_tour) + 1;
        void* useless;
        defiler_tri(&data->attr->tete, &data->attr->queue, &useless, 6, data->tuile->id); 
        gtk_widget_destroy(data->box);
        init_att d = data->attr;
        free(data->solveur);
        free(data);
        for (k = 0; k < 2; k++) gtk_accel_group_disconnect(data->accel_group_solv, data->closure[k]);
        loop((gpointer)d);
    }
}

void loop_init(GtkApplication *app, gpointer user_data) {

    GtkWidget* window_main_menu;
    GtkWidget* grid;
    GtkWidget* button;

    /*Création de la fenêtre*/
	window_main_menu = gtk_application_window_new(app);
	gtk_window_set_title (GTK_WINDOW(window_main_menu), "Main Menu");
	gtk_window_set_default_size(GTK_WINDOW(window_main_menu), 200, 200);
	gtk_container_set_border_width (GTK_CONTAINER (window_main_menu), 50);

    /*Création du label*/
    //label = gtk_label_new("Main Menu");
    //gtk_container_add(GTK_CONTAINER(window_main_menu), label);
	
    /*Création de la grille de buttons*/
    grid = gtk_grid_new();
    gtk_grid_set_row_spacing(GTK_GRID(grid), 5);
    gtk_grid_set_row_homogeneous(GTK_GRID(grid), TRUE);
    gtk_grid_set_column_homogeneous(GTK_GRID(grid), TRUE);
    gtk_container_add(GTK_CONTAINER(window_main_menu), grid);

	struct appWindow *args = malloc(sizeof(struct appWindow));
	args->app = app;
	args->window = window_main_menu;

    ((init_att)user_data)->fenetre = args;

    /*Création des boutons*/
    button = gtk_button_new_with_label("Play");
    g_signal_connect(button, "clicked", G_CALLBACK(initialisation_parametres_jeu), user_data);
	gtk_grid_attach(GTK_GRID(grid), button, 0, 0, 1, 2);
	
	button = gtk_button_new_with_label("Credits");
	g_signal_connect(button, "clicked", G_CALLBACK(gtk_affichage_credits), args);
	gtk_grid_attach(GTK_GRID(grid), button, 0, 2, 1, 1);

	button = gtk_button_new_with_label("Exit");
	g_signal_connect_swapped(button, "clicked", G_CALLBACK(gtk_widget_destroy), window_main_menu);
	gtk_grid_attach(GTK_GRID(grid), button, 0, 3, 1, 1);

    /*Affichage de toute la grille*/
	gtk_widget_show_all(window_main_menu);
	
}

void loop_anim(gpointer user_data) {
     
    data_loop_gtk data = (data_loop_gtk)user_data;
    GtkWidget       *window         =   data->attr->fenetre->window;
    GtkWidget       *box_externe    =   data->box_externe;
    GtkWidget       *grid           =   data->grid;
    GtkWidget       *image;
    GtkWidget       *image_old;
    GtkAccelGroup   *accel_group;
    GClosure        *closure;
    int             i               =   data->i;
    int             j               =   data->j;
    int             r               =   data->r;
    int             k, l;

    /*Destruction de la sélection des tuiles*/
    if (box_externe != NULL) {
        gtk_widget_destroy(data->box_externe);
        data->box_externe = NULL;
    }

    /*Création des raccourcis claviers*/
    accel_group = gtk_accel_group_new();
    data->accel_group = accel_group;
    closure = g_cclosure_new_swap(G_CALLBACK(deplacement_droit), data, NULL);
    gtk_accel_group_connect(accel_group, GDK_KEY_Right, 2, GTK_ACCEL_MASK, closure);
    data->closure[2] = closure;

    closure = g_cclosure_new_swap(G_CALLBACK(deplacement_gauche), data, NULL);
    gtk_accel_group_connect(accel_group, GDK_KEY_Left, 2, GTK_ACCEL_MASK, closure);
    data->closure[3] = closure;

    closure = g_cclosure_new_swap(G_CALLBACK(deplacement_haut), data, NULL);
    gtk_accel_group_connect(accel_group, GDK_KEY_Up, 2, GTK_ACCEL_MASK, closure);
    data->closure[4] = closure;

    closure = g_cclosure_new_swap(G_CALLBACK(deplacement_bas), data, NULL);
    gtk_accel_group_connect(accel_group, GDK_KEY_Down, 2, GTK_ACCEL_MASK, closure);
    data->closure[5] = closure;

    closure = g_cclosure_new_swap(G_CALLBACK(deplacement_rotation), data, NULL);
    gtk_accel_group_connect(accel_group, GDK_KEY_R, 2, GTK_ACCEL_MASK, closure);
    data->closure[6] = closure;

    closure = g_cclosure_new_swap(G_CALLBACK(end_loop_anim), data, NULL);
    gtk_accel_group_connect(accel_group, GDK_KEY_Return, 2, GTK_ACCEL_MASK, closure);
    data->closure[7] = closure;
    gtk_window_add_accel_group(GTK_WINDOW(window), accel_group);

    /*Apparition de la tuile à l'endroit initiale*/
    Bool isPlacable = test_recouvrement(data->attr->parc->tour, j, i, r);
    if (isPlacable) {
        for (k = 0; k < 2; k++) {
            for (l = 0; l < 3; l++) {
                char* tuile = lire_tuile(data->tuile);
                char carac = tuile[3 * k + l];
                switch (carac) {
                    case 'P' :
                        image = gtk_image_new_from_file("./res/plain25px.png");
                        break;
                    case 'F' :
                        image = gtk_image_new_from_file("./res/forest25px.png");
                        break;
                    case 'V' :
                        image = gtk_image_new_from_file("./res/city25px.png");
                        break;
                    case 'R' :
                        image = gtk_image_new_from_file("./res/ressource25px.png");
                        break;
                    case 'U' :
                        image = gtk_image_new_from_file("./res/factory25px.png");
                        break;
                    case 'L' :
                        image = gtk_image_new_from_file("./res/lake25px.png");
                        break;
                    default :
                        image = gtk_image_new_from_file("./res/void25px.png");
                        break;
                }
                gtk_image_set_pixel_size(GTK_IMAGE(image), 5);
                switch (3 * k + l) {
                    case 0 :
                        image_old = gtk_grid_get_child_at(GTK_GRID(grid), 0, 0);
                        gtk_widget_destroy(image_old);
                        gtk_grid_attach(GTK_GRID(grid), image, 0, 0, 1, 1);
                        break;
                    case 1 :
                        image_old = gtk_grid_get_child_at(GTK_GRID(grid), 1, 0);
                        gtk_widget_destroy(image_old);
                        gtk_grid_attach(GTK_GRID(grid), image, 1, 0, 1, 1);
                        break;
                    case 2 :
                        image_old = gtk_grid_get_child_at(GTK_GRID(grid), 0, 1);
                        gtk_widget_destroy(image_old);
                        gtk_grid_attach(GTK_GRID(grid), image, 0, 1, 1, 1);
                        break;
                    case 3 :
                        image_old = gtk_grid_get_child_at(GTK_GRID(grid), 1, 1);
                        gtk_widget_destroy(image_old);
                        gtk_grid_attach(GTK_GRID(grid), image, 1, 1, 1, 1);
                        break;
                    case 4 :
                        image_old = gtk_grid_get_child_at(GTK_GRID(grid), 0, 2);
                        gtk_widget_destroy(image_old);
                        gtk_grid_attach(GTK_GRID(grid), image, 0, 2, 1, 1);
                        break;
                    case 5 :
                        image_old = gtk_grid_get_child_at(GTK_GRID(grid), 1, 2);
                        gtk_widget_destroy(image_old);
                        gtk_grid_attach(GTK_GRID(grid), image, 1, 2, 1, 1);
                        break;
                    default :
                        break;
                }
                free(tuile);
            }
        }
    }
    else {
        for (k = 0; k < 2; k++) {
            for (l = 0; l < 3; l++) {
                char* tuile = lire_tuile(data->tuile);
                char carac = tuile[3 * k + l];
                switch (carac) {
                    case 'P' :
                        image = gtk_image_new_from_file("./res/plain25pxred.png");
                        break;
                    case 'F' :
                        image = gtk_image_new_from_file("./res/forest25pxred.png");
                        break;
                    case 'V' :
                        image = gtk_image_new_from_file("./res/city25pxred.png");
                        break;
                    case 'R' :
                        image = gtk_image_new_from_file("./res/ressource25pxred.png");
                        break;
                    case 'U' :
                        image = gtk_image_new_from_file("./res/factory25pxred.png");
                        break;
                    case 'L' :
                        image = gtk_image_new_from_file("./res/lake25pxred.png");
                        break;
                    default :
                        image = gtk_image_new_from_file("./res/void25pxred.png");
                        break;
                }
                gtk_image_set_pixel_size(GTK_IMAGE(image), 5);
                switch (3 * k + l) {
                    case 0 :
                        image_old = gtk_grid_get_child_at(GTK_GRID(grid), 0, 0);
                        gtk_widget_destroy(image_old);
                        gtk_grid_attach(GTK_GRID(grid), image, 0, 0, 1, 1);
                        break;
                    case 1 :
                        image_old = gtk_grid_get_child_at(GTK_GRID(grid), 1, 0);
                        gtk_widget_destroy(image_old);
                        gtk_grid_attach(GTK_GRID(grid), image, 1, 0, 1, 1);
                        break;
                    case 2 :
                        image_old = gtk_grid_get_child_at(GTK_GRID(grid), 0, 1);
                        gtk_widget_destroy(image_old);
                        gtk_grid_attach(GTK_GRID(grid), image, 0, 1, 1, 1);
                        break;
                    case 3 :
                        image_old = gtk_grid_get_child_at(GTK_GRID(grid), 1, 1);
                        gtk_widget_destroy(image_old);
                        gtk_grid_attach(GTK_GRID(grid), image, 1, 1, 1, 1);
                        break;
                    case 4 :
                        image_old = gtk_grid_get_child_at(GTK_GRID(grid), 0, 2);
                        gtk_widget_destroy(image_old);
                        gtk_grid_attach(GTK_GRID(grid), image, 0, 2, 1, 1);
                        break;
                    case 5 :
                            image_old = gtk_grid_get_child_at(GTK_GRID(grid), 1, 2);
                            gtk_widget_destroy(image_old);
                            gtk_grid_attach(GTK_GRID(grid), image, 1, 2, 1, 1);
                            break;
                    default :
                        break;
                }
                free(tuile);
            }
        }
    }
    gtk_widget_show_all(window);
}

void loop(gpointer user_data) {

    init_att            data                    =   (init_att)user_data;
    GtkApplication      *app                    =   data->fenetre->app;
    GtkWidget           *window                 =   data->fenetre->window;
    GtkWidget           *box;
    GtkWidget           *box_interne;
    GtkWidget           *box_externe;
    GtkWidget           *box_v_selection;
    GtkWidget           *box_h_selection;
    GtkWidget           *grid;
    GtkWidget           *grid_selection;
    GtkWidget           *event_box;
    GtkWidget           *separator;
    GtkWidget           *image;
    GtkWidget           *label;
    GtkAccelGroup       *accel_group;
    GClosure            *closure;
    int                 i, j;
    int                 nbr_tuiles_shown;
    int                 nbr_tuiles_alignees;
    int                 sz_main                 =   *(data->sz_main);
    int                 sz_grille               =   *(data->sz_grille);
    int                 nbr_tour                =   *(data->nbr_tour);
    parcours_p          parc                    =   data->parc;
    file_p              tete                    =   data->tete;
    grille_p            grille;
    grid_item_tuiles    *tab_grid_tuiles;
    data_solveur_gtk    data_solv               = (data_solveur_gtk)malloc(sizeof(struct _data_solveur_gtk));
    data_loop_gtk       data_loop               = (data_loop_gtk)malloc(sizeof(struct _data_loop_gtk));

    data_loop->attr = data;
    data_solv->app = app;
    data_solv->jeu = parc;
    data_solv->main_jeu_tete = tete;
    data_solv->nbr_tour = nbr_tour;
    data_solv->sz_grille = sz_grille;
    data_solv->sz_main = sz_main;
    data_solv->isSet = FALSE;
    data_loop->solveur = data_solv;

    char phrase[100];
    sprintf(phrase, "Votre choix :\nnombre de cases : %d\nnombre de tuiles : %d\n", *(data->sz_grille), *(data->sz_main));
    
    /*vérification de la fin du jeu*/
    if (nbr_tour == sz_main) goto coda;
    
    /*Création des raccourcis claviers*/
    accel_group = gtk_accel_group_new();
    data_loop->accel_group_solv = accel_group;

    closure = g_cclosure_new_swap(G_CALLBACK(gtk_widget_destroy), window, NULL);
    gtk_accel_group_connect(accel_group, GDK_KEY_X, GDK_CONTROL_MASK, GDK_CONTROL_MASK, closure);
    data_loop->closure[0] = closure;

    closure = g_cclosure_new_swap(G_CALLBACK(affichage_solution_gtk), (gpointer)data_solv, NULL);
    gtk_accel_group_connect(accel_group, GDK_KEY_S, GDK_CONTROL_MASK, GDK_CONTROL_MASK, closure);
    data_loop->closure[1] = closure;
    gtk_window_add_accel_group(GTK_WINDOW(window), accel_group);
     
    /*Création de la box principale*/
    box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    gtk_box_set_baseline_position(GTK_BOX(box), GTK_BASELINE_POSITION_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(box), 20);
    gtk_container_add(GTK_CONTAINER(window), box);
    data_loop->box = box;

    /*Création de la box interne*/
    box_interne = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5);
    gtk_box_set_baseline_position(GTK_BOX(box_interne), GTK_BASELINE_POSITION_CENTER);
    gtk_container_set_border_width(GTK_CONTAINER(box_interne), 100);
    gtk_container_add(GTK_CONTAINER(box), box_interne);
    gtk_box_set_homogeneous(GTK_BOX(box_interne), TRUE);

    /*Création de la grille*/
    grid = gtk_grid_new();
    //gtk_grid_set_column_homogeneous(GTK_GRID(grid), TRUE);
    //gtk_grid_set_row_homogeneous(GTK_GRID(grid), TRUE);
    gtk_grid_set_row_spacing(GTK_GRID(grid), 1);
    gtk_grid_set_column_spacing(GTK_GRID(grid), 1);
    gtk_widget_set_hexpand(grid, TRUE);
    gtk_container_add(GTK_CONTAINER(box_interne), grid);
    data_loop->grid = grid;

    /*Création de la box de la seconde fenêtre*/
    box_externe = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    //gtk_container_add(GTK_CONTAINER(window_selection), box_externe);
    gtk_container_add(GTK_CONTAINER(box), box_externe);
    data_loop->box_externe = box_externe;

    /*Création du label titre*/
    label = gtk_label_new("Choisissez votre tuile :");
    gtk_container_add(GTK_CONTAINER(box_externe), label);

    /*Création du séparateur entre le label et la box*/
    separator = gtk_separator_new(GTK_ORIENTATION_HORIZONTAL);
    gtk_container_add(GTK_CONTAINER(box_externe), separator);

    /*Création de la box de sélection verticale*/
    box_v_selection = gtk_box_new(GTK_ORIENTATION_VERTICAL, 5);
    gtk_container_add(GTK_CONTAINER(box_externe), box_v_selection);

    /*Test de remplissage de la grille*/
    for (i = 0; i < sz_grille; i++) {
        for (j = 0; j < sz_grille; j++) {
            grille = parc->tour->g;
            char tuile = lire_dalle(grille, i, j);
            switch (tuile) {
                case 'P' :
                    image = gtk_image_new_from_file("./res/plain25px.png");
                    break;
                case 'F' :
                    image = gtk_image_new_from_file("./res/forest25px.png");
                    break;
                case 'V' :
                    image = gtk_image_new_from_file("./res/city25px.png");
                    break;
                case 'R' :
                    image = gtk_image_new_from_file("./res/ressource25px.png");
                    break;
                case 'U' :
                    image = gtk_image_new_from_file("./res/factory25px.png");
                    break;
                case 'L' :
                    image = gtk_image_new_from_file("./res/lake25px.png");
                    break;
                default :
                    image = gtk_image_new_from_file("./res/void25px.png");
                    break;
            }
            gtk_image_set_pixel_size(GTK_IMAGE(image), 5);
            gtk_grid_attach(GTK_GRID(grid), image, j, i, 1, 1);
        }
    }

    //label = gtk_label_new(phrase);
    //gtk_container_add(GTK_CONTAINER(box), label);

    /*Création de la fenêtre sélection des tuiles*/
    //window_selection = gtk_application_window_new(app);
    //gtk_window_set_title(GTK_WINDOW(window_selection), "Sélection");
    //gtk_container_set_border_width(GTK_CONTAINER(window_selection), 10);

    /*Boucle d'affichage des tuiles restantes*/
    file_p current = tete;
    nbr_tuiles_shown = 0;
    nbr_tuiles_alignees =   sz_grille < 7  ? 6  :
                            sz_grille < 10 ? 9  : 
                            sz_grille < 12 ? 12 : 
                            sz_grille < 15 ? 14 : 
                            sz_grille < 20 ? 16 :
                            sz_grille < 23 ? 18 :
                            sz_grille < 26 ? 20 :
                            sz_grille < 30 ? 22 :
                            24;

    /*Création du tableau de contenant l'ensemble des grilles du tableau*/
    tab_grid_tuiles = (grid_item_tuiles*)malloc((sz_main - nbr_tour) * sizeof(grid_item_tuiles));       
    data_loop->tab_grid_tuiles = tab_grid_tuiles;

    while (current != NULL && nbr_tour < sz_main) {

        /*Création de la box de sélection horizontale*/
        if (nbr_tuiles_shown % nbr_tuiles_alignees == 0) {
            box_h_selection = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 5); 
            gtk_container_add(GTK_CONTAINER(box_v_selection), box_h_selection);
        }

        /*Céation de l'event_box*/
        event_box = gtk_event_box_new();
        gtk_container_add(GTK_CONTAINER(box_h_selection), event_box);
        gulong handlerID = g_signal_connect(event_box, "button_press_event", G_CALLBACK(init_loop_anim), (gpointer)data_loop);

        /*Création de la grille contenant les tuiles restantes à sélectionner*/
        grid_selection = gtk_grid_new();
        gtk_grid_set_row_spacing(GTK_GRID(grid_selection), 1);
        gtk_grid_set_column_spacing(GTK_GRID(grid_selection), 1);
        gtk_container_add(GTK_CONTAINER(event_box), grid_selection);
        
        /*Affichage des tuiles*/
        ptuile* val = current->data;
        char* tuile = lire_tuile(*val);
        tab_grid_tuiles[nbr_tuiles_shown] = (grid_item_tuiles)malloc(sizeof(struct _grid_item_tuiles));
        tab_grid_tuiles[nbr_tuiles_shown]->event_box  = event_box;
        tab_grid_tuiles[nbr_tuiles_shown]->tuile = val;
        tab_grid_tuiles[nbr_tuiles_shown]->handlerID = handlerID;
        tab_grid_tuiles[nbr_tuiles_shown]->index = nbr_tuiles_shown;
        for (i = 0; i < 2; i++) {
            for (j = 0; j < 3; j++) {
                char carac = tuile[3 * i + j];
                switch (carac) {
                    case 'P' :
                        image = gtk_image_new_from_file("./res/plain25px.png");
                        break;
                    case 'F' :
                        image = gtk_image_new_from_file("./res/forest25px.png");
                        break;
                    case 'V' :
                        image = gtk_image_new_from_file("./res/city25px.png");
                        break;
                    case 'R' :
                        image = gtk_image_new_from_file("./res/ressource25px.png");
                        break;
                    case 'U' :
                        image = gtk_image_new_from_file("./res/factory25px.png");
                        break;
                    case 'L' :
                        image = gtk_image_new_from_file("./res/lake25px.png");
                        break;
                    default :
                        image = gtk_image_new_from_file("./res/void25px.png");
                        break;
                }   
                gtk_image_set_pixel_size(GTK_IMAGE(image), 5);
                switch (3 * i + j) {
                    case 0 :
                        gtk_grid_attach(GTK_GRID(grid_selection), image, 0, 0, 1, 1);
                        break;
                    case 1 :
                        gtk_grid_attach(GTK_GRID(grid_selection), image, 1, 0, 1, 1);
                        break;
                    case 2 :
                        gtk_grid_attach(GTK_GRID(grid_selection), image, 0, 1, 1, 1);
                        break;
                    case 3 :
                        gtk_grid_attach(GTK_GRID(grid_selection), image, 1, 1, 1, 1);
                        break;
                    case 4 :
                        gtk_grid_attach(GTK_GRID(grid_selection), image, 0, 2, 1, 1);
                        break;
                    case 5 :
                        gtk_grid_attach(GTK_GRID(grid_selection), image, 1, 2, 1, 1);
                        break;
                    default :
                        break;
                }
            }
        }
        current = current->suite;
        free(tuile);
        nbr_tuiles_shown++;
    }
    gtk_widget_show_all(window);
    //free(data_solv);
    
coda :
    if (nbr_tour == sz_main) conclusion_jeu((gpointer)data_loop);
}

void conclusion_jeu(gpointer user_data) {

    data_loop_gtk data = (data_loop_gtk)user_data;
    GtkWidget       *window         =   data->attr->fenetre->window;
    GtkWidget       *label;
    GtkAccelGroup   *accel_group;
    GClosure        *closure;
    points          resultat;

    /*Comptage des points finaux*/
    resultat = comptage(data->attr->parc->tour->g, 0, 0);
    /*Création du label de victoire*/
    char victoire[200];
    sprintf(victoire, "Bravo vous avez gagnez !\n\n\nVous avez accumulées %d points !\npar les forêts : %d\npar les villes : %d\npar les ressources et usines : %d\npar les lacs : %d\n\nMerci et à bientôt !\n", resultat.somme, resultat.points_forets, resultat.points_villes, resultat.points_r_and_u, resultat.points_lacs);

    /*Création du raccouci*/
    accel_group = gtk_accel_group_new();
    closure = g_cclosure_new_swap(G_CALLBACK(gtk_widget_destroy), window, NULL);
    gtk_accel_group_connect(accel_group, GDK_KEY_X, GDK_CONTROL_MASK, GDK_CONTROL_MASK, closure);
    gtk_window_add_accel_group(GTK_WINDOW(window), accel_group);

    /*Création du label*/
    label = gtk_label_new(victoire);
    gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_CENTER); 
    gtk_container_add(GTK_CONTAINER(window), label);

    /*On libère toute la place mémoire*/
    remove_all_tour(&data->attr->parc);
    free(data->attr->fenetre);
    free(data->attr);
    free(data);

    gtk_widget_show_all(window);
}
