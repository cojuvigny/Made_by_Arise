#ifndef _TEST_CUNIT_LOT_A_
#define _TEST_CUNIT_LOT_A_

#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

/**
 * @file test_cunit_lot_A.h
 * 
 * Ce fichier décrit un ensemble de fonctions concernant les test unitaires du lot A.
 * Il contient 25 fonctions.
 * 
 * - setup(void) 							fonction de mise à jour du catalogue
 * - teardown(void)							fonction de mise à jour du catalogue
 * - initialisation_catalogue_C(void) 		fonction d'initialisation du catalogue de tests du lot A
 * - test_func(void) 						teste la fonction func sur son résultat attendu
 */


/**
 * @brief	fonction de mise à jour du catalogue
 * @return  un entier
 */
int setup(void) ;

/**
 * @brief	fonction de mise à jour du catalogue
 * @return  un entier
 */
int teardown(void);

/**
 * @brief	fonction d'initialisation du catalogue de tests du lot A
 * @return  un entier
 */
int initialisation_catalogue_A(void);


/**
 * @brief	Fonction de test de test_limitation
 * @return  rien
 */
void test_init_tuile_random(void);

/**
 * @brief	Fonction de test de init_tuile_random_tab
 * @return  rien
 */
void test_init_tuile_random_tab(void);

/**
 * @brief	Fonction de test de init_tuile_random_tab
 * @return  rien
 */
void test_init_tuile_tab(void);

/**
 * @brief	Fonction de test de init_tuile_tab
 * @return  rien
 */
void test_lire_tuile(void);

/**
 * @brief	Fonction de test de lire_tuile
 * @return  rien
 */
void test_copie_tuile(void);

/**
 * @brief	Fonction de test de copie_tuile
 * @return  rien
 */
void test_rotation_tuile_lever_coucher_tuile(void);

/**
 * @brief	Fonction de test de create_grille_vide
 * @return  rien
 */
void test_create_grille_vide(void);

/**
 * @brief	Fonction de test de create_grille_random
 * @return  rien
 */
void test_create_grille_random(void);

/**
 * @brief	Fonction de test de create_grille_full_random
 * @return  rien
 */
void test_create_grille_full_random(void);

/**
 * @brief	Fonction de test de copy_grille
 * @return  rien
 */
void test_copy_grille(void);

/**
 * @brief	Fonction de test de lire_dalle
 * @return  rien
 */
void test_lire_dalle(void);

/**
 * @brief	Fonction de test de insert_tuile
 * @return  rien
 */
void test_insert_tuile(void);

/**
 * @brief	Fonction de test de lecture_ecriture_ficher_tuile
 * @return  rien
 */
void test_lecture_ecriture_ficher_tuile(void);

/**
 * @brief	Fonction de test de lecture_ecriture_ficher_grille
 * @return  rien
 */
void test_lecture_ecriture_ficher_grille(void);

/**
 * @brief	Fonction de test de ajout_placement
 * @return  rien
 */
void test_ajout_placement (void );

/**
 * @brief	Fonction de test de modif_nbr_cases
 * @return  rien
 */
void test_modif_nbr_cases (void);

/**
 * @brief	Fonction de test de modif_index
 * @return  rien
 */
void test_modif_index(void);

/**
 * @brief	Fonction de test de create_tour_vide
 * @return  rien
 */
void test_create_tour_vide(void);

/**
 * @brief	Fonction de test de create_tour_vide_random
 * @return  rien
 */
void test_create_tour_vide_random(void);

/**
 * @brief	Fonction de test de create_tour
 * @return  rien
 */
void test_create_tour (void);

/**
 * @brief	Fonction de test de add_tour
 * @return  rien
 */
void test_add_tour (void);

/**
 * @brief	Fonction de test de remove_tour
 * @return  rien
 */
void test_remove_tour(void);

#endif
