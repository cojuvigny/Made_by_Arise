#ifndef _COMPTAGE_
#define _COMPTAGE_


#include "grille.h"



/**
 * @file comptage.h
 * 
 * Ce fichier décrit un ensemble de fonctions concernant la file utilisée pour le comptage des points. 
 * Il contient 8 fonctions et 1 type.
 * 
 * - le type \a points définit le bilan des points d'une partie
 * 
 * - test_village2(grille_p g,int x, int y, int size, int *presence_carre)            															test la présence d'un village aux coordonnées (x,y)
 * - test_presence(grille_p g,int x, int y, int size, int ** matrice, char terrain) 															test la présence d'un lac aux coordonnées (x,y)
 * - initialisation_mat ( int ** matrice, int size)              																			   initialise une matrice de taille n
 * - create_grille_full_random2(int n)                           																				crée une grille de taille n et remplie aléatoirement
 * - test_presence_totale (grille_p g, char terrain)                                      														somme les points associés aux différents lacs mitoyens
 * - min (int a, int b)                                          																				calcule le minimum entre deux nombres
 * - max (int a, int b)                                      																					calcule le maximum entre deux nombres			
 * - pts_forets(int regle, int nbr_forets, int verbose) 											    										calcule le nombre de points donnés par les forêts en fonction de la règle choisie
 * - pts_lacs(int regle, int nbr_lacs, grille_p g, int verbose) 																				calcule le nombre de points donnés par les lacs en fonction de la règle choisie
 * - pts_r_and_u(int regle, int nbr_usines,int nbr_ressources, int verbose)																		calcule le nombre de points donnés par les ressources et usines en fonction de la règle choisie
 * - pts_plaines(int regle, int nbr_plaines, grille_p g, int verbose)																			calcule le nombre de points donnés par les plaines en fonction de la règle choisie 
 * - pts_villes(int regle, int nbr_villes, int verbose, int presence_carre)																		calcule le nombre de points donnés par les villes en fonction de la règle choisie
 * - comptage (grille_p g, int regle, int verbose)                                       														compte le nombre de points associé à une grille (avec texte)
 * - test_pb (grille_p g,int pb_lac, int pb_ressource, int pb_usine, int pb_foret, int pb_village, int pb_plaines, int regle, int verbose)	  compte le nombre de points associé à une grille (sans texte) selon le nombre de lacs/ village /etc.
 * - test_pb2 (grille_p g,int pb_lac, int pb_village,int pb_somme, int regle)																	compte le nombre de points associé à une grille (sans texte) selon le nombre de lacs/ village /etc (v2)
 */





/**
 * \struct points 
 * \brief structure de bilan des points d'une partie
 *
 * Permet de garder en mémoire le détail des points d'une partie
 *
 */
typedef struct points{
	int points_forets;
	int points_r_and_u;
	int points_villes;
	int points_lacs;
	int points_plaines;
	int somme;
}points, * ppoints;


/**
 * @brief 						test la présence d'un village aux coordonnées (x,y)
 * @param  g    				la grille
 * @param  x    				la coordonnée x de la case
 * @param  y    				la coordonnées y de la case
 * @param  size 				la taille de la grille
 * @param  presence_carre 		si il y a un carré de village
 * @return     					le nombre de villes composant le village
 */
int test_village2(grille_p g,int x, int y, int size, int *presence_carre);


/**
 * @brief 		   test la présence d'un lac aux coordonnées (x,y)
 * @param  g       la grille
 * @param  x       la coordonnée x de la case
 * @param  y       la coordonnées y de la case
 * @param  size    la taille de la grille
 * @param  matrice la matrice des cases visitées
 * @param  terrain le terrain dont on cherche l'occurence
 * @return         le nombre de lacs mitoyens au lac de coordonnées (x,y)
 */
int test_presence(grille_p g,int x, int y, int size, int ** matrice, char terrain);

/**
 * @brief 			initialise une matrice de taille n
 * @param	matrice la matrice de début
 * @param 	size    la taille de la matrice
 * @return 			rien
 */
void initialisation_mat ( int ** matrice, int size);

/**
 * @brief		crée une grille de taille n et remplie aléatoirement
 * @param 	n 	la taille de la grille
 * @return 		la grille allouée
 */
grille_p create_grille_full_random2(int n);

/**
 * @brief 			somme les points associés aux différents lacs mitoyens
 * @param 	g 		une grille 
 * @param   terrain	le terrain dont on cherche l'occurence
 * @return 			le nombre de point total
 */
int test_presence_totale (grille_p g, char terrain);


/**
 * @brief    	calcule le minimum entre deux nombres
 * @param  a 	un entier
 * @param  b 	un entier
 * @return   	un entier qui est le minimum de a et b
 */
int min (int a, int b);

/**
 * @brief    	calcule le maximum entre deux nombres
 * @param  a 	un entier
 * @param  b 	un entier
 * @return   	un entier qui est le maximum de a et b
 */
int max (int a, int b);

/**
 * @brief			  calcule le nombre de points donnés par les forêts en fonction de la règle choisie
 * @param  regle      la regle du jeu choisie
 * @param  nbr_forets le nombre de forets
 * @param   verbose   si l'on veut du texte ou pas
 * @return            le nombre de points donné par les forêts
 */
int pts_forets(int regle, int nbr_forets, int verbose);

/**
 * @brief			  calcule le nombre de points donnés par les lacs en fonction de la règle choisie 
 * @param  regle      la regle du jeu choisie
 * @param  nbr_lacs   le nombre de lacs
 * @param  g 		  la grille de jeu
 * @param   verbose   si l'on veut du texte ou pas
 * @return            le nombre de points donné par les lacs
 */
int pts_lacs(int regle, int nbr_lacs, grille_p g, int verbose);

/**
 * @brief			  		calcule le nombre de points donnés par les ressources et usines en fonction de la règle choisie 
 * @param  regle      		la regle du jeu choisie
 * @param  nbr_usines   	le nombre de lacs
 * @param  nbr_ressources   le nombre de lacs
 * @param  verbose   		si l'on veut du texte ou pas
 * @return            		le nombre de points donné par les usines et resources
 */
int pts_r_and_u(int regle, int nbr_usines,int nbr_ressources, int verbose);


/**
 * @brief			  		calcule le nombre de points donnés par les plaines en fonction de la règle choisie 
 * @param  regle      		la regle du jeu choisie
 * @param  nbr_plaines  	le nombre de plaines
 * @param  g 		 		la grille de jeu
 * @param  verbose   		si l'on veut du texte ou pas
 * @return            		le nombre de points donné par les plaines
 */
int pts_plaines(int regle, int nbr_plaines, grille_p g, int verbose);

/**
 * @brief			  		calcule le nombre de points donnés par les villes en fonction de la règle choisie 
 * @param  regle      		la regle du jeu choisie
 * @param  nbr_villes  		le nombre de villes
 * @param  presence_carre 	si il y a un carré de village
 * @return            		le nombre de points donné par les plaines
 */
int pts_villes(int regle, int nbr_villes, int verbose, int presence_carre);

/**
 * @brief 			compte le nombre de points associé à une grille (avec texte)
 * @param 	g 		une grille g
 * @param   regle	la regle du jeu choisie
 * @param   verbose si l'on veut du texte ou pas
 * @return 			le bilan des points
 */
points comptage(grille_p g, int regle, int verbose);

/**
 * @brief  				compte le nombre de points associé à une grille (sans texte) selon le nombre de lacs/ village /etc.
 * @param  g            une grille
 * @param  pb_lac       le nombre de lac
 * @param  pb_ressource le nombre de ressource
 * @param  pb_usine     le nombre d'usine
 * @param  pb_foret     le nombre de forêt
 * @param  pb_village   le nombre de village
 * @param  pb_plaines   le nombre de plaines
 * @param  regle      	la regle du jeu choisie
 * @param  verbose 		si l'on veut du texte ou pas
 * @return              le bilan des points
 */
points test_pb (grille_p g,int pb_lac, int pb_ressource, int pb_usine, int pb_foret, int pb_village, int pb_plaines, int regle, int verbose);


/**
 * @brief  				compte le nombre de points associé à une grille (sans texte) selon le nombre de lacs/ village /etc (v2).
 * @param  g            une grille
 * @param  pb_lac       le nombre de lac
 * @param  pb_village   le nombre de village
 * @param  pb_somme 	le nombre total de points
 * @param  regle      	la regle du jeu choisie
 * @return              le bilan des points
 */
int test_pb2 (grille_p g,int pb_lac, int pb_village,int pb_somme, int regle);

#endif
