#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "tuile.h"
#include "grille.h"
#include "couleur.h"
#define N 6

grille_p create_grille_vide(int n)
{
	grille_p g = (grille *) malloc(sizeof(grille));


	if (g == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}

	cases ** p=(cases **) malloc (n*sizeof(cases *));

	if (p == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}

	int i,j;
	for (i=0;i<n;i++){
		p[i] = (cases *) malloc(n*sizeof(cases));
		if (p[i] == NULL){
			fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
			exit(EXIT_FAILURE);
		}

		for (j = 0; j < n; ++j){
			p[i][j].type = '.' ;
			p[i][j].id = -1;
		}
	}
	g->cells=p;
	g->size=n;

	return g;
}

grille_p create_grille_random()
{
	srand(time(NULL));
  int n=rand()/RAND_MAX * (100) +3; //grille de taille au moins 3
  grille_p g = (grille *) malloc(sizeof(grille));

  if (g == NULL){
  	fprintf(stderr, "Problème allocation dynamique 1%s\n",__func__);
  	exit(EXIT_FAILURE);
  }


  cases ** p=(cases **) malloc (n*sizeof(cases *));

  if (p == NULL){
  	fprintf(stderr, "Problème allocation dynamique 2%s\n",__func__);
  	exit(EXIT_FAILURE);
  }


  int i,j;
  for (i=0;i<n;i++){
  	p[i] = (cases *) malloc(n*sizeof(cases));

  	if (p[i] == NULL){
  		fprintf(stderr, "Problème allocation dynamique 3%s\n",__func__);
  		exit(EXIT_FAILURE);
  	}

  	for (j = 0; j < n; ++j){
  		p[i][j].type = '.' ;
  		p[i][j].id = -1;
  	}
  }
  g->cells=p;
  g->size=n;
  return g;
}

grille_p create_grille_full_random(int n)
{
	char type[N] = {'L','V','P','F','U','R'};
	srand(time(NULL));
	grille_p g = (grille *) malloc(sizeof(grille));

	if (g == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}


	cases ** p=(cases **) malloc (n*sizeof(cases *));

	if (p == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}


	int i,j;
	for (i=0;i<n;i++){
		p[i] = (cases *) malloc(n*sizeof(cases));

		if (p[i] == NULL){
			fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
			exit(EXIT_FAILURE);
		}



		for (j = 0; j < n; ++j){
      p[i][j].type = type[rand()%6] ;/*Pour avoir plus de V et des test intéressants*/
			p[i][j].id = -1;
		}
	}
	g->cells=p;
	g->size=n;
	return g;
}

void copy_grille (grille_p g1,grille_p g2)
{
	int i;
	int j;
	g2->size=g1->size;
	for (i=0;i<g1->size;i++){
		for (j=0;j<g1->size;j++){
			g2->cells[i][j].type=g1->cells[i][j].type;
			g2->cells[i][j].id=g1->cells[i][j].id;
		}
	}
}

void free_grille(grille_p g)
{
	int i;
	for (i=0;i<g->size;i++)
	{
		free(g->cells[i]);
	}
	free(g->cells);
	free(g); 
}

char lire_dalle(grille_p g, int x, int y)
{
	return g->cells[x][y].type;
}


int est_valide(grille_p g, int x, int y)
{
	int size = g->size;

	if ( (x<0 || x>=size) ||(y<0 ||y>=size))
		return 0;
	return 1;
}

void insert_tuile_no_limit(grille_p g, int x, int y, ptuile p, int o)
{
	int tableau_orientation[4][6] ={{0,1,2,3,4,5},{4,2,0,5,3,1},{5,4,3,2,1,0},{1,3,5,0,2,4}};

	if (o==0 || o==2)
	{
		if (est_valide(g,x,y)){
			g->cells[x][y].type=p->tab[tableau_orientation[o][0]];
			g->cells[x][y].id=p->id;
		} 

		if (est_valide(g,x,y+1)){
			g->cells[x][y+1].type=p->tab[tableau_orientation[o][1]];
			g->cells[x][y+1].id=p->id;
		}

		if (est_valide(g,x+1,y)){
			g->cells[x+1][y].type=p->tab[tableau_orientation[o][2]];
			g->cells[x+1][y].id=p->id;
		} 

		if (est_valide(g,x+1,y+1)){
			g->cells[x+1][y+1].type=p->tab[tableau_orientation[o][3]];
			g->cells[x+1][y+1].id=p->id;
		} 

		if (est_valide(g,x+2,y)){
			g->cells[x+2][y].type=p->tab[tableau_orientation[o][4]];
			g->cells[x+2][y].id=p->id;

		} 
		if (est_valide(g,x+2,y+1)){
			g->cells[x+2][y+1].type=p->tab[tableau_orientation[o][5]];
			g->cells[x+2][y+1].id=p->id;
		} 
	}
	else
	{
		if (est_valide(g,x,y)){
			g->cells[x][y].type=p->tab[tableau_orientation[o][0]];
			g->cells[x][y].id=p->id;
		} 

		if (est_valide(g,x,y+1)){
			g->cells[x][y+1].type=p->tab[tableau_orientation[o][1]];
			g->cells[x][y+1].id=p->id;
		}

		if (est_valide(g,x,y+2)){
			g->cells[x][y+2].type=p->tab[tableau_orientation[o][2]];
			g->cells[x][y+2].id=p->id;
		} 

		if (est_valide(g,x+1,y)){
			g->cells[x+1][y].type=p->tab[tableau_orientation[o][3]];
			g->cells[x+1][y].id=p->id;
		} 

		if (est_valide(g,x+1,y+1)){
			g->cells[x+1][y+1].type=p->tab[tableau_orientation[o][4]];
			g->cells[x+1][y+1].id=p->id;

		} 
		if (est_valide(g,x+1,y+2)){
			g->cells[x+1][y+2].type=p->tab[tableau_orientation[o][5]];
			g->cells[x+1][y+2].id=p->id;
		}      
	} 
}

void insert_tuile(grille_p g, int x, int y, ptuile p, int o)
{

	if (o==0)
	{

		g->cells[x][y].type=p->tab[0];
		g->cells[x][y+1].type=p->tab[1];
		g->cells[x+1][y].type=p->tab[2];
		g->cells[x+1][y+1].type=p->tab[3];
		g->cells[x+2][y].type=p->tab[4];
		g->cells[x+2][y+1].type=p->tab[5];

		g->cells[x][y].id=p->id;
		g->cells[x][y+1].id=p->id;
		g->cells[x+1][y].id=p->id;
		g->cells[x+1][y+1].id=p->id;
		g->cells[x+2][y].id=p->id;
		g->cells[x+2][y+1].id=p->id;
	}
	else if (o==1)
	{
		g->cells[x][y].type=p->tab[4];
		g->cells[x][y+1].type=p->tab[2];
		g->cells[x][y+2].type=p->tab[0];
		g->cells[x+1][y].type=p->tab[5];
		g->cells[x+1][y+1].type=p->tab[3];
		g->cells[x+1][y+2].type=p->tab[1];

		g->cells[x][y].id=p->id;
		g->cells[x][y+1].id=p->id;
		g->cells[x][y+2].id=p->id;
		g->cells[x+1][y].id=p->id;
		g->cells[x+1][y+1].id=p->id;
		g->cells[x+1][y+2].id=p->id;     
	} 

	else if (o==2)
	{

		g->cells[x][y].type=p->tab[5];
		g->cells[x][y+1].type=p->tab[4];
		g->cells[x+1][y].type=p->tab[3];
		g->cells[x+1][y+1].type=p->tab[2];
		g->cells[x+2][y].type=p->tab[1];
		g->cells[x+2][y+1].type=p->tab[0];

		g->cells[x][y].id=p->id;
		g->cells[x][y+1].id=p->id;
		g->cells[x+1][y].id=p->id;
		g->cells[x+1][y+1].id=p->id;
		g->cells[x+2][y].id=p->id;
		g->cells[x+2][y+1].id=p->id;
	}
	else
	{
		g->cells[x][y].type=p->tab[1];
		g->cells[x][y+1].type=p->tab[3];
		g->cells[x][y+2].type=p->tab[5];
		g->cells[x+1][y].type=p->tab[0];
		g->cells[x+1][y+1].type=p->tab[2];
		g->cells[x+1][y+2].type=p->tab[4];

		g->cells[x][y].id=p->id;
		g->cells[x][y+1].id=p->id;
		g->cells[x][y+2].id=p->id;
		g->cells[x+1][y].id=p->id;
		g->cells[x+1][y+1].id=p->id;
		g->cells[x+1][y+2].id=p->id;     
	}
}
