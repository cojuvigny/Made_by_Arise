#ifndef _GRILLE_
#define _GRILLE_

#include "tuile.h"

/**
 * @file grille.h
 * 
 * Ce fichier décrit un ensemble de fonctions concernant la grille de jeu. 
 * Il contient 9 fonctions et 2 types.
 * 
 * - le type \a cases définit une case de la grille de jeu, elle est composée de deux surcouches : le terrain et l'id de la tuile posée sur cette case
 * - le type \a grille définit un ensemble de cases de type \a cases ainsi que la taille size de la grille
 * 
 * - create_grille_vide(int n) 											alloue une grille vide de taille n
 * - create_grille_random() 											crée une grille vide de taille aléatoire
 * - create_grille_full_random(int n) 									alloue une grille de taille n remplie aléatoirement
 * - copy_grille (grille_p g1,grille_p g2) 								copie une grille
 * - free_grille(grille_p g) 											libère la mémoire allouée pour la grille
 * - lire_dalle(grille_p g, int x, int y) 								renvoie le type de la case/dalle (x,y)
 * - insert_tuile(grille_p g, int x, int y,ptuile p,int o)   			insère une tuile dans la grille en x,y en vérifiant les règles de recouvrement
 * - insert_tuile_no_limit(grille_p g, int x, int y, ptuile p, int o)	insère une tuile dans la grille en x,y sans vérifier les règles de recouvrement
 * - est_valide(grille_p g, int x, int y) 								vérifie si le terrain aux cooordonnées x,y de la tuile peut être inséré
 */



/**
 * \struct cases 
 * \brief structure d'une case de la grille
 *
 * \a cases définit une case de la grille de jeu, elle est composée de deux surcouches : le terrain et l'id de la tuile posée sur cette case
 */
typedef struct cases{
  char type;
  int id;
}cases,*cases_p;


/**
 * \struct grille
 * \brief structure d'une grille de jeu
 *
 * \a grille définit un ensemble de cases de type cases ainsi que la taille size de la grille
 */
typedef struct grille{
  cases ** cells;;
  int size;
}grille,*grille_p;

/**
 * @brief 			alloue une grille vide de taille n
 * @attention 		n >= 3
 * @param 		n 	un entier
 * @return 			une grille vide
 */
grille_p create_grille_vide(int n);

/**
 * @brief 		crée une grille vide de taille aléatoire
 * @attention 	n aléatoire et >= 3
 * @return 		une grille vide
 */
grille_p create_grille_random();


/**
 * @brief 			alloue une grille de taille n remplie aléatoirement
 * @attention 		n >= 3
 * @param 		n 	un entier 
 * @return 			une grille remplie aléatoirement
 */
grille_p create_grille_full_random(int n);


/**
 * @brief 			copie une grille
 * @attention 		g1,g2 deux grilles construites
 * @param 		g1 	une grille
 * @param 		g2 	une grille
 * @return 			rien
 */
void copy_grille (grille_p g1,grille_p g2);


/**
 * @brief 		libère la mémoire allouée pour la grille
 * @param 	g 	une grille
 * @return 		rien
 */
void free_grille(grille_p g);

/** 
 * @brief 			renvoie le type de la case/dalle (x,y)
 * @attention 		x<n ; y<n 
 * @param 		g 	une grille 
 * @param 		x 	un entier positif
 * @param 		y 	un entier positif
 * @return 			le terrain de la case/dalle (x,y)
 */
char lire_dalle(grille_p g, int x, int y);

/** 
 * @brief 			insère une tuile dans la grille en x,y en vérifiant les règles de recouvrement
 * @attention 		x<n ; y<n ; 1<=o<=4
 * @param 		g 	une grille
 * @param 		x 	un entier positif
 * @param 		y 	un entier positif
 * @param 		p 	une tuile
 * @param 		o 	un entier d'orientation
 */
void insert_tuile(grille_p g, int x, int y,ptuile p,int o);


/** 
 * @brief 		insère une tuile dans la grille en x,y sans vérifier les règles de recouvrement
 * @param 	g 	une grille
 * @param 	x 	un entier positif
 * @param 	y	un entier positif
 * @param 	p 	une tuile
 * @param 	o 	un entier d'orientation
 */
void insert_tuile_no_limit(grille_p g, int x, int y, ptuile p, int o);

/** 
 * @brief 		vérifie si le terrain aux cooordonnées x,y de la tuile peut être inséré
 * @param 	g 	une grille
 * @param 	x 	un entier positif
 * @param	y 	un entier positif
 */

int est_valide(grille_p g, int x, int y);

#endif
