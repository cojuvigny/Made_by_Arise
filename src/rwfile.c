#if __STDC_VERSION__ >= 199901L
#define _XOPEN_SOURCE 600
#else
#define _XOPEN_SOURCE 500
#endif /* __STDC_VERSION__ */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <dirent.h>
#include <time.h>
#include <string.h>


#include "tuile.h"
#include "rwfile.h"
#include "couleur.h"
#include "file.h"

/**
 * @brief structure permettant de passer les données nécessaires en arguments des threads
 */
typedef struct _tuile_f tuile_f;
struct _tuile_f {
    int start;        //indice où se trouve la tuile
    char* tab_file;   //buffer du fichier lu
    ptuile* t_tuile;  //tableau contenant les tuiles lues
};

/**
 * @var nameprocessus
 * nom du processus courant
 * @attention : ne pas oublier la ligne : "nameProcessus = argv[0];" dans le main pour initialiser nameProcessus
 */
#if defined(__APPLE__) || defined(__FreeBSD__)
const char * nameProcessus = getprogname();
#elif defined(_GNU_SOURCE)
const char * nameProcessus = program_invocation_name;
#else
const char * nameProcessus = "?";
#endif



ptuile* read_file_tuile(char* name, int* size)
{
    //initialisation des threads
    pthread_attr_t att;
    pthread_attr_init(&att);
    //Lecture du fichier
    FILE* fd = fopen(name,"r");
    if (fd == NULL) {
        fprintf(stderr, "%s: error while opening %s in r mode : %s\n", nameProcessus, name, strerror(errno));
        exit(1);
    }
    struct stat info;
    if (fstat(fileno(fd),&info) == -1) {
        fprintf(stderr, "%s: error stat : %s\n", nameProcessus, strerror(errno));
        exit(1);
    }
    int sz = info.st_size;
    char* buf = (char*)calloc(sz*sz,sizeof(char));

    if (buf == NULL) {
        fprintf(stderr, "%s: not enough memory remaining\n", nameProcessus);
        exit(1);
    }
    int nblu = fread(buf, 1, sz, fd);
    if (nblu == 0) {
        fprintf(stderr, "%s: error while reading of %s : %s\n", nameProcessus, name, strerror(errno));
        exit(1);
    } 
    fclose(fd);
    
    //recuperation du nombre de tuiles
    int i, j;
    for (i = 0; buf[i] != '\n'; i++);
    char* nb_of_tuiles_char = (char*)calloc(i+1, sizeof(char));
    for (j = 0; j < i; j++)
       nb_of_tuiles_char[j] = buf[j];
    int nb_of_tuiles = strtol(nb_of_tuiles_char,NULL,10);
    free(nb_of_tuiles_char);
    i++;
    
    //création du tableau marquant la position de la fin de chaque ligne
    int* pos_bn_tab = pos_bn(buf, sz);

    //création des tuiles dans les threads
    int k;
    int l = 0;
    int start = pos_bn_tab[l] + 1;
    ptuile* tab_t = (ptuile*)malloc(nb_of_tuiles * sizeof(ptuile));
    pthread_t* thtab = (pthread_t*)malloc(nb_of_tuiles * sizeof(pthread_t));
    tuile_f** compt_tuile_tab = (tuile_f**)malloc(nb_of_tuiles * sizeof(tuile_f*));
    for (k = 0; k < nb_of_tuiles; k++)
       compt_tuile_tab[k] = (tuile_f*)malloc(sizeof(tuile_f));
    for (k = 0; k < nb_of_tuiles; k++) {
        compt_tuile_tab[k]->start = start;
        compt_tuile_tab[k]->tab_file = buf;
        compt_tuile_tab[k]->t_tuile = tab_t;
        pthread_create(&thtab[k], &att, creation_tuile, (void*)compt_tuile_tab[k]);
        l += 4;
        start = pos_bn_tab[l] + 1;
    }
    void* status;
    for (k = 0; k < nb_of_tuiles; k++) pthread_join(thtab[k], &status);
    *size = nb_of_tuiles;
    
    //libération de la mémoire allouée
    free(buf); //ok
    free(pos_bn_tab);
    for (k = 0; k < nb_of_tuiles; k++) free(compt_tuile_tab[k]);
    free(compt_tuile_tab);
    free(thtab);
    
    return tab_t;
}

void* creation_tuile(void* arg) {
    int i, j, start, end;

    //recupération des arguments d’entrée
    tuile_f* t = (tuile_f*)arg;

    //recherche du numéro de la tuile
    for (i = t->start; t->tab_file[i] != '\n'; i++);
    char* num_of_tuiles_char = (char*)calloc(i, sizeof(char));
    for (j = 0; j < i; j++) num_of_tuiles_char[j] = t->tab_file[j + t->start];
    start = i + 1;
    end = i + 12;
    int num_of_tuile = strtol(num_of_tuiles_char,NULL,10);

    //recuperation des éléments composants la tuile associée a ce numéro
    char* tab_tuile = (char*)calloc(end - start, sizeof(char));
    for (i = start; i < end; i++) tab_tuile[i - start] = t->tab_file[i]; 
    char* tuile_char = (char*)malloc(6 * sizeof(char));
    tuile_char[0] = tab_tuile[0];
    tuile_char[1] = tab_tuile[2];
    tuile_char[2] = tab_tuile[4];
    tuile_char[3] = tab_tuile[6];
    tuile_char[4] = tab_tuile[8];
    tuile_char[5] = tab_tuile[10];

    //création et enregistrement de la tuile dans le tableau final a l'indice donne par son numéro
    ptuile tuile_init = init_tuile_tab(tuile_char,num_of_tuile);
    t->t_tuile[num_of_tuile] = tuile_init;

    //libération des pointeurs crées
    free(tab_tuile);
    free(tuile_char);
    free(num_of_tuiles_char);
    return NULL;
}

int* pos_bn(char* file, int size)
{
    int i;
    int j = 0;
    int* pos_bn_tab = (int*)malloc(size * sizeof(int));
    for (i = 0; i < size; i++) pos_bn_tab[i] = 0;
    for (i = 0; i < size; i++) {
        if (file[i] == '\n') {
            pos_bn_tab[j] = i;
            j++;
        }
    }
    return pos_bn_tab;
}

int write_file_tuile(char* name, ptuile* t, int len)
{
    int k, nbwrite;
    int sz = 0;
    char lenchar[10];
    
    //ouverture du fichier
    FILE* fd = fopen(name, "w");
    if (fd == NULL) {
        fprintf(stderr, "%s: error while opening %s : %s\n", nameProcessus, name, strerror(errno));
        exit(1);
    }

    //écriture du nombre de tuiles à écrire
    sprintf(lenchar, "%d\n", len);
    nbwrite = len < 10 ? fwrite(lenchar, 1, 2, fd) : fwrite(lenchar, 1, 3, fd);
    if (nbwrite == 0) {
        fprintf(stderr, "%s: error while writing %s : %s\n", nameProcessus, name, strerror(errno));
        exit(1);
    }
    sz += nbwrite;

    //écriture des tuiles
    for (k = 0; k < len; k++) {
        char message[25];
        char* tuile_get = lire_tuile(t[k]);
        sprintf(message, "%d\n%c %c\n%c %c\n%c %c\n", k, tuile_get[0], tuile_get[1],
                tuile_get[2], tuile_get[3], tuile_get[4], tuile_get[5]);
        nbwrite = k < 10 ? fwrite(message, 1, 14, fd) : fwrite(message, 1, 15, fd);
        if (nbwrite == 0) {
            fprintf(stderr, "%s: error while writing %s : %s\n", nameProcessus, name, strerror(errno));
            exit(1);
        }
        sz += nbwrite;
        free(tuile_get);
    }

    //fermeture du fichier
    fclose(fd);
    return sz;
}

void read_file_grille(char* name, struct info_grille* info_t)
{    
    //lecture du fichier
    int fd = open(name, O_RDONLY);
    if (fd == -1) {
        fprintf(stderr, "%s: error while opening %s : %s\n", nameProcessus, name, strerror(errno));
        exit(1);
    }
    struct stat info;
    if (fstat(fd, &info) == -1) {
        fprintf(stderr, "%s: error stat : %s\n", nameProcessus, name);
        exit(1);
    }
    int sz = info.st_size;
    char* buf = (char*)malloc(sz);
    if (buf == NULL) {
        fprintf(stderr, "Not enough memory remaining\n");
        exit(1);
    }
    int nbread = read(fd, buf, sz); 
    if (nbread == -1) {
        fprintf(stderr, "%s: error while reading %s :%s\n", nameProcessus, name, strerror(errno));
        exit(1);
    }
    close(fd);
    
    //création du tableau marquant la position de la fin de chaque ligne
    int* pos_bn_tab = pos_bn(buf, sz);
    
    //lecture première ligne
    int i;
    int space = 0;
    for (i = 0; i < pos_bn_tab[0]; i++) if (buf[i] == ' ') space = i;
    char* sz_char = (char*)calloc((space + 1),sizeof(char));
    for (i = 0; i < space; i++) sz_char[i] = buf[i];
    (*info_t).sz_grille = strtol(sz_char,NULL,10);
    sz_char = (char*)realloc(sz_char, (pos_bn_tab[0] - space + 1) * sizeof(char));
    for (i = 0; i < (pos_bn_tab[0] - space + 1); i++) sz_char[i] = '\0';
    for (i = space + 1; i < pos_bn_tab[0]; i++) sz_char[i - space - 1] = buf[i];
    (*info_t).sz_main = strtol(sz_char,NULL,10);

    //lecture seconde ligne
    int* ids = (int*)calloc((pos_bn_tab[1] - pos_bn_tab[0] + 1) / 2,sizeof(int));
    int k = 0;
    int j = 0;
    int l;
    char T_number[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    for (i = pos_bn_tab[0] + 1; i <= pos_bn_tab[1]; i++) {
        if (buf[i] == ' ' || i == pos_bn_tab[1]) {
            ids[k] = strtol(T_number,NULL,10);
            j = 0;
            k++;
            for (l = 0; l < 10; l++) T_number[l] = '\0'; //on réinitialise le tableau
            continue;
        }
        T_number[j] = buf[i];
        j++;
    }
    (*info_t).ids = ids; 

    //lecture dernière ligne
    sz_char = (char*)realloc(sz_char, (sz - pos_bn_tab[1]) * sizeof(char));
    for (i = pos_bn_tab[1] + 1; i < sz; i++) {
        if (buf[i] == ' ') break;
        sz_char[i - pos_bn_tab[1] - 1] = buf[i];
    }
    (*info_t).id_depart = strtol(sz_char,NULL,10);
    free(pos_bn_tab);
    free(sz_char);
    free(buf);
}   
// on doit faire le free du malloc ids non? Oui je n'y touche pas
void write_file_grille(char* name, struct info_grille info_t)
{ 
    int i, nbwrite;

    //ouverture du fichier
    mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
    int fd = open(name, O_WRONLY|O_TRUNC|O_CREAT, mode);
    if (fd == -1) {
        fprintf(stderr, "%s: error while opening %s : %s\n", nameProcessus, name, strerror(errno));
        exit(1);
    }

    //écriture dans le fichier
    char buf[100];
    sprintf(buf, "%d %d\n", info_t.sz_grille, info_t.sz_main);
    for (i = 0; i < 100; i++) if (buf[i] == '\n') break;
    nbwrite = write(fd, buf, i + 1); 
    if (nbwrite <= 0) {
        fprintf(stderr, "%s : error while writing %s : %s\n", nameProcessus, name, strerror(errno));
        exit(1);
    }
    for (i = 0; i < info_t.sz_main; i++) {
        int j;
        char bufint[10];
        i == info_t.sz_main - 1 ? sprintf(bufint, "%d\n", info_t.ids[i]) : sprintf(bufint, "%d ", info_t.ids[i]);
        for (j = 0; j < 10; j++) if (bufint[j] == ' ' || bufint[j] == '\n') break;
        nbwrite = write(fd, bufint, j + 1);
        if (nbwrite <= 0) {
            fprintf(stderr, "%s : error while writing %s : %s\n", nameProcessus, name, strerror(errno));
            exit(1);
        }
    }
    sprintf(buf, "%d\n", info_t.id_depart);
    for (i = 0; i < 100; i++) if (buf[i] == '\n') break;
    nbwrite = write(fd, buf, i + 1); 
    if (nbwrite <= 0) {
        fprintf(stderr, "%s : error while writing %s : %s\n", nameProcessus, name, strerror(errno));
        exit(1);
    }

    //fermeture du fichier
    close(fd);
}


int compt_fic(char * name_d,file_p * tete_fic,file_p * queue_fic)
{
    int c=0;
    DIR *rep = opendir (name_d);
 
    if (rep != NULL) {
      struct dirent *lecture;
 
      while ((lecture = readdir (rep))) {
        struct stat st;
 
        stat (lecture->d_name, &st);
        if (!(strcmp(lecture->d_name,".")==0 || strcmp(lecture->d_name,"..")==0)){
            enfiler_tri_fichier(&(lecture->d_name),tete_fic,queue_fic,256*sizeof(char));
            c++;
        }
      }
      closedir (rep), rep = NULL;
   }
   return c;
}


void affiche_fic(file_p tete,int choix,char ** fic_choisi)
{
    int i=0;
    while(tete !=NULL) {
        //char *a = malloc(256*sizeof(char));
        char *a = tete->data;

        if (i==choix){
            printf(INV "%s\n" RESET,a);
            strcpy(*fic_choisi,a);
        } else
            printf("%s\n",a);

        tete=tete->suite;
        i++;
    }
}



void enfiler_tri_fichier(const void * data,file_p *tete,file_p *queue,size_t size)
{

    file_p file_element = NULL;
    file_element = (file_p)calloc(1,sizeof(file));

    if (file_element == NULL){
        fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
        exit(EXIT_FAILURE);
    }

    file_element->data = malloc(size);
    memcpy(file_element->data,data,size);

    char* insertion = file_element->data;
    file_p maillon_courant = *tete;
    file_p maillon_precedent = *tete;
    if (*tete == NULL){
        *tete = file_element;
        *queue = file_element;
        return;
    }
    char* courant = maillon_courant->data;
    if (strcmp(insertion,courant)<=0) {
        if (*tete == NULL && *queue == NULL) {
            *tete = file_element;
            *queue = file_element;
        }
        
        else {
            file_element->suite = *tete;
            (*tete) = file_element;
        }

    } else {
        while (maillon_courant != NULL) {
            maillon_precedent = maillon_courant;
            maillon_courant = maillon_courant->suite;

            if (maillon_courant == NULL) {
                (*queue)->suite = file_element;
                *queue = file_element;
                file_element->suite = NULL;
            } else {
                char* courant = maillon_courant->data;
                if (strcmp(insertion,courant)<=0){
                    maillon_precedent->suite = file_element;
                    file_element->suite = maillon_courant;
                    return;
                }
            }
             
        }
    }

}


void vider_liste_fic(file_p *tete,file_p *queue,int nb_fic)
{
    char ** inventaire_nom_fic = calloc(nb_fic,sizeof(char*));

    for (int i = 0; i < nb_fic; ++i)
        inventaire_nom_fic[i] = calloc(256,sizeof(char));

    for (int i=0; i < nb_fic; ++i)
        defiler(tete,queue,inventaire_nom_fic[i],sizeof(inventaire_nom_fic[i]));

    nettoyer(tete,queue);

    for (int i = 0; i < nb_fic; ++i)
        free(inventaire_nom_fic[i]);
    free(inventaire_nom_fic);

}
