#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>

#include "tuile.h"
#include "grille.h"
#include "tour.h"
#include "affichage.h"
#include "test.h"
#include "couleur.h"


void ajout_placement(pplacement p, int idm, int nbrcasesm, int orientationm, int xm, int ym)
{
	p->id=idm;
	p->nbrcases=nbrcasesm;
	p->orientation=orientationm;
	p->x=xm;
	p->y=ym;
}

void modif_nbr_cases (ptour t, int id)
{
	if (id != -1)
		t->index[recherche_id(t, id)]->nbrcases=(t->index[recherche_id(t, id)]->nbrcases)-1;
}

void modif_index(ptour  precedent,ptour p, int orientation, int x, int y,int id)
{
	int i;
	for (i=0;i<precedent->sz_index;i++)
	{
		int id2=precedent->index[i]->id;
		int nbr2=precedent->index[i]->nbrcases;
		int orientation2=precedent->index[i]->orientation;
		int x2=precedent->index[i]->x;
		int y2=precedent->index[i]->y;
		ajout_placement(p->index[i],id2,nbr2,orientation2,x2,y2);
	}

	ajout_placement(p->index[precedent->sz_index],id,6,orientation,x,y);
	
	if (orientation==0 || orientation==2)
	{
		modif_nbr_cases(p,precedent->g->cells[x][y].id);
		modif_nbr_cases(p,precedent->g->cells[x][y+1].id);
		modif_nbr_cases(p,precedent->g->cells[x+1][y].id);
		modif_nbr_cases(p,precedent->g->cells[x+1][y+1].id);
		modif_nbr_cases(p,precedent->g->cells[x+2][y].id);
		modif_nbr_cases(p,precedent->g->cells[x+2][y+1].id);
		
	}
	else
	{
		modif_nbr_cases(p,precedent->g->cells[x][y].id);
		modif_nbr_cases(p,precedent->g->cells[x][y+1].id);
		modif_nbr_cases(p,precedent->g->cells[x][y+2].id);
		modif_nbr_cases(p,precedent->g->cells[x+1][y].id);
		modif_nbr_cases(p,precedent->g->cells[x+1][y+1].id);
		modif_nbr_cases(p,precedent->g->cells[x+1][y+2].id);
	}
	
}

ptour create_tour_vide(int sz_grille, int nb_tuile){
	int i;
	ptour t=(ptour) malloc(sizeof(tour));

	if (t == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}


	grille_p h =create_grille_vide(sz_grille);
	t->g=h;

	pplacement * p= (pplacement *)malloc(nb_tuile*sizeof(pplacement));

	if (p == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}


	for (i = 0; i < nb_tuile; ++i)
	{
		p[i] = (pplacement)malloc(sizeof(placement));

		if (p[i] == NULL){
			fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
			exit(EXIT_FAILURE);
		}

	}

	t->index=p;
	t->sz_index=nb_tuile;
	
	return t;
}

ptour create_tour_vide_random(int nb_tuile){
	int i;
	ptour t=(tour*) malloc(sizeof(tour));

	if (t == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}

	grille_p h =create_grille_random();
	pplacement * p= (pplacement *)malloc(nb_tuile*sizeof(pplacement));

	if (p == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}


	for (i = 0; i < nb_tuile; ++i)
	{
		p[i] = (pplacement)malloc(nb_tuile*sizeof(placement));

		if (p[i] == NULL){
			fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
			exit(EXIT_FAILURE);
		}

	}
	
	t->g=h;
	t->sz_index=nb_tuile;
	t->index=p;
	return t;
}

void free_tour(ptour t) {
	int i;
	free_grille(t->g);
	for (i = 0; i < t->sz_index; ++i)
	{
		free(t->index[i]);
	}

	free(t->index);

	free(t);
}
