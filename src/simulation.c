#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "rwfile.h"
#include "affichage.h"
#include "grille.h"
#include "test.h"
#include "parcours.h"
#include "comptage.h"
#include "file.h"
#include "tuile.h"
#include "tour.h"
#include "simulation.h"
#include "couleur.h"

#define NB_TUILES 12
#define TAILLE_MAP 10
#define TAILLE_ECHANTILLON 100


void simulation_point(){
	int pts_villes=0;
	int pts_us_res=0;
	int pts_lacs=0;
	int pts_forets=0;
	int pts_plaines=0;
	int somme=0;
	int nombre_test=TAILLE_ECHANTILLON;
	int regle = 4;

	while (nombre_test>0){
		ptuile t0 = init_tuile_random(-2);
		parcours_p partie_de_jeu =  create_tour(t0,TAILLE_MAP/2-1,TAILLE_MAP/2-1,1,TAILLE_MAP);
		int nb_t=0;
        //grille_p grille_nav = NULL;
		srand(time(NULL));
		sleep(1);
		int x=0;
		int y=0;
		int o=0;
		int compteur=0;
		ptuile* tab_tuile = init_tuile_random_tab(NB_TUILES);

		while(nb_t<NB_TUILES)
		{
			ptuile t = tab_tuile[nb_t];
			x = rand()%(TAILLE_MAP-1);
			y = rand()%(TAILLE_MAP-1);
			o = 0;
			compteur=0;
            //affichage_tour(partie_de_jeu->tour);
            //printf("%d et %d\n",x,y);
			while(!add_tour(&partie_de_jeu,t,x,y,o,TAILLE_MAP) && compteur<1000){ 
				o ++;
				compteur++;
				if (o>3){
					x = rand()%(TAILLE_MAP-1);
					y = rand()%(TAILLE_MAP-1);
					o = 0;
				}
                //printf("%d\n",o);
                //grille_nav = grille_evolution(partie_de_jeu->tour->g);
                //insert_tuile_no_limit(grille_nav,x,y,t,o);
                //affichage_grille_no_limit(grille_nav,t->id,1);
                //free(grille_nav);
                //printf("%d et %d\n",x,y);
                //printf("%d\n",compteur);
			}
			
			affichage_tour(partie_de_jeu->tour,regle);
			free(t);
			nb_t++;
		}
		free(t0);
		points pts = comptage(partie_de_jeu->tour->g,regle,1);
		pts_villes+=pts.points_villes;
		pts_us_res+=pts.points_r_and_u;
		pts_lacs+=pts.points_lacs;
		pts_forets+=pts.points_forets;
		pts_plaines+=pts.points_plaines;
		somme+=pts.somme;
        //affichage_grille(partie_de_jeu->tour->g,1);
		free(tab_tuile);
		remove_all_tour(&partie_de_jeu);

		nombre_test--;
        //printf("nb = %d\n",nombre_test);
	}

	printf("villes : %d et %f\n",pts_villes,(float)pts_villes/(float)somme);
	printf("usines et ressources : %d  et %f\n",pts_us_res,(float)pts_us_res/(float)somme);
	printf("lacs : %d  et %f\n",pts_lacs,(float)pts_lacs/(float)somme);
	printf("forets : %d  et %f\n",pts_forets,(float)pts_forets/(float)somme);
	printf("plaines : %d  et %f\n",pts_plaines,(float)pts_plaines/(float)somme);


}
