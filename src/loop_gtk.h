#ifndef _LOOP_GTK_
#define _LOOP_GTK_

#include <gtk-3.0/gtk/gtk.h>
#include "parcours.h"
#include "file.h"

/**
 * @file loop_gtk.h
 * 
 * Ce fichier décrit l'ensemble des fonctions permettant de réaliser une boucle de jeu. 
 * Il contient 6 fonctions et 5 types.
 * 
 * - la structure \a init_att contient toutes les valeurs nécessaires pour une boucle de jeu
 * - la structure \a appWindows contient une application et une fenêtre pour les fonctions qui en ont besoin
 * - la structure \a data_solveur_gtk contient les donnees necessaires au solveur
 * 
 * - initialisation_parametres_jeu(GtkWideget *widget, gpointer data)   affiche les tuiles disponibles
 * - nbr_cases_modif_select(GtkRange *range, gpointer user_data)        enregister la nouvelle valeur du nombre de cases 
 * - nbr_tuiles_modif_select(GtkRange *range, gpointer user_data)       enregister la nouvelle valeur du nombre de tuiles
 * - loop_init(GtkApplication *app, gpointer user_data) 		        initialise la boucle de jeu
 * - gtk_creation_credits(GtkWidget *button, appWindow args) 	        affiche les crédits du jeu
 * - end_credits(GtkWidget *button)							 	        relance le jeu et retourne au menu
 * - creation_jeu_random(gpointer user_data)                            initialise le jeu
 * - loop_anim(gpointer user_data)                                      boucle de jeu lorsqu'une tuile est selectionnee
 * - loop(gpointer user_data)                                           boucle de jeu lorsque aucune tuile n'est selectionnee
 * - conclusion_jeu(gpointer user_data)                                 termine le jeu et affiche le resultat du jeu
 */

/**
 * @struct 	appWindow
 * @brief 	structure contenant une référence vers une application de une fenêtre de cette application
 *
 * Ces arguments sont : GtkApplication  *app    l'application du jeu
 *                      GtkWidget       *window la fenêtre du jeu
 */
typedef struct appWindow * appWindow;
struct appWindow {
	GtkApplication  *app;
	GtkWidget       *window;
};

/**
 * @struct 	init_att
 * @brief 	structure définissant l'ensemble des arguments necessaires pour une boucle de jeu
 *
 * Ces arguments sont : parcours_p  parc        le parcours de jeu
 *                      file_p      tete        la tête de la file des tuiles dans la main
 *                      file_p      queue       la queue de la file des tuiles dans la main
 *                      int*        sz_main     nombre de tuiles dans la main
 *                      int*        sz_grille   taille de la grille de jeu
 *                      int*        nbr_tour    le nombre de tour actuel
 *                      appWindow   fenetre     les données de la fenêtre de jeu 
 */ 
typedef struct _init_att * init_att;
struct _init_att{
	parcours_p  parc;
	file_p      tete;
	file_p      queue;
	int*        sz_main;
	int*        sz_grille;
    int*        nbr_tour;
    appWindow   fenetre;
};

/**
 * @struct  data_solveur_gtk
 * @brief   structure contenant toutes les informations necessaires à la réalisation du solveur
 *
 * Ces arguments sont : int             sz_main         la taille de la main du jeu
 *                      int             sz_grille       la taille de la grille du jeu
 *                      int             nbr_tour        le nombre de tour actuel
 *                      file_p          main_jeu_tete   la tête de la file des tuiles dans la main
 *                      parcours_p      jeu             le jeu actuel
 *                      resultat        result          le résultat actuel du solveur
 *                      GtkApplication  *app            la référence à l'application du jeu
 */
typedef struct _data_solveur_gtk * data_solveur_gtk;
struct _data_solveur_gtk {
    int             sz_main;
    int             sz_grille;
    int             nbr_tour;
    file_p          main_jeu_tete;
    parcours_p      jeu;
    resultat        result;
    GtkApplication  *app;
    int             isSet; 
};

/**
 * @struct  grid_item_tuiles
 * @brief   structure contenant une grille et la tuile contenue dans cette grille
 *
 * Ces arguments sont : GtkWidget   *event_box  l'even_box box associée
 *                      ptuile      tuile       la tuile contenue dans la grille
 *                      gulong      handlerID  l'handlerID de l'event_box
 *                      int         index       l'index de la tuile sélectionnée
 */
typedef struct _grid_item_tuiles * grid_item_tuiles;
struct _grid_item_tuiles {
    GtkWidget   *event_box;
    ptuile      *tuile;
    gulong      handlerID;
    int         index;
};

/**
 * @struct  data_loop_gtk
 * @brief   contient toutes les varaibles necessaires au bon déroulement de l'animation du jeu
 *
 * Ces arguments sont : init_att            attr            les attibuts de départs du jeu
 *                      grid_item_tuiles    tab_grid_tuiles contient toutes les tuiles restantes avec la grille le contenant
 *                      ptuile              tuile           la tuile sélectionnée par le joueur
 *                      int                 i               position de la tuile sélectionnée en i
 *                      int                 j               position de la tuile sélectionnée en j
 *                      int                 r               indique la rotation de la tuile
 *                      GtkWidget           *box            box principale de la fenêtre
 *                      GtkWidget           *box_externe    box contenant la sélection des tuiles
 *                      GtkWidget           *grid           contient la grille de jeu principale
 *                      
 */
typedef struct _data_loop_gtk * data_loop_gtk;
struct _data_loop_gtk {
    init_att            attr;
    grid_item_tuiles    *tab_grid_tuiles;
    ptuile              tuile;
    int                 i;
    int                 j;
    int                 r;
    GtkWidget           *box;
    GtkWidget           *box_externe;
    GtkWidget           *grid;
    GtkAccelGroup       *accel_group_solv;
    GtkAccelGroup       *accel_group;
    GClosure            *closure[8];
    data_solveur_gtk    solveur;
};

/**
 * @brief 				réalise l'initialisatione de la boucle de jeu
 * @param 	app 		l'application sur laquel sera lancé la fenêtre du menu principal
 * @param 	user_data 	une structure contenant toutes les valeurs qui vont être initialisées par la fonction
 * @return 				rien
 */
void loop_init(GtkApplication* app, gpointer user_data);

/**
 * @brief 		    
 * @param 	widget 	un bouton
 * @param 	data 	les valeurs à afficher
 * @return 			rien
 */
void initialisation_parametres_jeu(GtkWidget *widget, gpointer data);

/**
 * @brief 			créer une fenêtre qui affiche les crédits du jeu
 * @param 	button 	le bouton sur lequel on a cliqué
 * @param 	args 	l'application et la fenêtre sur lesquels on affiche les crédits
 * @return 			rien
 */
void gtk_affichage_credits(GtkWidget *button, appWindow args);

/**
 * @brief 			relance le jeu
 * @param 	button 	le bouton sur lequel on a cliqué
 * @return 			void
 */
void end_credits();

/**
 * @brief           créer une fenêtre qui permet de choisir le nombre de tuiles et de grille présent dans le jeu pour le choix aléatoire
 * @return          void
 */
void gtk_aleatoire_choix(gpointer data);

/**
 * @brief               enregistre la nouvelle valeur du nombre de cases du tableau
 * @param   range       le curseur sur qui émit le signal
 * @param   user_data   les données de l'utilisateurs
 * @return              void
 */
void nbr_cases_modif_select(GtkRange *range, gpointer user_data);

/**
 * @brief               enregistre la nouvelle valeur du nombre de tuiles du tableau
 * @param   range       le curseur sur qui est émit le signal
 * @param   user_data   les données de l'utilisateurs
 * @return              void
 */
void nbr_tuiles_modif_select(GtkRange *range, gpointer _data);

/**
 * @brief               initialise le jeu
 * @param   user_data   les donnees du jeu
 * @return              void
 */
void creation_jeu_random(gpointer user_data);

/**
 * @brief               boucle du jeu lorsque la tuile est en cours de deplacement sur la grille de jeu
 * @param   user_data   l'ensemble des donnees du jeu
 * @return              void
 */
void loop_anim(gpointer user_data);

/**
 * @brief               boucle du jeu lorsqu'aucune tuile n'est selectionnee
 * @param   user_data   les donnees du jeu
 * @return              void
 */
void loop(gpointer user_data);

/**
 *  @brief              termine le jeu est affiche le score
 *  @param  user_data   les donnees du jeu
 *  @return             void
 */
void conclusion_jeu(gpointer user_data);

#endif
