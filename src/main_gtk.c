#include <gtk-3.0/gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "tuile.h"
#include "rwfile.h"
#include "affichage.h"
#include "grille.h"
#include "test.h"
#include "parcours.h"
#include "comptage.h"
#include "loop_gtk.h"


int main(int argc, char* argv[]) {

	GtkApplication *app = NULL;
	int status;
	int sz_main;
	int sz_grille;
    int nbr_tour;
	file_p main_jeu_tete = NULL, main_jeu_queue = NULL;
	parcours_p parc = NULL;

	init_att att    = (init_att)malloc(sizeof(struct _init_att));
	att->parc       = parc;
	att->tete       = main_jeu_tete;
	att->queue      = main_jeu_queue;
	att->sz_grille  = &sz_grille;
	att->sz_main    = &sz_main;
    att->nbr_tour   = &nbr_tour;

	app = gtk_application_new("honshu.app", G_APPLICATION_FLAGS_NONE);
	g_signal_connect(app, "activate", G_CALLBACK(loop_init), (gpointer) att);
    status = g_application_run(G_APPLICATION(app), argc, argv);
    if (status) {
        fprintf(stderr, "%s(%s) : error %d while starting this app\n", argv[0], __func__,  status);
        return status;
    }
    g_object_unref(app);
    free(att);

	return EXIT_SUCCESS;
}
