#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "tuile.h"
#include "rwfile.h"
#include "affichage.h"
#include "grille.h"
#include "test.h"
#include "tour.h"
#include "parcours.h"
#include "file.h"
#include "comptage.h"
#include "optimisation.h"
#include "test_cunit_lot_C.h"
#include "couleur.h"

#include "aetoile.h"

void a_etoile(int nb_tuile_restante,int nombre_tuile_totale,int choix,file_p tete_file_tuile,parcours_p * partie,resultat * res, int regle)
{


    /***************Étape 1 : création du tas****************/

    open_tas_p fp = creation_fp(nb_tuile_restante); /*Création du tas*/
    /*printf("nb tuile : %d\n",nb_tuile);*/



    /***************Étape 2 : création des différents tableaux récapitulatifs****************/

    int * tab_pred = NULL, * tab_pos_noeud = NULL;

    tab_pred = allocation_dynamique_tableau(tab_pred,nombre_tuile_totale+1);/*tabpred["id de la tuile posée"]= "id de la tuile précédemment posée".*/
    tab_pos_noeud = allocation_dynamique_tableau(tab_pos_noeud,nombre_tuile_totale+1);/*Prend la position de la tuile dans le tas tab_pos_noeud[tuile] = "position dans le tas de la tuile". Tableau qui permet d'aller plus vite */

    /***************Étape 3 : Intialisation de nos variables****************/

    int id_courant = (*partie)->tour->index[(*partie)->tour->sz_index-1]->id;
    /*affichage_tour(partie->tour);*/
    int heuri = points_heuri1(tete_file_tuile,id_courant,(*partie)->tour,regle);
    //printf("heuri debut %d\n",heuri);
    //printf("passé : heuri %d\n",heuri); 
    int cout = comptage((*partie)->tour->g,regle,0).somme;
    //printf("points debut %d\n",cout);
    //printf("passé : cout %d\n",cout);

    insertion_tas(id_courant,partie,&heuri,&cout,&fp,tab_pos_noeud); /*On insère la premiere tuile ainsi que la première partie dans le tas*/
    /*afficher_tas(fp);*/

    tab_pred[id_courant] = -1;

    int cout_tire=0;
    int id_choix;
    int fin=0;
    *partie = NULL;


    while ((fp->taille > 0) && fin == 0)  /*Tant que le tas n'est pas vide ou que l'on a pas trouvé la meilleure solution*/
    {
        if(fin==0)
        {
            remove_all_tour(partie); 
        }

        *partie = NULL;
        id_courant = extraction_tas(&fp,&cout_tire,partie,tab_pos_noeud); /*On extraie la situation qui nous donne le plus de point*/
        
        
        file_p tete_file_tuile_copie = tete_file_tuile;/*On prépare la file de tuile*/

        ptuile *t_choix;/*Conteneur pour les tuiles défilées*/
        if ((*partie)->tour->sz_index == nombre_tuile_totale+1 )
        {
            fin = 1;
        }

        while(tete_file_tuile_copie != NULL && fin==0)/*On fait défiler toute les tuiles de manière à mettre à jour le tas pour la prochain départ!*/
        {
            t_choix = tete_file_tuile_copie->data; /*On récupère la première tuile courante */

            id_choix = (*t_choix)->id;           

            if (recherche_id((*partie)->tour,id_choix) == -1 ) /*Si on a trouvé une tuile pas encore posée*/
            {
                parcours_p partie_tire = NULL;
                copie_partie(partie,&partie_tire);

                if (calcul_distance(&heuri,&partie_tire,&cout_tire,t_choix,choix,tete_file_tuile,id_choix,regle))
                {
                    recherche_tas(tab_pred,&fp,&heuri,&partie_tire,&cout_tire,id_courant,id_choix,tab_pos_noeud);/*... et on met à jour le tas*/
                }

                else
                {
                    remove_all_tour(&partie_tire);
                }                
            }

            tete_file_tuile_copie = tete_file_tuile_copie->suite;
        }

        /*afficher_tas(fp);*/

    }
    if (choix == 0)
    {
        res->description = "technique de A * modifiée avec heuristique";
    }
    else
    {
        res->description = "technique de A * modifiée sans heuristique";
    }
    
        
    res->points_partie = cout_tire;
    res->partie_opti = *partie;

    destruction_fp(fp);

    free(tab_pos_noeud);
    free(tab_pred);

    //nettoyer(&tete_file_tuile,&queue_file_tuile);
    
}


int calcul_distance(int* heuri,parcours_p * partie, int *cout_tire,ptuile *t_choix, int choix_heuristique,file_p tete_file_tuile, int id_choix, int regle)
{

    couple c = {0,0,0};
    int cout_tire_copie = 0;
    int retour = 0;
    c=max_position(partie,*t_choix,&cout_tire_copie,regle); /*On met à jour le coût*/

    if (cout_tire_copie)
    {
        *cout_tire = cout_tire_copie;
    }
    if(!remplace_tour(partie,*t_choix, c.x, c.y, c.r,(*partie)->tour->g->size)){
        retour = 0;
    } 
    else{
        retour = 1;
    }
    /*affichage_tour((*partie)->tour);*/

    if (choix_heuristique == 1)/* Sans heuristique*/
    {
        *heuri = 0;
    } 

    else /*Avec heuristique*/
    {
        *heuri = points_heuri1(tete_file_tuile,id_choix,(*partie)->tour,regle);
        /*printf("heuri : %d et points : %d\n",*heuri,*cout_tire);*/
    } 

    return retour;
}

int * allocation_dynamique_tableau(int* tab, int taille)
{
    tab = (int*)calloc(taille,sizeof(int));
    if ( tab == NULL)
    {
        fprintf(stderr, "Erreur allocation dynamique dans allocation_dynamique_tableau\n");
        exit(EXIT_FAILURE);
    }

    return tab;
}

void recherche_tas(int *tab_pred,open_tas_p *fp,int *heuri,parcours_p *partie,int *cout,int id_courant,int id_choix, int *tab_pos_noeud)
{
    int present = 0;

    if (tab_pos_noeud[id_choix] != 0) /*Si le noeud est déjà présent dans le tas ...*/
    {
        if ( ((*fp)->tas[tab_pos_noeud[id_choix]]).cout <= *cout)/*... et si il a un cout plus grand*/
        {
    		/*On met à jour*/
            ((*fp)->tas[tab_pos_noeud[id_choix]]).cout = *cout;
            ((*fp)->tas[tab_pos_noeud[id_choix]]).heuri = *heuri;
            ((*fp)->tas[tab_pos_noeud[id_choix]]).cout_heuri = *heuri + *cout;
            if((*fp)->taille !=0)
            {
                remove_all_tour(&((*fp)->tas[tab_pos_noeud[id_choix]]).partie_en_cours);
            }            
            ((*fp)->tas[tab_pos_noeud[id_choix]]).partie_en_cours = *partie;
            tab_pred[id_choix] = id_courant+1;
            modification_tas(tab_pos_noeud[id_choix],fp,tab_pos_noeud);

            return;
        }   

        present +=1;
    }

    else if (((*fp)->tas[0]).id == id_choix && (*fp)->taille !=0) /*Même chose si le noeud est la racine ...*/
    {
        if ( ((*fp)->tas[0]).cout <= *cout)/*... et a un cout plus grand*/
        {
    		/*On met à jour*/
            ((*fp)->tas[0]).cout = *cout;
            ((*fp)->tas[0]).heuri = *heuri;
            ((*fp)->tas[0]).cout_heuri = *heuri + *cout;
            if((*fp)->taille !=0)
            {
                remove_all_tour(&(((*fp)->tas[0]).partie_en_cours));
            }
            ((*fp)->tas[0]).partie_en_cours = *partie;
            tab_pred[id_choix] = id_courant+1;
            modification_tas(0,fp,tab_pos_noeud);

            return;
        }   

        present +=1;
    }

    if (present)/*Si le noeud est présent dans le tas avec un cout plus petit, on ne fait rien*/
    {
        remove_all_tour(partie);
        /*printf("on fait rien\n");*/
        return;
    }

    else/*Si il n'est pas déjà présent, on l'ajoute au tas*/
    {
        tab_pred[id_choix] = id_courant+1;
        insertion_tas(id_choix,partie,heuri,cout,fp,tab_pos_noeud);
        return;
    }
}

int extraction_tas(open_tas_p *fp,int *cout_tire,parcours_p *partie, int *tab_pos_noeud)
{
    int max = ((*fp)->tas[0]).id; /*On enlève le dernier noeud du tas et on le met à la place du premier que l'on retire du tas*/
    *partie = ((*fp)->tas[0]).partie_en_cours; /*On recupère la partie en cours*/
    *cout_tire = ((*fp)->tas[0]).cout;/*On recupère les points de cette partie*/

    int v = (*fp)->tas[(*fp)->taille-1].cout_heuri;
    int ancien = (*fp)->taille-1;

    tab_pos_noeud[((*fp)->tas[0]).id] = 0;
    (*fp)->tas[0] = (*fp)->tas[(*fp)->taille-1];
    tab_pos_noeud[((*fp)->tas[(*fp)->taille-1]).id] = 0;
    (*fp)->taille --;

    int i = 0;

    while (2*i+1 < (*fp)->taille)/*Tant que le père est plus grand que l'un de ses deux fils */
    {
        int j = 2*i+1;
        if ( (j +1 < (*fp)->taille) && ( (*fp)->tas[j+1].cout_heuri > (*fp)->tas[j].cout_heuri) )/*Si l'un des deux fils est plus petit*/
        {
            j++;
        } 
        if ( v > (*fp)->tas[j].cout_heuri)/*Si le père est plus petit qu ses deux fils*/
            break;

        (*fp)->tas[i] = (*fp)->tas[j];
        tab_pos_noeud[((*fp)->tas[i]).id] = i;
        i = j;           
    }

    (*fp)->tas[i] = (*fp)->tas[ancien];/*On place l'ancienne racine à sa nouvelle place*/
    tab_pos_noeud[((*fp)->tas[i]).id] = i;
    return max;
} 


void insertion_tas(int id,parcours_p * partie,int *heuri,int*cout,open_tas_p *fp, int *tab_pos_noeud)
{
    int largeur = (*fp)->taille;
    int poids = *heuri + *cout;

    (*fp)->taille +=1;

    while (largeur > 0 && (*fp)->tas[(largeur-1)/2].cout_heuri <= poids)/*Tant que le noeud à remonter est plus grand que son père on le remonte*/
    {
        (*fp)->tas[largeur] = (*fp)->tas[(largeur-1)/2];
        tab_pos_noeud[((*fp)->tas[largeur]).id] = largeur;
        largeur = (largeur-1)/2;
    }

    ((*fp)->tas[largeur]).id = id;/*On met à jour les infos du noeud ajoutées au tas dans sa nouvelle place*/
    ((*fp)->tas[largeur]).cout = *cout;
    ((*fp)->tas[largeur]).heuri = *heuri;
    ((*fp)->tas[largeur]).cout_heuri = *heuri + *cout;
    ((*fp)->tas[largeur]).partie_en_cours = *partie;
    tab_pos_noeud[((*fp)->tas[largeur]).id] = largeur;
}

void modification_tas(int pos_fils,open_tas_p *fp,int *tab_pos_noeud)
{
    int largeur = pos_fils;
    info ret = (*fp)->tas[largeur];
    int poids = (*fp)->tas[largeur].cout_heuri;

    while (largeur > 0 && (*fp)->tas[(largeur-1)/2].cout_heuri <= poids)/*Tant que le noeud à remonter est plus grand que son père on le remonte*/
    
    {
        (*fp)->tas[largeur] = (*fp)->tas[(largeur-1)/2];
        tab_pos_noeud[((*fp)->tas[largeur]).id] = largeur;
        largeur = (largeur-1)/2;
    }

    ((*fp)->tas[largeur]) = ret;/*On met à jour les infos du noeud ajouté au tas dans sa nouvelle place*/
    tab_pos_noeud[((*fp)->tas[largeur]).id] = largeur;
}


open_tas_p creation_fp(int nb_tuile)
{

    open_tas_p fp = malloc(sizeof(open_tas));

    if (fp == NULL)
    {
        fprintf(stderr, "Erreur allocation dynamique dans creation_fp\n");
        exit(EXIT_FAILURE);
    }

    info * tab = (info *)calloc(nb_tuile,sizeof(info));

    fp->tas = tab;
    fp->taille = 0;

    if (fp->tas == NULL)
        exit(EXIT_FAILURE);

    return fp;
}

void destruction_fp(open_tas_p fp)
{
    for (int i = 0; i < fp->taille; ++i) {
        remove_all_tour(&(fp->tas[i].partie_en_cours));
    }
    free(fp->tas);
    free(fp);
}


int points_heuri1(file_p tete_file_tuile,int id_choix,ptour tour, int regle)
{
    int total=0;
    int entre=0;
    int id_courant = 0;
    char terrain=' ';
    int tab_bilan[6]={0,0,0,0,0,0};
    ptuile *t_courant;
    while(tete_file_tuile != NULL)
    {
        t_courant = tete_file_tuile->data; 
        id_courant = (*t_courant)->id;
        if( id_courant != id_choix && recherche_id(tour,id_courant) == -1)
        {
            for (int j = 0; j < 6; ++j)
            {
                terrain = (*t_courant)->tab[j];
                
                switch (terrain)
                {
                case 'P':
                    tab_bilan[0]+=1;
                    break;
                case 'V':
                    tab_bilan[1]+=1;
                    break;
                case 'R':
                    tab_bilan[2]+=1;
                    break;
                case 'L':
                    tab_bilan[3]+=1;
                    break;
                case 'U':
                    tab_bilan[4]+=1;
                    break;
                case 'F':
                    tab_bilan[5]+=1;
                    break;
                }
            }

            entre=1;
        }

        tete_file_tuile = tete_file_tuile->suite;
        
    }

    if (entre==0){
        return 0;
    }

    total = pts_forets(regle,tab_bilan[5],0) + pts_r_and_u(regle,tab_bilan[4],tab_bilan[2],0) + (regle != 3 ? tab_bilan[1] : tab_bilan[1]+4) + (regle != 0 ? (tab_bilan[3]-1)*3 : tab_bilan[3]*2)+(tab_bilan[0] != 1 ? 0 : (tab_bilan[0]/4)*4);

    return total;
}



int points_heuri2(file_p tete_file_tuile,int id_choix,ptour tour,int regle)
{
    int total=0;
    int id_courant = 0;
    ptuile *t_courant = (ptuile *)calloc(1,sizeof(ptuile));
    while(tete_file_tuile != NULL)
    {
        t_courant = tete_file_tuile->data;
        id_courant = (*t_courant)->id;

        if( id_courant != id_choix && recherche_id(tour,id_courant) == -1)
        {

            total += valeur_tuile(t_courant,regle);
        }

        tete_file_tuile = tete_file_tuile->suite;
    }

    return total;
}



/*--------------------------------Fonction pour le débogage--------------------------------*/

void afficher_tas(open_tas_p fp, int regle)
{
    printf("\n************************\n");
    printf("\n****Affichage du tas****\n");
    printf("\n************************\n");
    int compteur = fp->taille,i;
    for (i = 0; i < compteur; ++i)
    {
        fprintf(stdout,"# %d (%d/%d/%d)-",(fp->tas[i]).id,(fp->tas[i]).cout_heuri,(fp->tas[i]).cout,(fp->tas[i]).heuri);
        affichage_tour((fp->tas[i]).partie_en_cours->tour,regle);
    }
    
    for (i = 0; i < compteur; ++i)
    {
        printf("%p\n",((fp->tas[i]).partie_en_cours));
    }
    fprintf(stdout,"\n");
    
    printf("\n************************\n");
    printf("\n**Fin affichage du tas**\n");
    printf("\n************************\n");
}
