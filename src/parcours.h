#ifndef _PARCOURS_
#define _PARCOURS_

#include "tuile.h"
#include "grille.h"
#include "tour.h"
/**
 * @file parcours.h
 * 
 * Ce fichier décrit un ensemble de fonctions concernant la grille de jeu.
 * Il contient 8 fonctions et 1 types.
 * 
 * - le type \a parcours définit un ensemble contenant un tour et un parcours. C'est un maillon de pile
 * 
 * - create_tour( tuile t,int x, int y, int o,int size) 						crée un parcours contenant sa première étape
 * - add_tour(parcours_p p, tuile_p t,int x, int y, int o,int size)				ajoute une nouvelle étape au parcours
 * - int remplace_tour(parcours_p *p, ptuile t,int x, int y,int o, int size);	remplace une partie de jeu
 * - copie_partie(parcours_p *origine,parcours_p *copie) 						copie une partie de jeu
 * - copie_index(parcours_p *origine,parcours_p *copie) 						copie un index 
 * - remove_tour(parcours_p p) 													enlève la dernière étape du parcours
 * - remove_all_tour2(parcours_p *p)											annule toutes les étapes du parcours sauf la première
 * - remove_all_tour(parcours_p p) 												annule toutes les étapes du parcours (reset)
 */


/**
 * \struct parcours
 * \brief structure de tour d'une partie
 *
 * Maillon de pile contenant une structure de tour de jeu et un pointeur vers le tour de jeu précédent
 */
typedef struct parcours
{
	struct parcours * precedent;
	ptour tour;
}parcours,*parcours_p;

/** 
 * @brief 			   crée un parcours contenant sa première étape
 * @param 		t 	   une tuile
 * @param 		x 	   entier positif
 * @param 		y 	   entier positif
 * @param 		o 	   entier entre 0 et 3 étant l'orientation de la tuile
 * @param 	 	size   un entier étant la taille de la grille
 * @return 			   le nouveau parcours créé
 */
parcours_p create_tour(ptuile t, int x, int y, int o, int size);

/** 
 * @brief         	  ajoute une nouvelle étape au parcours
 * @attention    	  il doit exister une étape 
 * @param   	p     le parcours
 * @param   	t     une tuile
 * @param   	x     entier positif
 * @param   	y     entier positif
 * @param   	o     entier entre 0 et 3 étant l'orientation de la tuile
 * @param   	size  un entier étant la taille de la grille
 * @return        	  1 si le recouvrement a réussi et 0 sinon 
 */
int add_tour(parcours_p *p, ptuile t, int x, int y, int o, int size);

/**
 * @brief 				copie une partie de jeu
 * @param  origine      un pointeur sur la partie d'origine
 * @param  copie 		un pointeur sur la partie copiée
 * @return              rien
 */
void copie_partie(parcours_p *origine,parcours_p *copie);

/**
 * @brief 				remplace une partie de jeu
 * @param  origine      un pointeur sur la partie d'origine
 * @param  copie 		un pointeur sur la partie copiée
 * @return              rien
 */
int remplace_tour(parcours_p *p, ptuile t,int x, int y,int o, int size);

/**
 * @brief 				copie un index
 * @param  origine      un pointeur sur la partie d'origine
 * @param  copie 		un pointeur sur la partie copiée
 * @return              rien
 */
void copie_index(parcours_p *origine,parcours_p *copie);

/** 
 * @brief         enlève la dernière étape du parcours
 * @attention     il doit exister une étape 
 * @param     p   le parcours
 * @return        rien
 */
void remove_tour(parcours_p *p);

/** 
 * @brief          annule toutes les étapes du parcours sauf la première
 * @attention      il doit exister une étape 
 * @param      p   le parcours
 * @return         rien
 */
void remove_all_tour2(parcours_p *p);

/** 
 * @brief         annule toutes les étapes du parcours (reset)
 * @attention     il doit exister une étape 
 * @param     p   le parcours
 * @return        rien
 */
void remove_all_tour(parcours_p *p);
 #endif
