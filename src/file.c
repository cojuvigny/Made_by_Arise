#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/ioctl.h>

#include "tuile.h"
#include "affichage.h"
#include "file.h"
#include "couleur.h"

void enfiler(const void * data,file_p *tete,file_p *queue,size_t size)
{
	file_p file_element = (file_p)malloc(sizeof(file));
	if (file_element == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}

	file_element->data = malloc(size);

	memcpy(file_element->data,data,size);
	file_element->suite = NULL;

	if (*tete == NULL && *queue == NULL)
	{
		*tete = file_element;
		*queue = file_element;

	}
	
	else
	{
		(*queue)->suite = file_element;
		(*queue) = file_element;
	}
}

void enfiler_tri(const void * data,file_p *tete,file_p *queue,size_t size)
{

	file_p file_element = NULL;
	file_element = (file_p)calloc(1,sizeof(file));

	if (file_element == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	}

	file_element->data = malloc(size);
	memcpy(file_element->data,data,size);

	ptuile* insertion = file_element->data;
	int id_insertion = (*insertion)->id;
	file_p maillon_courant = *tete;
	file_p maillon_precedent = *tete;
	if (*tete == NULL){
		*tete = file_element;
		*queue = file_element;
		return;
	}

	ptuile *courant = maillon_courant->data;
	int id_debut = (*courant)->id;
	if (id_insertion <= id_debut) {
		if (*tete == NULL && *queue == NULL) {
			*tete = file_element;
			*queue = file_element;
		} else {
			file_element->suite = *tete;
			(*tete) = file_element;
		}
	} else {
      while (maillon_courant != NULL) {
			maillon_precedent = maillon_courant;
			maillon_courant = maillon_courant->suite;
			if (maillon_courant == NULL) {
				(*queue)->suite = file_element;
				*queue = file_element;
				file_element->suite = NULL;
			} else {
				ptuile *courant = maillon_courant->data;
				int id_courant = (*courant)->id;
				if (id_insertion <= id_courant){
					maillon_precedent->suite = file_element;
					file_element->suite = maillon_courant;
					return;
				}
			}
			
		}
	}

}

int defiler_tri(file_p *tete,file_p *queue, void * sortie,size_t size,int id_a_defiler)
{
	if (*tete == NULL){
		return 1;
	}
	
	
	file_p maillon_courant = *tete;
	file_p maillon_precedent = *tete;

	ptuile *courant = maillon_courant->data;
	int id_debut = (*courant)->id;

	if (id_debut == id_a_defiler){

		if (maillon_courant == *queue){
			*queue=NULL;
		}
		memcpy(sortie,(*tete)->data,size);
		maillon_courant=(*tete)->suite;
		(*tete)->suite=NULL;
		free((*tete)->data);
		free(*tete);
		*tete = maillon_courant;
		return 0;
	}
	

	while (maillon_courant->suite != NULL)
	{
		maillon_precedent = maillon_courant;
		maillon_courant = maillon_courant->suite;

		ptuile *courant = maillon_courant->data;
		int id_courant = (*courant)->id;


		if (id_a_defiler == id_courant){
			if (sortie != NULL) memcpy(sortie,maillon_courant->data,size);

			if (maillon_courant == *queue){
				*queue=maillon_precedent;
			}

			maillon_precedent->suite = maillon_courant->suite;
			maillon_courant->suite = NULL;
			
			free(maillon_courant->data);
			free(maillon_courant);
			return 0;

		}            
	}

	return 1;
}


void defiler(file_p *tete,file_p *queue, void * sortie, size_t size)
{
	if (*tete!= NULL)
	{
		file_p file_element = *tete;

		memcpy(sortie,file_element->data,size);

		if (*tete == *queue)
		{
			*tete = NULL;
			*queue = NULL;
		}
		else
		{
			*tete = (*tete)->suite;
		}

		file_element->suite = NULL;
		free(file_element->data);
		free(file_element);
	}
}

int supprimer_elem(file_p *tete, void* elem) {
    int isDeleted = 0;
    if (*tete != NULL) {
        file_p prescedent = NULL;
        file_p current = *tete;
        while (current != NULL) {
            if (current->data == elem) {
                if (current == *tete) {
                    *tete = current->suite;
                    free(current);
                    isDeleted = 1;
                    break;
                }
                prescedent->suite = current->suite;
                free(current);
                isDeleted = 1;
                break;
            }
            prescedent = current;
            current = current->suite;
        }
    }
    return isDeleted;
}

int print_file_tuile(file_p tete,int choix,int nb_tuile)
{
	int i=0,j=0,k=0,l=0;
	int id_choisi=0;
	int fin =1;

	ptuile * tab_tuile = (ptuile *)malloc(nb_tuile*sizeof(ptuile));
	while(tete != NULL) {
		ptuile *a = tete->data;
		tab_tuile[i] = *a;
		tete=tete->suite;
		i++;
	}

	struct winsize w;
	ioctl(0, TIOCGWINSZ, &w);

	int nb_tuile_ligne = w.ws_col/9;
	int iter=0;
	int depart=0;
	printf("\n");

	while (fin) {
		if (nb_tuile <= nb_tuile_ligne){
			iter = nb_tuile;
			fin=0;
		}
		else{
			iter = nb_tuile_ligne;
			nb_tuile = nb_tuile-nb_tuile_ligne;
		}
		

		for (j = 0; j < iter; ++j)
		{
			
			if (j+depart==choix){
				printf(INV "n°:%2d " RESET,tab_tuile[depart+j]->id);
				id_choisi = tab_tuile[depart+j]->id;
			}

			else{
				printf("n°:%2d ",tab_tuile[depart+j]->id);
			}

			printf("   ");
			
		}
		printf("\n");
		k=0;

		while (k<5){
			for (l = 0; l < iter; ++l)
			{
				affichage_couleur(tab_tuile[depart+l]->tab[k],0);
				affichage_couleur(tab_tuile[depart+l]->tab[k+1],0);
				printf("   ");
			}
			printf("\n");
			k+=2;
		}

		depart+=nb_tuile_ligne;

	}

	free(tab_tuile);

	return id_choisi;
}

int cardinal (file_p tete)
{
	int count=0;
	while (tete!=NULL)
	{
		tete=tete->suite;
		count++;
	}
	return count;
}


int plus_petit(int a, int b)
{
	if (a<b){
		return a;
	}
	else
		return a-b;
}

void nettoyer(file_p *tete, file_p *queue)
{   
	while (!(*tete == NULL && *queue == NULL))
	{
		file_p file_element = *tete;

		if (*tete == *queue)
		{
			*tete = NULL;
			*queue = NULL;
		}
		else
		{
			*tete = (*tete)->suite;
		}
		
		file_element->suite = NULL;
		free(file_element->data);
		free(file_element);      
	}

}

void nettoyer_tuile(file_p *tete, file_p *queue)
{   
	ptuile *t_choix;

	while (!(*tete == NULL && *queue == NULL))
	{
		file_p file_element = *tete;

		if (*tete == *queue)
		{
			*tete = NULL;
			*queue = NULL;
		}
		else
		{
			*tete = (*tete)->suite;
		}
		
		file_element->suite = NULL;
      t_choix = file_element->data;
		
		free_tuile(*t_choix);
		free_tuile(file_element->data);
		free(file_element);      
	}

}

int ** allocation_dynamique_matrice(int ** mat,int size)
{
	int i;
	mat = (int **)calloc(size,sizeof(int *));

	if (mat == NULL){
		fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
		exit(EXIT_FAILURE);
	} 	


	for (i = 0; i < size; ++i)
	{
		mat[i] = (int *)calloc(size,sizeof(int));

		if (mat[i] == NULL){
			fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
			exit(EXIT_FAILURE);
		}

	}

	return mat;

}

int ** liberer(int ** mat,int size)
{
	for (int i = 0; i < size; ++i)
	{
		free(mat[i]);
	}

	free(mat);
	return mat;
}
