#ifndef _SIMULATION_
#define _SIMULATION_



/**
 * @file simulation.h
 * 
 * Ce fichier décrit un ensemble de fonctions permettant de réaliser des études de preconception du solveur
 * Il contient 1 fonction.
 * 
 * - simulation_point  simule une partie de manière naïve afin de voir le nombre de point moyen amrqué par type de terrain
 */



/**
 * @brief 	simule une partie de manière naïve afin de voir le nombre de point moyen marqué par type de terrain
 * @param 	aucun
 * @return 	aucun
 */
void simulation_point();




#endif
