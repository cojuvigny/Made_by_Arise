#ifndef _RWFILE_
#define _RWFILE_

#include "tuile.h"
#include "file.h"

/**
 * @file rwfile.h
 * 
 * Ce fichier décrit comment lire et écrire un ensemble de tuile à partir d'un fichier externe.
 * Il contient 10 fonctions, 1 type et une variable globale.
 *  
 * - le type \b info_grille définit les valeurs à savoir sur les grilles
 * 
 * - la variable globale \b nameProcessus permet d'utiliser le nom du processus dans tous les messages d'erreurs des fonctions
 * 
 * - read_file_tuile(char* name, int* size) 											crée un tableau de taille \b size de tuiles définie par le fichier \b name 
 * - creation_tuile(void* arg) 															crée la tuile t suivant la position start et l'enregistre dans le tableau t_tuile à la position donnée par le premier entier suivant start
 * - pos_bn(char* file, int size) 														écrit dans le fichier \b name les tuiles présentent dans le tableau t. Si le fichier n'existe pas il sera créé sinon il sera réécrit.
 * - write_file_tuile(char* name, ptuile* t, int len) 									lit le fichier \b name et enregistre les valeurs intéressantes dans une struct info_grille (voir definition struct info_grille pour plus de détails  
 * - read_file_grille(char* name, struct info* info_t) 									lit le fichier name et en extrait la taille de la grille, de la main, la tuile de départ, les ids des tuiles
 * - write_file_grille(char* name, struct info_grille info_t) 							lit le fichier name et en extrait la taille de la grille, de la main, la tuile de départ, les ids des tuiles
 * - compt_fic(char * name_d,file_p * tete_fic,file_p * queue_fic) 						donne le nombre de fichiers du repertoire courant et crée la liste de fichier
 * - affiche_fic(file_p tete,int choix,char ** fic_choisi)								affiche les fichiers du repertoire courant en mettant en surbrillance celui choisi
 * - enfiler_tri_fichier(const void * data,file_p *tete,file_p *queue,size_t size) 		enfile un nom de fichier dans la liste triée
 * - vider_liste_fic(file_p *tete,file_p *queue,int nb_fic) 							nettoye la file de fichier
 */


/**
 * @struct 	info_grille
 * @brief 	définit les valeurs à savoir sur les grilles :
 * - sz_grille : la taille de la grille
 * - sz_main   : le nombre de tuile dans la main de départ
 * - id_depart : la tuile de départ (posée sur la grille avant le début du jeu)
 * - ids       : les ids des tuiles utilisées dans le jeu
 */
struct info_grille {
	int sz_grille;
	int sz_main;
	int id_depart;
	int* ids;
};

/**
 * @brief 				crée un tableau de taille \b size de tuiles définie par le fichier \b name 
 * @param 		name 	un string correspondant à un ficher existant  
 * @param 		size 	l'adresse d'un entier
 * @attention 			name doit être un fichier existant et possédant toutes les caractéristiques d'un fichier de tuile. De plus il faut absolument que dans le fichier les ids des tuiles soient toutes comprises entre 0 et size - 1 sinon les résultats ne sont pas garantis
 * @return 				un tableau de tuiles d'entier
 */
ptuile* read_file_tuile(char* name, int* size);

/**
 * @brief 				crée la tuile t suivant la position start et l'enregistre dans le tableau t_tuile à la position donnée par le premier entier suivant start
 * @param 	  	arg 	une struct tuile_f
 * @attention 			arg doit être casté en void*
 * @return 				NULL
 */
void* creation_tuile(void* arg);

/**
 * @brief			crée un tableau de taille size contenant la position des '\.n' du fichier file
 * @param  	file 	un buffer de fichier
 * @param  	size 	la taille du fichier
 * @return 			un tableau d'entier de taille size dont l'élément d'indice i contient la position en byte du ième '\n' du fichier
 */
int* pos_bn(char* file, int size);

/**
 * @brief 				écrit dans le fichier \b name les tuiles présentent dans le tableau t. Si le fichier n'existe pas il sera créé sinon il sera réécrit.
 * @attention 			0 < size < 100
 * @param 		name 	le nom du fichier à créer
 * @param 		t 		une liste de tuiles 
 * @param 		len 	la taille de la liste de tuile
 * @return 				le nombre de byte total écrit
 */
int write_file_tuile(char* name, ptuile* t, int len);


/**
 * @brief 				lit le fichier \b name et enregistre les valeurs intéressantes dans une struct info_grille (voir définition struct info_grille pour plus de détails
 * @attention 			\b name doit exister et respecter le format d'un fichier de partie
 * @param 		name   	le nom du fichier à lire
 * @param 		info_t 	l'adresse d'une structure info_t
 * @return 				rien
 */
void read_file_grille(char* name, struct info_grille* info_t);


/**
 * @brief 			lit le fichier name et en extrait la taille de la grille, de la main, la tuile de départ, les ids des tuiles
 * @param 	name  	le nom du fichier à ouvrir
 * @param 	info_t 	toutes les infos sur la grille
 * @return 			rien
 */	
void write_file_grille(char* name, struct info_grille info_t);

/**
 * @brief 				donne le nombre de fichiers du repertoire courant et crée la liste de fichier
 * @param  	name_d    	le nom du dossier courant où sont localisés nos fichiers de tuiles ou de grilles
 * @param  	tete_fic  	la tête de la liste de fichiers
 * @param  	queue_fic 	la queue de la liste de fichiers
 * @return           	le nombre de fichier du repertoire courant
 */
int compt_fic(char * name_d,file_p * tete_fic,file_p * queue_fic);


/**
 * @brief 				affiche les fichiers du repertoire courant en mettant en surbrillance celui choisi
 * @param 	tete      	la tête de la liste de fichier
 * @param 	choix      	le choix de fichier en chaîne de caractères
 * @param 	fic_choisi 	le fichier choisi
 * @return 				rien
 */
void affiche_fic(file_p tete,int choix,char ** fic_choisi);


/**
 * @brief			enfile un nom de fichier dans la liste triée
 * @param 	data  	le fichier à rentrer
 * @param 	tete  	la tête de la liste de fichiers
 * @param 	queue	la queue de la liste de fichiers
 * @param 	size 	la taille du pointeur sur le nom de fichier
 * @return 			rien
 */
void enfiler_tri_fichier(const void * data,file_p *tete,file_p *queue,size_t size);


/**
 * @brief 			nettoye la liste de fichiers
 * @param 	tete   	la tête de la liste de fichiers
 * @param 	queue  	la queue de la liste de fichiers
 * @param 	nb_fic 	le nombre d'éléments de la liste
 * @return 			rien
 */
void vider_liste_fic(file_p *tete,file_p *queue,int nb_fic);

#endif
