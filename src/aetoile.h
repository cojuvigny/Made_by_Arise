#ifndef __AETOILE__
#define __AETOILE__

#include "tuile.h"
#include "rwfile.h"
#include "affichage.h"
#include "grille.h"
#include "test.h"
#include "tour.h"
#include "parcours.h"
#include "file.h"
#include "comptage.h"
#include "optimisation.h"
#include "test_cunit_lot_C.h"


/**
 * @file aetoile.h
 * 
 * Ce fichier décrit l'ensemble des fonctions du solveur avec "A* modifié". 
 * Il contient 12 fonctions et 2 types.
 *
 * - le type \a info définit une case du tas
 * - le type \a open_tas définit le tas
 * 
 * - a_etoile(int nb_tuile_restante,int nb_tuile_totale,int choix,file_p tete_file_tuile,parcours_p * partie,resultat * res, int regle);            						Aetoile modifié qui place les tuiles en fonction des points et d'une heuristique particulière
 * - calcul_distance(int* heuri,parcours_p * partie, int *cout_tire,ptuile *t_choix, int choix_heuristique,file_p tete_file_tuile,int id_choix, int regle);   
 calcule les points donnés par le rajout d'une tuile ainsi que l'heuristique et rajoute la tuile au tour de jeu
 * - allocation_dynamique_tableau(int* tab, int taille)																										allocation dynamique de l'espace mémoire pour un tableau
 * - recherche_tas(int *tab_pred,open_tas_p *fp,int *heuri,parcours_p *partie,int *cout,int id_courant,int id_choix, int *tab_pos_noeud) 					recherche de la présence d'une tuile dans le tas et modification ou non de sa place
 * - extraction_tas(open_tas_p *fp,int *cout_tire,parcours_p *partie, int *tab_pos_noeud)																	extraction de la partie de cout heuristique le plus élevé
 * - insertion_tas(int id,parcours_p * partie,int *heuri,int*cout,open_tas_p *fp, int *tab_pos_noeud)														insertion d'une nouvelle partie dans le tas
 * - modification_tas(int pos_fils,open_tas_p *fp,int *tab_pos_noeud)																						modification de la valeur d'un partie dans le tas
 * - destruction_fp(open_tas_p fp)																															destruction de la file de priorité
 * - creation_fp(int nb_tuile)																																création de la file de priorité
 * - points_heuri1(file_p tete_file_tuile,int id_choix,ptour tour, int regle)																				l'heuristique de la partie en cours
 * - points_heuri2(file_p tete_file_tuile,int id_choix,ptour tour, int regle) 														
 						l'heuristique de la partie en cours (moins efficace que la première)
 * - afficher_tas(open_tas_p fp, int regle)																															affichage la file de priorité
 */
 

/**
 * \struct info
 * \brief case du tas qui contient de nombreuses information sur la partie insérée
 *
 * Permet de sotcker toutes les infos nécesaires d'une partie notamment les points et heuristiques
 *
 */
typedef struct info 
{
   	int id;
    parcours_p partie_en_cours;
    int cout;
    int heuri;
    int cout_heuri;

}info, *pt_info;


/**
 * \struct open_tas
 * \brief open liste sous forme de tas
 *
 * Permet de sotcker toutes les parties envisagées par l'algortithme en mettant au sommet du tas celle possédant le meilleur cout heuristique
 *
 */
typedef struct open_tas
{
    int taille;
    pt_info tas;
    
}open_tas, *open_tas_p;



/**
 * @brief  							Aetoile modifié qui place les tuiles en fonction des points et d'une heuristique particulière
 * @param 	nb_tuile_restante       le nombre de tuiles restantes à poser
 * @param   nb_tuile_totale			le nombre de tuiles totales de la partie
 * @param 	choix           		si choix == 1, pas d'heuristique sinon une heuristique
 * @param 	tete_file_tuile 		la tête de la file de tuile à poser
 * @param 	partie          		le debut de partie avec une tuile deja posée
 * @param   res  					le resultat de l'optimisation
 * @param   regle		la regle du jeu choisie
 * @return  						rien
 */


void a_etoile(int nb_tuile_restante,int nb_tuile_totale,int choix,file_p tete_file_tuile,parcours_p * partie,resultat * res, int regle);

/**
 * @brief   					calcule les points donnés par le rajout d'une tuile ainsi que l'heuristique
 * @param  	heuri             	un pointeur vers l'heuristique calculée
 * @param  	partie            	la partie en cours
 * @param  	cout_tire         	le nombre de points actuels de la partie
 * @param  	t_choix           	la tuile à placer sut la grille
 * @param  	choix_heuristique 	le type d'heuristique choisie
 * @param 	tete_file_tuile 	la tête de la file de tuile à poser
 * @param  	id_choix          	l'id de la tuile choisie
 * @param   regle				la regle du jeu choisie
 * @return                   	1 si la tuile a pu être posée, 0 sinon
 */		
int calcul_distance(int* heuri,parcours_p * partie, int *cout_tire,ptuile *t_choix, int choix_heuristique,file_p tete_file_tuile, int id_choix, int regle);

/**
 * @brief  			allocation 	dynamique de l'espace mémoire pour un tableau
 * @param  	tab    	un tableau à alloué
 * @param  	taille 	la taille du tableau
 * @return        	le tableau alloué
 */
int * allocation_dynamique_tableau(int* tab, int taille);

/**
 * @brief  				recherche de la présence d'une tuile dans le tas et qui modification ou non de sa place
 * @param tab_pred      le tableau des prédécesseur de chaque tuile
 * @param fp            la file de priorité
 * @param heuri         l'heuristique courante
 * @param partie        la partie courante
 * @param cout          le nombre de points courant
 * @param id_courant    l'id de la tuile courante
 * @param id_choix      l'id de la tuile à placer
 * @param tab_pos_noeud le tableau des positions des tuiles dans le tas
 * @return        		rien
 */
void recherche_tas(int *tab_pred,open_tas_p *fp,int *heuri,parcours_p *partie,int *cout,int id_courant,int id_choix, int *tab_pos_noeud);


/**
 * @brief  				 extraction de la partie de cout heuristique le plus élevé
 * @param  fp            la file de priorité
 * @param  cout_tire     le nombre de points donné par cette partie
 * @param  partie        la partie courante			
 * @param  tab_pos_noeud le tableau des positions des tuiles dans le tas
 * @return               l'id de la dernière tuile posée
 */
int extraction_tas(open_tas_p *fp,int *cout_tire,parcours_p *partie, int *tab_pos_noeud);

/**
 * @brief 				insertion d'une nouvelle partie dans le tas
 * @param id            l'id de la dernière tuile posée
 * @param partie        la partie à insérer
 * @param heuri         le heuristique de cette partie
 * @param cout          le nombre de points de cette partie
 * @param fp            la file de priorité de cette partie
 * @param tab_pos_noeud le tableau des positions des tuiles dans le tas
 * @return        		rien
 */
void insertion_tas(int id,parcours_p * partie,int *heuri,int*cout,open_tas_p *fp, int *tab_pos_noeud);

/**
 * @brief  				modification de la valeur d'un partie dans le tas
 * @param pos_fils      la position de la partie dans le tas à modifier
 * @param fp            la file de priorité
 * @param tab_pos_noeud le tableau des positions des tuiles dans le tas
 * @return        		rien
 */
void modification_tas(int pos_fils,open_tas_p *fp,int *tab_pos_noeud);


/**
 * @brief  		destruction de la file de priorité
 * @param 	fp 	le file de priorité à détruire
 * @return 		rien
 */
void destruction_fp(open_tas_p fp);

/**
 * @brief  				création de la file de priorité
 * @param  nb_tuile 	le nombre de tuiles	
 * @return              le file de priorité
 */
open_tas_p creation_fp(int nb_tuile);

/**
 * @brief   				l'heuristique de la partie en cours
 * @param  tete_file_tuile	la tete de la file de tuile à poser
 * @param  id_choix   		l'id de la dernière tuile choisie
 * @param  tour      		le tour de jeu courant
 * @param  regle			la regle du jeu choisie
 * @return           		l'heuristique
 */
int points_heuri1(file_p tete_file_tuile,int id_choix,ptour tour, int regle);

/**
 * @brief   				l'heuristique de la partie en cours (moins efficace que la première)
 * @param  tete_file_tuile	la tete de la file de tuile à poser
 * @param  id_choix   		l'id de la dernière tuile choisie
 * @param  tour      		le tour de jeu courant
 * @param  regle			la regle du jeu choisie
 * @return           		l'heuristique
 */
int points_heuri2(file_p tete_file_tuile,int id_choix,ptour tour, int regle);


/**
 * @brief				affichage la file de priorité
 * @param 	fp 			la file de prioritié
 * @param   regle		la regle du jeu choisie
 * @return 				rien
 */	
void afficher_tas(open_tas_p fp, int regle);


#endif