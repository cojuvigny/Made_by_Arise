#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

#include "tuile.h"
#include "rwfile.h"
#include "affichage.h"
#include "grille.h"
#include "test.h"
#include "parcours.h"
#include "test_cunit_lot_B.h"
#include "file.h"
#include "solveur.h"
#include "loop.h"
#include "file.h"
#include "comptage.h"
#include "couleur.h"
#include "rwfile.h"
#include "aetoile.h"
#include "optimisation.h"




#define M 5

int setup(void)  { return 0; }
int teardown(void) { return 0; }

int initialisation_catalogue_B(void){

	if (CUE_SUCCESS != CU_initialize_registry())
		return CU_get_error();

	CU_pSuite pSuite = CU_add_suite("echantillon_test",setup,teardown);

	if(( NULL == CU_add_test(pSuite, "Test id tuile ", test_test_limitation)) ||
		( NULL == CU_add_test(pSuite, "Test recouvrement tuiles", test_test_recouvrement)) ||
		( NULL == CU_add_test(pSuite, "Test creation tableau tuile aleatoire ", test_enfiler) ) ||
		( NULL == CU_add_test(pSuite, "Test creation d'une tuile avec tableau d'entrée", test_defiler)))
		
	{
		CU_cleanup_registry();
		return CU_get_error();
	}

	CU_basic_run_tests();
	CU_basic_show_failures(CU_get_failure_list());
	CU_cleanup_registry();

	return 0;
}

/**********************/
/*****Test test.c******/
/**********************/



void test_test_limitation(void){
	ptuile t = init_tuile_random(1);
	ptuile tuile_copie = copie_tuile(t);
	int compt=0;

	grille_p g = create_grille_vide(15);
    /*Batteries de tests, tous faux, sur le bord de la map*/
	if(test_limitation(0,-1,15,0)){
		insert_tuile(g,7,7,tuile_copie,0);
		compt++;
	}

	if(test_limitation(13,0,15,0)){
		insert_tuile(g,13,0,tuile_copie,0);
		compt++;
	}

	if(test_limitation(0,14,15,2)){
		insert_tuile(g,7,7,tuile_copie,0);
		compt++;
	}

	if(test_limitation(13,13,15,2)){
		insert_tuile(g,7,7,tuile_copie,0);
		compt++;
	}

	CU_ASSERT_EQUAL(compt,0);

	free(t);
	free(tuile_copie);
	free_grille(g);
}


void test_test_recouvrement(void){

	int n = 10;
	int compt=0;
	char tab0[6] = {'V','V','V','R','R','R'};
	ptuile t0 = init_tuile_tab(tab0,0);

	parcours_p partie_de_jeu =  create_tour(t0,n/2-1,n/2-1,1,n);

	char tab1[6] = {'F','V','R','U','V','L'};
	ptuile t1 = init_tuile_tab(tab1,1);

	ptuile t2 = init_tuile_random(2);

	add_tour(&partie_de_jeu,t1,4,6,0,n);

    /*Test recouvrement total tuile impossible*/
	if(add_tour(&partie_de_jeu,t2,4,4,0,n)){
		compt++;
	}
	
    /*Test recouvrement lac impossible*/
	if(add_tour(&partie_de_jeu,t2,6,7,0,n)){
		compt++;
	}

    /*Test recouvrement case vide impossible*/
	if(add_tour(&partie_de_jeu,t2,0,0,0,n)){
		compt++;
	}

	free(t0);
	free(t1);
	free(t2);

	CU_ASSERT_EQUAL(compt,0);


	remove_all_tour(&partie_de_jeu);

}

void test_enfiler(void){
	file_p tete = NULL,queue = NULL;
	int compt = 0;
	int x = 2, y = 3;

	int entree[2] = {x,y};

	enfiler(entree,&tete,&queue,sizeof(entree));

	int sortie[2];

	
	memcpy(sortie,tete->data,sizeof(entree));

	if (sortie[0] == 2 && sortie[1] == 3)
		compt++;

	CU_ASSERT_EQUAL(compt,1);
	CU_ASSERT_PTR_NULL(tete->suite);

	nettoyer(&tete,&queue);

}

void test_defiler(void){
	file_p tete = NULL,queue = NULL;
	int x = 2, y = 3;
	int entree[2] = {x,y};

	int sortie[2];

	enfiler(entree,&tete,&queue,sizeof(sortie));

	defiler(&tete,&queue,sortie,sizeof(sortie));

	CU_ASSERT_EQUAL(sortie[0],2);
	CU_ASSERT_EQUAL(sortie[1],3);

	CU_ASSERT_PTR_NULL(tete);

	CU_ASSERT_PTR_NULL(queue);

	nettoyer(&tete,&queue);

}


