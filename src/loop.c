#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

#include "loop.h"
#include "file.h"
#include "comptage.h"
#include "couleur.h"
#include "rwfile.h"
#include "affichage.h"
#include "solveur.h"
#include "aetoile.h"
#include "optimisation.h"

void loop_init(parcours_p* parc, file_p* main_jeu_tete,file_p* main_jeu_queue, int* sz_main, int* sz_grille, resultat * save, int *regle) {

	int scan=0;
	int choix=0, id_depart;
	ptuile tuile_initiale;
	int c1=0;
	int type_charge=0;
	file_p tete_fic_t=NULL,queue_fic_t=NULL;
	file_p tete_fic_g=NULL,queue_fic_g=NULL;
	int choix_regle=-1;

	int nombre_fic_t = 0;
	nombre_fic_t = compt_fic("bin/tuile",&tete_fic_t,&queue_fic_t);
	int nombre_fic_g = 0;
	nombre_fic_g = compt_fic("bin/grille",&tete_fic_g,&queue_fic_g); 

	srand(time(NULL));

	while(!(c1==13))
	{   

		if( c1==67 || c1==68)
		{
			type_charge+=1;  
		}

		printf(LAC "##################################################\n");
		printf("##################Jeu de Honshu###################\n");
		printf("#######Tout le jeu se joue avec les flèches#######\n");
		printf("#######Pour de l'aide en jeu, appuyer sur h#######\n");
		printf("####Pour faire tourner une tuile, appuyer sur r###\n");
		printf("###Si vous vous retrouvez bloqué, appuyer sur x###\n");
		printf("##Pour plus d'informations en jeu, appuyer sur i##\n");
		printf("##################################################\n\n" RESET);
		printf(ROUGE "Comment voulez-vous choisir vos tuiles ?\n\n" RESET);
		if (type_charge%2==0){
			printf(INV "aléatoirement" RESET " ""fichier\n\n");
			choix=1;
		}
		else{
			printf( "aléatoirement" " "INV "fichier\n\n" RESET);
			choix=0;
		}

		if(system("/bin/stty raw")) {exit(EXIT_FAILURE);}
		c1 = getchar();
		if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);}

		clrscr();
	}

	if(system("/bin/stty raw")) {exit(EXIT_FAILURE);}
	if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);}

	c1=0;

	
	if (choix == 1) {

		printf(ROUGE  "Quelle est la taille de votre grille ? (Entre 5 et 30) \n\n" RESET);
		scan=0;
		while (scan != 1 || (*sz_grille<5 || *sz_grille>30)){
			printf(">  ");
			scan = scanf("%d",sz_grille);
			vider_buffer();
		}

		printf(ROUGE  "Combien de tuiles voulez-vous dans votre main ? (Entre 0 et 50) \n\n" RESET);
		scan=0;
		while (scan != 1 || (*sz_main<0 || *sz_main>50)){
			printf(">  ");
			scan = scanf("%d",sz_main);
			vider_buffer();
		}
		clrscr();


		c1=0;
		int biaise=0;    
		while(!(c1==13))
		{   
			if( c1==67 || c1==68)
			{
				biaise+=1;  
			}
			printf(ROUGE "Voulez vous jouer avec une tirage de tuiles biaisees ?\n\n" RESET);
			if (biaise%2==0)
				printf(INV "non" RESET " oui\n\n");
			else
				printf( "non" " "INV "oui\n\n" RESET);

			if(system("/bin/stty raw")) {exit(EXIT_FAILURE);}
			c1 = getchar();
			if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);}

			clrscr();
		}

		clrscr();

		id_depart = *sz_main;
		ptuile * tab_tuile = NULL;
		float resP = 0,resF = 0,resV = 0,resR = 0,resU = 0,resL = 0;

		if (biaise%2 == 1)
		{
			printf(ROUGE  "Rentrer les différentes proportions de chacune des tuiles (en pourcentage flottant) \n\n" RESET);

			scan=0;
			while (scan != 1 || (resP<0.0 || resP>100.0))
			{
				printf("P >  ");
				scan = scanf("%f",&resP);
				vider_buffer();
			}
			scan=0;
			while (scan != 1 || (resF<0.0 || resF>100.0)){
				printf("F >  ");
				scan = scanf("%f",&resF);
				vider_buffer();
			}
			scan=0;
			while (scan != 1 || (resV<0.0 || resV>100.0)){
				printf("V >  ");
				scan = scanf("%f",&resV);
				vider_buffer();
			}
			scan=0;
			while (scan != 1 || (resR<0.0 || resR>100.0)){
				printf("R >  ");
				scan = scanf("%f",&resR);
				vider_buffer();
			}
			scan=0;
			while (scan != 1 || (resU<0.0 || resU>100.0)){
				printf("U >  ");
				scan = scanf("%f",&resU);
				vider_buffer();
			}
			scan=0;
			while (scan != 1 || (resL<0.0 || resL>100.0)){
				printf("L >  ");
				scan = scanf("%f",&resL);
				vider_buffer();
			}

			tab_tuile = init_tuile_random_tab_biaise(*sz_main+1,resP,resF,resV,resR,resU,resL);
			printf(ROUGE "... Chargement\n" RESET);
		}

		else
		{
			tab_tuile = init_tuile_random_tab(*sz_main+1);
			printf(ROUGE "... Chargement\n" RESET);
		}

		clrscr();

		init_main_jeu(tab_tuile,main_jeu_tete,main_jeu_queue,*sz_main);
		tuile_initiale = tab_tuile[id_depart]; 		

		free(tab_tuile);
	}

	else 
	{
		char* tuile_f_name = calloc(30,sizeof(char));
		char* grille_f_name = calloc(30,sizeof(char));
		int tuile_t_sz;


		scan=0;
		c1=0;

        while(!(c1==13)) /*Choix de la tuile*/
		{   
			if( c1==66){
				scan+=1;
			} 

			if( c1==65){
				scan-=1;
				if (scan <0){
					scan = nombre_fic_t-1;
				}
			}   

			printf(ROUGE "Choisissez le fichier de tuile : \n\n" RESET);
			affiche_fic(tete_fic_t,scan%nombre_fic_t,&tuile_f_name);

			if(system("/bin/stty raw")) {exit(EXIT_FAILURE);} 
			c1 = getchar();
			if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);} 
			clrscr();
		}


		c1=0;
		scan=0;

		while(!(c1==13))
		{   
			if( c1==66){
				scan+=1;

			} 

			if( c1==65){
				scan-=1;
				if (scan <0){
					scan = nombre_fic_g-1;
				}
			}   

			printf(ROUGE "Choisissez le fichier de grille : \n\n" RESET);
			affiche_fic(tete_fic_g,scan%nombre_fic_g,&grille_f_name);

			if(system("/bin/stty raw")) {exit(EXIT_FAILURE);} 
			c1 = getchar();
			if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);}
			clrscr(); 
		}

		clrscr();
		printf(ROUGE "... Chargement\n" RESET);


		char dos_t[80];
		char dos_g[80];
		strcpy(dos_t, "bin/tuile/");
		strcpy(dos_g, "bin/grille/");

		ptuile * pre_tab_tuile = read_file_tuile(strcat(dos_t,tuile_f_name), &tuile_t_sz);

		struct info_grille info;
		read_file_grille(strcat(dos_g,grille_f_name), &info);
		*sz_grille = info.sz_grille;
		*sz_main = info.sz_main;

		ptuile * tab_tuile = calloc(*sz_main,sizeof(ptuile));

		tuile_initiale = pre_tab_tuile[info.id_depart];
		tuile_initiale->id = *sz_main;

		int i = 0;
		while (i < *sz_main)
		{
			ptuile tuile_courante = copie_tuile(pre_tab_tuile[info.ids[i]]);
			tuile_courante->id = i;
			tab_tuile[i] = tuile_courante;        
			i++;
		}

		init_main_jeu(tab_tuile,main_jeu_tete,main_jeu_queue,*sz_main);
		free(tuile_f_name);
		free(grille_f_name);

		for (int i = 0; i < tuile_t_sz; ++i){
			if (i != info.id_depart)
				free_tuile(pre_tab_tuile[i]);
		}
		free(info.ids);
		free(pre_tab_tuile);
		free(tab_tuile);
	}

	clrscr();
	choix=0;
	c1=0;
    choix_regle = 0;
	while(!(c1==13))
	{   

		if( c1==67 || c1==68)
		{
			choix_regle+=1;  
		}

		printf(ROUGE  "Voulez jouer avec une nouvelle règle ?\n\n" RESET);
		if (choix_regle%2==0){
			printf(INV "non" RESET " ""oui\n\n");
			choix=1;
		}
		else{
			printf( "non" " "INV "oui\n\n" RESET);
			choix=0;
		}

		if(system("/bin/stty raw")) {exit(EXIT_FAILURE);}
		c1 = getchar();
		if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);}

		clrscr();
	}

	c1=0;
	choix_regle=-1;
	if (choix==0)
	{
		choix_regle=0;
		while(!(c1==13))
		{   
			if( c1==66)
			{
				choix_regle+=1; 
			} 


			if( c1==65)
			{
				choix_regle-=1;
				if (choix_regle <0)
				{
					choix_regle = 4;
				}  
			}


			printf(ROUGE  "Quelle règle voulez vous choisir ?\n\n" RESET);
			if (choix_regle%5==0){

				printf(INV"-Une case Lac vaut 2 (override règle 3.)\n"RESET) ;
				printf("-Une plaine de quatre cases vaut 4pts (se rajoute)\n") ;
				printf("-Une Usine peut accepter deux Ressources (override règle 4)\n") ;
				printf("-Un carré de quatre cases village, s’il est dans la ville comptabilisée, donne 4pts bonus (ajout a la règle 1)\n") ;
				printf("-Chaque forêt, la première case vaut 1, la seconde 2, la troisième 3, etc. (max5) (override règle 2)\n");
				choix=0;
			}
			else if (choix_regle%5==1){
				printf("-Une case Lac vaut 2 (override règle 3.)\n") ;
				printf(INV"-Une plaine de quatre cases vaut 4pts (se rajoute)\n"RESET) ;
				printf("-Une Usine peut accepter deux Ressources (override règle 4)\n") ;
				printf("-Un carré de quatre cases village, s’il est dans la ville comptabilisée, donne 4pts bonus (ajout a la règle 1)\n") ;
				printf("-Chaque forêt, la première case vaut 1, la seconde 2, la troisième 3, etc. (max5) (override règle 2)\n");
				choix=1;
			}
			else if (choix_regle%5==2){
				printf("-Une case Lac vaut 2 (override règle 3.)\n") ;
				printf("-Une plaine de quatre cases vaut 4pts (se rajoute)\n") ;
				printf(INV"-Une Usine peut accepter deux Ressources (override règle 4)\n"RESET) ;
				printf("-Un carré de quatre cases village, s’il est dans la ville comptabilisée, donne 4pts bonus (ajout a la règle 1)\n") ;
				printf("-Chaque forêt, la première case vaut 1, la seconde 2, la troisième 3, etc. (max5) (override règle 2)\n");
				choix=2;
			}
			else if (choix_regle%5==3){
				printf("-Une case Lac vaut 2 (override règle 3.)\n") ;
				printf("-Une plaine de quatre cases vaut 4pts (se rajoute)\n") ;
				printf("-Une Usine peut accepter deux Ressources (override règle 4)\n") ;
				printf(INV"-Un carré de quatre cases village, s’il est dans la ville comptabilisée, donne 4pts bonus (ajout a la règle 1)\n"RESET) ;
				printf("-Chaque forêt, la première case vaut 1, la seconde 2, la troisième 3, etc. (max5) (override règle 2)\n");
				choix=3;
			}
			else{
				printf("-Une case Lac vaut 2 (override règle 3.)\n") ;
				printf("-Une plaine de quatre cases vaut 4pts (se rajoute)\n") ;
				printf("-Une Usine peut accepter deux Ressources (override règle 4)\n") ;
				printf("-Un carré de quatre cases village, s’il est dans la ville comptabilisée, donne 4pts bonus (ajout a la règle 1)\n") ;
				printf(INV"-Chaque forêt, la première case vaut 1, la seconde 2, la troisième 3, etc. (max5) (override règle 2)\n"RESET);
				choix=4;
			}

			if(system("/bin/stty raw")) {exit(EXIT_FAILURE);}
			c1 = getchar();
			if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);}

			clrscr();
		}

	}
	choix_regle = choix_regle%5;

	clrscr();

	*parc = create_tour(tuile_initiale, (int)floor(*sz_grille / 2), (int)floor(*sz_grille / 2), 1, *sz_grille);

	if (*sz_main >0)
	{
		*save = solveur(*sz_main,*sz_main,*sz_grille,*main_jeu_tete,*parc,*regle);
	}

	free(tuile_initiale);
	vider_liste_fic(&tete_fic_t,&queue_fic_t,nombre_fic_t);
	vider_liste_fic(&tete_fic_g,&queue_fic_g,nombre_fic_g);
	nettoyer(&tete_fic_t,&queue_fic_t);
	nettoyer(&tete_fic_g,&queue_fic_g);
	*regle  = choix_regle;

}

void init_main_jeu(ptuile * tab_tuile,file_p* main_jeu_tete,file_p* main_jeu_queue,int sz_main)
{
	int i=0;
	ptuile tuile_courante = NULL;
	while(i != sz_main)
	{
		tuile_courante = tab_tuile[i];
		enfiler_tri(&tuile_courante,main_jeu_tete,main_jeu_queue,sizeof(ptuile));
		i++;
	}
}

void loop(parcours_p par,file_p* main_jeu_tete,file_p* main_jeu_queue, int sz_main, int sz_grille, resultat * save, int *regle) {

	int  x=0, y=0, o=0;
	int scan=0;
	int t_choix=0;
	int nb_tour = 0;
	char retrait=' ';
	int c1=0;
	int coda=0;
	int ret=0;
	int solution=2;
	int chargement=0;
	resultat solut;

	ptuile * inventaire_tuile = calloc(sz_main,sizeof(ptuile));

	while(*main_jeu_tete != NULL && coda!=1)
	{   
		affichage_tour(par->tour,*regle);

		scan=0;
		c1=0;
		t_choix=0;
		solution=0;
		chargement = 0;
        while(!(c1==13)) /*Choix de la tuile*/
		{   
			if( c1==67){
				scan+=1; 
			}

			if( c1==68){
				scan-=1;
				if (scan <0){
					scan = sz_main - nb_tour-1;
				}
			} 

			printf(ROUGE "Choisissez votre tuile : \n\n" RESET);
			affichage_tour(par->tour,*regle);

			if(solution == 1)
			{

				affichage_solution(&solut);
			}

			t_choix=print_file_tuile(*main_jeu_tete,scan%(sz_main - nb_tour),sz_main - nb_tour);

			if(system("/bin/stty raw")) {exit(EXIT_FAILURE);} 
			c1 = getchar();
			if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);}

			if( c1=='s') /*Si savoir comment marquer des points, on appuie sur h*/
			{
				if (chargement==0)
				{
					chargement++;
					clrscr();
					printf("Chargement ...\n");
					solut = solveur(sz_main - nb_tour,sz_main,sz_grille,*main_jeu_tete,par,*regle);
				}				
				solution=abs(solution-1);
				printf("solution = %d\n",solution);
			}

		}

		if (chargement==1)
		{
			remove_all_tour(&solut.partie_opti);
		}

		defiler_tri(main_jeu_tete,main_jeu_queue,&inventaire_tuile[nb_tour],sizeof(inventaire_tuile[nb_tour]),t_choix);

		affichage_tuile(inventaire_tuile[nb_tour],0);

		ret = bouger_tuile(par->tour,inventaire_tuile[nb_tour],&x,&y,&o,*regle);

        if (ret==1)/*Soit on decide de quitter le jeu (on a appuyr sur x)*/
		{
			coda=1;
		}

		else
		{
            add_tour(&par, inventaire_tuile[nb_tour], x, y, o, sz_grille);/*Soit push la tuile*/

			scan = 0;
			c1=0;
			retrait=0;    
            while(!(c1==13))/*Choix du retrait d'une tuile*/
			{   
				if( c1==67 || c1==68)
				{
					retrait+=1;  
				}

				affichage_tour(par->tour,*regle);
				printf(ROUGE "Voulez vous insérer la tuile avec (x,y,o) = (%d,%d,%d) ?\n\n" RESET,x,y,o);


				if (retrait%2==0)
					printf(INV "oui" RESET " non\n\n");
				else
					printf( "oui" " "INV "non\n\n" RESET);

				if(system("/bin/stty raw")) {exit(EXIT_FAILURE);}
				c1 = getchar();
				if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);}

				clrscr();
			}

			if (retrait%2==1)
			{
				remove_tour(&par);
				clrscr();
				enfiler_tri(&inventaire_tuile[nb_tour],main_jeu_tete,main_jeu_queue,sizeof(ptuile));
			}
			else
			{
				nb_tour++;          
			}
		}
	}

	affichage_tour(par->tour,*regle);

	printf("\n\n*********Le résultat est %d points. Bravo !*********\n\n",comptage(par->tour->g,*regle,1).somme);



	if (sz_main >0){
		c1=0;
		int solution=0;    
		while(!(c1==13))
		{   
			if( c1==67 || c1==68)
			{
				solution+=1;  
			}
			affichage_tour(par->tour,*regle);

			printf("\n\n*********Le résultat est %d points. Bravo !*********\n\n",comptage(par->tour->g,*regle,1).somme);

			printf(ROUGE "Voulez vous afficher la solution ?\n\n" RESET);


			if (solution%2==0)
				printf(INV "oui" RESET " non\n\n");
			else
				printf( "oui" " "INV "non\n\n" RESET);

			if(system("/bin/stty raw")) {exit(EXIT_FAILURE);}
			c1 = getchar();
			if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);}

			clrscr();
		}

		if (solution%2 == 0)
		{
			affichage_tour(par->tour,*regle);

			printf("\n\n*********Le résultat est %d points. Bravo !*********\n\n",comptage(par->tour->g,*regle,1).somme);

			affichage_solution(save);
		}


		remove_all_tour(&save->partie_opti);
	}




	printf("Au nom de toute l'équipe de Made_by_Arise, nous vous remercions d'avoir participé à cette partie de Honshu.\nA bientôt !\n\n\n\n");

	for (int i=nb_tour+1; i < sz_main+12; ++i)
	{
		defiler(main_jeu_tete,main_jeu_queue,&inventaire_tuile[i],sizeof(inventaire_tuile[i]));
	}
	nettoyer(main_jeu_tete,main_jeu_queue);
	for (int i = 0; i < sz_main; ++i)
	{
		free_tuile(inventaire_tuile[i]);
	}
	free(inventaire_tuile);
	remove_all_tour(&par);


}


void vider_buffer(){
	int c = 0;
	while (c != '\n' && c != EOF)
	{
		c = getchar();
	}
}
