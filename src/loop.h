#ifndef _LOOP_
#define _LOOP_

#include "rwfile.h"
#include "grille.h"
#include "tuile.h"
#include "file.h"
#include "tour.h"
#include "parcours.h"
#include "comptage.h"
#include "affichage.h"
#include "optimisation.h"

/**
 * @file loop.h
 * 
 * Ce fichier décrit l'ensemble des fonctions permettant de réaliser une boucle de jeu.
 * Il contient 4 fonctions.
 *   
 * - loop_init(parcours_p* parc, file_p* main_jeu_tete,file_p* main_jeu_queue, int* sz_main, int* sz_grille,resultat *save, int *regle) 	réalise la boucle de jeu
 * - loop(parcours_p par,file_p* main_jeu_tete,file_p* main_jeu_queue, int sz_main, int sz_grille,resultat *save, int *regle) 				initialise le jeu
 * - init_main_jeu(ptuile * tab_tuile,file_p* main_jeu_tete,file_p* main_jeu_queue,int sz_main) 								initialise la main de jeu
 * - vider_buffer()																							 					vide le buffer
 */



/**
 * @brief 					réalise la boucle de jeu
 * @param 	parc           	un parcours de jeu
 * @param 	main_jeu_tete  	un pointeur sur la tete de file de tuiles
 * @param 	main_jeu_queue 	un pointeur sur la queue de file de tuiles
 * @param 	sz_main     	le nombre de tuiles dans la main
 * @param 	sz_grille     	la taille de la grille
 * @param   regle			la regle du jeu choisie
 * @return 					rien
 */
void loop_init(parcours_p* parc, file_p* main_jeu_tete,file_p* main_jeu_queue, int* sz_main, int* sz_grille,resultat *save, int *regle);


/**
 * @brief 					initialise la boucle de jeu
 * @param 	par           	un parcours de jeu
 * @param 	main_jeu_tete  	un pointeur sur la tete de file de tuiles
 * @param 	main_jeu_queue 	un pointeur sur la queue de file de tuiles
 * @param 	sz_main        	le nombre de tuiles dans la main
 * @param 	sz_grille      	la taille de la grille
 * @param   save			un pointeur sur le resultat du solveur
 * @param   regle			la regle du jeu choisie
 * @return 					rien
 */
void loop(parcours_p par,file_p* main_jeu_tete,file_p* main_jeu_queue, int sz_main, int sz_grille, resultat *save, int *regle);


/**
 * @brief 					initialise la main de jeu
 * @param 	tab_tuile       le tableau des tuiles selectionnées
 * @param 	main_jeu_tete  	un pointeur sur la tete de file de tuiles
 * @param 	main_jeu_queue 	un pointeur sur la queue de file de tuiles
 * @param 	sz_main        	le nombre de tuiles dans la main
 * @param   save			un pointeur sur le resultat du solveur
 * @return 					rien
 */
void init_main_jeu(ptuile * tab_tuile, file_p* main_jeu_tete, file_p* main_jeu_queue, int sz_main);


/**
 * @brief 	vide le buffer
 * @param 	rien
 * @return 	rien
 */
void vider_buffer();

#endif
