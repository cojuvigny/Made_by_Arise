#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

#include "tuile.h"
#include "rwfile.h"
#include "affichage.h"
#include "grille.h"
#include "test.h"
#include "tour.h"
#include "parcours.h"
#include "file.h"
#include "comptage.h"
#include "optimisation.h"
#include "test_cunit_lot_C.h"
#include "aetoile.h"
#include "solveur.h"

#define NOMBRE_TUILE 10
#define TAILLE_GRILLE 10

int main() {

	initialisation_catalogue_C();

	/*
	printf("\n------------------------------------\n");
	printf("\n--------Tests file générique--------\n");
	printf("\n------------------------------------\n");

	file_p tete = NULL,queue = NULL;
    //int a,b;
	int nombre_tuile = NOMBRE_TUILE ;
	int taille_grille = TAILLE_GRILLE;

	time_t depart1,arrivee1,depart2,arrivee2;*/


    /*Rappel : init_tuile_random_tab_biaise(nbtuile,%P,%F,%V,%R,%U,%L)
    La somme des pourcentage doit valoir 100 ....
    On génère un tableau de 13 tuiles, dont une pour le debut de partie, de manière aléatoire*/

    /*Ici tableau initialisé de manière classique non biaisé puisque chaque terrain à la même chance de sortir !*/
    /*ptuile* tab_tuile =  init_tuile_random_tab_biaise(nombre_tuile,100.0/6.0,100.0/6.0,100.0/6.0,100.0/6.0,100.0/6.0,100.0/6.0);*/
	
	
	//ptuile* tab_tuile =  init_tuile_random_tab_biaise(nombre_tuile+1,00.0,0.0,0.0,50.0,50.0,0.0);
	/*ptuile* tab_tuile = init_tuile_random_tab(nombre_tuile+1);*/
	//ptuile* tab_tuile =  init_tuile_random_tab_biaise(nombre_tuile+1,20.0,0.0,0.0,0.0,0.0,80.0);

	/*
	for (int i = 0; i <= nombre_tuile; ++i)
	{
		printf("id : %d\n",tab_tuile[i]->id);
		affichage_tuile(tab_tuile[i],0);
		printf("\n");
	}


	printf("************************************\n");
	printf("*************Debut opti*************\n");
	printf("************************************\n");


	ptuile *sortie2 = (ptuile *)calloc(1,sizeof(ptuile));*/
    /*
    for (int i = 1; i < nombre_tuile; ++i)
	{
		enfiler_tri(&tab_tuile[i],&tete,&queue,sizeof(&tab_tuile[i]));
	}

    printf("maximum général=%d \n",max_rec(&p,tete,comptage2(p->tour->g).somme));
    remove_all_tour(&p);
    nettoyer(&tete,&queue);
    */

    /*
    couple c1=place_opt_lacs(&p,tete);
	printf("x=%d, y=%d\n",c1.x,c1.y);

    couple c2=place_opt_village(&p,tete);
    printf("x=%d, y=%d\n",c2.x,c2.y);
  	ptuile t5 = init_tuile_random(20);
    couple c3=max_position (&p,t5);
    //affichage_tour(p->tour);
 	printf("x=%d, y=%d, r=%d \n",c3.x,c3.y,c3.r);

    
    b=max_village_rec (&p,tete);
    printf("maximum général pour les villages=%d \n",b);
	remove_all_tour2(&p);
	defiler(&tete,&queue,sortie2,sizeof(ptuile));
	defiler(&tete,&queue,sortie2,sizeof(ptuile));
    */
   
   /*
   	for (int i = 0; i < nombre_tuile; ++i)
	{
		enfiler_tri(&tab_tuile[i],&tete,&queue,sizeof(&tab_tuile[i]));
	}

	time(&depart1);
	parcours_p p1=create_tour(tab_tuile[nombre_tuile], (taille_grille/2)-1, (taille_grille/2)-1, 1, taille_grille);
	printf("\n*****A etoile modifié avec heuri*****\n");
	resultat res1;
   	a_etoile(nombre_tuile,nombre_tuile,0,tete,&p1,&res1);
   	affichage_tour(res1.partie_opti->tour);
	printf("%s\n",res1.description);
	printf("Points : %d\n",res1.points_partie);



   	
   	
   	parcours_p p2=create_tour(tab_tuile[nombre_tuile], (taille_grille/2)-1, (taille_grille/2)-1, 1, taille_grille);

   	for (int i = 0; i < nombre_tuile; ++i)
	{
		enfiler_tri(&tab_tuile[i],&tete,&queue,sizeof(&tab_tuile[i]));
	}
	printf("\n*****A etoile modifié sans heuri*****\n");
	resultat res2;
   	a_etoile(nombre_tuile,nombre_tuile,1,tete,&p2,&res2);
   	affichage_tour(res2.partie_opti->tour);
	printf("%s\n",res2.description);
	printf("Points : %d\n",res2.points_partie);
   	nettoyer(&tete,&queue);;

   	parcours_p p3=create_tour(tab_tuile[nombre_tuile], (taille_grille/2)-1, (taille_grille/2)-1, 1, taille_grille);
	for (int i = 0; i < nombre_tuile; ++i)
	{
		enfiler_tri(&tab_tuile[i],&tete,&queue,sizeof(&tab_tuile[i]));
	}
	
	
	printf("\n*****Min*****\n");
	resultat res3;
	inclusion_rec(&p3,&tete,&queue,sortie2,0,1,&res3); 
	affichage_tour(res3.partie_opti->tour);
	printf("%s\n",res3.description);
	printf("Points : %d\n",res3.points_partie);



	parcours_p p4=create_tour(tab_tuile[nombre_tuile], (taille_grille/2)-1, (taille_grille/2)-1, 1, taille_grille);
	for (int i = 0; i < nombre_tuile; ++i)
	{
		enfiler_tri(&tab_tuile[i],&tete,&queue,sizeof(&tab_tuile[i]));
	}
	
	
	printf("\n*****Max*****\n");
	resultat res4;
	inclusion_rec(&p4,&tete,&queue,sortie2,0,0,&res4);
	affichage_tour(res4.partie_opti->tour);
	printf("%s\n",res4.description);
	printf("Points : %d\n",res4.points_partie);




	parcours_p p5=create_tour(tab_tuile[nombre_tuile], (taille_grille/2)-1, (taille_grille/2)-1, 1, taille_grille);
	for (int i = 0; i < nombre_tuile; ++i)
	{
		enfiler_tri(&tab_tuile[i],&tete,&queue,sizeof(&tab_tuile[i]));
	}
	
	
	printf("\n*****Max village*****\n");
	resultat res5;
	village_inclusion_rec(&p5,&tete,&queue,sortie2,0,&res5);
	affichage_tour(res5.partie_opti->tour);
	printf("%s\n",res5.description);
	printf("Points : %d\n",res5.points_partie);

	parcours_p p6=create_tour(tab_tuile[nombre_tuile], (taille_grille/2)-1, (taille_grille/2)-1, 1, taille_grille);
	for (int i = 0; i < nombre_tuile; ++i)
	{
		enfiler_tri(&tab_tuile[i],&tete,&queue,sizeof(&tab_tuile[i]));
	}
	
	
	printf("\n*****Max Lac*****\n");
	resultat res6;
	lac_inclusion_rec(&p6,&tete,&queue,sortie2,0,&res6);
	affichage_tour((res6.partie_opti)->tour);
	printf("%s\n",res6.description);
	printf("Points : %d\n",res6.points_partie);


	parcours_p p7=create_tour(tab_tuile[nombre_tuile], (taille_grille/2)-1, (taille_grille/2)-1, 1, taille_grille);
	for (int i = 0; i < nombre_tuile; ++i)
	{
		enfiler_tri(&tab_tuile[i],&tete,&queue,sizeof(&tab_tuile[i]));
	}


	
	printf("\n*****Max proba grille*****\n");
	resultat res7;
	max_modifgrille_rec(&p7,&tete,&queue,sortie2,0,&res7);
	affichage_tour(res7.partie_opti->tour);
	printf("%s\n",res7.description);
	printf("Points : %d\n",res7.points_partie);


	parcours_p p8=create_tour(tab_tuile[nombre_tuile], (taille_grille/2)-1, (taille_grille/2)-1, 1, taille_grille);
	for (int i = 0; i < nombre_tuile; ++i)
	{
		enfiler_tri(&tab_tuile[i],&tete,&queue,sizeof(&tab_tuile[i]));
	}
	printf("\n*****Max proba tuile*****\n");
	resultat res8;
	max_modiftuile_rec (&p8,&tete,&queue,sortie2,0,proba_tuiles (tete),&res8);
	time(&arrivee1);
	printf("Tu t'es absente pendant %f secondes.", difftime(arrivee1, depart1));
	affichage_tour(res8.partie_opti->tour);
	printf("%s\n",res8.description);
	printf("Points : %d\n",res8.points_partie);*/

	
	/*
	parcours_p p10=create_tour(tab_tuile[nombre_tuile], (taille_grille/2)-1, (taille_grille/2)-1, 1, taille_grille);
	for (int i = 0; i < nombre_tuile; ++i)
	{
		enfiler_tri(&tab_tuile[i],&tete,&queue,sizeof(&tab_tuile[i]));
	}
	
	
	printf("\n*****Min avec 2 tuiles*****\n");
	resultat res10;
	inclusion2_rec(&p10,&tete,&queue,sortie2,0,1,&res10); 
	affichage_tour(res10.partie_opti->tour);
	printf("%s\n",res10.description);
	printf("Points : %d\n",res10.points_partie);



	parcours_p p11=create_tour(tab_tuile[nombre_tuile], (taille_grille/2)-1, (taille_grille/2)-1, 1, taille_grille);
	for (int i = 0; i < nombre_tuile; ++i)
	{
		enfiler_tri(&tab_tuile[i],&tete,&queue,sizeof(&tab_tuile[i]));
	}
	
	
	printf("\n*****Max avec 2 tuiles*****\n");
	resultat res11;
	inclusion2_rec(&p11,&tete,&queue,sortie2,0,0,&res11); 
	affichage_tour(res11.partie_opti->tour);
	printf("%s\n",res11.description);
	printf("Points : %d\n",res11.points_partie);

	parcours_p p12=create_tour(tab_tuile[nombre_tuile], (taille_grille/2)-1, (taille_grille/2)-1, 1, taille_grille);
	for (int i = 0; i < nombre_tuile; ++i)
	{
		enfiler_tri(&tab_tuile[i],&tete,&queue,sizeof(&tab_tuile[i]));
	}
	
	
	printf("\n*****Min avec 3 tuiles*****\n");
	resultat res12;
	inclusion3_rec(&p12,&tete,&queue,sortie2,0,1,&res12); 
	affichage_tour(res12.partie_opti->tour);
	printf("%s\n",res12.description);
	printf("Points : %d\n",res12.points_partie);

	parcours_p p13=create_tour(tab_tuile[nombre_tuile], (taille_grille/2)-1, (taille_grille/2)-1, 1, taille_grille);
	for (int i = 0; i < nombre_tuile; ++i)
	{
		enfiler_tri(&tab_tuile[i],&tete,&queue,sizeof(&tab_tuile[i]));
	}
	
	
	printf("\n*****Max avec 3 tuiles*****\n");
	resultat res13;
	inclusion3_rec(&p13,&tete,&queue,sortie2,0,0,&res13); 
	affichage_tour(res13.partie_opti->tour);
	printf("%s\n",res13.description);
	printf("Points : %d\n",res13.points_partie);*/
	

	//
	
	/*parcours_p p9=create_tour(tab_tuile[nombre_tuile], (taille_grille/2)-1, (taille_grille/2)-1, 1, taille_grille);
	for (int i = 0; i < nombre_tuile; ++i)
	{
		enfiler_tri(&tab_tuile[i],&tete,&queue,sizeof(&tab_tuile[i]));
	}

	time(&depart2);
	resultat save = solveur(nombre_tuile,nombre_tuile,taille_grille,tete,p9);
	time(&arrivee2);
	printf("Tu t'es absente pendant %f secondes.", difftime(arrivee2, depart2));

	nettoyer(&tete,&queue);

	affichage_tour(save.partie_opti->tour);
	printf("%s\n",save.description);
	printf("Points Best of the Best : %d\n",save.points_partie);
	printf("nombre_tuile : %d\n",save.partie_opti->tour->sz_index);


	for (int i = 0; i <= nombre_tuile; ++i)
	{
		free_tuile(tab_tuile[i]);
	}

	remove_all_tour(&p1);
	
	remove_all_tour(&p2);
	remove_all_tour(&p3);
	remove_all_tour(&p4);
	remove_all_tour(&p5);
	remove_all_tour(&p6);
	remove_all_tour(&p7);
	remove_all_tour(&p8);
	remove_all_tour(&p9);*/
	/*remove_all_tour(&p10);
	remove_all_tour(&p11);
	remove_all_tour(&p12);
	remove_all_tour(&p13);*/
	
	/*remove_all_tour(&save.partie_opti);
	free(tab_tuile);
	free(sortie2);
	nettoyer(&tete,&queue);*/

	return EXIT_SUCCESS;
}
