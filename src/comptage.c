#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#include "tuile.h"
#include "grille.h"
#include "test.h"
#include "parcours.h"
#include "file.h"
#include "comptage.h"
#include "affichage.h"


int test_village2(grille_p g,int x, int y, int size, int *presence_carre){

	int nbville=0;
	int carre_1=0;
	int carre_2=0;
	int carre_3=0;
	int carre_4=0;

	if (g->cells[x][y].type != 'V'){
		return nbville;
	}

	file_p tete = NULL,queue = NULL;

	nbville++;
	int ** mat_visite = NULL;
	mat_visite = allocation_dynamique_matrice(mat_visite,size);

	int entree[2] = {x,y};

	enfiler(entree,&tete,&queue,sizeof(entree));
	mat_visite[x][y]=1;

	while (!(tete == NULL && queue == NULL))
	{
		int sortie[2] ; 
		defiler(&tete,&queue,sortie,sizeof(sortie));
		int x = sortie[0];
		int y = sortie[1];
		/*test des cases normales*/
		if (x+1>=0 && x+1<size)
		{
			if (g->cells[x+1][y].type == 'V')
			{
				carre_3+=1;
				carre_4+=1;
				if (mat_visite[x+1][y] == 0)
				{
					entree[0]=x+1;
					entree[1]=y;
					enfiler(entree,&tete,&queue,sizeof(entree));
					mat_visite[x+1][y] = 1;
					nbville+=1;
					
				}
				
			}
		}

		if (x-1>=0 && x-1<size)
		{
			if (g->cells[x-1][y].type == 'V')
			{
				carre_1+=1;
				carre_2+=1;
				if (mat_visite[x-1][y] == 0)
				{
					entree[0]=x-1;
					entree[1]=y;
					enfiler(entree,&tete,&queue,sizeof(entree));
					mat_visite[x-1][y] = 1;
					nbville+=1;
					
				}
				
			}    		
		}

		if (y+1>=0 && y+1<size)
		{
			if (g->cells[x][y+1].type == 'V')
			{
				carre_2+=1;
				carre_4+=1;
				if (mat_visite[x][y+1] == 0)
				{
					entree[0]=x;
					entree[1]=y+1;
					enfiler(entree,&tete,&queue,sizeof(entree));
					mat_visite[x][y+1] = 1;
					nbville+=1;
					
				}
				
			}    		
		}

		if (y-1>=0 && y-1<size)
		{
			if (g->cells[x][y-1].type == 'V')
			{
				carre_1+=1;
				carre_3+=1;
				if (mat_visite[x][y-1] == 0)
				{
					entree[0]=x;
					entree[1]=y-1;
					enfiler(entree,&tete,&queue,sizeof(entree));
					mat_visite[x][y-1] = 1;
					nbville+=1;
					
				}
				
			}
			
		}
		/*test des coins pour les carrés*/
		if (x-1>=0 && x-1<size && y-1>=0 && y-1<size){
			if (g->cells[x-1][y-1].type == 'V')
			{
				carre_1+=1;
			}
		}

		if (x-1>=0 && x-1<size && y+1>=0 && y+1<size){
			if (g->cells[x-1][y+1].type == 'V')
			{
				carre_2+=1;
			}    		
		}

		if (x+1>=0 && x+1<size && y-1>=0 && y-1<size){
			if (g->cells[x+1][y-1].type == 'V')
			{
				carre_3+=1;
			}    		
		}

		if (x+1>=0 && x+1<size && y+1>=0 && y+1<size){
			if (g->cells[x+1][y+1].type == 'V')
			{
				carre_4+=1;
			}
			
		}

		if (carre_1==3 || carre_2==3 || carre_3==3 || carre_4==3)
		{
			*presence_carre=1;
		}
		carre_1=0;
		carre_2=0;
		carre_3=0;
		carre_4=0;

	}

	
	liberer(mat_visite,size);
	nettoyer(&tete,&queue);
	return nbville;

}




int test_presence(grille_p g,int x, int y, int size, int ** matrice, char terrain){
	int nbterrains=0;

	if (g->cells[x][y].type != terrain){
		return (nbterrains);
	}

	file_p tete = NULL,queue = NULL;
	int ** mat_visite = NULL;
	mat_visite = allocation_dynamique_matrice(mat_visite,size);
	nbterrains+=1;

	int entree[2] = {x,y};

	enfiler(entree,&tete,&queue,sizeof(entree));
	matrice[x][y]=1;
	while (!(tete == NULL && queue == NULL)){
		int sortie[2] ; 
		defiler(&tete,&queue,sortie,sizeof(sortie));
		int x = sortie[0];
		int y = sortie[1];
		if (x+1>=0 && (x+1)<size){
			if (g->cells[(x+1)][y].type  == terrain && mat_visite[x+1][y] == 0 &&  matrice[x+1][y]==-1){

				entree[0]=x+1;
				entree[1]=y;
				enfiler(entree,&tete,&queue,sizeof(entree));
				mat_visite[x+1][y] = 1;
				matrice[x+1][y]=1;
				nbterrains+=1;
   			   	/*printf("1: %d %d %d\n",nbterrains,x,y);*/
			}
		}

		if (x-1>=0 && (x-1)<size){
			if (g->cells[x-1][y].type == terrain && mat_visite[x-1][y] == 0 && matrice[x-1][y]==-1){

				entree[0]=x-1;
				entree[1]=y;
				enfiler(entree,&tete,&queue,sizeof(entree));
				mat_visite[x-1][y] = 1;
				matrice[x-1][y]=1;
				nbterrains+=1;
   			   	/*printf("2: %d %d %d\n",nbterrains,x,y);*/
			}    		
		}

		if (y+1>=0 && (y+1)<size){
			if (g->cells[x][y+1].type == terrain && mat_visite[x][y+1] == 0 && matrice[x][y+1]==-1){

				entree[0]=x;
				entree[1]=y+1;
				enfiler(entree,&tete,&queue,sizeof(entree));
				mat_visite[x][y+1] = 1;
				matrice[x][y+1]=1;
				nbterrains+=1;
   			   	/*printf("3:  %d %d %d\n",nbterrains,x,y);*/
			}    		
		}

		if (y-1>=0 && (y-1)<size){
			if (g->cells[x][y-1].type == terrain && mat_visite[x][y-1] == 0  && matrice[x][y-1]==-1){

				entree[0]=x;
				entree[1]=y-1;
				enfiler(entree,&tete,&queue,sizeof(entree));
				mat_visite[x][y-1] = 1;
				matrice[x][y-1]=1;
				nbterrains+=1;
   			   	/*printf("4:  %d %d %d\n",nbterrains,x,y);*/
			}
			
		}

	}

	
	liberer(mat_visite,size);
	nettoyer(&tete,&queue);
	return nbterrains;

}

void initialisation_mat ( int ** matrice, int size)
{
	int i;
	int j;
	for (i=0;i<size;i++)
	{
		for (j=0;j<size;j++)
		{
			matrice[i][j]=-1;
		}
	}
}

grille_p create_grille_full_random2(int n)
{
	char type[N] = {'L','V','P','F','U','R'};
	srand(time(NULL));
	grille_p g = (grille *) malloc(sizeof(grille));
	cases ** p=(cases **) malloc (n*sizeof(cases *));
	int i,j;
	for (i=0;i<n;i++){
		p[i] = (cases *) malloc(n*sizeof(cases));

		for (j = 0; j < n; ++j){
      p[i][j].type = type[rand()%6] ;/*Pour avoir plus de V et des test intéressants*/
			p[i][j].id = -1;
		}
	}
	g->cells=p;
	g->size=n;
	return g;
}

int test_presence_totale(grille_p g, char terrain)
{
	int ** mat = NULL;
	int somme=0;
	int i;
	int j;
	int k;
	mat = allocation_dynamique_matrice(mat,g->size);
	initialisation_mat ( mat, g->size);
	for (i=0;i<g->size;i++)
	{
		for (j=0;j<g->size;j++)
		{
			if (terrain == 'P')
			{
				k=test_presence(g, i, j, g->size, mat,terrain);
				if (k>=4)
				{
					somme+=(4);
				}
			}
			else
			{
				k=test_presence(g, i, j, g->size, mat,terrain);
				if (k>0)
				{
					somme+=(k-1);
				}
			}
			
		}
	}
	liberer(mat,g->size);
	return somme;
}

int min (int a, int b){
	if (a<b) {return a;}
	else return b;
}


int max (int a, int b){
	if (a>b) {return a;}
	else return b;
}

int pts_forets(int regle, int nbr_forets, int verbose)
{
	int pts_courants=0;

	if(regle == 4)
	{
		for (int i = 1; i <= nbr_forets; ++i)
		{
			if (i<5)
			{
				pts_courants+=i;
			}
			else
			{
				pts_courants+=5;
			}
		}
		if (verbose)
		{
			printf("Le nombre de forêts est de %d soit %d points (new)\n",nbr_forets,pts_courants);
		}		
		return pts_courants;
	}	
	else
	{
		if (verbose)
		{
			printf("Le nombre de forêts est de %d soit %d points \n",nbr_forets,nbr_forets*2);
		}		
		return nbr_forets*2;
	}

}

int pts_lacs(int regle, int nbr_lacs, grille_p g, int verbose)
{
	if(regle == 0)
	{
		if (verbose)
		{
			printf("Le nombre de lacs est %d soit %d points (new)\n",nbr_lacs,2*(nbr_lacs));
		}		
		return 2*(nbr_lacs);
	}	
	else
	{
		nbr_lacs=test_presence_totale(g,'L');
		if (verbose)
		{
			printf("Le nombre de lacs est %d soit %d points \n",nbr_lacs,3*(nbr_lacs));
		}	
		return 3*(nbr_lacs);
	}

}

int pts_plaines(int regle, int nbr_plaines, grille_p g, int verbose)
{
	if(regle == 1)
	{
		nbr_plaines=test_presence_totale(g,'P');
		if (verbose)
		{
			printf("Le nombre de plaines est %d soit %d points \n",nbr_plaines,nbr_plaines);
		}	
		return 3*(nbr_plaines);
	}
	else
	{
		return 0;
	}
}

int pts_r_and_u(int regle, int nbr_usines,int nbr_ressources, int verbose)
{
	if(regle == 2)
	{
		if (verbose)
		{
			printf("Le nombre d'usines est %d et celui de ressources est %d, soit %d points \n",nbr_usines,nbr_ressources,4*(min (2*nbr_usines,nbr_ressources)));
		}
		return 4*(min (2*nbr_usines,nbr_ressources));
	}	
	else
	{
		if (verbose)
		{
			printf("Le nombre d'usines est %d et celui de ressources est %d, soit %d points \n",nbr_usines,nbr_ressources,4*(min (nbr_usines,nbr_ressources)));
		}
		return 4*(min (nbr_usines,nbr_ressources));
	}

}

int pts_villes(int regle, int nbr_villes, int verbose, int presence_carre)
{
	int tot=0;
	if(regle == 3)
	{
		tot = presence_carre == 1 ? nbr_villes+4 : nbr_villes;
		if (verbose)
		{
			printf("Le nombre de villes est %d, soit %d points \n",nbr_villes,tot);
		}
		return tot;
	}	
	else
	{
		if (verbose)
		{
			printf("Le nombre de villes est %d, soit %d points \n",nbr_villes,nbr_villes);
		}
		return nbr_villes;
	}

}



points comptage(grille_p g, int regle, int verbose)
{
	int nbr_forets=0;
	int nbr_ressources=0;
	int nbr_usines=0;
	int nbr_villes=0;
	int nbr_lacs=0;
	int nbr_plaines=0;
	int presence_carre=0;
	int presence_carre_max=0;
	int resultat_village=0;
	points points_partie;
	int i,j;
	for (i=0;i<g->size;i++)
	{
		for (j=0;j<g->size;j++)
		{
			if (g->cells[i][j].type=='F')
			{
				nbr_forets+=1;
			}
			if (g->cells[i][j].type=='R')
			{
				nbr_ressources+=1;
			}
			if (g->cells[i][j].type=='U')
			{
				nbr_usines+=1;
			}
			if (g->cells[i][j].type=='P')
			{
				nbr_plaines+=1;
			}
			if (g->cells[i][j].type=='L')
			{
				nbr_lacs+=1;
			}
			if (g->cells[i][j].type=='V')
			{
				presence_carre=0;
				resultat_village=test_village2(g,i,j, g->size, &presence_carre);
				if (nbr_villes<=resultat_village)
				{
					if (!(presence_carre_max==1 && nbr_villes==resultat_village))
					{
						nbr_villes = resultat_village;
						presence_carre_max = presence_carre;
					}					
				}
			}	
		}
	}


	points_partie.points_forets= pts_forets(regle,nbr_forets, verbose);
	points_partie.points_lacs= pts_lacs(regle,nbr_lacs,g, verbose);
	points_partie.points_r_and_u= pts_r_and_u(regle,nbr_usines,nbr_ressources, verbose);
	points_partie.points_plaines= pts_plaines(regle,nbr_plaines,g,verbose);
	points_partie.points_villes= pts_villes(regle,nbr_villes,verbose,presence_carre_max);

	points_partie.somme=points_partie.points_forets+points_partie.points_lacs+points_partie.points_villes+points_partie.points_r_and_u+points_partie.points_plaines;

	return points_partie;

}

points test_pb(grille_p g,int pb_lac, int pb_ressource, int pb_usine, int pb_foret, int pb_village, int pb_plaines, int regle, int verbose)
{
	int nbr_forets=0;
	int nbr_ressources=0;
	int nbr_usines=0;
	int nbr_villes=0;
	int nbr_lacs=0;
	int nbr_plaines=0;
	int presence_carre=0;
	int presence_carre_max=0;
	int resultat_village=0;
	points points_partie;
	int i,j;
	for (i=0;i<g->size;i++)
	{
		for (j=0;j<g->size;j++)
		{
			if (g->cells[i][j].type=='F')
			{
				nbr_forets+=1;
			}
			if (g->cells[i][j].type=='R')
			{
				nbr_ressources+=1;
			}
			if (g->cells[i][j].type=='U')
			{
				nbr_usines+=1;
			}
			if (g->cells[i][j].type=='P')
			{
				nbr_plaines+=1;
			}
			if (g->cells[i][j].type=='L')
			{
				nbr_lacs+=1;
			}
			if (g->cells[i][j].type=='V')
			{
				presence_carre=0;
				resultat_village=test_village2(g,i,j, g->size, &presence_carre);
				if (nbr_villes<=resultat_village)
				{
					if (!(presence_carre_max==1 && nbr_villes==resultat_village))
					{
						nbr_villes = resultat_village;
						presence_carre_max = presence_carre;
					}					
				}
			}	
		}
	}
	points_partie.points_forets= pts_forets(regle,nbr_forets, verbose);
	points_partie.points_lacs= pts_lacs(regle,nbr_lacs,g, verbose);
	points_partie.points_r_and_u= pts_r_and_u(regle,nbr_usines,nbr_ressources, verbose);
	points_partie.points_plaines= pts_plaines(regle,nbr_plaines,g,verbose);
	points_partie.points_villes= pts_villes(regle,nbr_villes,verbose,presence_carre_max);

	points_partie.somme=points_partie.points_forets+points_partie.points_lacs+points_partie.points_villes+points_partie.points_r_and_u+points_partie.points_plaines;

	points_partie.points_forets=(points_partie.somme-points_partie.points_forets)*(100-pb_foret);
	points_partie.points_lacs=(points_partie.somme-points_partie.points_lacs)*(100-pb_lac);
	points_partie.points_villes=(points_partie.somme-points_partie.points_villes)*(100-pb_village);
	points_partie.points_r_and_u=(points_partie.somme-points_partie.points_r_and_u)*(100-pb_usine-pb_ressource);
	points_partie.points_plaines=(points_partie.somme-points_partie.points_plaines)*(100-pb_plaines);
	points_partie.somme = points_partie.points_forets+points_partie.points_lacs+points_partie.points_villes+points_partie.points_r_and_u + points_partie.points_plaines;

	return points_partie;
}

int test_pb2(grille_p g, int pb_lac, int pb_village, int pb_somme, int regle)
{
	int villes=pb_village*100/pb_somme;
	int lacs=pb_lac*100/pb_somme;

	int nbr_forets=0;
	int nbr_ressources=0;
	int nbr_usines=0;
	int nbr_villes=0;
	int nbr_lacs=0;
	int nbr_plaines=0;
	int presence_carre=0;
	int presence_carre_max=0;
	int resultat_village=0;
	int i,j;
	for (i=0;i<g->size;i++)
	{
		for (j=0;j<g->size;j++)
		{
			if (g->cells[i][j].type=='F')
			{
				nbr_forets+=1;
			}
			if (g->cells[i][j].type=='R')
			{
				nbr_ressources+=1;
			}
			if (g->cells[i][j].type=='U')
			{
				nbr_usines+=1;
			}
			if (g->cells[i][j].type=='P')
			{
				nbr_plaines+=1;
			}
			if (g->cells[i][j].type=='L')
			{
				nbr_lacs+=1;
			}
			if (g->cells[i][j].type=='V')
			{
				presence_carre=0;
				resultat_village=test_village2(g,i,j, g->size, &presence_carre);
				if (nbr_villes<=resultat_village)
				{
					if (!(presence_carre_max==1 && nbr_villes==resultat_village))
					{
						nbr_villes = resultat_village;
						presence_carre_max = presence_carre;
					}					
				}
			}	
		}
	}
  /*
  calculer le nombre de ressources/usines si +usines privilégier les ressources et inversement dans l ajout des tuiles;*/
	if (villes<90)
	{
		if (lacs<10)
		{
			return (pts_r_and_u(regle,nbr_usines,nbr_ressources, 0)+pts_forets(regle,nbr_forets,0)+((pts_lacs(regle,nbr_lacs,g, 0)+pts_villes(regle,nbr_villes,0,presence_carre_max))/2));
		}
		else
		{
			return (pts_r_and_u(regle,nbr_usines,nbr_ressources, 0)+pts_forets(regle,nbr_forets,0)+pts_lacs(regle,nbr_lacs,g, 0)+((pts_villes(regle,nbr_villes,0,presence_carre_max))/2));
		}

	}
	else
	{
		if (lacs<4)
		{
			return (pts_r_and_u(regle,nbr_usines,nbr_ressources, 0)+pts_forets(regle,nbr_forets,0)+pts_villes(regle,nbr_villes,0,presence_carre_max)+((pts_lacs(regle,nbr_lacs,g, 0))/2));
		}
		else
		{
			if (lacs<4)
			{
				return (pts_r_and_u(regle,nbr_usines,nbr_ressources, 0)+pts_forets(regle,nbr_forets,0)+pts_villes(regle,nbr_villes,0,presence_carre_max)+pts_lacs(regle,nbr_lacs,g, 0));
			}
		}
	}
	return 0;
}