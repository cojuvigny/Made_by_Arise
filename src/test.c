#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "file.h"
#include "test.h"
#include "affichage.h"

Bool test_limitation(int x, int y, int size, int o)
{

	if (x>=size || y >= size || x<0 || y<0)
		return False;

	if ( o==0 || o==2){
		if (x+2 >= size || y+1 >= size)
			return False;
	}

	else{
		if (x+1 >= size || y+2 >= size)
			return False;
	}
	
	return True;
}


Bool test_recouvrement(ptour s, int x, int y, int o) {
	int szg=s->g->size;
	int szi=s->sz_index;
	if (test_limitation(x,y,szg,o)) {
		int dx,dy;
		if (o%2==0){
			dx=2;
			dy=1;
		}
		else {
			dx=1;
			dy=2;
		}
		
		int *verif_reco = (int*)malloc(szi*sizeof(int));

		if (verif_reco == NULL){
  			fprintf(stderr, "Problème allocation dynamique %s\n",__func__);
			exit(EXIT_FAILURE);
		}

		int k;
		for (k=0;k<szi;k++){
			verif_reco[k]=s->index[k]->nbrcases;
		}
		
		int i=0,j=0;
		cases current;
		int reco_case_vide=0;
		for (i=x ; i<=x+dx ; i++){
			for (j=y ; j<=y+dy ; j++){                
				current=s->g->cells[i][j];
				if (current.type=='L') {
                    /*fprintf(stderr, "\nVous ne pouvez pas recouvrir de lac\n");*/
					free(verif_reco);
					return False;
				}
				
				if (current.id==-1) {
					reco_case_vide++;
				}

				else {
					verif_reco[recherche_id(s,current.id)]-=1;
					if (verif_reco[recherche_id(s,current.id)]==0) {
                        /*fprintf(stderr, "\nVous ne pouvez pas recouvrir complètement une tuile\n");*/
						free(verif_reco);
						return False;
					}
				}

			}
		}

		if (reco_case_vide==6) {
            /*fprintf(stderr, "\nVous devez recouvrir au moins une case de l’une des tuiles précédentes\n");*/
			free(verif_reco);
			return False;
		}

		free(verif_reco);
		return True;
	}

	else{
        /*printf("\nVous dépassez les limites de la grille\n");*/
		return False;
	}
	
}

int recherche_id( ptour s,int id_recherche){
	pplacement * tab = s->index;
	int taille = s->sz_index;
	int i;

	for (i = 0; i < taille; ++i)
	{
		if (tab[i]->id == id_recherche){
			return i;
		}
	} 

	return -1;
}

int test_village(grille_p g,int x, int y, int size, int regle){
/*Conseil : pour tester cette fonction, passer en entrée une grille générée par create_grille_full_random en restregnant les
type à V et autre chose histoire d'avoir plus de villages et ainsi vérifier que ca marche ;)*/

	affichage_grille(g,1,0,regle);
	int nbville=0;

	if (g->cells[x][y].type != 'V'){
		printf("\nLe nombre de ville est : %d\n",nbville);
		return 1;
	}

	file_p tete = NULL,queue = NULL;

	nbville++;
	int ** mat_visite = NULL;
	mat_visite = allocation_dynamique_matrice(mat_visite,size);
	int entree[2] = {x,y};
	enfiler(entree,&tete,&queue,sizeof(entree));
	mat_visite[x][y]=1;

	while (!(tete == NULL && queue == NULL)){
		int sortie[2] ; 
		defiler(&tete,&queue,sortie,sizeof(sortie));
		int x = sortie[0];
		int y = sortie[1];

		if (x+1>=0 && x+1<size){
			if (g->cells[x+1][y].type == 'V' && mat_visite[x+1][y] == 0 ){
				entree[0]=x+1;
				entree[1]=y;
				enfiler(entree,&tete,&queue,sizeof(entree));
				mat_visite[x+1][y] = 1;
				nbville+=1;
			}
		}

		if (x-1>=0 && x-1<size){
			if (g->cells[x-1][y].type == 'V' && mat_visite[x-1][y] == 0 ){
				entree[0]=x-1;
				entree[1]=y;
				enfiler(entree,&tete,&queue,sizeof(entree));
				mat_visite[x-1][y] = 1;
				nbville+=1;
			}    		
		}

		if (y+1>=0 && y+1<size){
			if (g->cells[x][y+1].type == 'V' && mat_visite[x][y+1] == 0 ){
				entree[0]=x;
				entree[1]=y+1;
				enfiler(entree,&tete,&queue,sizeof(entree));
				mat_visite[x][y+1] = 1;
				nbville+=1;
			}    		
		}

		if (y-1>=0 && y-1<size){
			if (g->cells[x][y-1].type == 'V' && mat_visite[x][y-1] == 0 ){
				entree[0]=x;
				entree[1]=y-1;
				enfiler(entree,&tete,&queue,sizeof(entree));
				mat_visite[x][y-1] = 1;
				nbville+=1;
			}
			
		}

	}
	printf("\nLe nombre de ville est : %d\n",nbville);

	
	liberer(mat_visite,size);
	nettoyer(&tete,&queue);

	return 0;
}

