#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

#include "tuile.h"
#include "rwfile.h"
#include "affichage.h"
#include "grille.h"
#include "test.h"
#include "parcours.h"
#include "comptage.h"
#include "file.h"


int main(int argc, char* argv[]) {
    printf("\n-----------------------------------------------------------\n");
    printf("\n--------Récuperation des arguments d'entrée (lot A)--------\n");
    printf("\n-----------------------------------------------------------\n");
    
    nameProcessus = argv[0];
    char* name = argc == 1 ? "bin/tuile/tuile.01" : argv[1]; 
    char* grille_name_f = argc == 3 ? argv[2] : "bin/grille/grille.01";
    printf("\n-------------------------------------------------------------------------------\n");
    printf("\n--------Test sur l'orientation des tuiles et sur leur affichage (lot A)--------\n");
    printf("\n-------------------------------------------------------------------------------\n");

    ptuile t = init_tuile_random(1);
    ptuile tuile_copie = copie_tuile(t);
    int i;
    int regle = 0;

    rotation_tuile(t,&tuile_copie,0);/*On la fait pivoter de 0°*/
    affichage_tuile(tuile_copie,0);
    rotation_tuile(t,&tuile_copie,1);/*On la fait pivoter de 90*/
    affichage_tuile(tuile_copie,1);
    rotation_tuile(t,&tuile_copie,2);/*On la fait pivoter de 180°*/
    affichage_tuile(tuile_copie,2);
    rotation_tuile(t,&tuile_copie,3);/*On la fait pivoter de 270°*/
    affichage_tuile(tuile_copie,3);

    printf("\n---------------------------------------------------------------------------------------\n");
    printf("\n--------Test sur l'affichage de la grille, sur l'insertion d'une tuile ----------------\n");
    printf("\n--------sur l'affichage d'une case et sur les limites de la map (lot B)----------------\n");
    printf("\n---------------------------------------------------------------------------------------\n");

    
    grille_p g = create_grille_vide(15);

    if(test_limitation(6,6,15,2)){
        insert_tuile(g,7,7,tuile_copie,1);
    }
    affichage_grille(g,1,0,regle);


    printf("\n--------------------------------------------------------\n");
    printf("\n--------Test sur la recherche de village (lot C)--------\n");
    printf("\n--------------------------------------------------------\n");

    int x=14,y=0;
    grille_p g2 = create_grille_full_random(15);

    printf("\n La dalle choisie est (ligne/colonne) %d/%d est : %c \n\n",x,y,lire_dalle(g2,x,y));

    

    test_village(g2,x,y,15,regle);

    free_grille(g2);

    free_grille(g);
    free_tuile(t);
    free_tuile(tuile_copie);

	printf("\n---------------------------------------------------------\n");
    printf("\n---------Test sur la lecture de fichiers (lot A)---------\n");
    printf("\n---------------------------------------------------------\n");

    int size;
    ptuile* tuile_test = read_file_tuile(name, &size);
    for (i = 0; i < size; i++) fprintf(stderr, "tuile_test[%d] : %p\n", i, tuile_test[i]);
    char** liste_tuile = malloc(size * sizeof(char*));
    for (i = 0; i < size; i++) liste_tuile[i] = lire_tuile(tuile_test[i]); 
    for (i = 0; i < size; i++) printf("%s\n", liste_tuile[i]);

    printf("\n-------------------------------------------------------------------------------\n");
    printf("\n--------Test sur la création d'une partie et sur sa suppression (lot A)--------\n");
    printf("\n-------------------------------------------------------------------------------\n");



/*Vous ne pensez pas que faire le = partie de jeu est inutile puisque c'est un pointeur que l'on peut faire cibler à volonté?
Autant le faire dans la fonction add_tour je pense. J'aimerais aussi savoir si vous voulez creez une partie de jeu random 
sans besoin d'init les tuiles avant de lancer la partie de jeu 
( on ne demanderait que la taille du graphe et le nombre de tour par exemple)*/

    int taille_map = 10;
    
    ptuile t0 = init_tuile_random(0);

    parcours_p partie_de_jeu =  create_tour(t0,taille_map/2-1,taille_map/2-1,1,taille_map);

    ptuile t1 = init_tuile_random(1);
    ptuile t2 = init_tuile_random(2);
    ptuile t3 = init_tuile_random(3);

    if(add_tour(&partie_de_jeu,t1,3,3,0,taille_map)){
        affichage_tour(partie_de_jeu->tour,regle);
    }
		

	if(add_tour(&partie_de_jeu,t2,5,5,0,taille_map)){
        affichage_tour(partie_de_jeu->tour,regle);
    }
		

	if(add_tour(&partie_de_jeu,t3,6,6,0,taille_map)){
        affichage_tour(partie_de_jeu->tour,regle);
    }
		
    
    printf("\n-----On efface les différents tours-----\n\n"); 

    
    remove_tour(&partie_de_jeu);
    affichage_tour(partie_de_jeu->tour,regle);

    remove_tour(&partie_de_jeu);
    affichage_tour(partie_de_jeu->tour,regle);

    remove_tour(&partie_de_jeu);
    affichage_tour(partie_de_jeu->tour,regle);



    free(t0);
    free(t1);
    free(t2);
    free(t3);


    remove_all_tour(&partie_de_jeu);

    printf("\n-------------------------------------------\n");
    printf("\n--------Test comptage point (lot C)--------\n");
    printf("\n-------------------------------------------\n");
    

    grille_p gtest = NULL;
    gtest = create_grille_full_random(4);
    affichage_grille(gtest,1,0,regle);
    printf("%d",gtest->size);
    int a=comptage(gtest,regle,1).somme;
    printf("Le résultat est %d",a);
    free_grille(gtest);



    printf("\n---------------------------------------------\n");
    printf("\n--------Test écriture fichier (lot A)--------\n");
    printf("\n---------------------------------------------\n");

    ptuile * tab_tuile_rand = init_tuile_random_tab(50);


    int nbwrite = write_file_tuile("written_grille", tab_tuile_rand, 50);
    printf("nbwrite = %d\n", nbwrite);

    /*L'ensemble des free*/
    for (i = 0; i < size; i++) free_tuile(tuile_test[i]);
    free(tuile_test);
    for (i = 0; i < 50; i++) free_tuile(tab_tuile_rand[i]);
    free(tab_tuile_rand);
    for (i = 0; i < size; i++) free(liste_tuile[i]);
    free(liste_tuile);

    printf("\n-----------------------------------------------------\n");
    printf("\n--------Test ouverture fichier grille (lot A)--------\n");
    printf("\n-----------------------------------------------------\n");


    struct info_grille info;
    read_file_grille(grille_name_f, &info);
    for (i = 0; i < info.sz_main; i++) printf("%d ", info.ids[i]);
    printf("%p\n", info.ids);
    printf("sz_grille : %d, sz_main : %d, id_depart : %d\n", info.sz_grille, info.sz_main, info.id_depart);
    free(info.ids);
    struct info_grille info_t_test;
    info_t_test.sz_grille = 15;
    info_t_test.sz_main = 10;
    info_t_test.id_depart = 8;
    info_t_test.ids = malloc(info_t_test.sz_main * sizeof(int));
    info_t_test.ids[0] = 5;
    info_t_test.ids[1] = 2;
    info_t_test.ids[2] = 8;
    info_t_test.ids[3] = 3;
    info_t_test.ids[4] = 1;
    info_t_test.ids[5] = 10;
    info_t_test.ids[6] = 4;
    info_t_test.ids[7] = 6;
    info_t_test.ids[8] = 9;
    info_t_test.ids[9] = 7;
    write_file_grille("written_grille", info_t_test);
    free(info_t_test.ids);


    printf("\n--------------------------------------------------------\n");
    printf("\n--------Test sur la génération biaisée de terrain--------\n");
    printf("\n--------------------------------------------------------\n");

    int choix=0;
    int nombre_tuile = 12;
    int tab_res[6] = {0,0,0,0,0,0};
    for (int i = 0; i < 10000001; ++i)
    {
        choix = choix_aleatoire_biaise(100.0/6.0,100.0/6.0,100.0/6.0,100.0/6.0,100.0/6.0,100.0/6.0);
        tab_res[choix]+=1;
    }
     

    for (int i = 0; i < 6; ++i)
    {
        /*On voit que ca marche car on a tiré a peu pres le meme nombre de chaque tuile*/
        printf("terrain n°%d : %d\n",i,tab_res[i]);
    }

    /*Maintenant on applique sur un exemple, la somme des %age doit valoir 100 pour des resultats cohérents*/
    ptuile* tab_tuile_biaisee =  init_tuile_random_tab_biaise(nombre_tuile,70.0,00.0,00.0,30.0,00.0,00.0);

    /*Rappel : init_tuile_random_tab_biaise(nbtuile,%P,%F,%V,%R,%U,%L)*/

    for (int i = 0; i < nombre_tuile; ++i)
    {
        affichage_tuile(tab_tuile_biaisee[i],0);
    }
    for (int i = 0; i < nombre_tuile; ++i)
    {
        free_tuile(tab_tuile_biaisee[i]);
    }

    free(tab_tuile_biaisee);
    
    


    return EXIT_SUCCESS;
}
