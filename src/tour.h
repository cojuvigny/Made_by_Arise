#ifndef _TOUR_
#define _TOUR_

#include "tuile.h"
#include "grille.h"


/**
 * @file tour.h
 * 
 * Ce fichier décrit un ensemble de fonctions concernant les tours de jeu.
 * Il contient 6 fonctions et 2 types.
 *  
 * - le type \a placement contient des informations sur les tuiles posées sur le plateau de jeu à un tour donné
 * - le type \a tour contient l'ensemble des informations sur un tour de jeu donné
 * 
 * - ajout_placement(pplacement p, int idm, int nbrcasesm, int orientationm, int xm, int ym)	 modifie les valeurs d'un placement
 * - modif_nbr_cases (ptour t, int id)															 modifie la variable qui compte le nombre de cases encore présentes sur la grille ayant l'id entré
 * - modif_index(ptour precedant,ptour p, int orientation, int x, int y,int id)					 modifie l'index du tour p
 * - create_tour_vide(sz_grille, nb_tuile) 														 crée un tour de jeu à partir d'une grille vide de taille n 
 * - create_tour_vide_random(nb_tuile) 															 crée un tour de jeu à partir d'une grille vide de taille aléatoire
 * - free_tour(ptour t) 																		 libère la mémoire utilisée par une structure \a tour
 */



/**
 * \struct 	placement
 * \brief 	structure contenant des informations sur les tuiles posées sur le plateau de jeu à un tour donné
 *
 * Structure qui répertorie les tuiles posées sur la grille c'est-à-dire leur id,
 * leur nombre de cases non recouvertes, leur orientation et leur position
 */
typedef struct placement
{
	int id;
	int nbrcases;
	int orientation;
	int x;
	int y;
}placement,*pplacement;



/**
 * \struct 	tour
 * \brief 	structure contenant l'ensemble des informations sur un tour de jeu donné
 *
 * Structure de tour qui est composé du tableau d'index (tableau de placement),
 * de sa taille et de la grille de jeu du tour correspondant
 */
typedef struct tour
{
	pplacement * index;
	int sz_index;
	grille_p g;
}tour,*ptour;


/**
 * @brief 					modifie les valeurs d'un placement 
 * @param 	p 				un pointeur sur un placement 
 * @param 	idm 			l'id de la tuile courante
 * @param 	nbrcasesm 		le nombre de cases non recouvertes de la tuile
 * @param 	orientationm 	l'orientation de la tuile
 * @param 	xm 				la coordonnée x de la tuile
 * @param 	ym 				la coordonnée y de la tuile
 * @return 					rien
 */
void ajout_placement(pplacement p, int idm, int nbrcasesm, int orientationm, int xm, int ym);

/**
 * @brief 		modifie la variable qui compte le nombre de cases encore présentes sur la grille ayant l'id entré
 * @param 	t 	un tour 
 * @param 	id 	un entier
 * @return 		rien
 */
void modif_nbr_cases (ptour t, int id);

/**
 * @brief 					modifie l'index du tour p
 * @param 	precedent 		le tour précédent
 * @param 	p				le tour actuel
 * @param 	orientation		l'orientation de la tuile
 * @param 	x 				la coordonnée x de la tuile
 * @param 	y 				la coordonnée y de la tuile
 * @param 	id 				l'id de la tuile
 * @return 					rien
 */
void modif_index(ptour precedant,ptour p, int orientation, int x, int y,int id);


/**
 * @brief 					crée un tour de jeu à partir d'une grille vide de taille n 
 * @attention 				sz_grille supérieur à 3
 * @param 		sz_grille 	la taille de la grille,
 * @param 		nb_tuile 	le nombre de tuiles
 * @return 					un tour de jeu
 */
ptour create_tour_vide(int sz_grille, int nb_tuile);


/**
 * @brief 				crée un tour de jeu à partir d'une grille vide de taille aléatoire
 * @param 	nb_tuile 	le nombre de tuile
 * @return 				un tour de jeu
 */
ptour create_tour_vide_random(int nb_tuile);


/**
 * @brief 		libère la mémoire utilisée par une structure \a tour
 * @param 	t 	un pointeur de tour
 * @return 		rien
 */
void free_tour(ptour t);

#endif
