#ifndef _TEST_CUNIT_LOT_C_
#define _TEST_CUNIT_LOT_C_

#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

/**
 * @file test_cunit_lot_C.h
 * 
 * Ce fichier décrit un ensemble de fonctions concernant les test unitaires du lot C.
 * Il contient 5 fonctions.
 * 
 * - setup(void) 							fonction de mise à jour du catalogue
 * - teardown(void)							fonction de mise à jour du catalogue
 * - initialisation_catalogue_C(void) 		fonction d'initialisation du catalogue de tests du lot C
 * - test_func(void) 						teste la fonction func sur son résultat attendu
 */



/**
 * @brief	fonction de mise à jour du catalogue
 * @return  un entier
 */
int setup(void) ;

/**
 * @brief	fonction de mise à jour du catalogue
 * @return  un entier
 */
int teardown(void);

/**
 * @brief	fonction d'initialisation du catalogue de tests du lot C
 * @return  un entier
 */
int initialisation_catalogue_C(void);

/**
 * @brief	Fonction de test de comptage
 * @return  rien
 */
void test_comptage(void);

/**
 * @brief	Fonction de test de test_village
 * @return  rien
 */
void test_test_village(void);




#endif