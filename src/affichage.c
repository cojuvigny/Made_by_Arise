#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include "tuile.h"
#include "grille.h"
#include "affichage.h"
#include "tour.h"
#include "couleur.h"

void affichage_tour(ptour t,int regle){
	/*Utilisé pour le debeug*/
    //affichage_index_tour(t);
	affichage_grille(t->g,0,0,regle);
}

void affichage_tuile(ptuile t, int o){

	char* contenu_tuile = lire_tuile(t);
	int i,j,ligne,colonne;

	/**
	 * Si 0 ou 2 : tuile levée, sinon 1 ou 3
	 * ligne et colonne vont modifier la manière d'itérer
	 * sur la tuile
	 */
	if (o == 0 || o == 2){
		ligne = 3;
		colonne = 2;		
	}
	
	else{
		ligne = 2;
		colonne = 3;
	}	

	/**
	 * On itère sur la tuile différemment en fonction de
	 * son orientation
	 */
	for (i = 0; i < ligne; i++) {
		for (j = 0; j < colonne; j++) 
			affichage_couleur(contenu_tuile[colonne * i + j],0);
		printf("\n");
	}
	printf("\n");
	free(contenu_tuile);
}

void affichage_grille(grille_p g,int info_sup, int help, int regle){
	printf("les regles  %d\n",regle);

	int size = g->size ;
	int i,j;

	/*On efface l'écran*/
	clrscr();

	printf(ROUGE "Placer une tuile : \n\n" RESET);

	printf("x\\y");

	/**
	 * On affiche la grille de jeu
	 */
	for (i = 0; i < size; ++i) 
	{
		printf("%2d ",i);
	}

	printf("\n");
	
	for (i = 0; i < size; ++i)
	{
		printf("%2d ",i);
		for (j = 0; j < size; ++j)
		{
			affichage_couleur(g->cells[i][j].type,0);
		}

		printf("\n");
	}

	/**
	 * Si on veut afficher la grille des id qui donne des infos en plus
	 */
	if (info_sup==1)
	{
		printf("\n\n");
		for (i = 0; i < size; ++i)
		{
			for (j = 0; j < size; ++j)
			{
				printf("%2d ",g->cells[i][j].id);
			}

			printf("\n");
		}
	}

	/*On affiche de l'aide pour le marquage de points*/
	printf("\n");
	if (help)
	{
		if (regle == 0)
		{
			printf(LAC "Lac" RESET"  		  : nblac*2 \n");
		}
		else
		{
			printf(LAC "Lac" RESET"  		  : (nblac-1)*3 \n");
		}

		if (regle == 3)
		{
			printf(VILLAGE "Ville" RESET" 	          : nbville(max(village)) + carre*4" RESET "\n");
		}
		else
		{
			printf(VILLAGE "Ville" RESET" 	          : nbville(max(village))" RESET "\n");
		}
		
		if (regle == 4)
		{
			printf(FORET "Forêt" RESET" 	          : 1ere=1, 2eme=2,...,5eme=5, 6eme=5,... \n");
		}
		else
		{
			printf(FORET "Forêt" RESET" 	          : nbforet*2 \n");
		}
		
		if (regle == 2)
		{
			printf(USINE "Usine" RESET" et " RESSOURCE "Ressource" RESET": 4*min(2*nbusine,nbressource) \n");
		}
		else
		{
			printf(USINE "Usine" RESET" et " RESSOURCE "Ressource" RESET": 4*min(nbusine,nbressource) \n");
		}	

		if (regle == 1)
		{
			printf(PLAINE "Plaine" RESET" 	          : nbplainede4cases*4" RESET "\n");
		}
		printf("\n");
		printf("Le nombre de points est de %d\n",comptage(g,regle,1).somme);
	}
}

void affichage_solution(resultat * save){

	grille_p g = save->partie_opti->tour->g;

	int size = g->size ;
	int i,j;


	printf("x\\y");

	/**
	 * On affiche la grille de jeu
	 */
	for (i = 0; i < size; ++i) 
	{
		printf("%2d ",i);
	}

	printf("\n");
	
	for (i = 0; i < size; ++i)
	{
		printf("%2d ",i);
		for (j = 0; j < size; ++j)
		{
			affichage_couleur(g->cells[i][j].type,0);
		}

		printf("\n");
	}

	printf("%s\n",save->description);
	printf("Points : %d\n",save->points_partie);
}

void affichage_grille_no_limit(grille_p g,int id_courant,int info_sup, int help, int regle){

	int size = g->size ;
	int i,j;
	clrscr();

	printf(ROUGE "Placer une tuile : \n\n" RESET);

	printf("x\\y");

	for (i = 0; i < size; ++i) /*Horizontale*/
	{
		printf("%2d ",i);
	}

	printf("\n");
	
	/**
	 * On affiche la grille de jeu
	 */
	for (i = 0; i < size; ++i)
	{
		printf("%2d ",i);
		for (j = 0; j < size; ++j)
		{
			char recup =g->cells[i][j].type;
			if (recup!='.' && id_courant==g->cells[i][j].id)
			{
				affichage_couleur(recup,1);
			}
			else
				affichage_couleur(g->cells[i][j].type,0);
			
		}

		printf("\n");
	}

	/**
	 * Si on veut afficher la grille des id qui donne des infos en plus
	 */
	if (info_sup==1)
	{
		printf("\n\n");
		for (i = 0; i < size; ++i)
		{
			for (j = 0; j < size; ++j)
			{
				printf("%2d ",g->cells[i][j].id);
			}

			printf("\n");
		}
	}

	/*On affiche de l'aide pour le marquage de points*/
	printf("\n");
	if (help)
	{
		if (regle == 0)
		{
			printf(LAC "Lac" RESET"  		  : nblac*2 \n");
		}
		else
		{
			printf(LAC "Lac" RESET"  		  : (nblac-1)*3 \n");
		}

		if (regle == 3)
		{
			printf(VILLAGE "Ville" RESET" 	          : nbville(max(village)) + carre*4" RESET "\n");
		}
		else
		{
			printf(VILLAGE "Ville" RESET" 	          : nbville(max(village))" RESET "\n");
		}
		
		if (regle == 4)
		{
			printf(FORET "Forêt" RESET" 	          : 1ere=1, 2eme=2,...,5eme=5, 6eme=5,... \n");
		}
		else
		{
			printf(FORET "Forêt" RESET" 	          : nbforet*2 \n");
		}
		
		if (regle == 2)
		{
			printf(USINE "Usine" RESET" et " RESSOURCE "Ressource" RESET": 4*min(2*nbusine,nbressource) \n");
		}
		else
		{
			printf(USINE "Usine" RESET" et " RESSOURCE "Ressource" RESET": 4*min(nbusine,nbressource) \n");
		}	

		if (regle == 1)
		{
			printf(PLAINE "Plaine" RESET" 	          : nbplainede4cases*4" RESET "\n");
		}
		printf("\n");
		printf("Le nombre de points est de %d\n",comptage(g,regle,1).somme);
	}
}


void affichage_index_tour(ptour tour){

	int size = tour->sz_index ;
	int i;
	/**
	 * Informations utiles pour le debug
	 */
	printf("\n\n----------Index tour n°%d----------\n",size-1);
	for (i = 0; i < size; ++i)
	{	
		printf("\n-----id %d-----\n",tour->index[i]->id);
		printf("nbrcases : %d\n",tour->index[i]->nbrcases);
		printf("orientation : %d\n",tour->index[i]->orientation);
		printf("x : %d\n",tour->index[i]->x);
		printf("y : %d\n",tour->index[i]->y);
	}
}


void affichage_couleur(char terrain, int danger)
{
	switch (terrain){
	case 'F':
		if (danger) printf(DANGER FORET "%2c " RESET,terrain);
		else printf(FORET "%2c " RESET,terrain);
		break;
	case 'P':
		if (danger) printf(DANGER PLAINE "%2c " RESET,terrain);
		else printf(PLAINE "%2c " RESET,terrain);
		break; 
	case 'L':
		if (danger) printf(DANGER LAC "%2c " RESET,terrain);
		else printf(LAC "%2c " RESET,terrain);
		break;
	case 'V':
		if (danger) printf(DANGER VILLAGE "%2c " RESET,terrain);
		else printf(VILLAGE "%2c " RESET,terrain);
		break;
	case 'R':
		if (danger) printf(DANGER RESSOURCE "%2c " RESET,terrain);
		else printf(RESSOURCE "%2c " RESET,terrain);
		break;
	case 'U':
		if (danger) printf(DANGER USINE "%2c " RESET,terrain);
		else printf(USINE "%2c " RESET,terrain);
		break;
		default:
		printf(NOIR  "%2c " RESET,terrain);
	}

}
