#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <pthread.h>

#include "CUnit/CUnit.h"
#include "CUnit/Basic.h"

#include "tuile.h"
#include "rwfile.h"
#include "affichage.h"
#include "grille.h"
#include "test.h"
#include "tour.h"
#include "parcours.h"
#include "file.h"
#include "comptage.h"
#include "optimisation.h"
#include "test_cunit_lot_C.h"
#include "aetoile.h"
#include "optimisation.h"
#include "solveur.h"



static void *task1(void *p_data)
{
	if (p_data != NULL)
	{
		data_solveur * p = p_data;
		int nombre_tuile_restante = p->nombre_tuile_restante;
		int nombre_tuile_totale = p->nombre_tuile_totale;
		file_p tete = p->tete,queue = p->queue;;
		resultat *resu  = p->res;
		parcours_p partie = p->partie;
		int regle = p->regle;

		a_etoile(nombre_tuile_restante,nombre_tuile_totale,0,tete,&partie,resu,regle);
		nettoyer_tuile(&tete,&queue);
	}
	
	return NULL;
}

static void *task2(void *p_data)
{
	if (p_data != NULL)
	{
		data_solveur * p = p_data;
		int nombre_tuile_restante = p->nombre_tuile_restante;
		int nombre_tuile_totale = p->nombre_tuile_totale;
		file_p tete = p->tete,queue = p->queue;
		resultat *resu  = p->res;
		parcours_p partie = p->partie;
		int regle = p->regle;

		a_etoile(nombre_tuile_restante,nombre_tuile_totale,1,tete,&partie,resu,regle);
		nettoyer_tuile(&tete,&queue);
	}
	return NULL;
}

static void *task3(void *p_data)
{
	if (p_data != NULL)
	{
		ptuile *sortie2 = (ptuile *)calloc(1,sizeof(ptuile));

		data_solveur * p = p_data;
		file_p tete = p->tete,queue = p->queue;
		resultat *resu  = p->res;
		parcours_p partie = p->partie;
		int nombre_tuile_restante = p->nombre_tuile_restante;
		int regle = p->regle;

		ptuile * tab_tuile = (ptuile *)calloc(nombre_tuile_restante,sizeof(ptuile));
		int i=0;
		ptuile *t_choix;
		file_p copie_tete = tete;
		while (copie_tete != NULL)
		{
        	t_choix = copie_tete->data;
			tab_tuile[i] = *t_choix; 
			i++;
			copie_tete = copie_tete->suite;

		}

		inclusion_rec(&partie,&tete,&queue,sortie2,0,1,resu,regle); /*optimisation.c l 444*/
		for (int i = 0; i < p->nombre_tuile_restante; ++i)
		{
			free_tuile(tab_tuile[i]);
		}
		free(tab_tuile);
		free(sortie2);
	}
	return NULL;

}

static void *task4(void *p_data)
{
	if (p_data != NULL)
	{
		ptuile *sortie2 = (ptuile *)calloc(1,sizeof(ptuile));

		data_solveur * p = p_data;
		file_p tete = p->tete,queue = p->queue;
		resultat *resu  = p->res;
		parcours_p partie = p->partie;
		int nombre_tuile_restante = p->nombre_tuile_restante;
		int regle = p->regle;

		ptuile * tab_tuile = (ptuile *)calloc(nombre_tuile_restante,sizeof(ptuile));
		int i=0;
		ptuile *t_choix;
		file_p copie_tete = tete;
		while (copie_tete != NULL)
		{
        	t_choix = copie_tete->data;
			tab_tuile[i] = *t_choix; 
			i++;
			copie_tete = copie_tete->suite;

		}
		inclusion_rec(&partie,&tete,&queue,sortie2,0,0,resu,regle);/*optimisation.c l 475*/
		for (int i = 0; i < p->nombre_tuile_restante; ++i)
		{
			free_tuile(tab_tuile[i]);
		}
		free(tab_tuile);
		free(sortie2);
	}
	return NULL;
}

static void *task5(void *p_data)
{
	if (p_data != NULL)
	{
		ptuile *sortie2 = (ptuile *)calloc(1,sizeof(ptuile));

		data_solveur * p = p_data;
		file_p tete = p->tete,queue = p->queue;
		resultat *resu  = p->res;
		parcours_p partie = p->partie;
		int nombre_tuile_restante = p->nombre_tuile_restante;
		int regle = p->regle;

		ptuile * tab_tuile = (ptuile *)calloc(nombre_tuile_restante,sizeof(ptuile));
		int i=0;
		ptuile *t_choix;
		file_p copie_tete = tete;
		while (copie_tete != NULL)
		{
        	t_choix = copie_tete->data;
			tab_tuile[i] = *t_choix; 
			i++;
			copie_tete = copie_tete->suite;

		}
		village_inclusion_rec(&partie,&tete,&queue,sortie2,0,resu,regle);/*optimisation.c l 585*/
		for (int i = 0; i < p->nombre_tuile_restante; ++i)
		{
			free_tuile(tab_tuile[i]);
		}
		free(tab_tuile);
		free(sortie2);
	}

	return NULL;
}

static void *task6(void *p_data)
{
	if (p_data != NULL)
	{
		ptuile *sortie2 = (ptuile *)calloc(1,sizeof(ptuile));

		data_solveur * p = p_data;
		file_p tete = p->tete,queue = p->queue;
		resultat *resu  = p->res;
		parcours_p partie = p->partie;
		int nombre_tuile_restante = p->nombre_tuile_restante;
		int regle = p->regle;

		ptuile * tab_tuile = (ptuile *)calloc(nombre_tuile_restante,sizeof(ptuile));
		int i=0;
		ptuile *t_choix;
		file_p copie_tete = tete;
		while (copie_tete != NULL)
		{
        	t_choix = copie_tete->data;
			tab_tuile[i] = *t_choix; 
			i++;
			copie_tete = copie_tete->suite;

		}

		lac_inclusion_rec(&partie,&tete,&queue,sortie2,0,resu,regle);
		for (int i = 0; i < p->nombre_tuile_restante; ++i)
		{
			free_tuile(tab_tuile[i]);
		}
		free(tab_tuile);
		free(sortie2);
	}
	return NULL;
}

static void *task7(void *p_data)
{
	if (p_data != NULL)
	{
		ptuile *sortie2 = (ptuile *)calloc(1,sizeof(ptuile));

		data_solveur * p = p_data;
		file_p tete = p->tete,queue = p->queue;
		resultat *resu  = p->res;
		parcours_p partie = p->partie;
		int nombre_tuile_restante = p->nombre_tuile_restante;
		int regle = p->regle;

		ptuile * tab_tuile = (ptuile *)calloc(nombre_tuile_restante,sizeof(ptuile));
		int i=0;
		ptuile *t_choix;
		file_p copie_tete = tete;
		while (copie_tete != NULL)
		{
        	t_choix = copie_tete->data;
			tab_tuile[i] = *t_choix; 
			i++;
			copie_tete = copie_tete->suite;

		}

		max_modifgrille_rec(&partie,&tete,&queue,sortie2,0,resu,regle);
		for (int i = 0; i < p->nombre_tuile_restante; ++i)
		{
			free_tuile(tab_tuile[i]);
		}
		free(tab_tuile);
		free(sortie2);
	}
	return NULL;
}

static void *task8(void *p_data)
{
	if (p_data != NULL)
	{
		ptuile *sortie2 = (ptuile *)calloc(1,sizeof(ptuile));

		data_solveur * p = p_data;
		file_p tete = p->tete,queue = p->queue;
		resultat *resu  = p->res;
		parcours_p partie = p->partie;
		int nombre_tuile_restante = p->nombre_tuile_restante;
		int regle = p->regle;

		ptuile * tab_tuile = (ptuile *)calloc(nombre_tuile_restante,sizeof(ptuile));
		int i=0;
		ptuile *t_choix;
		file_p copie_tete = tete;
		while (copie_tete != NULL)
		{
        	t_choix = copie_tete->data;
			tab_tuile[i] = *t_choix; 
			i++;
			copie_tete = copie_tete->suite;

		}

		max_modiftuile_rec (&partie,&tete,&queue,sortie2,0,proba_tuiles (tete),resu,regle);
		for (int i = 0; i < p->nombre_tuile_restante; ++i)
		{
			free_tuile(tab_tuile[i]);
		}
		free(tab_tuile);
		free(sortie2);
	}
	return NULL;
}

/*
static void *task9(void *p_data)
{
	if (p_data != NULL)
	{
		ptuile *sortie2 = (ptuile *)calloc(1,sizeof(ptuile));

		data_solveur * p = p_data;
		file_p tete = p->tete,queue = p->queue;
		resultat *resu  = p->res;
		parcours_p partie = p->partie;
		int nombre_tuile_restante = p->nombre_tuile_restante;

		ptuile * tab_tuile = (ptuile *)calloc(nombre_tuile_restante,sizeof(ptuile));
		int i=0;
		ptuile *t_choix = (ptuile *)calloc(1,sizeof(ptuile));
		file_p copie_tete = tete;
		while (copie_tete != NULL)
		{
        	t_choix = copie_tete->data;
			tab_tuile[i] = *t_choix; 
			i++;
			copie_tete = copie_tete->suite;

		}

		inclusion2_rec(&partie,&tete,&queue,sortie2,0,1,resu);
		for (int i = 0; i < p->nombre_tuile_restante; ++i)
		{
			free_tuile(tab_tuile[i]);
		}
		free(tab_tuile);
		free(sortie2);
	}
	return NULL;
}

static void *task10(void *p_data)
{
	if (p_data != NULL)
	{
		ptuile *sortie2 = (ptuile *)calloc(1,sizeof(ptuile));

		data_solveur * p = p_data;
		file_p tete = p->tete,queue = p->queue;
		resultat *resu  = p->res;
		parcours_p partie = p->partie;
		int nombre_tuile_restante = p->nombre_tuile_restante;

		ptuile * tab_tuile = (ptuile *)calloc(nombre_tuile_restante,sizeof(ptuile));
		int i=0;
		ptuile *t_choix = (ptuile *)calloc(1,sizeof(ptuile));
		file_p copie_tete = tete;
		while (copie_tete != NULL)
		{
        	t_choix = copie_tete->data;
			tab_tuile[i] = *t_choix; 
			i++;
			copie_tete = copie_tete->suite;

		}

		inclusion2_rec(&partie,&tete,&queue,sortie2,0,0,resu);
		for (int i = 0; i < p->nombre_tuile_restante; ++i)
		{
			free_tuile(tab_tuile[i]);
		}
		free(tab_tuile);
		free(sortie2);
	}
	return NULL;
}

static void *task11(void *p_data)
{
	if (p_data != NULL)
	{
		ptuile *sortie2 = (ptuile *)calloc(1,sizeof(ptuile));

		data_solveur * p = p_data;
		file_p tete = p->tete,queue = p->queue;
		resultat *resu  = p->res;
		parcours_p partie = p->partie;
		int nombre_tuile_restante = p->nombre_tuile_restante;

		ptuile * tab_tuile = (ptuile *)calloc(nombre_tuile_restante,sizeof(ptuile));
		int i=0;
		ptuile *t_choix = (ptuile *)calloc(1,sizeof(ptuile));
		file_p copie_tete = tete;
		while (copie_tete != NULL)
		{
        	t_choix = copie_tete->data;
			tab_tuile[i] = *t_choix; 
			i++;
			copie_tete = copie_tete->suite;

		}

		inclusion3_rec(&partie,&tete,&queue,sortie2,0,1,resu);
		for (int i = 0; i < p->nombre_tuile_restante; ++i)
		{
			free_tuile(tab_tuile[i]);
		}
		free(tab_tuile);
		free(sortie2);
	}
	return NULL;
}

static void *task12(void *p_data)
{
	if (p_data != NULL)
	{
		ptuile *sortie2 = (ptuile *)calloc(1,sizeof(ptuile));

		data_solveur * p = p_data;
		file_p tete = p->tete,queue = p->queue;
		resultat *resu  = p->res;
		parcours_p partie = p->partie;
		int nombre_tuile_restante = p->nombre_tuile_restante;

		ptuile * tab_tuile = (ptuile *)calloc(nombre_tuile_restante,sizeof(ptuile));
		int i=0;
		ptuile *t_choix = (ptuile *)calloc(1,sizeof(ptuile));
		file_p copie_tete = tete;
		while (copie_tete != NULL)
		{
        	t_choix = copie_tete->data;
			tab_tuile[i] = *t_choix; 
			i++;
			copie_tete = copie_tete->suite;

		}

		inclusion3_rec(&partie,&tete,&queue,sortie2,0,0,resu);
		for (int i = 0; i < p->nombre_tuile_restante; ++i)
		{
			free_tuile(tab_tuile[i]);
		}
		free(tab_tuile);
		free(sortie2);
	}
	return NULL;
}
*/

resultat solveur(int nombre_tuile_restante,int nombre_tuile_totale, int taille_grille,file_p main_jeu_tete, parcours_p jeu, int regle){
	
    pthread_attr_t att;
    pthread_attr_init(&att);
    /*printf("%d\n",nombre_tuile_restante);
    printf("%d\n",nombre_tuile_totale);
    printf("%d\n",taille_grille);
    print_file_tuile(main_jeu_tete,0,nombre_tuile_restante);
    affichage_tour(jeu->tour);*/


	resultat * tableau_resultat = (resultat *)calloc(12,sizeof(resultat));

	file_p main_jeu_tete1=NULL,main_jeu_queue1=NULL;
	file_p main_jeu_tete2=NULL,main_jeu_queue2=NULL;
	file_p main_jeu_tete3=NULL,main_jeu_queue3=NULL;
	file_p main_jeu_tete4=NULL,main_jeu_queue4=NULL;
	file_p main_jeu_tete5=NULL,main_jeu_queue5=NULL;
	file_p main_jeu_tete6=NULL,main_jeu_queue6=NULL;
	file_p main_jeu_tete7=NULL,main_jeu_queue7=NULL;
	file_p main_jeu_tete8=NULL,main_jeu_queue8=NULL;
	/*file_p main_jeu_tete9=NULL,main_jeu_queue9=NULL;
	file_p main_jeu_tete10=NULL,main_jeu_queue10=NULL;
	file_p main_jeu_tete11=NULL,main_jeu_queue11=NULL;
	file_p main_jeu_tete12=NULL,main_jeu_queue12=NULL;*/

	parcours_p copie_partie_1 = NULL;
	parcours_p copie_partie_2 = NULL;
	parcours_p copie_partie_3 = NULL;
	parcours_p copie_partie_4 = NULL;
	parcours_p copie_partie_5 = NULL;
	parcours_p copie_partie_6 = NULL;
	parcours_p copie_partie_7 = NULL;
	parcours_p copie_partie_8 = NULL;
	/*parcours_p copie_partie_9 = NULL;
	parcours_p copie_partie_10 = NULL;
	parcours_p copie_partie_11 = NULL;
	parcours_p copie_partie_12 = NULL;*/
	copie_partie(&jeu,&copie_partie_1);
	copie_partie(&jeu,&copie_partie_2);
	copie_partie(&jeu,&copie_partie_3);
	copie_partie(&jeu,&copie_partie_4);
	copie_partie(&jeu,&copie_partie_5);
	copie_partie(&jeu,&copie_partie_6);
	copie_partie(&jeu,&copie_partie_7);
	copie_partie(&jeu,&copie_partie_8);
	/*copie_partie(&jeu,&copie_partie_9);
	copie_partie(&jeu,&copie_partie_10);
	copie_partie(&jeu,&copie_partie_11);
	copie_partie(&jeu,&copie_partie_12);*/

	ptuile *t_choix; /*Conteneur pour les tuiles défilées*/
	while (main_jeu_tete != NULL) {
      size_t sz_tuile = sizeof(ptuile);
      t_choix = main_jeu_tete->data; /*On récupère la première tuile courante */
      ptuile t_choix_c1 = copie_tuile(*t_choix);
		enfiler_tri(&t_choix_c1,&main_jeu_tete1,&main_jeu_queue1,sz_tuile);

		ptuile t_choix_c2 = copie_tuile(*t_choix);
		enfiler_tri(&t_choix_c2,&main_jeu_tete2,&main_jeu_queue2,sz_tuile);

		ptuile t_choix_c3 = copie_tuile(*t_choix);
		enfiler_tri(&t_choix_c3,&main_jeu_tete3,&main_jeu_queue3,sz_tuile);

		ptuile t_choix_c4 = copie_tuile(*t_choix);
		enfiler_tri(&t_choix_c4,&main_jeu_tete4,&main_jeu_queue4,sz_tuile);

		ptuile t_choix_c5 = copie_tuile(*t_choix);
		enfiler_tri(&t_choix_c5,&main_jeu_tete5,&main_jeu_queue5,sz_tuile);

		ptuile t_choix_c6 = copie_tuile(*t_choix);
		enfiler_tri(&t_choix_c6,&main_jeu_tete6,&main_jeu_queue6,sz_tuile);

		ptuile t_choix_c7 = copie_tuile(*t_choix);
		enfiler_tri(&t_choix_c7,&main_jeu_tete7,&main_jeu_queue7,sz_tuile);

		ptuile t_choix_c8 = copie_tuile(*t_choix);
		enfiler_tri(&t_choix_c8,&main_jeu_tete8,&main_jeu_queue8,sz_tuile);

		/*ptuile t_choix_c9 = copie_tuile(*t_choix);
		enfiler_tri(&t_choix_c9,&main_jeu_tete9,&main_jeu_queue9,sizeof(*t_choix));

		ptuile t_choix_c10 = copie_tuile(*t_choix);
		enfiler_tri(&t_choix_c10,&main_jeu_tete10,&main_jeu_queue10,sizeof(*t_choix));

		ptuile t_choix_c11 = copie_tuile(*t_choix);
		enfiler_tri(&t_choix_c11,&main_jeu_tete11,&main_jeu_queue11,sizeof(*t_choix));

		ptuile t_choix_c12 = copie_tuile(*t_choix);
		enfiler_tri(&t_choix_c12,&main_jeu_tete12,&main_jeu_queue12,sizeof(*t_choix));*/


		main_jeu_tete = main_jeu_tete->suite;
	}
	pthread_t t1,t2,t3,t4,t5,t6,t7,t8/*,t9,t10,t11,t12*/;

	data_solveur data_1 = {nombre_tuile_restante,nombre_tuile_totale,taille_grille, main_jeu_tete1,main_jeu_queue1,&tableau_resultat[0],copie_partie_1,regle};

	data_solveur data_2 = {nombre_tuile_restante,nombre_tuile_totale,taille_grille, main_jeu_tete2,main_jeu_queue2,&tableau_resultat[1],copie_partie_2,regle};

	data_solveur data_3 = {nombre_tuile_restante,nombre_tuile_totale,taille_grille, main_jeu_tete3,main_jeu_queue3,&tableau_resultat[2],copie_partie_3,regle};

	data_solveur data_4 = {nombre_tuile_restante,nombre_tuile_totale,taille_grille, main_jeu_tete4,main_jeu_queue4,&tableau_resultat[3],copie_partie_4,regle};

	data_solveur data_5 = {nombre_tuile_restante,nombre_tuile_totale,taille_grille, main_jeu_tete5,main_jeu_queue5,&tableau_resultat[4],copie_partie_5,regle};

	data_solveur data_6 = {nombre_tuile_restante,nombre_tuile_totale,taille_grille, main_jeu_tete6,main_jeu_queue6,&tableau_resultat[5],copie_partie_6,regle};

	data_solveur data_7 = {nombre_tuile_restante,nombre_tuile_totale,taille_grille, main_jeu_tete7,main_jeu_queue7,&tableau_resultat[6],copie_partie_7,regle};

	data_solveur data_8 = {nombre_tuile_restante,nombre_tuile_totale,taille_grille, main_jeu_tete8,main_jeu_queue8,&tableau_resultat[7],copie_partie_8,regle};

	/*data_solveur data_9 = {nombre_tuile_restante,nombre_tuile_totale,taille_grille, main_jeu_tete9,main_jeu_queue9,&tableau_resultat[8],copie_partie_9};

	data_solveur data_10 = {nombre_tuile_restante,nombre_tuile_totale,taille_grille, main_jeu_tete10,main_jeu_queue10,&tableau_resultat[9],copie_partie_10};

	data_solveur data_11 = {nombre_tuile_restante,nombre_tuile_totale,taille_grille, main_jeu_tete11,main_jeu_queue11,&tableau_resultat[10],copie_partie_11};

	data_solveur data_12 = {nombre_tuile_restante,nombre_tuile_totale,taille_grille, main_jeu_tete12,main_jeu_queue12,&tableau_resultat[11],copie_partie_12};*/

	pthread_create (&t1, &att, task1, &data_1);
	pthread_create (&t2, &att, task2, &data_2);
	pthread_create (&t3, &att, task3, &data_3);
	pthread_create (&t4, &att, task4, &data_4);
	pthread_create (&t5, &att, task5, &data_5);
	pthread_create (&t6, &att, task6, &data_6);
	pthread_create (&t7, &att, task7, &data_7);
	pthread_create (&t8, &att, task8, &data_8);
	/*pthread_create (&t9, &att, task9, &data_9);
	pthread_create (&t10, &att, task10, &data_10);
	pthread_create (&t11, &att, task11, &data_11);
	pthread_create (&t12, &att, task12, &data_12);*/

	pthread_join (t1, NULL);
	pthread_join (t2, NULL);
	pthread_join (t3, NULL);
	pthread_join (t4, NULL);
	pthread_join (t5, NULL);
	pthread_join (t6, NULL);
	pthread_join (t7, NULL);
	pthread_join (t8, NULL);
	/*pthread_join (t9, NULL);
	pthread_join (t10, NULL);
	pthread_join (t11, NULL);
	pthread_join (t12, NULL);*/

	int id_max = best_of_the_best(tableau_resultat);

	resultat save;

	copie_partie(&(tableau_resultat[id_max].partie_opti),&(save.partie_opti));
	save.points_partie = tableau_resultat[id_max].points_partie;
	save.description = tableau_resultat[id_max].description;
	
	for (int i = 0; i < 12; ++i)
		remove_all_tour(&(tableau_resultat[i].partie_opti));
	
	free(tableau_resultat);

	return save;
}

int best_of_the_best(resultat * tableau_resultat)
{
	int max = tableau_resultat[0].points_partie;
	int id_max = 0;
	int courant = 0;
	for (int i = 0; i < 8; ++i)
	{
		courant = tableau_resultat[i].points_partie;
		if (tableau_resultat[i].points_partie > max)
		{
			id_max = i;
			max = courant;
		}
		
	}

	return id_max;
}


void nettoyer_tableau_tuile(ptuile * tab_tuile,int nombre_tuile)
{
	for (int i = 0; i <= nombre_tuile; ++i)
	{
		free_tuile(tab_tuile[i]);
	}

	free(tab_tuile);
}

	
