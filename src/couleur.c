#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <time.h>


#include "couleur.h"
#include "grille.h"
#include "parcours.h"
#include "tour.h"
#include "affichage.h"
#include "test.h"
#include "loop.h"


grille_p grille_evolution(grille_p g)
{
	int size = g->size;
	grille_p grille_nav = create_grille_vide(size);;
	copy_grille(g,grille_nav);	
	
	return grille_nav;
}


int bouger_tuile(ptour tour,ptuile t,int *x,int *y, int *o, int regle)
{
	
	grille_p grille_nav = NULL;
	grille_p g = tour->g;
	

	int c1 = 0;
	int invalide=0;
	int arret=0;
	int more_info=0;
	int help=0;
	int size = g->size;
	*x = 0;
	*y = 0;
	*o = 0;


	while(!(c1==13) || invalide==0)
	{	

		grille_nav = grille_evolution(g);
		//affichage_grille(grille_nav,more_info,help);
		

		if( c1=='r')
		{
			*o=(*o+1)%4;
		}
		if( c1==65)
		{
			free_grille(grille_nav);
			grille_nav = grille_evolution(g);
			(*x)--;
			if (*x<=-3 && (*o==0 ||*o==2))
				*x=size-1;
			if (*x<=-2 && (*o==1 ||*o==3))
				*x=size-1;
		} 
		if( c1==66)
		{
			free_grille(grille_nav);
			grille_nav = grille_evolution(g);
			(*x)++;
			if (*x>=size && (*o==0 ||*o==2))
				*x=-2;
			if (*x>=size && (*o==1 ||*o==3))
				*x=-1;
		} 
		if( c1==67)
		{
			free_grille(grille_nav);
			grille_nav = grille_evolution(g);
			(*y)++;
			if (*y>=size && (*o==0 ||*o==2))
				*y=-1;
			if (*y>=size && (*o==1 ||*o==3))
				*y=-2;
		} 
		if( c1==68)
		{
			free_grille(grille_nav);
			grille_nav = grille_evolution(g);
			(*y)--;
			if (*y<=-2 && (*o==0 ||*o==2))
				*y=size-1;
			if (*y<=-3 && (*o==1 ||*o==3))
				*y=size-1; 
		} 
		if(!test_recouvrement(tour,*x,*y,*o)){
			invalide=0;
			insert_tuile_no_limit(grille_nav,*x,*y,t,*o);
			affichage_grille_no_limit(grille_nav,t->id,more_info,help,regle);
		}
		
		else{
			invalide=1;
			insert_tuile(grille_nav,*x,*y,t,*o);
			affichage_grille(grille_nav,more_info,help,regle);
		}

		free_grille(grille_nav);

		if(system("/bin/stty raw")) {exit(EXIT_FAILURE);}
		c1 = getchar();
		if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);}

		if( c1=='x')
		{
			while(!(c1==13))
			{   
				clrscr();
				printf(ROUGE "Voulez vous vraiment abandonner le jeu ?\n\n" RESET);
				if( c1==67 || c1==68)
				{
					arret+=1;  
				}

				if (arret%2==0)
					printf(INV "oui" RESET " non\n\n");
				else
					printf( "oui" INV " non\n\n" RESET);

				if(system("/bin/stty raw")) {exit(EXIT_FAILURE);}
				c1 = getchar();
				if(system("/bin/stty cooked")) {exit(EXIT_FAILURE);}

				clrscr();
				
			}
			c1=0;

			if(arret%2==0)
				return 1;
		}


        if( c1=='i') /*Si on veut plus d'informations, on appuie sur i*/
		{
			more_info=abs(more_info-1);
		}

        if( c1=='h') /*Si savoir comment marquer des points, on appuie sur h*/
		{
			help=abs(help-1);
		}
	}

	more_info = 0;
	help=0;
	return 0;
}


