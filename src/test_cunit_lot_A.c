#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

#include "tuile.h"
#include "rwfile.h"
#include "affichage.h"
#include "grille.h"
#include "test.h"
#include "parcours.h"
#include "test_cunit_lot_A.h"
#define M 50

int setup(void)  { return 0; }
int teardown(void) { return 0; }

int initialisation_catalogue_A(void){

	if (CUE_SUCCESS != CU_initialize_registry())
		return CU_get_error();

	CU_pSuite pSuite = CU_add_suite("echantillon_test",setup,teardown);

	if(( NULL == CU_add_test(pSuite, "Test id tuile ", test_init_tuile_random) ) ||
		( NULL == CU_add_test(pSuite, "Test creation tableau tuile aleatoire ", test_init_tuile_random_tab) ) ||
		( NULL == CU_add_test(pSuite, "Test creation d'une tuile avec tableau d'entrée", test_init_tuile_tab) ) ||
		( NULL == CU_add_test(pSuite, "Test lecture tuile", test_lire_tuile) ) ||
		( NULL == CU_add_test(pSuite, "Test copie tuile", test_copie_tuile) ) ||
		( NULL == CU_add_test(pSuite, "Test rotation de tuile", test_rotation_tuile_lever_coucher_tuile))  ||
		( NULL == CU_add_test(pSuite, "Test creation grille vide standard", test_create_grille_vide) ) ||
		( NULL == CU_add_test(pSuite, "Test creation grille taille aleatoire", test_create_grille_random) ) ||
		( NULL == CU_add_test(pSuite, "Test creation grille tout aleatoire", test_create_grille_full_random))||
		( NULL == CU_add_test(pSuite, "Test copie grille ", test_copy_grille) ) ||
		( NULL == CU_add_test(pSuite, "Test lecture tuile ", test_lire_dalle) ) ||
		( NULL == CU_add_test(pSuite, "Test insertion tuile ", test_insert_tuile) ) ||
		( NULL == CU_add_test(pSuite, "Test lecture/ecriture fichier tuile", test_lecture_ecriture_ficher_tuile) )||
		( NULL == CU_add_test(pSuite, "Test lecture/ecriture fichier tuile", test_lecture_ecriture_ficher_tuile) )||
		( NULL == CU_add_test(pSuite, "Test lecture/ecriture fichier grille", test_lecture_ecriture_ficher_grille) )||
		( NULL == CU_add_test(pSuite, "Test ajout placement dans l'index", test_ajout_placement) )||
		( NULL == CU_add_test(pSuite, "Test modification nombre de cases non recouvertes", test_modif_nbr_cases) )||
		( NULL == CU_add_test(pSuite, "Test creation tour vide", test_create_tour_vide) )||
		( NULL == CU_add_test(pSuite, "Test creation tour vide aleatoire", test_create_tour_vide_random))||
		( NULL == CU_add_test(pSuite, "Test modification de l'index avec copie de l'index précédent", test_modif_index))||
		( NULL == CU_add_test(pSuite, "Test creation tour", test_create_tour) )||
		( NULL == CU_add_test(pSuite, "Test retrait de tour", test_remove_tour)) ||
		( NULL == CU_add_test(pSuite, "Test ajout de tour", test_add_tour) )) 
	{
		CU_cleanup_registry();
		return CU_get_error();
	}

	CU_basic_run_tests();
	CU_basic_show_failures(CU_get_failure_list());
	CU_cleanup_registry();

	return 0;
}

/**********************/
/*****Test tuile.c*****/
/**********************/

void test_init_tuile_random(void)
{
	int id =1;
	ptuile t = init_tuile_random(id);
	int i;
	int compt=0;
	for (i = 0; i < 6; ++i)
	{
		if (t->tab[i]=='V' || t->tab[i]=='R' ||t->tab[i]=='U' ||t->tab[i]=='P' ||t->tab[i]=='L' ||t->tab[i]=='F')
			compt++;
	}

	if (t->id == id)
		compt++;
	
	CU_ASSERT_EQUAL(compt,7);
	free_tuile(t);
}

void test_init_tuile_random_tab(void){
	int n = 10;
	ptuile* tabt = init_tuile_random_tab(n);
	int i,j;
	int compt = 0;
	for (i = 0; i < n; ++i)
	{
		if (tabt[i]->id == i)
			compt ++;
	}

	CU_ASSERT_EQUAL(compt,n);

	for (j = 0; j < 10; ++j)
	{
		free_tuile(tabt[j]);
	}

	free(tabt);

}

void test_init_tuile_tab(void){

	int id =5;
	int i;
	char t1[6] = {'V','V','V','L','L','L'};
	ptuile t2 = init_tuile_tab(t1,id);
	int compt=0;

	for (i = 0; i < 6; i++){
		if (t1[i] == t2->tab[i])
			compt ++;
	} 

	if (t2 -> id == id)
		compt++;

	CU_ASSERT_EQUAL(compt,7);

	free_tuile(t2);
}

void test_lire_tuile(void){
	int id =5;
	int compt=0;
	int i;
	char t1[6] = {'V','V','V','L','L','L'};
	ptuile t3 = init_tuile_tab(t1,id);

	char* t2 = lire_tuile(t3);

	for (i = 0; i < N; i++){
		if (t1[i] == t2[i])
			compt ++;
	} 
	CU_ASSERT_EQUAL(compt,6);
	free_tuile(t3);
	free(t2);


} 


void test_copie_tuile(void){

	int id =5;
	int i;
	int compt=0;
	char tab[6] = {'V','P','R','L','U','F'};
	ptuile t1 = init_tuile_tab(tab,id);
	ptuile t2 = copie_tuile(t1);

	for (i = 0; i < N; ++i){
		if (t1->tab[i] == t2->tab[i])
			compt ++;
	}

	if (t1->id == t2->id)
		compt++;
	

	CU_ASSERT_EQUAL(compt,7);
	free_tuile(t1);
	free_tuile(t2);
}

void test_rotation_tuile_lever_coucher_tuile(void)
{
	int compt = 0;
	int i;

	char tabt0[6] = {'P','U','P','P','U','P'};
	char tabt1[6] = {'U','P','P','P','P','U'};
	char tabt2[6] = {'P','U','P','P','U','P'};
	char tabt3[6] = {'U','P','P','P','P','U'};

	ptuile t0 = init_tuile_tab(tabt0,0);
	ptuile t0c = copie_tuile(t0);
	rotation_tuile(t0,&t0c,0);

	ptuile t1 = init_tuile_tab(tabt1,0);
	ptuile t1c = copie_tuile(t0);
	rotation_tuile(t0,&t1c,1);

	ptuile t2 = init_tuile_tab(tabt2,0);
	ptuile t2c = copie_tuile(t0);
	rotation_tuile(t0,&t2c,2);

	ptuile t3 = init_tuile_tab(tabt3,0);
	ptuile t3c = copie_tuile(t0);
	rotation_tuile(t0,&t3c,3);

	for (i = 0; i < 6; ++i)
	{
		if (t0->tab[i] == t0c->tab[i] && t1->tab[i] == t1c->tab[i] && t2->tab[i] == t2c->tab[i] && t3->tab[i] == t3c->tab[i])
			compt++;
	}

	free_tuile(t0);
	free_tuile(t0c);
	free_tuile(t1);
	free_tuile(t1c);
	free_tuile(t2);
	free_tuile(t2c);
	free_tuile(t3);
	free_tuile(t3c);

	CU_ASSERT_EQUAL(compt,6);

}

/***********************/
/*****Test grille.c*****/
/***********************/


void test_create_grille_vide(void)
{
	int n=4;
	grille_p g=create_grille_vide(n);
	int i,j;
	int compt=0;

	for (i=0;i<n;i++){
		for (j = 0; j < n; ++j){
			if ( (g->cells[i][j].type == '.') && (g->cells[i][j].id == -1)){
				compt++;
			}
		}
	}

	if (g->size == n)
		compt++;

	CU_ASSERT_EQUAL(compt,n*n+1);
	free_grille(g);
}


void test_create_grille_random(void)
{
	grille_p g=create_grille_random();
	int taille = g->size;
	int i,j;
	int compt=0;

	for (i=0;i<taille;i++){
		for (j = 0; j < taille; j++){
			if (g->cells[i][j].id == -1 && g->cells[i][j].type == '.'){
				compt++;
			}
		}
	}

	CU_ASSERT_EQUAL(compt,taille*taille);
	free_grille(g);
}

void test_create_grille_full_random(void)
{
	int n=10;
	grille_p g=create_grille_full_random(n);
	int i,j;
	int compt=0;
	char t;

	for (i=0;i<n;i++){
		for (j = 0; j < n; j++){
			t = g->cells[i][j].type;
			if (g->cells[i][j].id == -1 && (t=='V' || t=='R' ||t=='U' ||t=='P' ||t=='L' ||t=='F')){
				compt++;
			}
		}
	}

	if (g->size == n)
		compt++;

	CU_ASSERT_EQUAL(compt,n*n+1);
	free_grille(g);
}


void test_copy_grille(void)
{
	int n=10;
	int i,j;
	int compt=0;

	grille_p g1=create_grille_full_random(n);
	grille_p g2=create_grille_full_random(n);
	copy_grille (g1,g2);

	for (i=0;i<n;i++){
		for (j = 0; j < n; j++){
			if (g1->cells[i][j].id == g2->cells[i][j].id && g1->cells[i][j].type == g2->cells[i][j].type){
				compt++;
			}
		}
	}

	if (g1->size == g2->size)
		compt++;


	CU_ASSERT_EQUAL(compt,n*n+1);
	free_grille(g1);
	free_grille(g2);
}



void test_lire_dalle(void)
{
	grille_p g=create_grille_vide(4);
	CU_ASSERT_EQUAL(lire_dalle(g,1,2),'.');
	free_grille(g);
}



void test_insert_tuile(void)
{
	int i,j;
	int compt=0;
	grille_p g=create_grille_vide(4);
	ptuile t1 = init_tuile_random(0);
	insert_tuile(g,0,0,t1,0);

	for (i = 0; i < 3; ++i)
	{
		for (j = 0; j < 2; ++j)
		{
			if ( g->cells[i][j].type==t1->tab[i*2+j] && g->cells[i][j].type==t1->tab[i*2+j])
				compt +=1;
		}
	}

	CU_ASSERT_EQUAL(6,compt);
	free_grille(g);
	free_tuile(t1);
}

/***********************/
/*****Test rwfile.c*****/
/***********************/

void test_lecture_ecriture_ficher_tuile(void) {
	int i, sz;
	char* name = "test.tuile"; 
	ptuile* list_tuile_init = init_tuile_random_tab(M);
	int nbwrite = write_file_tuile(name, list_tuile_init, M);
	CU_ASSERT_NOT_EQUAL(nbwrite, 0);
	ptuile* list_tuile = read_file_tuile(name, &sz);
	char** list_char_tuile = (char**)malloc(sz * sizeof(char*));
	for (i = 0; i < sz; i++) list_char_tuile[i] = lire_tuile(list_tuile[i]);
		CU_ASSERT_PTR_NOT_NULL(list_tuile);
	char** list_char_tuile_init = (char**)malloc(sz * sizeof(char*));
	for (i = 0; i < M; i++) list_char_tuile_init[i] = lire_tuile(list_tuile_init[i]);
		for (i = 0; i < sz; i++) CU_ASSERT_STRING_EQUAL(list_char_tuile[i], list_char_tuile_init[i]);
			for (i = 0; i < sz; i++) {
				free_tuile(list_tuile[i]);
				free(list_char_tuile[i]);
			}
			for (i = 0; i < M; i++) {
				free_tuile(list_tuile_init[i]);
				free(list_char_tuile_init[i]);
			}
			free(list_char_tuile_init);
			free(list_tuile_init);
			free(list_tuile);
			free(list_char_tuile);
		}

		void test_lecture_ecriture_ficher_grille(void) {
			int i;
			char* name = "test.grille";
			struct info_grille info; 
			info.sz_grille = 20;
			info.sz_main = 10;
			info.id_depart = 8;
			info.ids = (int*)malloc(10 * sizeof(int));
			info.ids[0] = 5;
			info.ids[1] = 2;
			info.ids[2] = 8;
			info.ids[3] = 3;
			info.ids[4] = 1;
			info.ids[5] = 0;
			info.ids[6] = 4;
			info.ids[7] = 6;
			info.ids[8] = 9;
			info.ids[9] = 7;
			write_file_grille(name, info);
			struct info_grille info_lu;
			read_file_grille(name, &info_lu);
			CU_ASSERT_EQUAL(info_lu.sz_grille, info.sz_grille);
			CU_ASSERT_EQUAL_FATAL(info_lu.sz_main, info.sz_main);
			CU_ASSERT_EQUAL(info_lu.id_depart, info.id_depart);
			CU_ASSERT_PTR_NOT_NULL_FATAL(info_lu.ids);
			for (i = 0; i < info.sz_main; i++) CU_ASSERT_EQUAL(info_lu.ids[i], info.ids[i]);
				free(info_lu.ids);
			free(info.ids);
		}
/*********************/
/*****Test tour.c*****/
/*********************/

		void test_ajout_placement (void )
		{
			pplacement p= (pplacement)malloc(sizeof(placement));
			ajout_placement(p,1,6,1,0,0);
			CU_ASSERT_EQUAL(p->id,1);
			CU_ASSERT_EQUAL(p->nbrcases,6);
			CU_ASSERT_EQUAL(p->orientation,1);
			CU_ASSERT_EQUAL(p->x,0);
			CU_ASSERT_EQUAL(p->y,0);
			free(p);
		}

		void test_modif_nbr_cases (void){
			int n=10;
			char tab[6] = {'V','V','V','R','R','R'};
			ptuile t1 = init_tuile_tab(tab,0);
			parcours_p p =create_tour(t1,0,0,1,n);
			int nb_cases = p->tour->index[0]->nbrcases;
			modif_nbr_cases (p->tour,0);
			int nb_cases_2 = p->tour->index[0]->nbrcases;

			CU_ASSERT_EQUAL(nb_cases-1,nb_cases_2);

			remove_all_tour(&p);
			free(t1);
		}


		void test_modif_index(void){

			int n = 10;
			int i,j;
			int compt=0;
			char tab[6] = {'V','V','V','R','R','R'};
			ptuile t1 = init_tuile_tab(tab,0);
			char tab2[6] = {'P','P','P','F','F','F'};
			ptuile t2 = init_tuile_tab(tab2,1);
			parcours_p p =create_tour(t1,0,0,1,n);
			parcours_p mem = p;
			if(test_recouvrement(p->tour, 1,0, 1))
    add_tour(&p, t2, 1, 0, 1, n);/*modif_index est appelée dans recouvrement*/

				CU_ASSERT_EQUAL(p->tour->index[1]->id,1);
			CU_ASSERT_EQUAL(p->tour->index[1]->nbrcases,6);
			CU_ASSERT_EQUAL(p->tour->index[1]->orientation,1);
			CU_ASSERT_EQUAL(p->tour->index[1]->x,1);
			CU_ASSERT_EQUAL(p->tour->index[1]->y,0);

			CU_ASSERT_EQUAL(p->tour->index[0]->id,mem->tour->index[0]->id);
  CU_ASSERT_EQUAL(p->tour->index[0]->nbrcases+3,mem->tour->index[0]->nbrcases);/*On enleve deux car la deuxieme tuile recouvre 4 terrains de la premiere tuile*/
			CU_ASSERT_EQUAL(p->tour->index[0]->orientation,mem->tour->index[0]->orientation);
			CU_ASSERT_EQUAL(p->tour->index[0]->x,mem->tour->index[0]->x);
			CU_ASSERT_EQUAL(p->tour->index[0]->y,mem->tour->index[0]->y);

			grille_p g = p->tour->g;

			for (i = 0; i < 2; ++i){
				for (j = 0; j < 3; ++j){
					if (g->cells[i+1][j].type==t2->tab[i*3+j] && g->cells[i+1][j].type==t2->tab[i*3+j])
						compt +=1;
				}
			}

			remove_all_tour (&p);

			free_tuile(t1);
			free_tuile(t2);

		}

		void test_create_tour_vide(void)
		{
			ptour t =create_tour_vide(4,2);
			CU_ASSERT_EQUAL(t->g->size,4);
			CU_ASSERT_EQUAL(t->sz_index,2);
			free_tour(t);
		}

		void test_create_tour_vide_random(void)
		{
			ptour t =create_tour_vide_random(2);
			CU_ASSERT_EQUAL(t->sz_index,2);

			free_tour(t);
		}


/*************************/
/*****Test parcours.c*****/
/*************************/

		void test_create_tour (void) 
		{
			int regle = 0;
			int n = 10;
			char tab[6] = {'V','V','V','L','L','L'};
			ptuile t = init_tuile_tab(tab,0);
			parcours_p p =create_tour(t,0,0,1,n);


			CU_ASSERT_PTR_NULL(p->precedent);

			CU_ASSERT_EQUAL(p->tour->index[0]->id,0);
			CU_ASSERT_EQUAL(p->tour->index[0]->nbrcases,6);
			CU_ASSERT_EQUAL(p->tour->index[0]->orientation,1);
			CU_ASSERT_EQUAL(p->tour->index[0]->x,0);
			CU_ASSERT_EQUAL(p->tour->index[0]->y,0);
			CU_ASSERT_EQUAL(p->tour->sz_index,1);
			CU_ASSERT_EQUAL(p->tour->g->size,n);

			affichage_tour(p->tour,regle);
			

			remove_all_tour(&p);
			free(t);
		}

		void test_add_tour(void)
		{
			int n = 10;
			char tab[6] = {'V','V','V','R','R','R'};
			ptuile t1 = init_tuile_tab(tab,0);
			char tab2[6] = {'P','P','P','F','F','F'};
			ptuile t2 = init_tuile_tab(tab2,1);
			parcours_p p =create_tour(t1,0,0,1,n);
			parcours_p mem = p;
			if(test_recouvrement(p->tour, 1,0, 1))
				add_tour(&p, t2, 1, 0, 1, n);

			CU_ASSERT_PTR_EQUAL(p->precedent,mem);

			CU_ASSERT_EQUAL(p->tour->index[1]->id,1);
			CU_ASSERT_EQUAL(p->tour->index[1]->nbrcases,6);
			CU_ASSERT_EQUAL(p->tour->index[1]->orientation,1);
			CU_ASSERT_EQUAL(p->tour->index[1]->x,1);
			CU_ASSERT_EQUAL(p->tour->index[1]->y,0);

			CU_ASSERT_EQUAL(p->tour->sz_index,2);

			CU_ASSERT_EQUAL(p->tour->g->size,n);


			remove_all_tour (&p);

			free_tuile(t1);
			free_tuile(t2);
		}


		void test_remove_tour(void)
		{
			int taille = 10;
			
			ptuile t0 = init_tuile_random(0);

			parcours_p partie_de_jeu =  create_tour(t0,taille/2-1,taille/2-1,1,taille);

			ptuile t1 = init_tuile_random(1);
			ptuile t2 = init_tuile_random(2);
			ptuile t3 = init_tuile_random(3);

			if(test_recouvrement(partie_de_jeu->tour, 3, 3, 0))
				add_tour(&partie_de_jeu,t1,3,3,0,taille);

			if(test_recouvrement(partie_de_jeu->tour, 5, 5, 0))
				add_tour(&partie_de_jeu,t2,5,5,0,taille);

			if(test_recouvrement(partie_de_jeu->tour, 6, 6, 0))
				add_tour(&partie_de_jeu,t3,6,6,0,taille);

			
			remove_tour(&partie_de_jeu);

			free(t0);
			free(t1);
			free(t2);
			free(t3);


			remove_all_tour(&partie_de_jeu);
		} 
