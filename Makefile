CC=gcc -Wall -Wextra -ansi -std=c99 -O3 -pthread `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`
LIB=`pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

all : jeu jeu_gtk 

play : 
	make jeu && ./bin/jeu

play_ncurses : 
	make jeu_ncurses && ./bin/jeu_ncurses

simu :
	make simulation && valgrind --tool=callgrind --tool=memcheck --leak-check=yes ./bin/simulation

a.out : main_lot_A.o tuile.o rwfile.o affichage.o grille.o test.o tour.o parcours.o file.o comptage.o test_cunit_lot_A.o 
	cd obj/ && $(CC) $^ -o ../bin/$@ -lcunit

b.out : main_lot_B.o tuile.o rwfile.o affichage.o grille.o test.o tour.o parcours.o file.o loop.o loop_nc.o  test_cunit_lot_B.o comptage.o couleur.o solveur.o optimisation.o aetoile.o rwfile.o comptage.o solveur.o
	cd obj/ && $(CC) $^ -o ../bin/$@ -lm -lcunit -lncurses

c.out : main_lot_C.o tuile.o rwfile.o affichage.o grille.o test.o tour.o parcours.o file.o comptage.o optimisation.o test_cunit_lot_C.o aetoile.o solveur.o
	cd obj/ && $(CC) $^ -o ../bin/$@ -lcunit

test : main_test.o tuile.o rwfile.o affichage.o grille.o test.o tour.o parcours.o file.o comptage.o 
	cd obj/ && $(CC) $^ -o ../bin/$@ -lcunit

jeu : main.o tuile.o rwfile.o affichage.o grille.o test.o tour.o parcours.o file.o loop.o loop_nc.o test_cunit_lot_B.o comptage.o couleur.o solveur.o optimisation.o aetoile.o
	cd obj/ && $(CC) $^ -o ../bin/$@ -lm -lcunit -lncurses

jeu_ncurses : main_ncurses.o tuile.o rwfile.o affichage.o grille.o test.o tour.o parcours.o file.o loop.o loop_nc.o test_cunit_lot_B.o comptage.o couleur.o
	cd obj/ && $(CC) $^ -o ../bin/$@ -lm -lcunit -lncurses

jeu_gtk : main_gtk.o tuile.o rwfile.o affichage.o grille.o test.o tour.o parcours.o file.o loop_gtk.o comptage.o couleur.o solveur.o optimisation.o aetoile.o
	cd obj/ && $(CC) $^ -o ../bin/$@ -lm -lcunit `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0`

simulation : main_simulation.o tuile.o rwfile.o affichage.o grille.o test.o tour.o parcours.o file.o comptage.o optimisation.o test_cunit_lot_C.o simulation.o aetoile.o couleur.o
	cd obj/ && $(CC) $^ -o ../bin/$@ -lm -lcunit -lncurses

main.o : src/main.c 
	$(CC) -c $< -o obj/$@

main_gtk.o : src/main_gtk.c
	$(CC) -c $< -o obj/$@

main_ncurses.o : src/main_ncurses.c 
	$(CC) -c $< -o obj/$@

main_lot_A.o : src/main_lot_A.c 
	$(CC) -c $< -o obj/$@

main_lot_B.o : src/main_lot_B.c 
	$(CC) -c $< -o obj/$@

main_lot_C.o : src/main_lot_C.c 
	$(CC) -c $< -o obj/$@

main_test.o : src/main_test.c src/affichage.h src/tuile.h
	$(CC) -c $< -o obj/$@

main_simulation.o : src/main_simulation.c
	$(CC) -c $< -o obj/$@

grille.o : src/grille.c src/grille.h
	$(CC) -c $< -o obj/$@

tuile.o : src/tuile.c src/tuile.h
	$(CC) -c $< -o obj/$@

rwfile.o : src/rwfile.c src/rwfile.h
	$(CC) -c $< -o obj/$@

affichage.o : src/affichage.c src/affichage.h src/tuile.h src/grille.h
	$(CC) -c $< -o obj/$@

test.o : src/test.c src/test.h src/tuile.h src/file.h
	$(CC) -c $< -o obj/$@

tour.o : src/tour.c src/tour.h src/tuile.h src/grille.h
	$(CC) -c $< -o obj/$@

parcours.o : src/parcours.c src/parcours.h src/tuile.h src/grille.h src/tour.h
	$(CC) -c $< -o obj/$@

file.o : src/file.c src/file.h 
	$(CC) -c $< -o obj/$@

comptage.o : src/comptage.c src/comptage.h 
	$(CC) -c $< -o obj/$@

loop.o : src/loop.c src/loop.h
	$(CC) -c $< -o obj/$@ -lm

loop_nc.o : src/loop_nc.c src/loop_nc.h
	$(CC) -c $< -o obj/$@ -lm

optimisation.o: src/optimisation.c src/optimisation.h
	$(CC) -c $< -o obj/$@

couleur.o : src/couleur.c src/couleur.h
	$(CC) -c $< -o obj/$@ -lm

loop_gtk.o : src/loop_gtk.c src/loop_gtk.h
	$(CC) -c $< -o obj/$@

test_cunit_lot_A.o : src/test_cunit_lot_A.c src/test_cunit_lot_A.h 
	$(CC) -c $< -o obj/$@ -lcunit

test_cunit_lot_B.o : src/test_cunit_lot_B.c src/test_cunit_lot_B.h 
	$(CC) -c $< -o obj/$@ -lcunit

test_cunit_lot_C.o : src/test_cunit_lot_C.c src/test_cunit_lot_C.h 
	$(CC) -c $< -o obj/$@ -lcunit

simulation.o : src/simulation.c src/simulation.h 
	$(CC) -c $< -o obj/$@ -lcunit

aetoile.o : src/aetoile.c src/aetoile.h 
	$(CC) -c $< -o obj/$@ -lcunit

solveur.o : src/solveur.c src/solveur.h 
	$(CC) -c $< -o obj/$@ -lcunit

doxygen : doc/Doxyfile
	cd doc && doxygen ../$< && cd latex && make

.PHONY: clean
clean :
	#rm -f bin/*
	rm -f obj/*
