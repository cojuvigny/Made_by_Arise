    IMPORTANT: CE README EST SOUS FORMAT MARKDOWN. POUR UNE MEILLEURE LISIBILITÉ, OUVRIR doc/html/index.html SUR VOTRE NAVIGATEUR PRÉFÉRÉ 


 * $Auteurs: Adrien BLASSIAU, Alexandre BRUNOUD, Romain DERRE,  Corentin JUVIGNY $
 * $Version: 1.224 $
 * $Date: 22/03/2018 $
 *
 * ==================================================================== 
 *
 * Copyright (C) 2018 by Made_by_Arise
 *
 * Ce document correspond au lot A du projet Honshu.
 *
 * Pour plus d'informations, contactez nous :
 *
 * adrien.blassiau@ensiie.fr
 * alexandre.brunoud@ensiie.fr
 * romain.derre@ensiie.fr
 * corentin.juvigny@ensiie.fr
 *


//////////////////////////////////////////////////////////////////////////////////

README du lot A du projet Honshu de Made_by_Arise                 

//////////////////////////////////////////////////////////////////////////////////




    IMPORTANT: LIRE CE FICHIER AVANT DE LANCER LE PROGRAMME



  Sommaire
==================================================================================

1. Introduction
2. Instructions d'installation des différents outils
3. Conseils d'utilisation



1. Introduction                 
==================================================================================

Merci d'avoir télécharger notre document. L'application est inspirée du jeu de plateau Honshu. Ici, il est adapté pour un joueur réel ou une IA avec des règles simplifiées que vous trouverez dans le document 01_honshu.pdf.
La version proposée est celle du lot A qui correspond à la préparation des données du jeu, c'est à dire les différentes structures et fonctions qui seront à la base du jeu, ainsi que la lecture/écriture de fichier de jeu. 

La prochaine version, celle du lot B, sort le 8 avril 2018. Tenez-vous au courant des différentes tâches planifiées en vous rendant sur notre Trello (https://trello.com/).
La sortie complète du jeu est prévue quant à elle le 25 mai 2018.

À ce point de développement, nous vous invitons donc à tester les premières fonctionnalités implémentées dans notre application. Via l’exécutable a.out, vous pourrez voir les résultats des tests unitaires CUnit que nous avons réalisés de manière la plus exhaustive possible. Ensuite, via l’exécutable test vous pourrez observer les diverses fonctions de notre programme en œuvre sur des exemples simples. Enfin, via l'utilitaire valgrind, vous pourrez contrôler la présence de fuites mémoires. Toutes remarques est évidemment la bienvenue.


NOTE: Nous vous invitons à lire notre rapport Rapport_Lot_A.pdf qui vous renseignera sur la démarche et les choix engagés dans ce projet, ainsi que la répartition des tâches et le planning.

NOTE: De plus, suivez bien les conseils d'installation et d'utilisation qui vont suivre pour une utilisation optimale de notre application.
 


Développée par l'équipe Made_by_Arise
- Adrien BLASSIAU <adrien.blassiau@ensiie.fr>
- Alexandre BRUNOUD <alexandre.brunoud@ensiie.fr>
- Romain DERRE <romain.derre@ensiie.fr>
- Corentin Juvigny <corentin.juvigny@ensiie.fr>


2. Instructions d'installation des différents outils
==================================================================================


**Utilisateur Linux**


*********************************************************************************************

Doxygen
--------

Doxygen est un outil qui permet de gérer la documentation de son code. Il permet à terme de fournir une page web ou un pdf contenant la documentation du code que le développeur souhaite publier.

**Entrez dans votre console** : 
> sudo apt-get install doxygen

*********************************************************************************************

CUnit
--------

CUnit est une bibliothèque de tests unitaires pour C. Il permet de programmer des tests, de les exécuter, et d’afficher un résumé des tests réussis ou échoués. Un test unitaire ne teste qu’une partie atomique des spécifications sous des conditions précises qui, bien généralement, ne couvrent pas tous les cas.

**Entrez dans votre console** : 
> sudo apt-get install libcunit1 libcunit1-doc libcunit1-dev

*********************************************************************************************

Valgrind
--------

Valgrind est un outil qui permet de repérer les fuites mémoires (et autres erreurs)

**Entrez dans votre console** : 
> sudo apt-get install valgrind

*********************************************************************************************



3. Conseils d'utilisation
==================================================================================

Voici différentes commandes qui vont permettrons d'observer des résultats et informations sur le travail effectué sur le lot A :


Tests unitaires CUnit
---------------------


Pour obtenir l'ensemble des résultats de nos tests unitaires : 

**Entrez dans votre console** : 
> make a.out && bin/a.out


*Comment lire le résultat d'un test CUnit ?*

      CUnit - A unit testing framework for C - Version 2.1-3
      http://cunit.sourceforge.net/



    Run Summary:    Type  Total    Ran Passed Failed Inactive
                  suites      1      1    n/a      0        0
                   tests     23     23     23      0        0
                 asserts    168    168    168      0      n/a

    Elapsed time =    0.003 seconds


La ligne tests correspond au nombre de tests unitaires effectués, la ligne asserts correspond au nombres d'assertions vérifiées. Pour chaque tests, il peux y avoir plusieurs assertions. 


*********************************************************************************************

Tests sur exemples
------------------

Pour observer les diverses fonctions de notre programme en œuvre sur des exemples simples :

**Entrez dans votre console** : 
> make test && bin/test



*********************************************************************************************

Doxygen
-------

Pour obtenir la documentation générée par Doxygen, un document sous format pdf et web est déjà présent dans le dossier fournit, pour les ouvrir :

*Via navigateur fichier*

Le document est présent sous format pdf via doc/latex/refman.pdf Ouvrez le ensuite sur votre lecteur pdf favori
Il est aussi présent sous format html via doc/html/index.html Ouvrez le sur votre navigateur web favori


*Via console*


**Sous format pdf, entrez dans votre console** :
> evince doc/latex/refman.pdf  &

NOTE: Remplacez evince par votre lecteur pdf favori.



**Sous format html, entrez dans votre console** : 
> firefox doc/html/index.html &

NOTE: Remplacez firefox par votre navigateur web favori.

*Informations*

Vous pouvez aussi régénérer la documentation puis l' ouvrir : 

**Sous format pdf, entrez dans votre console** : 
> make doxygen && cd doc/latex make && evince refman.pdf && cd ../..

**Sous format html, entrez dans votre console** :
> make doxygen && firefox doc/html/index.html



*********************************************************************************************
Valgrind
--------

Pour contrôler la présence de fuites mémoires : 

**Sur les test unitaires, entrez dans votre console** : 

> make a.out && valgrind bin/a.out  

**Sur les exemples, entrez dans votre console** : 

> make test && valgrind bin/test


*Comment lire les résultats de valgrind*

Une fois lancé, rendez vous à la section HEAP SUMMARY : 
    
    ==18026== 
    ==18026== HEAP SUMMARY:
    ==18026==     in use at exit: 0 bytes in 0 blocks
    ==18026==   total heap usage: 578 allocs, 578 frees, 611,162 bytes allocated
    ==18026== 
    ==18026== All heap blocks were freed -- no leaks are possible
    ==18026== 
    ==18026== For counts of detected and suppressed errors, rerun with: -v
    ==18026== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)

Vous obtenez la mémoire allouée (allocs) et la mémoire libérée (frees) ainsi que certaines autres erreurs liées à une mauvaise gestion de la mémoire (ERROR SUMMARY).

Vous pouvez obtenir plus de détails sur les différentes fuites et erreurs via cette commande : 

**Sur les test unitaires, entrez dans votre console** : 

> make a.out && valgrind --track-origins=yes bin/a.out  

**Sur les exemples, entrez dans votre console** : 

> make test && valgrind --track-origins=yes bin/test

                                



*********************************************************************************************


*Merci pour votre lecture !*

**Made_by_Arise**
