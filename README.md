    IMPORTANT: CE README EST SOUS FORMAT MARKDOWN. POUR UNE MEILLEURE LISIBILITÉ, OUVRIR doc/html/index.html SUR VOTRE NAVIGATEUR PRÉFÉRÉ 


 * $Auteurs: Adrien BLASSIAU, Alexandre BRUNOUD, Romain DERRE,  Corentin JUVIGNY $
 * $Version: 4.483 $
 * $Date: 24/05/2018 $
 *
 * ==================================================================== 
 *
 * Copyright (C) 2018 by Made_by_Arise
 *
 * Ce document correspond au lot D du projet Honshu.
 *
 * Pour plus d'informations, contactez nous :
 *
 * adrien.blassiau@ensiie.fr
 * alexandre.brunoud@ensiie.fr
 * romain.derre@ensiie.fr
 * corentin.juvigny@ensiie.fr
 *


//////////////////////////////////////////////////////////////////////////////////

README du lot D du projet Honshu de Made_by_Arise                 

//////////////////////////////////////////////////////////////////////////////////




    IMPORTANT: LIRE CE FICHIER AVANT DE LANCER LE PROGRAMME



  Sommaire
==================================================================================

1. Introduction
2. Instructions d'installation des différents outils
3. Conseils d'utilisation



1. Introduction                 
==================================================================================

Merci d'avoir télécharger notre document. L'application est inspirée du jeu de plateau Honshu. Ici, il est adapté pour un joueur réel ou une IA avec des règles simplifiées que vous trouverez dans le document 01_honshu.pdf.
C'est avec joie que nous vous annonçons la sortie officielle en version finale de notre Honshu, soit le lot D, qui contient de nouvelles règles ainsi qu'une interface graphique sous GTK.

Nous vous invitons donc à tester dans son entièreté le jeu.

Via la console et la commande make play, découvrez toutes les subtilités du jeu via des parties préconçues par nos soins et accessibles aux débutants. Lancez-vous vos propres défis en jouant en mode génération aléatoires, un mode où tout peut arriver. Arriverez-vous à finir une partie composée de 90% de lac ? Marquerez-vous plus de 10 points dans un mode 100% plaines ? Tenterez-vous de battre notre coriace super-solveur à huit cœurs boostés aux hormones, prêt à tout pour vous arracher la victoire ? Et enfin, votre stratégie sera t-elle payante face à de nouvelles règles qui redéfinissent en partie le gameplay ?
En tout cas, c'est certifié valgrind ready !

Sinon, via une interface graphique construite de A à Z sous GTK, nous vous invitons à voir notre jeu ... sous un nouveau jour. Essayez-le, vous ne serez pas déçu.


NOTE: Nous vous invitons à lire notre rapport Rapport_Lot_D.pdf qui vous renseignera sur la démarche et les choix engagés dans ce projet, ainsi que la répartition des tâches.

NOTE: De plus, suivez bien les conseils d'installation et d'utilisation qui vont suivre pour une utilisation optimale de notre application.
 

Développée par l'équipe Made_by_Arise
- Adrien BLASSIAU <adrien.blassiau@ensiie.fr>
- Alexandre BRUNOUD <alexandre.brunoud@ensiie.fr>
- Romain DERRE <romain.derre@ensiie.fr>
- Corentin Juvigny <corentin.juvigny@ensiie.fr>


2. Instructions d'installation des différents outils
==================================================================================


**Utilisateur Linux**


*********************************************************************************************

Doxygen
--------

Doxygen est un outil qui permet de gérer la documentation de son code. Il permet à terme de fournir une page web ou un pdf contenant la documentation du code que le développeur souhaite publier.

**Entrez dans votre console** : 
> sudo apt-get install doxygen

*********************************************************************************************

CUnit
--------

CUnit est une bibliothèque de tests unitaires pour C. Il permet de programmer des tests, de les exécuter, et d’afficher un résumé des tests réussis ou échoués. Un test unitaire ne teste qu’une partie atomique des spécifications sous des conditions précises qui, bien généralement, ne couvrent pas tous les cas.

**Entrez dans votre console** : 
> sudo apt-get install libcunit1 libcunit1-doc libcunit1-dev

*********************************************************************************************

Valgrind
--------

Valgrind est un outil qui permet de repérer les fuites mémoires (et autres erreurs)

**Entrez dans votre console** : 
> sudo apt-get install valgrind

*********************************************************************************************



3. Conseils d'utilisation
==================================================================================

Voici différentes commandes qui vont permettrons d'observer des résultats et informations sur le travail effectué sur le lot D :


Jouer au jeu via console
------------------------

Pour jouer au Honshu en mode console :

**Entrez dans votre console** : 
> make jeu && bin/jeu

**ou sinon :** : 
> make play

Les règles du jeu sont celles données dans le sujet. Les choix se font avec les flèches directionnelles, puis la touche entrée pour valider. L'ensemble des déplacements de la tuile se font avec les flèches.

Lors du choix de la tuile, vous pouvez activer/désactiver le solveur avec 's' pour observer la réponse la plus optimale trouvée à ce stade de jeu.

Lors du déplacement de la tuile, vous pouvez :
- quitter le jeu en appuyant sur 'x' (notamment si vous êtes bloqué)
- obtenir/enlever les informations sur les id des tuiles posées en appuyant sur 'i'.
- obtenir/enlever les informations sur la manière de marquer des points et sur les points courants en appuyant sur 'h'

Aussi, lors du déplacement de la tuile sur la zone de jeu, tant que la tuile est rouge, elle ne peut pas être placée car ne respecte pas les tests.


Jouer au jeu via interface graphique
------------------------------------

Pour jouer au Honshu en mode interface graphique :

**Entrez dans votre console** : 
> make jeu_gtk && bin/jeu_gtk


Simulation de partie naïves
---------------------------

Pour simuler une partie "naïve": 

**Entrez dans votre console** : 
> make simu



*********************************************************************************************

Doxygen
-------

Pour obtenir la documentation générée par Doxygen, un document sous format pdf et web est déjà présent dans le dossier fournit, pour les ouvrir :

*Via navigateur fichier*

Le document est présent sous format pdf via doc/latex/refman.pdf Ouvrez le ensuite sur votre lecteur pdf favori
Il est aussi présent sous format html via doc/html/index.html Ouvrez le sur votre navigateur web favori


*Via console*


**Sous format pdf, entrez dans votre console** :
> evince doc/latex/refman.pdf  &

NOTE: Remplacez evince par votre lecteur pdf favori.

**Sous format html, entrez dans votre console** : 
> firefox doc/html/index.html &

NOTE: Remplacez firefox par votre navigateur web favori.

*Informations*

Vous pouvez aussi régénérer la documentation puis l' ouvrir : 

**Sous format pdf, entrez dans votre console** : 
> make doxygen && cd doc/latex make && evince refman.pdf && cd ../..

**Sous format html, entrez dans votre console** :
> make doxygen && firefox doc/html/index.html



*********************************************************************************************
Valgrind
--------

Pour contrôler la présence de fuites mémoires : 

**Sur le jeu console (attention, cela ralenti considérablement le jeu)** : 

> make jeu && valgrind ./bin/jeu 


*Comment lire les résultats de valgrind*

Une fois lancé, rendez vous à la section HEAP SUMMARY : 
    
      ==22390== 
      ==22390== HEAP SUMMARY:
      ==22390==     in use at exit: 0 bytes in 0 blocks
      ==22390==   total heap usage: 6,189,434 allocs, 6,189,434 frees, 269,465,331 bytes allocated
      ==22390== 
      ==22390== All heap blocks were freed -- no leaks are possible
      ==22390== 
      ==22390== For counts of detected and suppressed errors, rerun with: -v
      ==22390== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)


Vous obtenez la mémoire allouée (allocs) et la mémoire libérée (frees) ainsi que certaines autres erreurs liées à une mauvaise gestion de la mémoire (ERROR SUMMARY).

Vous pouvez obtenir plus de détails sur les différentes fuites et erreurs via cette commande : 

**Entrez dans votre console** : 

> make jeu && valgrind --track-origins=yes ./bin/jeu  


                                



*********************************************************************************************


*Merci pour votre lecture !*

**Made_by_Arise**
